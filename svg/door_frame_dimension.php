<svg class="door_frame_dimension" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="height: 380px;">
    <marker id="arrowStart" markerWidth="7" markerHeight="7" refx="2" refy="4" orient="auto">
        <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;" />
    </marker>
    <marker id="arrowEnd" markerWidth="7" markerHeight="7" refx="2" refy="4" orient="auto-start-reverse">
        <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;" />
    </marker>

    <g class="door_frame_svg" >
        <!-- SHAPE -->
        <rect class="shapes myframe" width="10" height="320" x="50"  y="40" />
        <rect class="shapes myframe" width="150" height="10" x="60"  y="40" />
        <rect class="shapes myframe" width="10" height="320" x="210"  y="40" />
        <rect class="shapes mydoor doorComponent" width="130" height="250" x="70"  y="60" />
        <path class="shapes ground" d="M40,335 230,335" />

        <!-- LABEL PATHS -->                   
        <path class="arrows door_width doorComponent" d="M73,90 197,90" />
        <path class="arrows door_length doorComponent" d="M80,63 80,307" />
        <path class="labels below_gap doorComponent" d="M25,310 67,310" />
        <path class="labels below_gap doorComponent" d="M25,335 37,335" />
        <path class="arrows below_gap doorComponent" d="M30,313 30,332" />
        <path class="labels beneath_length inner_length" d="M233,335 245,335" />
        <path class="labels beneath_length" d="M223,360 245,360" />
        <path class="arrows beneath_length" d="M240,338 240,357" />
        <path class="labels inner_length" d="M223,50 245,50" />
        <path class="arrows inner_length" d="M240,53 240,332" />
        <path class="labels frame_length" d="M223,40 275,40" />
        <path class="labels frame_length" d="M248,360 275,360" />
        <path class="arrows frame_length" d="M270,43 270,357" />
        <path class="labels frame_width" d="M50,37 50,20" />
        <path class="labels frame_width" d="M220,37 220,20" />
        <path class="arrows frame_width" d="M53,27 217,27" />

        <!-- LABEL TEXT -->
        <text class="svg_text ground" x="130" y="350">完成面</text>
        <text class="svg_text door_width doorComponent" id="df_door_width" x="130" y="85"></text>
        <text class="svg_text door_length doorComponent" id="df_door_length" x="93" y="190" transform="rotate(270 93,190)"></text>
        <text class="svg_text below_gap doorComponent" id="df_below_gap" x="25" y="323" transform="rotate(270 25,323)"></text>
        <text class="svg_text beneath_length" id="df_beneath_length" x="237" y="348" transform="rotate(270 237,348)"></text>
        <text class="svg_text inner_length" id="df_inner_length" x="237" y="180" transform="rotate(270 237,180)"></text>
        <text class="svg_text frame_length" id="df_frame_length" x="268" y="210" transform="rotate(270 268,210)"></text>
        <text class="svg_text frame_width" id="df_frame_width" x="130" y="23"></text>
    </g>

    <g class="double_door_frame_svg" >
        <!-- SHAPE -->
        <rect class="shapes myframe" width="10" height="320" x="50"  y="40" />
        <rect class="shapes myframe" width="230" height="10" x="60"  y="40" />
        <rect class="shapes myframe" width="10" height="320" x="290"  y="40" />
        <rect class="shapes mydoor" width="100" height="250" x="70"  y="60" />
        <rect class="shapes mydoor" width="100" height="250" x="180"  y="60" />
        <path class="shapes ground" d="M40,335 310,335" />

        <!-- LABEL PATHS -->
        <path class="arrows door_width" d="M73,90 167,90" />
        <path class="arrows door_length" d="M80,63 80,307" />
        <path class="arrows door_width_son" d="M183,90 277,90" />
        <path class="arrows door_length_son" d="M190,63 190,307" />
        <path class="labels below_gap" d="M25,310 67,310" />
        <path class="labels below_gap" d="M25,335 37,335" />
        <path class="arrows below_gap" d="M30,313 30,332" />
        <path class="labels beneath_length calculate_length" d="M313,335 325,335" />
        <path class="labels beneath_length" d="M303,360 325,360" />
        <path class="arrows beneath_length" d="M320,338 320,357" />
        <path class="labels calculate_length" d="M303,50 325,50" />
        <path class="arrows calculate_length" d="M320,53 320,332" />
        <path class="labels frame_length" d="M303,40 355,40" />
        <path class="labels frame_length" d="M328,360 355,360" />
        <path class="arrows frame_length" d="M350,43 350,357" />
        <path class="labels frame_width" d="M50,37 50,20" />
        <path class="labels frame_width" d="M300,37 300,20" />
        <path class="arrows frame_width" d="M53,27 297,27" />

        <!-- LABEL TEXT -->
        <text class="svg_text ground" x="175" y="350">完成面</text>
        <text class="svg_text" x="120" y="190">母</text>
        <text class="svg_text door_width" id="df_d_door_width" x="120" y="85"></text>
        <text class="svg_text door_length" id="df_d_door_length" x="93" y="190" transform="rotate(270 93,190)"></text>
        <text class="svg_text" x="230" y="190">子</text>
        <text class="svg_text door_width_son" id="df_d_door_width_son" x="230" y="85"></text>
        <text class="svg_text door_length_son" id="df_d_door_length_son" x="203" y="190" transform="rotate(270 203,190)"></text>
        <text class="svg_text below_gap" id="df_d_below_gap" x="25" y="323" transform="rotate(270 25,323)"></text>
        <text class="svg_text beneath_length" id="df_d_benath_length" x="317" y="348" transform="rotate(270 317,348)"></text>
        <text class="svg_text calculate_length" id="df_d_calculate_length" x="317" y="180" transform="rotate(270 317,180)"></text>
        <text class="svg_text frame_length" id="df_d_frame_length" x="348" y="210" transform="rotate(270 348,210)"></text>
        <text class="svg_text frame_width" id="df_d_frame_width" x="175" y="23"></text>
    </g>
</svg>