<svg viewBox="0 0 320 160" preserveAspectRatio="xMidYMid meet">
    <marker id="arrowStart" markerWidth="7" markerHeight="7" refx="2" refy="4" orient="auto">
        <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;" />
    </marker>
    <marker id="arrowEnd" markerWidth="7" markerHeight="7" refx="2" refy="4" orient="auto-start-reverse">
        <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;" />
    </marker>
    <g class="vertical_cut" >
        <!-- SHAPE -->
        <path   class="shapes"
                d="M 76,59  76,56  56, 56  56,106 136,106 136,136 256,136 256,56 236,56
                    236,59 253,59 253,133 139,133 139,103  59,103  59, 59  76,59Z" />

        <? if ($SVGMeasurement == 1) : ?>
        <!-- LABEL PATHS -->                   
        <path class="labels" d="M 56, 53  56, 41 M 76, 53  76, 41
                                M 56, 38  56, 20 M256, 53 256, 20
                                M 33, 56  53, 56 M 33,106  53,106
                                M259, 56 279, 56 M259,136 279,136" />

        <!--path class="labels" d="M 56,109  56,159 M136,139 136,159" /-->

        <path class="arrows" d="M 60, 46  72, 46" />
        <path class="arrows" d="M 60, 25 252, 25" />
        <path class="arrows" d="M 36, 60  36,102" />
        <!--path class="arrows" d="M 60,156 132,156" /-->
        <path class="arrows" d="M276, 60 276,132" />

        <!-- LABEL TEXT -->
        <text class="svg_text" id="v_north_length" x="66"  y="43"><?=$v_north_length?></text>
        <text class="svg_text" id="v_total_length" x="156" y="22"><?=$v_total_length?></text>
        <text class="svg_text" id="v_west_length"  x="33"  y="81" transform="rotate(270 33,81)"><?=$v_west_length?></text>
        <!--text class="svg_text" x="96" y="153"><?=$v_south_length?></text-->
        <text class="svg_text" id="v_east_length"  x="273" y="96" transform="rotate(270 273,96)"><?=$v_east_length?></text>
        <? endif; ?>
    </g>
</svg>