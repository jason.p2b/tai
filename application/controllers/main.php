<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Main extends CI_controller 
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('main_model');
        $this->load->model('issue_model');
    }

    public function index()
    {
        $data['title'] = '資訊中心 - ' . $this->session->userdata('company');
        $data['isNav'] = 1;
        $data['js_file'] = array(   'vendor/summernote.min.js',
                                    'vendor/summernote-zh-TW.js',
                                    'vendor/fileinput.js',
                                    'vendor/fileinput_locale_zh-TW.js',
                                    'issue_modal.js',
                                    'main.js');
        $data['css_file'] = array(  'font-awesome.min.css',
                                    'summernote.css',
                                    'fileinput.min.css',
                                    'issue_modal.css',
                                    'main.css');
        $data['selected_tab'] = "main";
        $data['content'] = 'main';
        $data['new_order'] = $this->main_model->getOrder($this->session->userdata('status'), array('status'=>'發送到工廠'));
        $data['to'] = $this->issue_model->getAvailableUsers($this->session->userdata('uid'));
        if ($this->session->userdata('status') > 3)
        {
            $data['company_tracker'] = $this->issue_model->getLastConversation('p.status_id', $this->session->userdata('status'));
        }
        else
        {
            $data['company_tracker'] = $this->issue_model->getLastConversation();
        }

        $this->load->view('template', $data);
    }

    public function get_update()
    {
        $interval = $this->input->post('interval');
        $data = $this->main_model->getOrderChange($this->session->userdata('status'), $interval);
        echo json_encode($data);
        return false;
    }

    function get_conversation()
    {
        $id = $this->input->post('id');

        $data = $this->issue_model->getConversation($id);

        echo json_encode($data);
        return false;
    }

    function get_new_order()
    {
        $time = $this->common_model->getData('user_last_read', array('new_order'), array('uid'=>$this->session->userdata('uid')));
        $data = $this->main_model->getOrder($this->session->userdata('status'), array('status'=>'發送到工廠'), $time['new_order']);

        echo json_encode($data, JSON_FORCE_OBJECT);
        return false;
    }

    function get_old_order()
    {
        $time = $this->common_model->getData('user_last_read', array('change_order'), array('uid'=>$this->session->userdata('uid')));
        $data = $this->main_model->getOrder($this->session->userdata('status'), array('status !='=>'發送到工廠'), $time['change_order']);

        echo json_encode($data, JSON_FORCE_OBJECT);
        return false;
    }

    function get_my_tracker()
    {
        $data = array();
        $time = $this->common_model->getData('user_last_read', array('my_issue'), array('uid'=>$this->session->userdata('uid')));
        $info = $this->issue_model->getLastConversation("c.to_uid", $this->session->userdata('uid'), $time['my_issue']);

        foreach ($info as $k => $v)
        {
            $file_html = "";
            foreach ($info['files'] as $file)
            {
                $file_html .= '<li><a href="' . base_url() . 'index.php/issue/download?c=' . $v['conversation_id'] . '&i=' . $file['file_id'] . '">';
                $file_html .= "<img src='" . $file['img'] . "' alt='" . $file['filename'] . "' title='" . $file['filename'] . "'>";
                $file_html .= "<span class='file_text'>" . $file['filename'] . "</span></a></li>";
            }
            $data[$k]['new'] = $v['new'];
            $data[$k]['tracker'] = '<span data-toggle="modal" data-target="#issue_modal" onclick="getConversation(\'' . $v['issue_id'] . '\');" class="beacon" id="i_' . $v['issue_id'] . '">' . $v['project'] . '-' . $v['sequence'] . '</span>';
            $data[$k]['tracker'] = ($v['new']) ? '<sup><span class="glyphicon glyphicon-star small bright-yellow"></span></sup>' .  $data[$k]['tracker'] : $data[$k]['tracker'];
            $data[$k]['date'] = $v['create_date'];
            $data[$k]['subject'] = $v['subject'];
            $data[$k]['to'] = $v['t_user'];
            $data[$k]['issue_type'] = $v['issue_type'];
            $data[$k]['conversation'] = '<span class="speaker">' . $v['o_user'] . "</span>說: " . $v['conversation'] . '<ul class="conv_files">' . $file_html . '</ul>';
        }

        echo json_encode($data, JSON_FORCE_OBJECT);
        return false;
    }

    function update_time()
    {
        $col = $this->input->post('col');
        $old = $this->common_model->update('user_last_read', 
                                    array($col => date("Y-m-d H:i:s")), 
                                    array('uid' => $this->session->userdata('uid')));
        echo json_encode(1);
        return false;
    }
}