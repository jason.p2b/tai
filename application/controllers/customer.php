<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Customer extends CI_controller 
{
    private $message = "";
    private $uid = "";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('customer_model');
        $this->load->model('status_model');

        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function view_customer() 
    {
        // Get data
        $customer = $this->customer_model->getCustomer($this->uid);

        $data['title'] = '檢視客戶';
        $data['editable'] = $this->status_model->isEditable($this->uid);
        $data['edit']     = ($data['editable']) ? "true" : "false";
        $data['content'] = 'customer';
        $data['js_file']  = array('vendor/moment.js','vendor/footable.min.js','vendor/footable.sorting.min.js');
        $data['css_file'] = array('style.css','bootstrap-theme.min.css','footable.bootstrap.min.css','footable.fontawesome.css');
        $data['customer'] = $customer;
        $this->load->view('_template', $data);
    }

    public function update() 
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        $data = array(
                    'company'   => $company,
                    'gui'       => $gui,
                    'address'   => $address,
                    'phone'     => $phone,
                    'fax'       => $fax,
                    'email'     => $email,
                    'contact'   => $contact,
                    'note'      => $note,
                    'status_id' => $status
                );
        $success = $this->customer_model->update($data, $id);
        echo json_encode($success);
        return false;
    }

    public function add()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        /* Prepare & Insert Customer */
        $data = array(
                    'company'   => $company,
                    'gui'       => $gui,
                    'address'   => $address,
                    'phone'     => $phone,
                    'fax'       => $fax,
                    'email'     => $email,
                    'contact'   => $contact,
                    'note'      => $note,
                    'status_id' => $status
                );

        $customer_id = $this->common_model->insert('customer', $data);

        echo json_encode($customer_id);
        return false;
    }
}
