<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Ny_accessories extends CI_controller 
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('ny_accessories_model');
        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function index()
    {
        // Get data
        $data['editable'] = $this->status_model->isEditable($this->uid);
        if (!$data['editable']) {redirect('login');}

        $data['rows']  = $this->ny_accessories_model->get();
        $data['title'] = '南亞五金資料表';
        $data['content'] = 'ny_accessories';
        $data['js_file']  = array('vendor/footable.min.js','vendor/footable.sorting.min.js','vendor/footable.export.min.js');
        $data['css_file'] = array('style.css','bootstrap-theme.min.css','footable.bootstrap.min.css','footable.fontawesome.css');
        $this->load->view('_template', $data);
    }

}
