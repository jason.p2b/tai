<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Order extends CI_controller 
{
    private $uid;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('project_model');
        $this->load->model('order_model');
        $this->load->model('translate_ny_model');
        $this->load->model('customer_model');
        $this->load->model('status_model');
        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function index()
    {
        // Get data
        // $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        $customer = $this->customer_model->get($this->uid);
        $project  = $this->project_model->getProject($this->uid);

        $data['editable'] = $this->status_model->isEditable($this->uid);
        $data['bolt'] = $this->order_model->getBolt();
        $data['closer'] = $this->order_model->getCloser();
        $data['handle'] = $this->order_model->getHandle();
        $data['hinge'] = $this->order_model->getHinge();
        $data['lock'] = $this->order_model->getLock();

        $data['title'] = '檢視訂單';
        $data['content'] = 'order';
        $data['js_file']  = array('vendor/jquery-confirm.min.js','vendor/footable.min.js','vendor/footable.sorting.min.js','vendor/jquery.stickytable.min.js','vendor/FileSaver.min.js','vendor/tableExport.min.js','order.js','vendor/html2canvas.min.js','vendor/jQuery.print.min.js');
        $data['css_file'] = array('style.css','bootstrap-theme.min.css','footable.bootstrap.min.css','footable.fontawesome.css','jquery.stickytable.min.css','jquery-confirm.min.css','svg.css', 'order.css');
        $data['project'] = $project;
        $data['customer'] = $customer;
        $this->load->view('_template', $data);
    }

    public function get_customer()
    {
        $project_id = $_POST['id'];
        $id = $this->order_model->getCustomerId($project_id);
        echo json_encode($id);
        return false;
    }

    public function get_site()
    {
        $customer_id = $_POST['id'];
        $data = $this->order_model->getSite($customer_id);
        echo json_encode($data);
        return false;
    }

    public function get_order()
    {
        $project_id = $_POST['id'];
        // $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        $rows = array();

        if ("" == $project_id)
        {
            echo json_encode(array('col' => "", 'row' =>"", 'success'=>0));
            return false;
        }

        $header[] = array("name" => "ny_order_id", "title" => "id", "visible" => false, "filterable" => false);
        $header[] = array("name" => "progress", "title" => "進度");
        $header[] = array("name" => "checker", "title"=>"", "filterable"=>false, "sortable"=>false);
        $header[] = array("name" => "order_number", "title" => "訂單編號");
        $header[] = array("name" => "name", "title" => "副工地");
        $header[] = array("name" => "frame", "title" => "框");
        $header[] = array("name" => "door", "title" => "扇");
        $header[] = array("name" => "customer_due_date", "title" => "客戶需要日");
        $header[] = array("name" => "factory_due_date", "title" => "工廠可出貨日", "breakpoints" => "xs");
        $header[] = array("name" => "received_date", "title" => "訂單收到日期", "breakpoints" => "xs","sorted"=>true, "direction"=>"DESC");
        $rows = $this->order_model->getNYOrder($project_id, $this->uid);

        echo json_encode(array('col' => $header, 'row' =>$rows, 'success'=>1));
        return false;
    }

    public function update_order()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        $ny_show = (preg_match('/自訂/',$name)) ? 0 : 1;
        $data = array(
                    'customer_due_date' => $customer_due_date,
                    'factory_due_date'  => $factory_due_date,
                    'received_date'     => $received_date,
                    'name'              => $name,
                    'ny_show'           => $ny_show
                );

        $success = $this->order_model->updateOrder($data, $ny_order_id);
        echo json_encode($success);
        return false;
    }

    public function add_order()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        $ny_show = (preg_match('/自訂/',$name)) ? 0 : 1;

        /* Prepare & Insert Project */
        $data = array(
                    'project_id'        => $project_id,
                    'order_number'      => $order_number,
                    'name'              => $name,
                    'customer_due_date' => $customer_due_date,
                    'factory_due_date'  => $factory_due_date,
                    'received_date'     => $received_date,
                    'progress'          => '收到訂單',
                    'ny_show'           => $ny_show
                );

        $ny_order_id = $this->common_model->insert('ny_order', $data);

        echo json_encode($ny_order_id);
        return false;
    }

    function update_trad_order_item()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        $data = array(
                    'order_name' => $order_name,
                    'name' => $name,
                    'progress' => $progress,
                    'due_date' => $due_date,
                    'door_frame' => $door_frame,
                    'coupled' => $coupled,
                    'type' => $type,
                    'colour' => $colour,
                    'width' => $width,
                    'height' => $height,
                    'depth' => $depth,
                    'amount_l' => $amount_l,
                    'amount_r' => $amount_r,
                    'lock' => $lock,
                    'handle' => $handle,
                    'handle_height' => $handle_height,
                    'hinge' => $hinge,
                    'closer' => $closer,
                    'bolt' => $bolt,
                    'door_thickness' => $door_thickness,
                    'v_total_length' => $v_total_length,
                    'v_north_length' => $v_north_length,
                    'v_east_length' => $v_east_length,
                    'v_west_length' => $v_west_length,
                    'v_south_east_length' => $v_south_east_length,
                    'note' => $note,
                    'frame_shape' => $frame_shape,
                    'fail_safe_lock' => $fail_safe_lock,
                    'fail_safe_lock_distance' => $fail_safe_lock_distance,
                    'fail_safe_lock_son' => $fail_safe_lock_son,
                    'magnetic_hole' => $magnetic_hole,
                    'magnetic_hole_distance' => $magnetic_hole_distance,
                    'magnetic_hole_son' => $magnetic_hole_son,
                    'falling_strip' => $falling_strip,
                    'ball' => $ball,
                    'material' => $material,
                    'mother_distance' => $mother_distance,
                    'is_mother' => $is_mother,
                    'standard' => $standard,
                    'user_id' => $this->encrypt->decode($this->session->userdata('uid'))
        );

        $success = $this->common_model->update('trad_order_item', $data, array('trad_order_item_id' => $trad_order_item_id));

        // Update ny_order progress
        $my_progress = $this->order_model->changeMainOrderStatus($trad_order_item_id, 'trad_order_item');

        echo json_encode($success);
        return false;
    }

    function update_order_item()
    {
        $info = $_POST;

        // Check if progress is the same
        $data = array('progress'=>$info['progress']);
        $cond = array('ny_order_item_id'=>$info['ny_order_item_id']);

        // Prepare for insert
        $data = array(
                    'name' => $info['name'],
                    'progress' => $info['progress'],
                    'due_date' => $info['due_date'],
                    'big_category' => $info['big_category'],
                    'middle_category' => $info['middle_category'],
                    'window' => $info['window'],
                    'width' => $info['width'],
                    'up_window' => $info['up_window'],
                    'total_height' => $info['total_height'],
                    'amount' => $info['amount'],
                    'price' => $info['price'],
                    'wind' => $info['wind'],
                    'colour' => $info['colour'],
                    'mixed_frame' => $info['mixed_frame'],
                    'Box' => $info['Box'],
                    'lock' => $info['lock'],
                    'lock_height' => $info['lock_height'],
                    'depth' => $info['depth'],
                    'threshold' => $info['threshold'],
                    'L1' => $info['L1'],
                    'L2' => $info['L2'],
                    'L3' => $info['L3'],
                    'L4' => $info['L4'],
                    'L5' => $info['L5'],
                    'f1' => $info['f1'],
                    'f2' => $info['f2'],
                    'f3' => $info['f3'],
                    'f4' => $info['f4'],
                    'user_id' => $this->encrypt->decode($this->session->userdata('uid'))
                );
        $success = $this->order_model->updateOrderItem($data, $info['ny_order_item_id']);

        // Update ny_order progress
        $my_progress = $this->order_model->changeMainOrderStatus($info['ny_order_item_id'], 'ny_order_item');

        echo json_encode(array('s'=> $success, 'p'=> $my_progress));
        return false;
    }

    function add_trad_order_item()
    {
        date_default_timezone_set("Asia/Taipei");
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        $data = array(
                    'ny_order_id' => $ny_order_id,
                    'row_number' => $row_number,
                    'order_name' => $order_name,
                    'name' => $name,
                    'progress' => $progress,
                    'due_date' => $due_date,
                    'standard' => $standard,
                    'door_frame' => $door_frame,
                    'coupled' => $coupled,
                    'type' => $type,
                    'colour' => $colour,
                    'width' => $width,
                    'height' => $height,
                    'depth' => $depth,
                    'amount_l' => $amount_l,
                    'amount_r' => $amount_r,
                    'lock' => $lock,
                    'handle' => $handle,
                    'handle_height' => $handle_height,
                    'hinge' => $hinge,
                    'closer' => $closer,
                    'bolt' => $bolt,
                    'door_thickness' => $door_thickness,
                    'v_total_length' => $v_total_length,
                    'v_north_length' => $v_north_length,
                    'v_east_length' => $v_east_length,
                    'v_west_length' => $v_west_length,
                    'v_south_east_length' => $v_south_east_length,
                    'note' => $note,
                    'frame_shape' => $frame_shape,
                    'fail_safe_lock' => $fail_safe_lock,
                    'fail_safe_lock_distance' => $fail_safe_lock_distance,
                    'fail_safe_lock_son' => $fail_safe_lock_son,
                    'magnetic_hole' => $magnetic_hole,
                    'magnetic_hole_distance' => $magnetic_hole_distance,
                    'magnetic_hole_son' => $magnetic_hole_son,
                    'falling_strip' => $falling_strip,
                    'ball' => $ball,
                    'material' => $material,
                    'mother_distance' => $mother_distance,
                    'is_mother' => $is_mother,
                    'user_id' => $this->encrypt->decode($this->session->userdata('uid'))
        );

        $trad_order_item_id = $this->common_model->insert('trad_order_item', $data);
        $data = array(
                    'trad_order_item_id' => $trad_order_item_id,
                    'user_id' => $this->encrypt->decode($this->session->userdata('uid')),
                    'date' => date('Y-m-d H:m:s'),
                    'field' => 'progress',
                    'old_value' => '',
                    'new_value' => '收到訂單'
                );
        $id = $this->common_model->insert('trad_order_item_history', $data);

        // Update ny_order progress
        $my_progress = $this->order_model->changeMainOrderStatus($trad_order_item_id, 'trad_order_item');

        echo json_encode(array('id' => $trad_order_item_id));
        return false;
    }

    function add_upload_order_item()
    {
        // currently read csv file only
        $rawData = file_get_contents($_FILES['file']['tmp_name']);
        $lines   = explode("\n", $rawData);
        $row     = 1;
        $ny_order_id = $_POST['ny_order_id'];
        

        foreach ($lines as $line)
        {
            $info = explode(",", $line);

            if ($info[12] == "QTY" || !array_key_exists("1", $info)) {
                continue;
            }

            if ($info[25] != "")
            {
                $Ls = explode(" ", str_replace('  ', ' ', $info[25]));
            }
            else 
            {
                $Ls = array(0,0,0,0,0);
            }

            $data = array(
                    'ny_order_id' => $ny_order_id,
                    'row_number' => $row,
                    'name' => $info[4],
                    'progress' => "收到訂單",
                    'due_date' => "0000-00-00",
                    'big_category' => substr($info[5],0,2),
                    'middle_category' => substr($info[5],2,2),
                    'window' => substr($info[5],4,3),
                    'width' => $info[6],
                    'up_window' => $info[7],
                    'total_height' => $info[8],
                    'amount' => $info[12],
                    'price' => $info[17],
                    'wind' => $info[9],
                    'colour' => $info[10],
                    'mixed_frame' => $info[14],
                    'Box' => $info[39],
                    'lock' => $info[35],
                    'lock_height' => $info[38],
                    'depth' => $info[34],
                    'threshold' => $info[13],
                    'L1' => $Ls[0],
                    'L2' => $Ls[1],
                    'L3' => $Ls[2],
                    'L4' => $Ls[3],
                    'L5' => $Ls[4],
                    'f1' => $info[45],
                    'f2' => $info[46],
                    'f3' => $info[47],
                    'f4' => $info[48],
                    'user_id' => $this->encrypt->decode($this->session->userdata('uid'))
            );

            $this->_add_one_order_item($data);
            $row++;
        }

        echo json_encode(array('success' => 1));
        return false;
    }

    function add_order_item()
    {
        date_default_timezone_set("Asia/Taipei");
        $info = $_POST;

        echo json_encode($this->_add_one_order_item($info));
        return false;
    }

    function _add_one_order_item($info)
    {
        // Prepare for insert
        $data = array(
                    'ny_order_id' => $info['ny_order_id'],
                    'row_number' => $info['row_number'],
                    'name' => $info['name'],
                    'progress' => $info['progress'],
                    'due_date' => $info['due_date'],
                    'big_category' => $info['big_category'],
                    'middle_category' => $info['middle_category'],
                    'window' => $info['window'],
                    'width' => $info['width'],
                    'up_window' => $info['up_window'],
                    'total_height' => $info['total_height'],
                    'amount' => $info['amount'],
                    'price' => $info['price'],
                    'wind' => $info['wind'],
                    'colour' => $info['colour'],
                    'mixed_frame' => $info['mixed_frame'],
                    'Box' => $info['Box'],
                    'lock' => $info['lock'],
                    'lock_height' => $info['lock_height'],
                    'depth' => $info['depth'],
                    'threshold' => $info['threshold'],
                    'L1' => $info['L1'],
                    'L2' => $info['L2'],
                    'L3' => $info['L3'],
                    'L4' => $info['L4'],
                    'L5' => $info['L5'],
                    'f1' => $info['f1'],
                    'f2' => $info['f2'],
                    'f3' => $info['f3'],
                    'f4' => $info['f4'],
                    'user_id' => $this->encrypt->decode($this->session->userdata('uid'))
                );
        $ny_order_item_id = $this->common_model->insert('ny_order_item', $data);

        $data = array(
                    'ny_order_item_id' => $ny_order_item_id,
                    'user_id' => $this->encrypt->decode($this->session->userdata('uid')),
                    'date' => date('Y-m-d H:m:s'),
                    'field' => 'progress',
                    'old_value' => '',
                    'new_value' => '收到訂單'
                );
        $id = $this->common_model->insert('ny_order_item_history', $data);

        // Update ny_order progress
        $my_progress = $this->order_model->changeMainOrderStatus($ny_order_item_id, 'ny_order_item');

        return array('id' => $ny_order_item_id, 'p' => $my_progress);
    }

    public function get_trad_order_item()
    {
        $id   = $_POST['id'];
        $type = $this->order_model->getProjectOrderType($id);
        $rows = array();

        if ("一般" == $type)
        {
            $rows = $this->order_model->getTradOrderItem($id);
        }

        $header = array();
        $header[] = array("name"=>"row_number", "type"=>"number", "title"=>"項次", "breakpoints"=>"xs", "sorted"=>true, "direction"=>"ASC");
        $header[] = array("name"=>"trad_order_item_id", "title"=>"id", "visible"=>false, "filterable"=>false);
        $header[] = array("name"=>"checker", "title"=>"<input type='checkbox' class='order_items' onclick='checkAll(\"#order-item-table-traditional\");'>", "filterable"=>false, "sortable"=>false);
        $header[] = array("name"=>"progress", "title"=>"進度");
        $header[] = array("name"=>"due_date", "title"=>"預繳日", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"order_name","title"=>"訂單編號", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"name","title"=>"品名");
        $header[] = array("name"=>"door_frame","title"=>"框/扇");
        $header[] = array("name"=>"coupled","title"=>"單/雙", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"is_mother","title"=>"子/母", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"mother_distance","title"=>"母扇寬", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"type","title"=>"類別", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"material","title"=>"材質", "breakpoints"=>"xs sm"); 
        $header[] = array("name"=>"width","title"=>"寬");
        $header[] = array("name"=>"height","title"=>"高");
        $header[] = array("name"=>"depth","title"=>"插框深度");
        $header[] = array("name"=>"amount_t","title"=>"總數量");
        $header[] = array("name"=>"amount_l","title"=>"數量(L)");
        $header[] = array("name"=>"amount_r","title"=>"數量(R)");
        $header[] = array("name"=>"colour","title"=>"顏色");
        $header[] = array("name"=>"lock","title"=>"門鎖", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"handle","title"=>"把手", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"handle_height","title"=>"把手高度", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"hinge","title"=>"鉸鏈", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"closer","title"=>"門弓器", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"bolt","title"=>"天地栓", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"fail_safe_lock","title"=>"陽極鎖", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"fail_safe_lock_distance","title"=>"陽極鎖-距離", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"fail_safe_lock_son","title"=>"陽極鎖(雙)", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"magnetic_hole","title"=>"磁黃孔", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"magnetic_hole_distance","title"=>"磁黃孔-距離", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"magnetic_hole_son","title"=>"磁黃孔(雙)", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"falling_strip","title"=>"下降條", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"ball","title"=>"龍吐珠", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"door_thickness","title"=>"門厚", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"frame_shape","title"=>"框型");
        $header[] = array("name"=>"v_total_length","title"=>"框深(A)", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"v_north_length","title"=>"框B", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"v_west_length","title"=>"框C", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"v_east_length","title"=>"框D", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"v_south_east_length","title"=>"框E", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"standard","title"=>"規範","breakpoints"=>"xs sm");
        $header[] = array("name"=>"note","title"=>"備註", "breakpoints"=>"xs sm");
        
        echo json_encode(array('col' => $header, 'row' => $rows, 'type' => $type));
        return false;
    }


    public function get_ny_order_item()
    {
        $ny_order_id = $_POST['id'];
        $type = $this->order_model->getProjectOrderType($ny_order_id);
        $rows = array();

        if ("南亞" == $type)
        {
            $rows = $this->order_model->getNYOrderItem($ny_order_id);
        }

        $header = array();
        $header[] = array("name"=>"row_number", "type"=>"number", "title"=>"項次", "breakpoints"=>"xs", "sorted"=>true, "direction"=>"ASC");
        $header[] = array("name"=>"ny_order_item_id", "title"=>"id", "visible"=>false, "filterable"=>false);
        $header[] = array("name"=>"checker", "title"=>"<input type='checkbox' class='order_items' onclick='checkAll(\"#order-item-table-original\");'>", "filterable"=>false, "sortable"=>false);
        $header[] = array("name"=>"progress", "title"=>"進度");
        $header[] = array("name"=>"due_date", "title"=>"預繳日", "breakpoints" => "xs");
        $header[] = array("name"=>"name","title"=>"品名");
        $header[] = array("name"=>"big_category", "title"=>"大類");
        $header[] = array("name"=>"middle_category", "title"=>"中類");
        $header[] = array("name"=>"window", "title"=>"上中下窗");
        $header[] = array("name"=>"width", "title"=>"寬度");
        $header[] = array("name"=>"up_window", "title"=>"上窗高度");
        $header[] = array("name"=>"total_height", "title"=>"總高度");
        $header[] = array("name"=>"wind", "title"=>"風壓");
        $header[] = array("name"=>"colour", "title"=>"色號");
        $header[] = array("name"=>"amount", "title"=>"數量");
        $header[] = array("name"=>"price", "title"=>"單價", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"total_price", "title"=>"總價", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"threshold", "title"=>"門框總類","breakpoints"=>"xs sm md");
        $header[] = array("name"=>"mixed_frame", "title"=>"混合框");
        $header[] = array("name"=>"Box", "title"=>"BOX色號");
        $header[] = array("name"=>"L1", "title"=>"L1","breakpoints"=>"xs sm md");
        $header[] = array("name"=>"L2", "title"=>"L2","breakpoints"=>"xs sm md");
        $header[] = array("name"=>"L3", "title"=>"L3","breakpoints"=>"xs sm md");
        $header[] = array("name"=>"L4", "title"=>"L4","breakpoints"=>"xs sm md");
        $header[] = array("name"=>"L5", "title"=>"L5","breakpoints"=>"xs sm md");
        $header[] = array("name"=>"depth", "title"=>"埋入深度");
        $header[] = array("name"=>"lock", "title"=>"鎖");
        $header[] = array("name"=>"lock_height", "title"=>"鎖高");
        $header[] = array("title"=>"框型一", "name"=>"f1","breakpoints"=>"xs sm md");
        $header[] = array("title"=>"框型二", "name"=>"f2","breakpoints"=>"xs sm md");
        $header[] = array("title"=>"框型三", "name"=>"f3","breakpoints"=>"xs sm md");
        $header[] = array("title"=>"框型四", "name"=>"f4","breakpoints"=>"xs sm md");

        echo json_encode(array('col' => $header, 'row' => $rows));
        return false;
    }

    // Translate 南亞訂單
    public function get_translate_order()
    {
        $ny_order_id = $_POST['id'];
        $type = $this->order_model->getProjectOrderType($ny_order_id);
        $header = array();
        $data   = array();

        if ("南亞" == $type)
        {
            $data = $this->translate_ny_model->getTranslateOrderItem($ny_order_id);
        }

        $header[] = array("name"=>"row_number", "type"=>"number", "title"=>"項次", "breakpoints"=>"xs", "sorted"=>true, "direction"=>"ASC");
        $header[] = array("name"=>"ny_order_item_id", "title"=>"id", "visible"=>false, "filterable"=>false);
        $header[] = array("name"=>"progress", "title"=>"進度");
        $header[] = array("name"=>"due_date", "title"=>"預繳日", "breakpoints" => "xs");
        $header[] = array("name"=>"name","title"=>"品名");
        $header[] = array("name"=>"類別", "title"=>"類別");
        $header[] = array("name"=>"類型", "title"=>"類型", "breakpoints" => "xs");
        if ("框" == $data[0]['類別'])
        {
            $header[] = array("name"=>"框寬", "title"=>"框寬");
            $header[] = array("name"=>"框高", "title"=>"框高");
            $header[] = array("name"=>"框深", "title"=>"框深");
            $header[] = array("name"=>"門檻", "title"=>"門檻","breakpoints"=>"xs sm md");
            $header[] = array("name"=>"框型", "title"=>"框型");
            $header[] = array("name"=>"埋入深度", "title"=>"埋入深度");
        }
        else
        {
            $header[] = array("name"=>"門寬", "title"=>"門寬");
            $header[] = array("name"=>"門高", "title"=>"門高");
            $header[] = array("name"=>"門厚", "title"=>"門厚");
        }
        $header[] = array("name"=>"skin", "title"=>"SKIN顏色" );
        $header[] = array("name"=>"開向", "title"=>"開向");
        $header[] = array("name"=>"數量", "title"=>"數量");
        $header[] = array("name"=>"封邊材質", "title"=>"材質");
        $header[] = array("name"=>"鎖", "title"=>"鎖");
        $header[] = array("name"=>"鎖高", "title"=>"鎖高");
        $header[] = array("name"=>"把手", "title"=>"把手");
        $header[] = array("name"=>"鉸鏈", "title"=>"鉸鏈");
        $header[] = array("name"=>"防橇栓", "title"=>"防橇栓");
        $header[] = array("name"=>"上加工", "title"=>"上加工");
        $header[] = array("name"=>"下加工", "title"=>"下加工");
        if ("扇" == $data[0]['類別'])
        {
            $header[] = array("name"=>"L1", "title"=>"頂距CH", "breakpoints"=>"xs sm md");
            $header[] = array("name"=>"L2", "title"=>"編距CW", "breakpoints"=>"xs sm md");
            $header[] = array("name"=>"L3", "title"=>"玻璃寬", "breakpoints"=>"xs sm md");
            $header[] = array("name"=>"L4", "title"=>"玻璃高", "breakpoints"=>"xs sm md");
            $header[] = array("name"=>"L5", "title"=>"玻璃厚", "breakpoints"=>"xs sm md");
        }
        $header[] = array("name"=>"單價", "title"=>"單價", "breakpoints"=>"xs sm");
        $header[] = array("name"=>"總價", "title"=>"總價", "breakpoints"=>"xs sm");

        echo json_encode(array('col' => $header, 'row' =>$data));
        return false;
    }

    public function change_all_progress()
    {
        $ids = $_POST['id'];
        $progress = $_POST['p'];
        $type = $_POST['t'];
        $uid = $this->encrypt->decode($this->session->userdata('uid'));

        $p = $this->order_model->changeAllProgress($ids, $progress, $type, $uid);
        echo json_encode($p);
        return false;
    }


    public function get_order_match()
    {
        $id = $_POST['id'];

        if ("" == $id)
        {
            echo json_encode("");
            return false;
        }

        $data = $this->order_model->getOrderMatch($id);
        $html = "";
        foreach ($data as $name => $info)
        {
            $f_l = 0;
            $f_r = 0;
            $d_l = 0;
            $d_r = 0;
            $thead_class = "";
            $l_class = "";
            $r_class = "";
            $fake_f_l = $f_l;
            $fake_f_r = $f_r;

            if (isset($data[$name]['f']))
            {
                $f_l = $data[$name]['f']['l'];
                $f_r = $data[$name]['f']['r'];
            }

            if (isset($data[$name]['d']))
            {
                $d_l = $data[$name]['d']['l'];
                $d_r = $data[$name]['d']['r'];
            }

            $fake_f_l = (preg_match('/雙開/', $name)) ? $f_l*2 : $f_l;
            $fake_f_r = (preg_match('/雙開/', $name)) ? $f_r*2 : $f_r;

            if ($fake_f_l != $d_l || $fake_f_r != $d_r)
            {
                $thead_class = "text-danger";
            }

            if ($fake_f_l != $d_l)
            {
                $l_class = "bg-danger";
            }

            if ($fake_f_r != $d_r)
            {
                $r_class = "bg-danger";
            }

            $myhtml = "<div class='col-lg-2 col-md-3 col-sm-4'>";
            $myhtml .= "<table class='table table-bordered'><thead class='thead-dark'><tr><th class='$thead_class' colspan='3'>$name</th></tr></thead>";
            $myhtml .= "<tbody><tr><td></td><td>L</td><td>R</td></tr>";
            $myhtml .= "<tr><td>框</td><td class='$l_class'>$f_l</td><td class='$r_class'>$f_r</td></tr>";
            $myhtml .= "<tr><td>扇</td><td class='$l_class'>$d_l</td><td class='$r_class'>$d_r</td></tr></tbody>";
            $myhtml .="</table></div>";

            $html .= $myhtml;
        }

        echo json_encode($html);
        return false;
    }

    public function get_fsl()
    {
        $id = $_POST['id'];
        if ("" == $id)
        {
            echo json_encode("");
            return false;
        }
        $data = $this->order_model->getFSL($id);

        echo json_encode($data);
        return false;
    }

    public function add_fsl()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        $data = array(
                    'project_id'=> $id,
                    'name'      => $name,
                    'type'      => $type,
                    'fsl_l'     => $fsl_l,
                    'fsl_w'     => $fsl_w,
                    'fsl_a'     => $fsl_a,
                    'fsl_b'     => $fsl_b,
                    'fsl_c'     => $fsl_c,
                    'fsl_d'     => $fsl_d
                );

        $exist = $this->order_model->checkFSL($id);

        if ($exist)
        {
            $this->order_model->updateFSL($data, $id);
        }
        else
        {
            $id = $this->common_model->insert('fsl_info', $data);
        }

        echo json_encode(true);
        return false;
    }

    public function add_fs()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        $data = array(
                    'project_id' => $id,
                    'la_a' => $la_a,
                    'la_b' => $la_b,
                    'la_c' => $la_c,
                    'la_d' => $la_d,
                    'ta_a' => $ta_a,
                    'ta_b' => $ta_b,
                    'ta_c' => $ta_c,
                    'ta_d' => $ta_d,
                    'ta_e' => $ta_e,
                    'gap' => $gap,
                    'horizontal_reduce' => $horizontal_reduce,
                    '45degree' => $_POST['45degree'],
                    'handle_orientation' => $handle_orientation
                );
        $exist = $this->order_model->checkFS($id);

        if ($exist)
        {
            $this->order_model->updateFS($data, $id);
        }
        else
        {
            $id = $this->common_model->insert('frame_setting', $data);
        }

        echo json_encode(true);
        return false;
    }

    public function get_fs()
    {
        $id = $_POST['id'];
        if ("" == $id)
        {
            echo json_encode("");
            return false;
        }
        $data = $this->order_model->getFS($id);

        echo json_encode($data);
        return false;
    }

    public function get_qa_content() {
        $html = "<input type='text' name='dd' value='aa'>";
        echo $html;
        return false;
    }
}