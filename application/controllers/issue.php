<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Issue extends CI_controller 
{
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('project_model');
        $this->load->model('issue_model');
        $this->load->model('order_model');
    }

    public function index()
    {
        $this->data['title'] = '問題追蹤';
        $this->data['isNav'] = 1;
        $this->data['js_file'] = array( 'vendor/summernote.min.js',
                                        'vendor/summernote-zh-TW.js',
                                        'vendor/fileinput.js',
                                        'vendor/fileinput_locale_zh-TW.js',
                                        'issue_modal.js',
                                        'vendor/validator.min.js',
                                        'issue.js');
        $this->data['css_file'] = array('font-awesome.min.css',
                                        'summernote.css',
                                        'fileinput.min.css',
                                        'issue_modal.css',
                                        'issue.css');
        $this->data['selected_tab'] = "issue";
        $this->data['content'] = 'issue';
        $this->data['to'] = $this->issue_model->getAvailableUsers($this->session->userdata('uid'));
        $this->data['projects'] = $this->project_model->getAllProjects($this->session->userdata('status'));
        
        $this->load->view('template', $this->data);
    }

    public function get_order()
    {
        $project_id = $this->input->post('project');
        $data = $this->issue_model->getOrders($project_id);
        echo json_encode($data);
        return false;
    }

    public function insert()
    {
        if (isset($_POST['order']) && $_POST['order'] != "")
        {
            list($type, $order_id) = explode('_', $this->input->post('order'));
        }
        else
        {
            $type = null;
            $order_id = null;
        }

        // Get sequence number
        $sequence = 1 + $this->issue_model->getIssueSequence($this->input->post('project'));

        $issue_data = array(
                        'project_id' => $this->encrypt->decode($this->input->post('project')),
                        'subject' => $this->input->post('subject'),
                        'order_id' => $order_id,
                        'type' => $type,
                        'issue_type' => $this->input->post('type'),
                        'progress' => '未開始',
                        'creator_id' => $this->session->userdata('uid'),
                        'belong_uid' => $this->input->post('to'),
                        'sequence' => $sequence
                    );
        $issue_id = $this->common_model->insert('issue', $issue_data);

        $conv_data = array(
                        'issue_id' => $issue_id,
                        'uid' => $this->session->userdata('uid'),
                        'to_uid' => $this->input->post('to'),
                        'conversation' => $this->input->post('conversation')
                    );
        $conversation_id = $this->common_model->insert('conversation', $conv_data);

        // Insert File
        $this->_insert_conv_files($conversation_id);

        echo json_encode(1);
        return false;
    }

    function get_conversation()
    {
        $id = $this->input->post('id');

        $data = $this->issue_model->getConversation($id);

        echo json_encode($data);
        return false;
    }

    function insert_conv()
    {
        // Add Conversation
        $conv_data = array(
                        'issue_id' => $this->input->post('issue_id'),
                        'uid' => $this->session->userdata('uid'),
                        'to_uid' => $this->input->post('to'),
                        'conversation' => $this->input->post('conversation')
                    );
        $conversation_id = $this->common_model->insert('conversation', $conv_data);

        // Insert File
        $this->_insert_conv_files($conversation_id);

        echo json_encode(1);
        return false;
    }

    function _insert_conv_files($conversation_id)
    {
        foreach ($_FILES as $key => $value)
        {
            if ($value['error'] != "0")
            {
                continue;
            }
            $path_part = pathinfo($value['name']);
            $target    = './files/' . $conversation_id . '_' . time() . '_' . rand() . '.' . $path_part['extension'];
            move_uploaded_file($value['tmp_name'], $target);

            $data = array(
                        'conversation_id' => $conversation_id,
                        'filename' => $value['name'],
                        'type' => $value['type'],
                        'location' => $target
                    );
            $id = $this->common_model->insert('conversation_file', $data);
        }
    }

    function download()
    {
        $conversation_id = $this->input->get('c');
        $conversation_file_id = $this->input->get('i');

        $info = $this->issue_model->download($conversation_id, $conversation_file_id);
        $this->load->helper('download');
        $data = file_get_contents($info['location']);
        $name = $info['filename'];
        force_download($name, $data);
    }

    function get_company_tracker()
    {
        $data = array();
        $time = $this->common_model->getData('user_last_read', array('company_issue'), array('uid'=>$this->session->userdata('uid')));
        
        if ($this->session->userdata('status') > 3)
        {
            $info = $this->issue_model->getLastConversation('p.status_id', $this->session->userdata('status'), $time['company_issue']);
        }
        else
        {
            $info = $this->issue_model->getLastConversation("", "", $time['company_issue']);
        }

        $data = $data = $this->_format_issue_data($info);

        echo json_encode($data, JSON_FORCE_OBJECT);
        return false;
    }

    function get_tracker_by_project()
    {
        $data = array();
        $project_id = $this->encrypt->decode($this->input->post('myId'));
        $info = $this->issue_model->getLastConversation('p.project_id', $project_id);
        $data = $this->_format_issue_data($info);

        echo json_encode($data, JSON_FORCE_OBJECT);
        return false;
    }

    function _format_issue_data($info)
    {
        $data = array();
        foreach ($info as $k => $v)
        {
            $file_html = "";
            foreach ($v['files'] as $file)
            {
                $file_html .= '<li><a href="' . base_url() . 'index.php/issue/download?c=' . $v['conversation_id'] . '&i=' . $file['file_id'] . '">';
                $file_html .= "<img src='" . $file['img'] . "' alt='" . $file['filename'] . "' title='" . $file['filename'] . "'>";
                $file_html .= "<span class='file_text'>" . $file['filename'] . "</span></a></li>";
            }
            $data[$k]['new'] = $v['new'];
            $data[$k]['tracker'] = '<span data-toggle="modal" data-target="#issue_modal" onclick="getConversation(\'' . $v['issue_id'] . '\');" class="beacon" id="i_' . $v['issue_id'] . '">' . $v['project'] . '-' . $v['sequence'] . '</span>';
            $data[$k]['tracker'] = ($v['new']) ? '<sup><span class="glyphicon glyphicon-star small bright-yellow"></span></sup>' .  $data[$k]['tracker'] : $data[$k]['tracker'];
            $data[$k]['date'] = $v['create_date'];
            $data[$k]['subject'] = $v['subject'];
            $data[$k]['to'] = $v['t_user'];
            $data[$k]['issue_type'] = $v['issue_type'];
            $data[$k]['conversation'] = '<span class="speaker">' . $v['o_user'] . "</span>說: " . $v['conversation'] . '<ul class="conv_files">' . $file_html . '</ul>';
        }
        return $data;
    }
}