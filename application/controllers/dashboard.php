<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Dashboard extends CI_controller 
{
    private $uid;
    private $username;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('dashboard_model');

        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function index() 
    {

    	$data['title'] = '資訊中心 - ' . $this->session->userdata('company');
        // $data['board1'] = $this->dashboard_model->getJob($this->uid);
        // $data['mixed_progress'] = $this->dashboard_model->getMixedProgress($this->uid);
        $data['editable'] = $this->status_model->isEditable($this->uid);
    	$data['content'] = 'dashboard';
    	$data['js_file']  = array('vendor/jquery-ui.min.js','vendor/footable.min.js','vendor/footable.sorting.min.js','vendor/bootstrap-toggle.min.js','dashboard.js');
    	$data['css_file'] = array('style.css', 'footable.bootstrap.min.css', 'footable.fontawesome.css', 'bootstrap-toggle.min.css');
    	$this->load->view('_template', $data); 
    }

    public function newBoard() 
    {

    	$data['title'] = '資訊中心 - ' . $this->session->userdata('company');
        $data['editable'] = $this->status_model->isEditable($this->uid);
    	$data['content'] = 'dashboard_new';
    	$data['js_file']  = array('vendor/jquery-ui.min.js','vendor/footable.min.js','vendor/footable.sorting.min.js','vendor/bootstrap-toggle.min.js','dashboard.js');
    	$data['css_file'] = array('style.css', 'footable.bootstrap.min.css', 'footable.fontawesome.css', 'bootstrap-toggle.min.css');
    	$this->load->view('_template', $data); 
    }

    public function get_progress()
    {
        $progress = $_POST['p'];

        $cols[] = array("name" => "dates", "title" => "進度日期", "sorted"=>true, 'direction'=>'DESC');
        $cols[] = array("name" => "order_type", "title" => "屬性");
        $cols[] = array("name" => "name", "title" => "訂單");
        $cols[] = array("name" => "frame", "title" => "框");
        $cols[] = array("name" => "door", "title" => "門");

        if ($progress == "已出貨") {
            $rows = $this->dashboard_model->getShipped($this->uid);
        } else {
            $rows = $this->dashboard_model->getProgress($progress, $this->uid);
        }
     
        echo json_encode(array('cols'=>$cols, 'rows'=>$rows));
        return false;
    }

    public function get_progress_board()
    {
        $type = $_POST['t'];

        $cols[] = array("name" => "df", "title" => "");
        $cols[] = array("name" => "name", "title" => "訂單", "sorted"=>true, 'direction'=>'DESC');
        $cols[] = array("name" => "total", "title" => "數量", 'type'=>'number');
        $cols[] = array("name" => "customer_due_date", "title" => "客需日");
        $cols[] = array("name" => "progress", "title" => "訂單進度");
        $cols[] = array("name" => "received", "title" => "收到訂單");
        $cols[] = array("name" => "fold", "title" => "發包中");
        $cols[] = array("name" => "wait", "title" => "備料中");
        $cols[] = array("name" => "production", "title" => "組裝中");
        $cols[] = array("name" => "paint", "title" => "烤漆包裝");
        $cols[] = array("name" => "wait_ship", "title" => "等出貨");
        if ($type == "door") {
            $cols[] = array("name" => "skin", "title" => "等SKIN板");
        } 
        $cols[] = array("name" => "response", "title" => "等回復");
        
        $rows = $this->dashboard_model->getProgressBoard($type, $this->uid);

        echo json_encode(array('cols'=>$cols, 'rows'=>$rows));
        return false;
    }

    public function get_date_progress()
    {
        $date = $_POST['d'];

        $cols = array();
        $cols[] = array("name" => "time", "title" => "時間", "sorted"=>true, 'direction'=>'ASC');
        $cols[] = array("name" => "site", "title" => "工地");
        $cols[] = array("name" => "order", "title" => "訂單");
        $cols[] = array("name" => "model", "title" => "型號");
        $cols[] = array("name" => "amount", "title" => "數量");
        $cols[] = array("name" => "size", "title" => "長寬");
        $cols[] = array("name" => "field", "title" => "更變");
        $cols[] = array("name" => "old_value", "title" => "舊進度");
        $cols[] = array("name" => "new_value", "title" => "新進度");
        $cols[] = array("name" => "user", "title" => "更變者");

        $rows = $this->dashboard_model->getDateProgress($date);

        echo json_encode(array('cols'=>$cols, 'rows'=>$rows));
        return false;
    }

    public function get_date_progress_ny()
    {
        $date = $_POST['d'];

        $cols = array();
        $cols[] = array("name" => "time", "title" => "時間", "sorted"=>true, 'direction'=>'ASC');
        $cols[] = array("name" => "site", "title" => "工地");
        $cols[] = array("name" => "order", "title" => "訂單");
        $cols[] = array("name" => "model", "title" => "型號");
        $cols[] = array("name" => "amount", "title" => "數量");
        $cols[] = array("name" => "size", "title" => "長寬");
        $cols[] = array("name" => "field", "title" => "更變");
        $cols[] = array("name" => "old_value", "title" => "舊進度");
        $cols[] = array("name" => "new_value", "title" => "新進度");
        $cols[] = array("name" => "user", "title" => "更變者");

        $rows = $this->dashboard_model->getDateProgressNY($date);

        echo json_encode(array('cols'=>$cols, 'rows'=>$rows));
        return false;
    }

    public function getTableData() {
        header('Content-Type: application/json');
        $data = json_decode(file_get_contents('php://input'), true); 

        switch($data['type']) {
            case 'frame':
                list($headers, $rows) = $this->dashboard_model->getTableFrame();
                break;
            case 'smc':
                list($headers, $rows) = $this->dashboard_model->getTableSMC();
                break;
            case 'door':
                list($headers, $rows) = $this->dashboard_model->getTableDoor();
                break;
            case 'mainDoor':
                list($headers, $rows) = $this->dashboard_model->getTableMainDoor();
                break;
            default:
                $headers = array();
                $rows = array();
                break;
        }

        echo json_encode(array( 'header'=>$headers, 'data'=>$rows));
        return false;
    }
}
