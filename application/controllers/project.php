<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Project extends CI_controller 
{
    private $uid; 

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('status_model');
        $this->load->model('customer_model');
        $this->load->model('project_model');
        $this->load->model('common_model');

        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function index()
    {
        // Get data
        $project = $this->project_model->getProject($this->uid);
        $options = $this->customer_model->get($this->uid);

        $data['editable'] = $this->status_model->isEditable($this->uid);
        $data['title']    = '建案 - ' . $this->session->userdata('company');
        $data['content']  = 'project';
        $data['js_file']  = array('vendor/moment.js','vendor/footable.min.js','vendor/footable.sorting.min.js');
        $data['css_file'] = array('style.css','bootstrap-theme.min.css','footable.bootstrap.min.css','footable.fontawesome.css','svg.css');
        $data['project']  = $project;
        $data['options']  = $options;
        $this->load->view('_template', $data);
    }

    public function update() 
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }
        
        $data = array(
                    "site" => $site,
                    "address" => $address,
                    "site_contact" => $site_contact, 
                    "site_phone" => $site_phone,
                    "company_contact" => $company_contact,
                    "company_phone" => $company_phone,
                    "note" => $note
                );
        $success = $this->project_model->update($data, $id);
        echo json_encode($success);
        return false;
    }

    public function add()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        /* Prepare & Insert Project */
        $data = array(
                    'customer_id'   => $company,
                    'date'          => date("Y-m-d", time()),
                    'site'          => $site,
                    'address'       => $address,
                    'site_contact'  => $site_contact,
                    'site_phone'    => $site_phone,
                    'company_contact' => $company_contact,
                    'company_phone' => $company_phone,
                    'note'          => $note,
                    'order_type'    => $order_type
                );

        $project_id = $this->common_model->insert('project', $data);

        echo json_encode($project_id);
        return false;
    }

}