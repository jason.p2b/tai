<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Report extends CI_controller 
{
    private $data = array(
                        'isNav' => 1,
                        'SVGMeasurement' => 0
                    );

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('report_model');
    }

    public function index()
    {
        $this->data['title'] = '檢視訂單';
        $this->data['js_file'] = array('report.js');
        $this->data['css_file'] = array('report.css','svg.css');
        $this->data['selected_tab'] = "report";
        $this->data['content'] = 'report';
        $this->data['data'] = $this->report_model->viewData();
        $this->load->view('template', $this->data);
    }

    public function report_new()
    {
        if ($this->session->userdata('status') > 3)
        {
            redirect('report');
        }

        $this->data['title'] = '新增防火報告';
        $this->data['js_file'] = array('report_new.js');
        $this->data['css_file'] = array('report_new.css','svg.css');
        $this->data['selected_tab'] = "report";
        $this->data['content'] = 'report_new';
        $this->data['frame_shape'] = $this->report_model->getFrameShape();
        $this->data['fireproof_report'] = $this->report_model->getMainReport();
        $this->data['frame_material'] = $this->report_model->getFrameMaterial();
        $this->data['door_material'] = $this->report_model->getDoorMaterial();
        $this->data['lock'] = $this->report_model->getLock();
        $this->data['hinge'] = $this->report_model->getHinge();
        $this->data['hardware'] = $this->report_model->getHardware();
        $this->data['closer'] = $this->report_model->getCloser();
        $this->data['handle'] = $this->report_model->getHandle();
        $this->data['bolt'] = $this->report_model->getBolt();

        $this->load->view('template', $this->data); 
    }

    public function insert()
    {
        $frame_material = array();
        $door_material = array();
        $lock = array();
        $hinge = array();
        $hardware = array();
        $closer = array();
        $handle = array();
        $bolt = array();
        $new_frame_material = array();
        $new_door_material = array();
        $new_lock = array();
        $new_hinge = array();
        $new_hardware = array();
        $new_closer = array();
        $new_handle = array();
        $new_bolt = array();

        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        // Preparation & insertion for TABLE fireproof_report
        $main_report_id = (isset($m_report)) ? $main_report : null;
        $glass_dimension = ($glass != "no") ? $glass_width . " x " . $glass_length : null;
        $report_data = array(
                    'name' => $report,
                    'door_thickness' => $door_thickness,
                    'min_frame_length' => $min_frame_length,
                    'max_frame_length' => $max_frame_length,
                    'min_frame_width' => $min_frame_width,
                    'max_frame_width' => $max_frame_width,
                    'min_door_length' => $min_door_length,
                    'max_door_length' => $max_door_length,
                    'min_door_width' => $min_door_width,
                    'max_door_width' => $max_door_width,
                    'glass' => $glass,
                    'glass_dimension' => $glass_dimension,
                    'single_door' => $door_amount,
                    'frame_type' => $frame_type,
                    'main_report_id' => $main_report_id
            );
        $fireproof_report_id = $this->common_model->insert('fireproof_report', $report_data);

        // Preparation & insertion for TABLE frame_shape
        foreach ($frame_shape as $id)
        {
            $fr_fs = array('fireproof_report_id' => $fireproof_report_id, 'frame_shape_id' => $id);
            $this->common_model->insert('fireproof_report_frame_shape', $fr_fs);
        }

        // Preparation & insertion for TABLE frame_material
        $this->_insert_part($frame_material, $new_frame_material, 'frame_material', 'name', $fireproof_report_id);
       
        // Preparation & insertion for TABLE door_material
        $this->_insert_part($door_material, $new_door_material, 'door_material', 'name', $fireproof_report_id);

        // Preparation & insertion for TABLE lock
        $this->_insert_part($lock, $new_lock, 'lock', 'model', $fireproof_report_id);

        // Preparation & insertion for TABLE hinge
        $this->_insert_part($hinge, $new_hinge, 'hinge', 'model', $fireproof_report_id);

        // Preparation & insertion for TABLE hardware
        $this->_insert_part($hardware, $new_hardware, 'hardware', 'model', $fireproof_report_id);
        
        // Preparation & insertion for TABLE closer
        $this->_insert_part($closer, $new_closer, 'closer', 'model', $fireproof_report_id);
        
        // Preparation & insertion for TABLE handle
        $this->_insert_part($handle, $new_handle, 'handle', 'model', $fireproof_report_id);
        
        // Preparation & insertion for TABLE bolt
        $this->_insert_part($bolt, $new_bolt, 'bolt', 'model', $fireproof_report_id);

        redirect('report');
    }

    function _insert_part($data, $new_data, $table, $column, $report_id)
    {
        if (isset($data))
        {
            foreach ($data as $id)
            {
                $array = array('fireproof_report_id' => $report_id, $table . '_id' => $id);
                $this->common_model->insert('fireproof_report_' . $table, $array);
            }
        }
        
        if (isset($new_data))
        {
            foreach ($new_data as $name)
            {
                $arr = array($column => $name);
                $id = $this->common_model->insert($table, $arr);
                $array = array('fireproof_report_id' => $report_id, $table . '_id' => $id);
                $this->common_model->insert('fireproof_report_' . $table, $array);
            }
        }
    }
}