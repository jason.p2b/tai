<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Svg extends CI_controller 
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('svg_model');

        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function get_frame_svg()
    {
        $data = array();
        $svg = "";

        $id = $this->input->post('id');
        $type = $this->input->post('type');
        
        if ("ny" == $type)
        {
            $data = $this->svg_model->getNYFrameSVG($id);

            switch (substr($data['window'],1,1)) {
                case 'T':
                    $material = "SECC 1.6t";
                    break;
                case 'S':
                    $material = "SUS 1.5t";
                    break;
                default:
                    $material = "找不到";
                    break;
            }

            switch (substr($data['window'],2,1)) 
            {
                case '3':
                    $l = 0;
                    $r = $data['amount'];
                    break;
                case '4':
                    $l = $data['amount'];
                    $r = 0;
                    break;
                case '5':
                    $l = $data['amount'];
                    $r = 0;
                    break;
                case '6':
                    $l = 0;
                    $r = $data['amount'];
                    break;
            }

            $s = array('$SITE$','$NAME$','$MATERIAL$','$HANDLE_HEIGHT$','$AMOUNT$','$DEPTH$', '$HEIGHT$', '$WIDTH$');
            $r = array($data['site'],$data['name'],$material,$data['lock_height'],"L" . $l . ", R" . $r ,$data['depth'],$data['total_height'],$data['width']);
        }
        else if ("trad" == $type)
        {
            $data = $this->svg_model->getTradFrameSVG($id);
            $myamounts = array();
            if ("0" != $data['amount_l'])
            {
                $myamounts[] = "L" . $data['amount_l'];
            }
            if ("0" != $data['amount_r'])
            {
                $myamounts[] = "R" . $data['amount_r'];
            }
            $amount = implode(', ', $myamounts);

            
            $s = array('$SITE$','$NAME$','$MATERIAL$','$HANDLE_HEIGHT$','$AMOUNT$','$DEPTH$', '$HEIGHT$', '$WIDTH$', '$COLOUR$');
            $r = array($data['site'],$data['name'],$data['material'],$data['handle_height'],$amount,$data['depth'],$data['height'],$data['width'],$data['colour']);
        }

        

        $svg = $data['main'];
        $svg = str_replace("\$FRAME\$", $data['frame'], $svg);
        $svg = str_replace("\$LOCK\$", $data['lock_svg'], $svg);
        $svg = str_replace("\$HINGE\$", $data['hinge_svg'], $svg);
        $svg = str_replace("\$MAGNETIC_HOLE\$", $data['magnetic_hole_svg'], $svg);
        $svg = str_replace("\$FAIL_SAFE_LOCK\$", $data['fail_safe_lock_svg'], $svg);
        $svg = str_replace("\$CLOSER\$", $data['closer_svg'], $svg);
        $svg = str_replace($s, $r, $svg);

        $css_search = array(
                        'class="arrows"',
                        'class="labels"',
                        'class="inner-title"',
                        'class="frame-outline"',
                        'id="outline"',
                        'id="outline-text"',
                        'id="company-title"',
                        'class="svg_text"',
                        'id="text-input"',
                        'id="inner-text-input"',
                        'class="pointers"'
            );
        $css_replace = array(
                        'style="fill:#ff0000;stroke:#ff0000;marker-start:url(#arrowEnd);marker-end:url(#arrowStart)"',
                        'style="fill:#ff0000;stroke:#ff0000;"',
                        'style="font-size:1.5em;"',
                        'style="fill:none;stroke:#000000;stroke-width:3;font-size:Arial;"',
                        'style="font-family:Arial;fill:#ffffff;stroke:gray;stroke-width:10;"',
                        'style="font-family:Arial;fill:gray;font-size:5em;"',
                        'style="font-size:1.1em;"',
                        'style="fill:#ff0000;stroke:#ff0000;font-size:3em;text-align:center;text-anchor:middle;stroke-width:1;"',
                        'style="fill:#ff0000;font-size:4.8em;"',
                        'style="fill:#ff0000;font-size:8em;"',
                        'style="marker-start:url(#arrowEnd);fill:#f00;stroke:#f00;"'
            );
        $svg = str_replace($css_search, $css_replace, $svg);


        $file = $data['name'] . ".jpg";
        // $fp = fopen($file, 'w');
        // fwrite($fp, $svg);
        // fclose($fp);
        // echo json_encode("/tai/".$file);
        echo json_encode(array('h'=>$svg,'f'=>$file));
        return false;
    }
}