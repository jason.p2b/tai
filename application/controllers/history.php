<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2019, Jason Liu
 * @category    Controller
 */

Class History extends CI_controller 
{
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('history_model');

        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function get_order_history()
    {
        $diary = array();
    	$ny_order_id = $_POST['id'];

        $diary = $this->history_model->getOrderHistory($ny_order_id);

        echo json_encode($diary);
        return false;
    }
}