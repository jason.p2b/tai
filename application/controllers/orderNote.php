<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class OrderNote extends CI_controller 
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('order_note_model');
        $this->load->model('common_model');       
        $this->load->model('status_model');
        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function index()
    {
        $ny_order_id = $_POST['id'];
        $data = $this->order_note_model->show($ny_order_id);
        echo json_encode($data);
        return false;
    }

    public function create()
    {
        $ny_order_id = $_POST['id'];
        $user_id = $this->uid;
        $note = str_replace("\n", "<br>", $_POST['note']);

        $success = $this->order_note_model->create($ny_order_id, $user_id, $note);
        echo json_encode(array('success'=>1));
        return false;
    }
}