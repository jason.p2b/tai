<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2019, Jason Liu
 * @category    Controller
 */

Class QualityControl extends CI_controller 
{
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('qc_model');

        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function get_qc_table()
    {
        $ny_order_id = $_POST['id'];
        $header = array();
        $rows = array();
        
        // $header[] = array("name" => "name", "title" => "品名");
        // $header[] = array("name" => "type", "title" => "類型");
        // $header[] = array("name" => "amount", "title"=>"數量");
        // $header[] = array("name" => "color", "title"=>"顏色");
        // $header[] = array("name" => "size", "title" => "尺寸");
        // $header[] = array("name" => "quantity", "title" => "數量");
        // $header[] = array("name" => "lock", "title" => "門鎖");
        // $header[] = array("name" => "hinge", "title" => "鉸鏈");
        // $header[] = array("name" => "other", "title" => "其他五金");
        // $header[] = array("name" => "package", "title" => "包裝");
        // $header[] = array("name" => "user", "title" => "品管員");
        // $header[] = array("name" => "date", "title" => "品管日期");
        $header[] = array("name" => "date", "title" => "日期");
        $header[] = array("name" => "row_number", "title" => "項次");
        $header[] = array("name" => "amount", "title"=>"數量");
        $header[] = array("name" => "width", "title"=>"寬度");
        $header[] = array("name" => "height", "title" => "高度");
        $header[] = array("name" => "thickness", "title" => "厚度");
        $header[] = array("name" => "diagonal", "title" => "對角差");
        $header[] = array("name" => "side", "title" => "對邊差");
        $header[] = array("name" => "lock_height", "title" => "鎖高");
        $header[] = array("name" => "fake_handle", "title" => "把手假安裝");
        $header[] = array("name" => "side_connect", "title" => "封邊緊密度");
        $header[] = array("name" => "side_tightness", "title" => "封邊鬆緊度");
        $header[] = array("name" => "colour", "title" => "塗裝色");
        $header[] = array("name" => "exterior", "title" => "外觀");
        $header[] = array("name" => "hardware", "title" => "五金");
        $header[] = array("name" => "adhesion", "title" => "密著度");
        $header[] = array("name" => "flatness", "title" => "平面度");
        $header[] = array("name" => "hinge_1", "title" => "鉸練1");
        $header[] = array("name" => "hinge_2", "title" => "鉸練2");
        $header[] = array("name" => "hinge_3", "title" => "鉸練3");
        $header[] = array("name" => "hinge_4", "title" => "鉸練4");
        $header[] = array("name" => "glass_top", "title" => "頂距");
        $header[] = array("name" => "glass_side", "title" => "邊距");
        $header[] = array("name" => "glass_width", "title" => "玻璃寬");
        $header[] = array("name" => "glass_height", "title" => "玻璃長");
        $header[] = array("name" => "user", "title" => "品管員");

        $rows = $this->qc_model->getQCTable($ny_order_id);

        echo json_encode(array('col' => $header, 'row' =>$rows, 'success'=>1));
        return false;
    }
}