<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Stock extends CI_controller 
{
    private $uid;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('common_model');
        $this->load->model('stock_model');
        $this->uid = $this->encrypt->decode($this->session->userdata('uid'));
        checkUser();
    }

    public function index()
    {
        // Get data
        $data['editable'] = $this->status_model->isEditable($this->uid);
        if (!$data['editable']) {redirect('login');}

        $data['title'] = '庫存資料';
        $data['content'] = 'stock';
        $data['js_file']  = array('vendor/moment.js','vendor/footable.min.js','vendor/footable.sorting.min.js','vendor/footable.export.min.js');
        $data['css_file'] = array('style.css','bootstrap-theme.min.css','footable.bootstrap.min.css','footable.fontawesome.css');
        $this->load->view('_template', $data);
    }

    public function get_material_select()
    {
        $data = $this->stock_model->getMaterialSelect();
        echo json_encode($data);
        return false;
    }

    public function check_material()
    {
        $name  = $this->input->post('name');
        $check = $this->stock_model->checkMaterial($name);
        echo json_encode($check);
        return false;
    }

    public function get_material()
    {
        $rows = $this->stock_model->getMaterial();
        $header = array();
        $header[] = array("name"=>"mid", "title"=>"id", "visible"=>false, "filterable"=>false);
        $header[] = array("name"=>"type", "title"=>"類型");
        $header[] = array("name"=>"name", "title"=>"品名");
        $header[] = array("name"=>"size", "title"=>"尺寸", "breakpoints" => "xs");
        $header[] = array("name"=>"unit","title"=>"單位");
        $header[] = array("name"=>"undersupply","title"=>"庫存線");
        $header[] = array("name"=>"used_in","title"=>"使用處", "breakpoints" => "xs");
        $header[] = array("name"=>"note","title"=>"備註", "breakpoints" => "xs");

        echo json_encode(array('col' => $header, 'row' => $rows));
        return false;
    }

    public function add_material()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }
        $filename = "";
        $file_type = "";
        $file_location = "";

        if ($_FILES['photo']['error'] === 0) {
            $filename  = time() . '_' . $_FILES['photo']['name'];
            $file_type = $_FILES['photo']['type'];
            $file_location = 'upload/' . $filename;
            $file = $_FILES['photo']['tmp_name'];
            move_uploaded_file($file, $file_location);
        }

        /* Prepare & Insert Project */
        $data = array(
                    'type'    => $type,
                    'name'    => $name,
                    'size'    => $size,
                    'unit'    => $unit,
                    'undersupply' => $undersupply,
                    'used_in' => $used_in,
                    'note'    => $note,
                    'filename' => $filename,
                    'file_type' => $file_type,
                    'file_location' => $file_location
                );

        $mid = $this->common_model->insert('material', $data);

        echo json_encode($mid);
        return false;
    }

    public function update_material()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }
        $filename = "";
        $file_type = "";
        $file_location = "";

        if ($_FILES['photo']['error'] === 0) {
            $filename  = time() . '_' . $_FILES['photo']['name'];
            $file_type = $_FILES['photo']['type'];
            $file_location = 'upload/' . $filename;
            $file = $_FILES['photo']['tmp_name'];
            move_uploaded_file($file, $file_location);
        }

        $data = array(
                    'type'    => $type,
                    'name'    => $name,
                    'size'    => $size,
                    'unit'    => $unit,
                    'undersupply' => $undersupply,
                    'used_in' => $used_in,
                    'note'    => $note,
                    'filename' => $filename,
                    'file_type' => $file_type,
                    'file_location' => $file_location
                );
        $success = $this->stock_model->updateMaterial($data, $mid);
        echo json_encode($success);
        return false;
    }

    public function get_stock()
    {
        $rows = $this->stock_model->getStock();
        $header = array();
        $header[] = array("name"=>"stock_id", "title"=>'stock_id', "visible"=>false, "filterable"=>false);
        $header[] = array("name"=>"mid", "title"=>"id", "visible"=>false, "filterable"=>false);
        $header[] = array("name"=>"isIn", "title"=>"isIn", "visible"=>false, "filterable"=>false);
        $header[] = array("name"=>"date", "title"=>"日期", "sorted"=>true, "direction"=>"DESC");
        $header[] = array("name"=>"name", "title"=>"品名");
        $header[] = array("name"=>"unit","title"=>"單位");
        $header[] = array("name"=>"amount","title"=>"數量");
        $header[] = array("name"=>"price","title"=>"單價");
        $header[] = array("name"=>"total","title"=>"總價");
        $header[] = array("name"=>"supplier","title"=>"供應商");
        $header[] = array("name"=>"note","title"=>"備註");

        echo json_encode(array('col' => $header, 'row' => $rows));
        return false;
    }

    public function add_stock()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }

        /* Prepare & Insert Project */
        $data = array(
                    'mid'       => $mid,
                    'isIn'      => $isIn,
                    'date'      => $date,
                    'amount'    => $amount,
                    'price'     => $price,
                    'supplier'  => $supplier,
                    'note'      => $note
                );

        $stock_id = $this->common_model->insert('stock', $data);
        $unit = $this->stock_model->getUnit($mid);

        echo json_encode(array('unit'=> $unit, 'id'=>$stock_id));
        return false;
    }

    public function update_stock()
    {
        foreach ($_POST as $key => $value)
        {
            $$key = $value;
        }
        
        /* Prepare & Insert Project */
        $data = array(
                    'mid'       => $mid,
                    'isIn'      => $isIn,
                    'date'      => $date,
                    'amount'    => $amount,
                    'price'     => $price,
                    'supplier'  => $supplier,
                    'note'      => $note
                );
        $success = $this->stock_model->updateStock($data, $stock_id);
        echo json_encode($success);
        return false;
    }

    function get_stocktake()
    {
        $head = array();
        $rows = array();

        $head[] = array("name"=>"type", "title"=>"類型");
        $head[] = array("name"=>"name", "title"=>"品名");
        $head[] = array("name"=>"amount", "title"=>"數量");

        $data = $this->stock_model->getStocktake();
        $files = $this->stock_model->getFile();
        foreach ($data as $type => $arr)
        {
            foreach ($arr as $name => $amount)
            {
                $info_sign = '&nbsp;<a data-toggle="tooltip" data-html="true" title="<img src=\'' . site_url() . $files[$name] . '\' />" onMouseOver="setTooltip()"><i class="glyphicon glyphicon-info-sign"></i></a>';
                $rows[] = array('type'=>$type, 'name'=>$name . $info_sign, 'amount'=>$amount[0] . " (" . $amount[1] . ")");
            }
        }

        echo json_encode(array('col'=>$head, 'row'=>$rows));
        return false;
    }

    function get_understock()
    {
        $head = array();
        $rows = array();

        $head[] = array("name"=>"type", "title"=>"類型");
        $head[] = array("name"=>"name", "title"=>"品名");
        $head[] = array("name"=>"amount", "title"=>"數量");
        $head[] = array("name"=>"undersupply", "title"=>"庫存線");

        $rows = $this->stock_model->getUnderStock();

        echo json_encode(array('col'=>$head, 'row'=>$rows));
        return false;
    }

    function stock_amount()
    {
        $mid = $this->input->post('id');
        list($amount, $price, $file) = $this->stock_model->getAmount($mid);
        echo json_encode(array('amount'=>$amount, 'price'=>$price, 'file'=>$file));
        return false;
    }

    function get_filter()
    {
        $data = $this->stock_model->getFilter();
        echo json_encode($data);
        return false;
    }

    function get_photo()
    {
        $name = $this->input->post('val');
        $photo = $this->stock_model->getPhoto($name);
        echo json_encode($photo);
        return false;
    }
}
