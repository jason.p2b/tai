<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');
/**
 *
 *
 * @package     CodeIgniter 2.13
 * @author      Jason Liu
 * @copyright   Copyright (c) 2014, Jason Liu
 * @category    Controller
 */

Class Login extends CI_controller 
{
    public $errorMsg = "";

    function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }

    public function index()
    {
        $data['title'] = '泰慶興業有限公司 - 登入';
        $data['content'] = 'login';
        $data['js_file'] = array();
        $data['css_file'] = array('style.css');
        $data['error'] = $this->errorMsg;

        $this->load->view('login', $data);
    }

    public function checkuser()
    {
        $username = preg_replace('/;/', '', $this->input->post('username'));
        $password = $this->input->post('password');

        $isAlive = $this->login_model->checkUserExists($username, $password);

        if (!$isAlive)
        {
            $this->errorMsg = "Wrong username or password";
            $this->index();
        }
        else
        {
            $row = $this->login_model->getUserDetail($username, $password);

            $session_data = array(
                    'username' => $this->encrypt->encode($username),
                    'uid' => $this->encrypt->encode($row[0]->uid),
                    'user' => $row[0]->name,
                    'company' => $row[0]->company,
                    'position' => $row[0]->position,
                    'logged_in' => true
                );
            $this->session->sess_expiration = '43200'; // session set 12 hours
            $this->session->set_userdata($session_data);

            $ip = "";
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            $date = date("Y-m-d H:i:s");
            $browser = $_SERVER['HTTP_USER_AGENT'];
            $data = array("uid"=>$row[0]->uid, "ip"=>$ip, "login_date"=>$date, "browser"=>$browser);
            $this->login_model->insertLastLogin($data);

            if (!$row[0]->first_time)
            {
                redirect('dashboard');
            }
            else
            {
                redirect('login/password');
            }
            
        }
    }

    public function password()
    {
        $data['title'] = '修改密碼';
        $data['content'] = 'password';
        $data['js_file'] = array();
        $data['css_file'] = array('style.css');

        $this->load->view('_template', $data);
    }

    public function change_password()
    {
        $opw = $_POST['opw'];
        $pw  = $_POST['pw'];
        $rpw = $_POST['rpw'];
        $user = $this->encrypt->decode($this->session->userdata('username'));
        $uid = $this->encrypt->decode($this->session->userdata('uid'));

        $success = $this->login_model->checkUserExists($user, $opw);

        if ($success && $pw == $rpw)
        {
            $isChanged = $this->login_model->changePassword($uid, $pw);
        }
        else
        {
            $success = false;
        }

        echo json_encode(array('success'=>$success));
        return false;
    }

    public function logout()
    {
        $session_data = array('logged_in' => false);
        $this->session->set_userdata($session_data);
        $this->session->sess_destroy();
        $this->index(); 
    }
}