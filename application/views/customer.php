<?if($editable):?>
<style>
  .modal-dialog {top: 200px;}
</style>
<!-- Begin Popup Modal -->
<div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
  <style scoped>
    /* provides a red astrix to denote required fields - this should be included in common stylesheet */
    .form-group.required .control-label:after {
      content:"*";
      color:red;
      margin-left: 4px;
    }
  </style>

  <div class="modal-dialog" role="document">
    <form class="modal-content form-horizontal" id="editor">
      <div class="modal-header">
        <h4 class="modal-title" id="editor-title">新增客戶</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        
      </div>
      <div class="modal-body">
        <input type="number" id="id" name="id" class="hidden" value=""/>
        <div class="form-group required">
          <label for="status" class="col-sm-3 control-label">屬性</label>
          <div class="col-sm-9">
            <select class="form-control" name="status" id="status">
              <option value="1">泰慶</option>
              <option value="2">英聖</option>
              <option value="3">南亞</option>
            </select>
          </div>
        </div>
        <div class="form-group required">
          <label for="company" class="col-sm-3 control-label">公司名稱</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="company" name="company" placeholder="公司名稱" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="gui" class="col-sm-3 control-label">統一編號</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="gui" name="gui" placeholder="統一編號" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="address" class="col-sm-3 control-label">地址</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="address" name="address" placeholder="地址" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="phone" class="col-sm-3 control-label">電話</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="phone" name="phone" placeholder="電話" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="fax" class="col-sm-3 control-label">傳真</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="fax" name="fax" placeholder="傳真" required>
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-3 control-label">Email</label>
          <div class="col-sm-9">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
          </div>
        </div>
        <div class="form-group required">
          <label for="contact" class="col-sm-3 control-label">聯絡人</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="contact" name="contact" placeholder="聯絡人" required>
          </div>
        </div>
        <div class="form-group">
          <label for="note" class="col-sm-3 control-label">備註</label>
          <div class="col-sm-9">
            <textarea class="form-control" id="note" name="note" row="3" placeholder="備註"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">確認</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </form>
  </div>
</div>
<!-- End Popup Modal -->
<?endif;?>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
      <h4 class="card-title">檢視客戶</h4>
      <div class="">
        <table id="editing-example" data-paging="true" class="table">
          <thead>
            <tr>
              <th data-name="status" data-visible="false" data-sorted="true" data-direction="ASC">屬性</th>
              <th data-name="company" data-sorted="true" data-direction="ASC">客戶</th>
              <th data-name="gui" data-breakpoints="xs sm" data-title="統編">統一編號</th>
              <th data-name="address" data-breakpoints="xs sm md" data-title="地址">地址</th>
              <th data-name="phone">電話</th>
              <th data-name="fax" data-breakpoints="xs" data-title="傳真">傳真</th>
              <th data-name="email" data-breakpoints="xs sm md" data-title="Email">Email</th>
              <th data-name="contact">聯絡人</th>
              <th data-name="note" data-breakpoints="all" data-title="備註">備註</th>
            </tr>
          </thead>
          <tbody>
            <?foreach ($customer as $row):?>
              <tr id="<?=$row->customer_id?>">
                <td><?=$row->status_id?></td>
                <td><?=$row->company?></td>
                <td><?=$row->gui?></td>
                <td><?=$row->address?></td>
                <td><?=$row->phone?></td>
                <td><?=$row->fax?></td>
                <td><?=$row->email?></td>
                <td><?=$row->contact?></td>
                <td><?=$row->note?></td>
              </tr>
            <?endforeach?>
          </tbody>
        </table>
      </div>
    </div>  
    </div>
  </div>
</div>

<script>
  
jQuery(function($){
    var $modal = $('#editor-modal'),
      $editor = $('#editor'),
      $editorTitle = $('#editor-title'),
      ft = FooTable.init('#editing-example', {
        sorting: {enabled: true},
        filtering: {enabled: true}<?if($editable):?>,
        editing: {
          enabled: true,
          addRow: function(){
            $modal.removeData('row');
            $editor[0].reset();
            $editorTitle.text('新增客戶');
            $modal.modal('show');
          },
          editRow: function(row){
            var values = row.val();
            $editor.find('#id').val(row.$el[0].id);
            $editor.find('#company').val(values.company);
            $editor.find('#gui').val(values.gui);
            $editor.find('#address').val(values.address);
            $editor.find('#phone').val(values.phone);
            $editor.find('#fax').val(values.fax);
            $editor.find('#email').val(values.email);
            $editor.find('#contact').val(values.contact);
            $editor.find('#note').val(values.note);
            $editor.find('#status').val(values.status);

            $modal.data('row', row);
            $editorTitle.text('修改客戶:' + values.company);
            jQuery.noConflict(); 
            $modal.modal('show');
          },
          'allowDelete': false,
          'showText': '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> 新增修改模式',
          'hideText': '取消',
          'addText': '新增客戶' 
        }<?endif;?>
      });
      $('span.caret').hide();
<?if($editable):?>

    $editor.on('submit', function(e){
      if (this.checkValidity && !this.checkValidity()) return;
      e.preventDefault();
      var row = $modal.data('row'),
        values = {
          id: $editor.find('#id').val(),
          company: $editor.find('#company').val(),
          gui: $editor.find('#gui').val(),
          address: $editor.find('#address').val(),
          phone: $editor.find('#phone').val(),
          fax: $editor.find('#fax').val(),
          email: $editor.find('#email').val(),
          contact: $editor.find('#contact').val(),
          note: $editor.find('#note').val(),
          status: $editor.find('#status').val()
        };

      if (row instanceof FooTable.Row){
        $.post("<?=site_url('customer/update')?>", values, function (result) {
          row.val(values);
        });
      } else {
        $.post("<?=site_url('customer/add')?>", values, function (result) {
          ft.rows.add(values);
          $('#editing-example>tbody>tr').each(function () {
            if ($(this).attr('id') == undefined) {
              $(this).attr('id', result);
            }
          });
        });
      }
      $modal.modal('hide');
    });
    <?endif;?>
  });

   

</script>