<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>泰慶興業有限公司 - 系統登入</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?=base_url('js/vendor/iconfonts/mdi/css/materialdesignicons.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('js/vendor/css/vendor.bundle.base.css');?>">
  <link rel="stylesheet" href="<?=base_url('js/vendor/css/vendor.bundle.addons.css');?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?=base_url('css/style.css');?>">
  <!-- endinject -->

</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <div class=""><?=$error?></div>
              <form method="post" action="<?=site_url("login/checkuser");?>">
                <div class="form-group">
                    <label class="label">名稱</label>
                  <div class="input-group">
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">密碼</label>
                  <div class="input-group">
                    <input name="password" id="password" type="password" class="form-control" placeholder="*********">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary submit-btn btn-block">Login</button>
                </div>
                <div class="form-group d-flex justify-content-between">
                  <!--div class="form-check form-check-flat mt-0">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" checked> Keep me signed in
                    </label>
                  </div-->
                  <a href="#" class="text-small forgot-password text-black">忘記密碼</a>
                </div>
                <!--div class="text-block text-center my-3">
                  <span class="text-small font-weight-semibold">Not a member ?</span>
                  <a href="register.html" class="text-black text-small">Create new account</a>
                </div-->
              </form>
            </div>
            <p class="footer-text text-center">copyright © 2018 泰慶興業有限公司. All rights reserved.</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?=base_url('js/vendor/js/vendor.bundle.base.js');?>"></script>
  <script src="<?=base_url('js/vendor/js/vendor.bundle.addons.js');?>"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?=base_url('js/off-canvas.js');?>"></script>
  <script src="<?=base_url('js/misc.js');?>"></script>
  <!-- endinject -->
</body>

</html>