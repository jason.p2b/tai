<!-- Modal -->
<div class="modal fade" id="fsl-modal" tabindex="-1" role="dialog" aria-labelledby="fsl-modal-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text text-primary" id="fsl-modal-label"></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12" style="border:2px solid #000000;border-radius: 5px;">
            <svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 600 410" style="font-family:Arial;">
              <marker id="arrowStart" markerWidth="12" markerHeight="10" refX="2" refY="6" orient="auto">
                <path d="M2,2 L2,10 L12,6 L2,2" style="fill: #ff0000;"></path>
              </marker>
              <marker id="arrowEnd" markerWidth="12" markerHeight="10" refX="2" refY="6" orient="auto-start-reverse">
                s<path d="M2,2 L2,10 L12,6 L2,2" style="fill: #ff0000;"></path>
              </marker>
              <g class="frame-outline" transform="translate(150,150)">
                <circle class="1-hole" cx="25" cy="50" r="12" />
                <circle class="1-hole" cx="25" cy="50" r="7" />
                <circle class="2-hole" cx="70" cy="25" r="12" />
                <circle class="2-hole" cx="70" cy="25" r="7" />
                <circle class="2-hole" cx="70" cy="75" r="12" />
                <circle class="2-hole" cx="70" cy="75" r="7" />
                <rect x="100" y="0" height="100" width="200"/>
                <circle cx="250" cy="50" r="25" />
                <circle class="1-hole" cx="375" cy="50" r="12" />
                <circle class="1-hole" cx="375" cy="50" r="7" />
                <circle class="2-hole" cx="330" cy="25" r="12" />
                <circle class="2-hole" cx="330" cy="25" r="7" />
                <circle class="2-hole" cx="330" cy="75" r="12" />
                <circle class="2-hole" cx="330" cy="75" r="7" />

                <line class="labels" x1="250" x2="250" y1="50" y2="-50"/>
                <line class="labels" x1="300" x2="300" y1="-10" y2="-100"/>
                <line class="labels" x1="300" x2="250" y1="-25" y2="-25"/>
                <text class="svg_text" transform="translate(275,-30)" id="fsl_a">A</text>
                <line class="labels" x1="100" x2="100" y1="-10" y2="-100"/>
                <line class="arrows" x1="270" x2="130" y1="-90" y2="-90"/>
                <text class="svg_text" transform="translate(200,-100)" id="fsl_l">L</text>

                <line class="2-hole labels" x1="70" x2="-10" y1="25" y2="25"/>
                <line class="2-hole labels" x1="70" x2="-10" y1="75" y2="75"/>
                <line class="2-hole labels" x1="0" x2="0" y1="25" y2="75"/>
                <text class="2-hole svg_text" transform="translate(-5,50) rotate(-90)" id="fsl_b">B</text>

                <line class="labels" x1="95" x2="-100" y1="0" y2="0"/>
                <line class="labels" x1="95" x2="-100" y1="100" y2="100"/>
                <line class="arrows" x1="-90" x2="-90" y1="30" y2="70"/>
                <text class="svg_text" transform="translate(-100,50) rotate(-90)" id="fsl_w">W</text>

                <line class="labels" x1="300" x2="300" y1="105" y2="130"/>
                <line class="2-hole labels" x1="330" x2="330" y1="75" y2="130"/>
                <line class="2-hole labels" x1="300" x2="330" y1="120" y2="120"/>
                <text class="2-hole svg_text" transform="translate(320,170)" id="fsl_c">C</text>

                <line class="1-hole labels" x1="300" x2="300" y1="130" y2="210"/>
                <line class="1-hole labels" x1="375" x2="375" y1="50" y2="210"/>
                <line class="1-hole arrows" x1="330" x2="345" y1="200" y2="200"/>
                <text class="1-hole svg_text" transform="translate(337.5,250)" id="fsl_d">D</text>

                <text class="svg_text" transform="translate(360,0)" id="fsl_hole" style='font-size:1.5em;stroke-width:1;'>M5皿頭孔</text>
            </g>
            </svg>
          </div>
        </div>
        <div class="form-row form-group mt-3">
          <input type="hidden" id="fsl_id" value="" />
          <div class="col-sm-6">
            <label for="fsl-name" class="control-label">品名:</label>
            <input type="text" id="fsl-name" class="form-control" name="fsl_name" value="" placeholder="品名">
          </div>
          <div class="col-sm-6">
            <label for="fsl-type" class="control-label">陽極鎖類型:</label>
            <select id="fsl-type" class="form-control" name="fsl_type">
              <option value="1-hole">1 個螺絲孔</option>
              <option value="2-hole">2 個螺絲孔</option>
              <option value="3-hole" selected>3 個螺絲孔</option>
            </select>
          </div>
        </div>
        <div class="form-row form-group">
          <div class="col-sm-4">
            <label for="fsl-l" class="control-label">L:</label>
            <input type="number" id="fsl-l" class="form-control" name="fsl_l" value="" placeholder="L" onkeyup="fslChange(this.name, this.value);">
          </div>
          <div class="col-sm-4">
            <label for="fsl-w" class="control-label">W:</label>
            <input type="number" id="fsl-w" class="form-control" name="fsl_w" value="" placeholder="W" onkeyup="fslChange(this.name, this.value);">
          </div>
          <div class="col-sm-4">
            <label for="fsl-a" class="control-label">A:</label>
            <input type="number" id="fsl-a" class="form-control" name="fsl_a" value="" placeholder="A" onkeyup="fslChange(this.name, this.value);">
            
          </div>
          <div class="col-sm-4">
    
          </div>
        </div>
        <div class="form-row form-group">
          <div class="col-sm-4 2-hole">
            <label for="fsl-b" class="control-label">B:</label>
            <input type="number" id="fsl-b" class="form-control" name="fsl_b" value="" placeholder="B" onkeyup="fslChange(this.name, this.value);">
          </div>
          <div class="col-sm-4 2-hole">
            <label for="fsl-c" class="control-label">C:</label>
            <input type="text" id="fsl-c" class="form-control" name="fsl_c" value="" placeholder="C" onkeyup="fslChange(this.name, this.value);">
          </div>
          <div class="col-sm-4 1-hole">
            <label for="fsl-d" class="control-label">D:</label>
            <input type="text" id="fsl-d" class="form-control" name="fsl_d" value="" placeholder="D" onkeyup="fslChange(this.name, this.value);">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary submit">儲存</button>
      </div>
    </div>
  </div>
</div>
<script>
  function fslChange(id,val) {
    $('svg #'+id).html(val);
  }

  $('#fsl-type').trigger('change');
  $('#fsl-type').change(function () {
    $val = $(this).val();

    if ('1-hole' == $val) {
      $('.1-hole').show();
      $('.2-hole').hide();
    } else if ('2-hole' == $val) {
      $('.2-hole').show();
      $('.1-hole').hide();
    } else {
      $('.1-hole').show();
      $('.2-hole').show();
    }
  });

  $('#fsl-modal .submit').click(function () {
    $('#fsl-modal .close').trigger('click');
    var $val = $('#fsl_id').val();

    if ("" != $val)
    {
      var data =  { 'id':$val,
                    'name':$('#fsl-name').val(),
                    'type':$('#fsl-type').val(),
                    'fsl_l':$('#fsl-l').val(),
                    'fsl_w':$('#fsl-w').val(),
                    'fsl_a':$('#fsl-a').val(),
                    'fsl_b':$('#fsl-b').val(),
                    'fsl_c':$('#fsl-c').val(),
                    'fsl_d':$('#fsl-d').val()};
      $.post('<?=site_url('order/add_fsl');?>',data,function(result){
      },'json');
    }
    
  });
</script>