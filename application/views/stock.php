<style>
  .tooltip.in {opacity: 1;}
  .tooltip-inner {max-width: 310px;}
  .table td img {
    width:100%;
    height:100%;
    border-radius: 3px;
  }

</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="o-tab">
                    <li class="active"><a data-toggle="tab" href="#stock">庫存資料</a></li>
                    <li><a data-toggle="tab" href="#material">型號表</a></li>
                </ul>
                <div class="tab-content">
                    <div id="stock" class="tab-pane fade in active">
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-8">
                            <div class="input-group mt-3 mb-3">
                              <input type="text" id="searchAll" class="form-control" placeholder="總搜尋" aria-label="search all table" aria-describedby="search all table" onkeyup="allSearch(this);">
                              <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="pressSearch(this);">尋找</button>
                              </div>
                            </div>  
                          </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-6">
                                <h3>現有庫存</h3>
                                <table id="stocktake" class="table table-hover table-striped"></table>
                            </div>
                            <div class="col-md-6">
                                <h3 class="bold text text-danger">需要訂貨</h3>
                                <table id="understock" class="table table-hover table-striped"></table>
                            </div>
                        </div>
                        <h3>庫存進出明細</h3>
                        <div class="table-responsive">
                            <table id="stock-table" class="table table-hover table-striped"></table>
                        </div>
                    </div>
                    <div id="material" class="tab-pane">
                        <h3>型號表</h3>
                        <div class="table-responsive">
                            <table id="material-table" class="table table-hover table-striped"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? include "_stock_modal.php"; ?>
<? include "_material_modal.php"; ?>

<script>

function allSearch(el) {
  $('.footable-filtering-search').hide();
  $('.footable-filtering-search input').val(el.value);
}

function pressSearch(el) {
  $('.footable-filtering-search button:first-of-type').trigger('click');

  if ('<span class="fooicon fooicon-remove"></span>' == $(el).html()) {
    $('#searchAll').val("").focus();
    $(el).html('尋找');
    $('.footable-filtering-search').show();
  } else {
    $(el).html('<span class="fooicon fooicon-remove"></span>');
  }
}

function getTables()
{
    $.post('<?=site_url('stock/get_filter')?>',{}, function (results) {

        FooTable.MyFiltering = FooTable.Filtering.extend({
            construct: function(instance){
                this._super(instance);
                this.statuses = results;
                this.def = '全部';
                this.$status = null;
            },
            $create: function(){
                this._super();
                var self = this,
                    $form_grp = $('<div/>', {'class': 'form-group'})
                        .append($('<label/>', {'class': 'sr-only', text: 'Status'}))
                        .prependTo(self.$form);

                self.$status = $('<select/>', { 'class': 'form-control' })
                    .on('change', {self: self}, self._onStatusDropdownChanged)
                    .append($('<option/>', {text: self.def}))
                    .appendTo($form_grp);

                $.each(self.statuses, function(i, status){
                    self.$status.append($('<option/>').text(status));
                });
            },
            _onStatusDropdownChanged: function(e){
                var self = e.data.self,
                    selected = $(this).val();
                if (selected !== self.def){
                    self.addFilter('type', selected, ['type']);
                } else {
                    self.removeFilter('type');
                }
                self.filter();
            },
            draw: function(){
                this._super();
                var status = this.find('type');
                if (status instanceof FooTable.Filter){
                    this.$status.val(status.query.val());
                } else {
                    this.$status.val(this.def);
                }
            }
        });

        get_stocktake();
        get_understock();


    }, 'json');
}



function get_stocktake()
{   
    $.post('<?=site_url('stock/get_stocktake')?>',{}, function (result) {
        var $st = $('#stocktake');

        ft2 = FooTable.init('#stocktake', {
                "columns": result['col'],
                "rows": result['row'],
                sorting: {enabled: true},
                filtering: {enabled: true},
                paging: {
                    enabled: true,
                    size: "10"
                },
                components: {
                    filtering: FooTable.MyFiltering
                }
        });
        $('span.caret').hide();
    }, 'json');
}

function get_understock()
{
    $.post('<?=site_url('stock/get_understock')?>',{}, function (result) {
        var $st = $('#understock');
        ft2 = FooTable.init('#understock', {
                "columns": result['col'],
                "rows": result['row'],
                sorting: {enabled: true},
                filtering: {enabled: true},
                paging: {
                    enabled: true,
                    size: "10"
                },
                components: {
                    filtering: FooTable.MyFiltering
                }
        });
        $('span.caret').hide();
    }, 'json');
}

$(document).ready(function () {
    getTables();

    // get_stocktake();
    // get_understock();
});

function setTooltip() {
  console.log('here');
  $('[data-toggle="tooltip"]').tooltip({
    html: true
  });
}
</script>