<div class="row">
  <div class="col-12">
    <div class="card">
    <div class="card-body">
                <? if ("" != $message) : ?>
                  <div class="alert alert-success alert-dismissible">
                    <strong><?=$message;?></strong>
                  </div>
                <? endif; ?>
                  <h4 class="card-title">新增客戶</h4>
                  <p class="card-description">
                    請填入客戶資料
                  </p>
                  <!--form class="forms-sample">
                    <div class="form-group">
                    <label for="type">客戶屬性</label>
                    <select class="form-control" id="type">
                      <option value="3">泰慶</option>
                      <option value="4">英聖</option>
                      <option value="5">南亞</option>
                    </select>
                  </div-->
                  <form action="<?=site_url('customer/insert');?>" method="post">
                    <div class="form-group">
                      <label for="company">公司名稱</label>
                      <input type="text" class="form-control" id="company" name="company" placeholder="公司名稱">
                    </div>
                    <div class="form-group">
                      <label for="gui">統一編號</label>
                      <input type="text" class="form-control" id="gui" name="gui" placeholder="統一編號">
                    </div>
                    <div class="form-group">
                      <label for="address">地址</label>
                      <input type="text" class="form-control" id="address" name="address" placeholder="地址">
                    </div>
                    <div class="form-group">
                      <label for="phone">電話</label>
                      <input type="text" class="form-control" id="phone" name="phone" placeholder="電話">
                    </div>
                    <div class="form-group">
                      <label for="fax">傳真</label>
                      <input type="text" class="form-control" id="fax" name="fax" placeholder="傳真">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <label for="contact">聯絡人</label>
                      <input type="text" class="form-control" id="contact" name="contact" placeholder="聯絡人">
                    </div>
                    
                    <div class="form-group">
                      <label for="note">備註</label>
                      <textarea class="form-control" name="note" id="note" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">確認</button>
                    <button type="reset" class="btn btn-danger">取消</button>
                  </form>
                </div>
  </div>
  </div>
</div>

                