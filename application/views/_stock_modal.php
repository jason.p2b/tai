<style>
    .stock-in {color:green;}
    .stock-out {color:red;}
    .inventory {
        display:inline-block;
        background-color: #fd9310;
    }
    .inventory-amount {
        display:inline-block;
        background-color: #acacac;
    }
</style>
<!-- Begin Popup Modal -->
<div class="modal fade" id="stock-modal" role="dialog" aria-labelledby="stock-modal-title">

    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="stock-editor">
            <div class="modal-header">
                <h4 class="modal-title" id="stock-modal-title">新增庫存進出</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="modal-body">
                <input type="number" id="mid" name="mid" class="hidden" value=""/>
                <input type="number" id="stock_id" name="stock_id" class="hidden" value=""/>
        
                <div class="form-group">
                    <label class="col-sm-3 control-label">進出貨</label>
                    <div class="col-sm-4">
                        <input type="radio" name="in-out" id="stock-in" value="1" checked=""> 
                        <label class="form-check-label control-label" for="stock-in">進貨</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="radio" name="in-out" id="stock-out" value="0" checked=""> 
                        <label class="form-check-label control-label" for="stock-out">出貨/使用</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="date" class="col-sm-3 control-label">日期</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="date" name="date" placeholder="日期">
                    </div>
                </div>
    
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">品名</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="name" id="name" style="width:100%;"></select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9" id="material_photo" style="height:150px;">
                        
                    </div>
                </div>

                <div class="form-group required">
                    <label for="amount" class="col-sm-3 control-label">數量</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <input type="text" class="form-control" id="amount" name="amount" placeholder="數量" required>
                            <div class="input-group-text">剩餘數量:&nbsp;<span id="amount-left">0</span></div>
                        </div>
                    </div>
                </div>
        
                <div class="form-group required">
                    <label for="price" class="col-sm-3 control-label">單價</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <div class="input-group-text">$</div>
                            <input type="text" class="form-control" id="price" name="price" placeholder="單價" required>
                        </div>
                        
                    </div>
                </div>

                <div class="form-group required">
                    <label for="supplier" class="col-sm-3 control-label">供應商</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="supplier" name="supplier" placeholder="供應商" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="note" class="col-sm-3 control-label">備註</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="note" name="note" placeholder="備註">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">確認</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </form>
    </div>
</div>
<!-- End Popup Modal -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    function material_select()
    {
        $.post('<?=site_url("stock/get_material_select")?>', {}, function (data) {
            var $mat = $('#stock-modal').find("#name");
            $mat.html("");
            // var wait1 = new Promise((a,b) => {
            //     $.each(data, function (key, arr) {
            //         $mat.append("<optgroup label='" + key + "'>");
            //         var waiting = new Promise((resolve, reject) => {

            //             $.each(arr, function (k,v) {
            //                 $mat.append("\t<option value='" + k + "'>" + v + "</option>");
            //                 console.log('hi');
            //             });
            //         });
            //         // waiting.then(() => {
            //             console.log('DONE!!!!!!');
            //             $mat.append("</optgroup>");
            //         // });
                    
            //     });
            // });
            
            for (const optKey in data) {
                $mat.append("<optgroup label='" + optKey + "'>");
                for ( const key in data[optKey]) {
                    $mat.append("<option value='" + key + "'>" + data[optKey][key] + " (" + optKey + ")"+ "</option>");
                }
                $mat.append("</optgroup>");
            }

            $('#stock-modal #name').select2();
        },'json');
    }

    var $modal_s  = $('#stock-modal');
    var $editor_s = $('#stock-editor');
    var $title_s  = $('#stock-modal-title');

    $editor_s.on('submit', function(e){
        if (this.checkValidity && !this.checkValidity()) return;
        e.preventDefault();

        var row = $modal_s.data('row'),
        values = {
            mid: $editor_s.find('#name').val(),
            isIn: $editor_s.find('input[name="in-out"]:checked').val(),
            date: $editor_s.find('#date').val(),
            name: $editor_s.find('#name option:selected').text(),
            price: $editor_s.find('#price').val(),
            supplier: $editor_s.find('#supplier').val(),
            note: $editor_s.find('#note').val(),
            amount: $editor_s.find('#amount').val()
        };

        if (row instanceof FooTable.Row){
            values['stock_id'] = $editor_s.find('#stock_id').val();
            $.post("<?=site_url('stock/update_stock')?>", values, function (result) {
                if (values['isIn'] == "1") {
                    values['amount'] = "<span class='stock-in'>" + values['amount'] + "</span>";
                } else {
                    values['amount'] = "<span class='stock-out'>-" + values['amount'] + "</span>";
                }
                row.val(values);
            },'json');
        } else {
            $.post("<?=site_url('stock/add_stock')?>", values, function (result) {
                values['stock_id'] = result['id'];
                values['unit'] = result['unit'];
                values['total'] = values['price'] * $editor_s.find('#amount').val();
                if (values['isIn'] == "1") {
                    values['amount'] = "<span class='stock-in'>" + values['amount'] + "</span>";
                } else {
                    values['amount'] = "<span class='stock-out'>-" + values['amount'] + "</span>";
                }
                ft1.rows.add(values);
            },'json');
        }
        getTables();
        $modal_s.modal('hide');
    });

    $.post('<?=site_url('stock/get_stock')?>', {}, function (result) {
        ft1 = FooTable.init('#stock-table', {
                "columns": result['col'],
                "rows": result['row'],
                editing: {
                    enabled: true,
                    addRow: function(){
                        $modal_s.removeData('row');
                        $editor_s[0].reset();
                        $editor_s.find('#date').val(moment().format('YYYY-MM-DD'));
                        $title_s.text('新增訂單');
                        $modal_s.modal('show');
                    },
                    editRow: function(row){
                        var values = row.val();
                        var amount = values.amount;

                        $editor_s.find('#mid').val(values.mid);
                        $editor_s.find('#stock_id').val(values.stock_id);
                        $editor_s.find('#date').val(values.date);
                        $editor_s.find('#name').val(values.mid);
                        $editor_s.find('#price').val(values.price);
                        if (values.isIn == "1") {
                            $('#stock-in').trigger('click');
                            amount = amount.replace('<span class=\'stock-in\'>', '');
                            amount = amount.replace('</span>','');
                        } else {
                            $('#stock-out').trigger('click');
                            amount = amount.replace('<span class=\'stock-out\'>-', '');
                            amount = amount.replace('</span>','');
                        }
                        $editor_s.find('#amount').val(amount);
                        $editor_s.find('#supplier').val(values.supplier);
                        $editor_s.find('#note').val(values.note);

                        $modal_s.data('row', row);
                        $title_s.text('修改訂單:');
                        jQuery.noConflict(); 
                        $modal_s.modal('show');
                    },
                    'alwaysShow': true,
                    'allowDelete': false,
                    'addText': '新增訂單'
                },
                sorting: {enabled: true},
                filtering: {enabled: true},
                paging: {
                    enabled: true,
                    size: "10"
                }
                
        });
        $('span.caret').hide();
        material_select();
    },'json');

    $editor_s.find('#name').change(function() {
        $('#amount-left').html("0");
        $.post('<?=site_url('stock/stock_amount');?>', {id: $(this).val()}, function (result) {
            $('#amount-left').html(result['amount']);
            $('#price').val(result['price']);
            $('#material_photo').html("<img style='display:center;max-width:200px;' src='" + result['file'] + "'>");
        },'json');
    });
</script>