<div class="row m-3">
  <div class="col-md-3 col-sm-6">
      <div class="checkbox">
        <label>
          <input id="d-0" type="checkbox" data-toggle="toggle" data-size="small" checked onchange="changeHighlight('d-0');">
          <span id="dd-0" class="p-1 bg-success text-light">今天修改</span>
        </label>
      </div>
  </div>
  <div class="col-md-3 col-sm-6">
      <div class="checkbox">
        <label>
          <input id="d-2" type="checkbox" data-toggle="toggle" data-size="small" checked onchange="changeHighlight('d-2');">
          <span id="dd-2" class="bg-warning text-light">昨天/前天修改</span>
        </label>
      </div>
  </div>
  <div class="col-md-3 col-sm-6">
      <div class="checkbox">
        <label>
          <input id="d-7" type="checkbox" data-toggle="toggle" data-size="small" checked onchange="changeHighlight('d-7');">
          <span id="dd-7" class="bg-info text-light">7天內修改</span>
        </label>
      </div>
  </div>
  <div class="col-md-3 col-sm-6">
      <div class="checkbox">
        <label>
          <input id="d-30" type="checkbox" data-toggle="toggle" data-size="small" onchange="changeHighlight('d-30');">
          <span id="dd-30" class="bg-danger">30天內修改</span>
        </label>
      </div>
  </div>
</div>
<div class="row">
  <div class="col-12">
      <div class="panel-group">
          <div class="panel panel-primary">
            <div class="panel-heading">框</div>
            <div class="panel-body table-responsive"><table class="table table-bordered table-striped table-hover" id="table-progress-frame"></table></div>
          </div>
      </div>
  </div>
</div>
<div class="row">
  <div class="col-12">
      <div class="panel-group">
          <div class="panel panel-success">
            <div class="panel-heading">扇</div>
            <div class="panel-body table-responsive"><table class="table table-bordered table-striped table-hover" id="table-progress-door"></table></div>
          </div>
      </div>
  </div>
</div>