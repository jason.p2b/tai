<!DOCTYPE html>

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?=$title?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?= base_url('js/vendor/iconfonts/mdi/css/materialdesignicons.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('js/vendor/css/vendor.bundle.base.css') ?>">
  <link rel="stylesheet" href="<?= base_url('js/vendor/css/vendor.bundle.addons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('css/bootstrap.css') ?>">
  <link rel="stylesheet" href="<?= base_url('css/bootstrap-theme.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('css/font-awesome.min.css') ?>">

  <!-- endinject -- >

  <!-- plugins:js -->
  <script src="<?= base_url('js/vendor/js/vendor.bundle.base.js');?>"></script>
  <script src="<?= base_url('js/vendor/js/vendor.bundle.addons.js');?>"></script>
  <script src="<?= base_url('js/vendor/jquery-1.10.2.min.js');?>"></script>
  <script src="<?= base_url('js/bootstrap.min.js');?>"></script>
  <!-- endinject -->
 
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <? for($i=0; $i<count($css_file); $i++) : ?>
      <link rel="stylesheet" href="<?= base_url("css/$css_file[$i]") ?>">
  <? endfor;?>
  <!-- endinject -->
  
</head>
<style>
  .bigWidth {width: 100%;}
  #toggle-sidebar{
    display:block;
    background-color: #fff;
    width:30px;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    border-left:0px;
    z-index: 10;
    position: absolute;
    padding: 5px 0px;
    margin-top: 10px;
    cursor: pointer;
  }

</style>
<body>
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    
    <? $this->load->view('_navbar'); ?> 

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_sidebar.html -->
      
      <? $this->load->view('_sidebar'); ?>

      <!-- partial -->
      <div class="main-panel">
        <div class="row">
          <div class="col-sm-1" id="toggle-sidebar"><i class="glyphicon glyphicon-resize-horizontal"></i></div>
        </div>
        
        <div class="content-wrapper">
          <? $this->load->view($content);?>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <? $this->load->view('_footer'); ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  


  <!-- inject:js -->
  <script src="<?=base_url('js/off-canvas.js');?>"></script>
  <script src="<?=base_url('js/misc.js');?>"></script>
  <script>
    $('#toggle-sidebar').click(function () {
      $('.main-panel').toggleClass('bigWidth');
      $('.sidebar').slideToggle("slow");
    });
  </script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <? for($i=0; $i<count($js_file); $i++) : ?>
      <script src="<?= base_url("js/$js_file[$i]") ?>"></script>
  <? endfor;?>
  <!-- End plugin js for this page-->
</body>

</html>
