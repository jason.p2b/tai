<fieldset>
    <p>
        <label>顯示</label>
        <ul class="criteria">
            <li><input checked type="checkbox" class="check_show" id="frame_show" name="frame_show" value="frame"><label for="frame_show">門框</label></li>
            <li><input checked type="checkbox" class="check_show" id="f5" name="f5" value="f_width"><label for="f5">門框寬</label></li>
            <li><input checked type="checkbox" class="check_show" id="f6" name="f6" value="f_length"><label for="f6">門框長</label></li>
            <li><input checked type="checkbox" class="check_show" id="f7" name="f7" value="7"><label for="f7">門框材質</label></li>
            <li><input checked type="checkbox" class="check_show" id="f8" name="f8" value="8"><label for="f8">門框種類</label></li>
            <li><input checked type="checkbox" class="check_show" id="f9" name="f9" value="9"><label for="f9">門框形狀</label></li>
        </ul>
        <ul class="criteria">
            <li><input checked type="checkbox" class="check_show" id="door_show" name="door_show" value="door"><label for="door_show">門扇</label></li>
            <li><input checked type="checkbox" class="check_show" id="f12" name="f12" value="d_width"><label for="f12">門扇寬</label></li>
            <li><input checked type="checkbox" class="check_show" id="f13" name="f13" value="d_length"><label for="f13">門扇長</label></li>
            <li><input checked type="checkbox" class="check_show" id="f14" name="f14" value="14"><label for="f14">門扇厚度</label></li>
            <li><input checked type="checkbox" class="check_show" id="f15" name="f15" value="15"><label for="f15">門扇數量</label></li>
            <li><input checked type="checkbox" class="check_show" id="f16" name="f16" value="16"><label for="f16">門扇材質</label></li>
            <li><input checked type="checkbox" class="check_show" id="f17" name="f17" value="glass"><label for="f17">玻璃</label></li>
        </ul>
        <ul class="criteria">
            <li><input checked type="checkbox" class="check_show" id="hardware" name="hardware" value="hardware"><label for="hardware">所有五金</label></li>
            <li><input checked type="checkbox" class="check_show" id="f19" name="f19" value="19"><label for="f19">門鎖</label></li>
            <li><input checked type="checkbox" class="check_show" id="f20" name="f20" value="20"><label for="f20">鉸鏈</label></li>
            <li><input checked type="checkbox" class="check_show" id="f21" name="f21" value="21"><label for="f21">把手</label></li>
            <li><input checked type="checkbox" class="check_show" id="f22" name="f22" value="22"><label for="f22">門弓器</label></li>
            <li><input checked type="checkbox" class="check_show" id="f23" name="f23" value="23"><label for="f23">天地栓</label></li>
            <li><input checked type="checkbox" class="check_show" id="f24" name="f24" value="24"><label for="f24">五金</label></li>
        </ul>
    </p>
    <p>
        <label for="search">搜尋</label>
        <select name="search" id="search">
            <option value=""> --- </option>
            <option value="1">防火報告</option>
            <option value="2">同型式</option>
            <option value="f_width">門框寬</option>
            <option value="f_length">門框長</option>
            <option value="7">門框材質</option>
            <option value="8">門框種類</option>
            <option value="shape">門框形狀</option>
            <option value="d_width">門扇寬</option>
            <option value="d_length">門扇長</option>
            <option value="14">門扇厚度</option>
            <option value="15">門扇數量</option>
            <option value="16">門扇材質</option>
            <option value="17">玻璃</option>
            <option value="18">玻璃尺寸</option>
            <option value="19">門鎖</option>
            <option value="20">鉸鏈</option>
            <option value="21">把手</option>
            <option value="22">門弓器</option>
            <option value="23">天地栓</option>
            <option value="24">五金</option>
        </select>
        <select name="shape_search" id="shape_search">
            <option value=""> --- </option>
            <option value="l_frame">Ｌ型框</option>
            <option value="la_frame">Ｌ型氣密框</option>
            <option value="t_frame">凸型框</option>
            <option value="ta_frame">凸型氣密框</option>
        </select>
        <input type="text" id="t_search" name="t_search" value="">
    </p>
    <table id="report_table">
        <thead>
            <th>防火報告</th>
            <th>同型式</th>
            <th data-hide="phone,tablet">門框寬(小)</th>
            <th data-hide="phone,tablet">門框寬(大)</th>
            <th data-hide="phone,tablet">門框長(小)</th>
            <th data-hide="phone,tablet">門框長(大)</th>
            <th data-hide="phone,tablet"class="hide">門框材質</th>
            <th data-hide="phone,tablet"class="hide">門框種類</th>
            <th data-hide="phone,tablet" class="hide">門框形狀</th>
            <th data-hide="phone,tablet">門扇寬(小)</th>
            <th data-hide="phone,tablet">門扇寬(大)</th>
            <th data-hide="phone,tablet">門扇長(小)</th>
            <th data-hide="phone,tablet">門扇長(大)</th>
            <th data-hide="phone,tablet" class="hide">門扇厚度</th>
            <th data-hide="phone,tablet" class="hide">門扇數量</th>
            <th data-hide="phone,tablet" class="hide">門扇材質</th>
            <th data-hide="phone,tablet" class="hide">玻璃</th>
            <th data-hide="phone,tablet" class="hide">玻璃尺寸</th>
            <th data-hide="phone">門鎖</th>
            <th data-hide="phone">鉸鏈</th>
            <th data-hide="phone,tablet">把手</th>
            <th data-hide="phone,tablet">門弓器</th>
            <th data-hide="phone,tablet">天地栓</th>
            <th data-hide="phone">五金</th>
        </thead>
        <tbody>
            <? foreach ($data as $key => $value) : ?>
            <tr>
                <td><?=$value['name']?></td>
                <td><?=$value['main_name']?></td>
                <td><?=$value['min_frame_width']?></td>
                <td><?=$value['max_frame_width']?></td>
                <td><?=$value['min_frame_length']?></td>
                <td><?=$value['max_frame_length']?></td>
                <td class="hide"><?=implode('<br>', $value['frame_material'])?></td>
                <td class="hide"><?=($value['frame_type'] == "normal") ? '普通框' : '四面框'; ?></td>
                <td class="hide">
                    <div class="svg_box">
                        <? foreach ($value['frame_shape'] as $index => $frame) : ?>
                            <? include "./svg/" . $frame . ".php"; ?>
                        <? endforeach; ?>
                    </div>
                </td>
                <td><?=$value['min_door_width']?></td>
                <td><?=$value['max_door_width']?></td>
                <td><?=$value['min_door_length']?></td>
                <td><?=$value['max_door_length']?></td>
                <td class="hide"><?=$value['door_thickness']?></td>
                <td class="hide"><?=($value['single_door']) ? '單開' : '雙開' ; ?></td>
                <td class="hide"><?=implode('<br>', $value['door_material'])?></td>
                <td class="hide"><?=$value['glass']?></td>
                <td class="hide"><?=$value['glass_dimension']?></td>
                <td><?=implode('<br>', $value['lock'])?></td>
                <td><?=implode('<br>', $value['hinge'])?></td>
                <td><?=implode('<br>', $value['handle'])?></td>
                <td><?=implode('<br>', $value['closer'])?></td>
                <td><?=implode('<br>', $value['bolt'])?></td>
                <td><?=implode('<br>', $value['hardware'])?></td>
            </tr>
            <? endforeach; ?>
        </tbody>
    </table>
</fieldset>