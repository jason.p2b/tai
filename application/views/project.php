<? if ($editable) : ?>
<style>
  .modal-dialog {top: 200px;}
</style>
<!-- Begin Popup Modal -->
<div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
  <style scoped>
    /* provides a red astrix to denote required fields - this should be included in common stylesheet */
    .form-group.required .control-label:after {
      content:"*";
      color:red;
      margin-left: 4px;
    }
  </style>

  <div class="modal-dialog" role="document">
    <form class="modal-content form-horizontal" id="editor">
      <div class="modal-header">
        <h4 class="modal-title" id="editor-title">新增客戶</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        
      </div>
      <div class="modal-body">
        <input type="number" id="id" name="id" class="hidden" value=""/>
        <div class="form-group required">
          <label for="order_type" class="col-sm-3 control-label">訂單類型</label>
          <div class="col-sm-9">
            <select name="order_type" class="form-control" id="order_type" required>
                <option value="一般">一般</option>
                <option value="南亞">南亞</option>
            </select>
          </div>
        </div>
        <div class="form-group required">
          <label for="company" class="col-sm-3 control-label">公司名稱</label>
          <div class="col-sm-9">
            <select name="company" class="form-control" id="company" required>
                    <option value=""> --- </option>
                <?foreach ($options as $key => $value):?>
                    <optgroup label="<?=$key?>">
                      <?foreach ($value as $k => $v):?>
                      <option value="<?=$k?>"><?=$v?></option>
                      <?endforeach;?>
                    </optgroup>
                <?endforeach;?>
            </select>
          </div>
        </div>
        <div class="form-group required">
          <label for="site" class="col-sm-3 control-label">建案名稱</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="site" name="site" placeholder="建案名稱" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="address" class="col-sm-3 control-label">出貨地址</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="address" name="address" placeholder="出貨地址" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="site_contact" class="col-sm-3 control-label">工地聯絡人</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="site_contact" name="site_contact" placeholder="工地聯絡人" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="site_phone" class="col-sm-3 control-label">工地電話</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="site_phone" name="site_phone" placeholder="工地電話" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="company_contact" class="col-sm-3 control-label">客戶聯絡人</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="company_contact" name="company_contact" placeholder="客戶聯絡人" required>
          </div>
        </div>
        <div class="form-group required">
          <label for="company_phone" class="col-sm-3 control-label">客戶電話</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="company_phone" name="company_phone" placeholder="客戶電話" required>
          </div>
        </div>
        <div class="form-group">
          <label for="note" class="col-sm-3 control-label">備註</label>
          <div class="col-sm-9">
            <textarea class="form-control" id="note" name="note" row="3" placeholder="備註"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">確認</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </form>
  </div>
</div>
<!-- End Popup Modal -->
<? endif; ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">檢視建案</div>
                <table id="project-table" class="table table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                            <th data-name="checker" data-sorted="false" data-filtered="false"></th>
                            <th data-name="order_type" data-breakpoints="xs sm md">訂單類型</th>
                            <th data-name="date" data-sorted="true" data-direction="DESC">成立日期</th>
                            <th data-name="company">建案客戶</th>
                            <th data-name="site">建案名稱</th>
                            <th data-name="address" data-breakpoints="xs sm md" data-title="出貨地址">出貨地址</th>
                            <th data-name="site_contact" data-breakpoints="xs sm" data-title="工地聯絡人">工地聯絡人</th>
                            <th data-name="site_phone" data-breakpoints="xs sm" data-title="工地電話">工地電話</th>
                            <th data-name="company_contact" data-breakpoints="xs sm md" data-title="客戶聯絡人">客戶聯絡人</th>
                            <th data-name="company_phone" data-breakpoints="xs sm md" data-title="客戶電話">客戶電話</th>
                            <th data-name="note" data-breakpoints="xs sm md" data-title="備註">備註</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?foreach ($project as $row):?>
                          <tr id="<?=$row->project_id?>">
                            <td>
                              <div class="btn-group btn-group-xs margin-right-2" data-toggle="tooltip" data-placement="top" title="<?=$row->site?>" role="group">
                                <button type="button" class="btn btn-default footable-edit">
                                  <span class="fooicon fooicon-pencil" aria-hidden="true"></span>
                                </button>
                              </div>
                              <div class="btn-group btn-group-xs margin-right-2" data-toggle="tooltip" data-placement="top" title="<?=$row->site?> - 門框設定" role="group">
                                <button onclick="frameInfo(<?=$row->project_id?>);" type="button" class="btn btn-default"  data-toggle="modal" data-target="#fs-modal">
                                  <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </button>
                              </div>
                              <div class="btn-group btn-group-xs" role="group" data-toggle="tooltip" data-placement="top" title="<?=$row->site?> - 陽極鎖規格">
                                <button onclick='fslInfo(<?=$row->project_id?>, "<?=$row->site?>");' type="button" class="btn btn-default" data-toggle="modal" data-target="#fsl-modal">
                                  <span class="glyphicon glyphicon-modal-window" aria-hidden="true"></span>
                                </button>
                              </div>
                            </td>
                            <td><?=$row->order_type?></td>
                            <td><?=$row->date?></td>
                            <td><?=$row->company?></td>
                            <td><?=$row->site?></td>
                            <td><?=$row->address?></td>
                            <td><?=$row->site_contact?></td>
                            <td><?=$row->site_phone?></td>
                            <td><?=$row->company_contact?></td>
                            <td><?=$row->company_phone?></td>
                            <td><?=$row->note?></td>
                          </tr>
                        <?endforeach?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<? include "_fsl_modal.php";?>
<? include "_frame_setting_modal.php"; ?>

<script>
  var curday = function(sp){
    today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //As January is 0.
    var yyyy = today.getFullYear();

    if(dd<10) dd='0'+dd;
    if(mm<10) mm='0'+mm;
    return (yyyy+sp+mm+sp+dd);
  };

jQuery(function($){
    var $modal = $('#editor-modal'),
      $editor = $('#editor'),
      $editorTitle = $('#editor-title'),
      ft = FooTable.init('#project-table', {
        paging: {enabled: true},
        sorting: {enabled: true},
        filtering: {enabled: true}<?if($editable):?>,
        editing: {
          enabled: true,
          addRow: function(){
            $modal.removeData('row');
            $editor.find('#order_type').removeAttr('disabled');
            $editor.find('#company option').removeAttr('selected');
            $editor.find('#company').removeAttr('disabled');
            $editor.find('#site').removeAttr('disabled');
            $editor[0].reset();
            $editorTitle.text('新增建案');
            $modal.modal('show');
          },
          editRow: function(row){
            var values = row.val();
            $editor.find('#id').val(row.$el[0].id);
            $('#editor #company option').each(function () {
                if (values.company == $(this).text()) {
                    $(this).attr('selected','selected');
                } else {
                    $(this).removeAttr('selected');
                }
            })
            $editor.find('#order_type').val(values.order_type).attr('disabled','true');
            $editor.find('#company').attr('disabled','true');
            $editor.find('#site').val(values.site).attr('disabled','true');
            $editor.find('#address').val(values.address);
            $editor.find('#site_contact').val(values.site_contact);
            $editor.find('#site_phone').val(values.site_phone);
            $editor.find('#company_contact').val(values.company_contact);
            $editor.find('#company_phone').val(values.company_phone);
            $editor.find('#note').val(values.note)

            $modal.data('row', row);
            $editorTitle.text('修改建案:' + values.company + " - " + values.site);
            jQuery.noConflict(); 
            $modal.modal('show');
          },
          'alwaysShow': true,
          'allowDelete': false,
          'addText': '新增建案'
        }

        <?endif;?>
      });

    $('.footable-editing').css('text-align','left');
    $('#project-table tbody td:last-of-type').remove();
    $('span.caret').hide();

    <?if($editable):?>
    $editor.on('submit', function(e){
      if (this.checkValidity && !this.checkValidity()) return;
      e.preventDefault();
      var row = $modal.data('row'),
        values = {
          id: $editor.find('#id').val(),
          company: $editor.find('#company').val(),
          site: $editor.find('#site').val(),
          address: $editor.find('#address').val(),
          site_contact: $editor.find('#site_contact').val(),
          site_phone: $editor.find('#site_phone').val(),
          company_contact: $editor.find('#company_contact').val(),
          company_phone: $editor.find('#company_phone').val(),
          note: $editor.find('#note').val(),
          order_type: $editor.find('#order_type').val()
        };

      if (row instanceof FooTable.Row){
        delete values['company'];
        $.post("<?=site_url('project/update')?>", values, function (result) {
          row.val(values);
        });
      } else {
        $.post("<?=site_url('project/add')?>", values, function (result) {
          values['date'] = curday('-');
          values['company'] = $editor.find('#company option:selected').text();

          ft.rows.add(values);
          $('#project-table>tbody>tr').each(function () {
            if ($(this).attr('id') == undefined) {
              $(this).attr('id', result);
            }
          });
        });
      }
      $modal.modal('hide');
    });<?endif;?>
  });
  
function fslInfo(id, site) {
    $('#fsl_id').val(id);
    $('#fsl-modal-label').html(site + " - 陽極鎖規格");
    $.post('<?=site_url("order/get_fsl")?>', {id:id}, function (data) {
        if (data === undefined || data.length == 0) {
            $('#fsl-name').val("");
            $('#fsl-type').val('3-hole').trigger('change');
            $('#fsl-l,#fsl-w,#fsl-a,#fsl-b,#fsl-c,#fsl-d').val("");
            $('#fsl_l').html('L');
            $('#fsl_w').html('W');
            $('#fsl_a').html('A');
            $('#fsl_b').html('B');
            $('#fsl_c').html('C');
            $('#fsl_d').html('D');
        } else {
            $('#fsl-name').val(data['name']);
            $('#fsl-type').val(data['type']).trigger('change');

            $('#fsl-l').val(data['fsl_l']).trigger('keyup');
            $('#fsl-w').val(data['fsl_w']).trigger('keyup');
            $('#fsl-a').val(data['fsl_a']).trigger('keyup');
            $('#fsl-b').val(data['fsl_b']).trigger('keyup');
            $('#fsl-c').val(data['fsl_c']).trigger('keyup');
            $('#fsl-d').val(data['fsl_d']).trigger('keyup');
        }
    },'json');
}

</script>