<!-- Issue Modal -->
<div id="issue_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" >
        <!-- Modal Content -->
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"></h3>
                類別：<span id="issueType"></span>
            </div>
            <div class="modal-body form-group">
                <div id="cl"></div>
                <div class="form">
                    <div class="form-group">
                        <label for="to">收件人：<span class="asterisk">*</span></label>
                        <select id="to" class="form-control input-lg">
                            <option value=""> --- </option>
                            <? foreach ($to as $key => $value) : ?>
                                <option value="<?=$key?>"><?=$value?></option>
                            <? endforeach;?>
                        </select>
                    </div>
                    <div class="panel panel-primary form-group">
                    <div data-toggle="collapse" class="panel-heading btn-primary" href="#collapse1">
                        <h4 class="panel-title">
                            <span class="glyphicon glyphicon-collapse-down"></span>
                            <label for="attachment">附件：</label>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <input id="attachment" name="file[]" type="file" class="file-loading" multiple>
                    </div>
                </div>        
                </div>
                
                <input type="hidden" id="issue_id" name="issue_id" value="">
                <textarea class="summernote" id="conversation" cols="30" rows="10"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="change" onclick="submitConversation();">修改</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
            </div>
        </div>
    </div>
</div>
