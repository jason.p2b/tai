<style>
 .table {
  width: 100%;
  border-collapse: collapse;
}

.myTable th,
.myTable td {
  border: 1px solid black;
}

.myTable th:first-child,
.myTable td:first-child {
  width: 300px;
  white-space: normal;
  position: sticky;
  left: 0;
  z-index: 1;
}

.myTable thead th {
  position: sticky:
  top: 63;
  z-index: 100;
}

/* .myTable th:last-child,
.myTable td:last-child {
  position: sticky;
  right: 0;
  z-index: 1;
} */

.myTable tr:nth-child(odd) td:first-child {
  background-color: #f2f8f9;
}

.myTable tr:nth-child(even) td:first-child {
  background-color: #ffffff;
}

.pagination {
  margin: 0px;
}

</style>

<div class="row" id="app">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        
        <Tab :tabs="mainTabs" start-tab='2'>
          <template #status-tab-slot>
            <? include "_dashboard_status.php"?>
          </template>
          <template #not-shipped-old-tab-slot>
            <? include "_dashboard_not_shipped.php" ?>
          </template>
          <template #not-shipped-tab-slot>
            <Tab :tabs="tabs" start-tab='1' tab-style="nav-pills">
              <template #manual-tab-slot>
                <div class="card">
                  <div class="card-body">
                    <h3>工地 欄位</h3>
                    <ul>
                      <li class="text-success">綠色 = 訂單完成 / 等出貨</li>
                      <li class="text-warning">橙色 = 收到訂單 / 開始繪圖</li>
                      <li class="text-danger">紅色 = 等待客戶</li>
                      <li>黑色 = 訂單進入製作流程</li>
                    </ul>
                    <hr>
                    <h3>客需日 欄位</h3>
                    <ul>
                      <li class="text-danger">紅色 = 交期日已過</li>
                      <li class="text-warning">橙色 = 交期7天(含)內到期</li>
                      <li class="text-success">綠色 = 交期8天以上</li>
                    </ul>
                    <hr>
                    <h3>進度相關 欄位</h3>
                    <ul>
                      <li class="text-success mb-2"><span class="bg-success text-dark rounded p-1">綠色 = 今天</span></li>
                      <li class="text-warning mb-2"><span class="bg-warning text-dark rounded p-1">橙色 = 昨天 / 前天</span></li>
                      <li class="text-secondary mb-1"><span class="bg-secondary text-dark rounded">灰色 = 7天內(含)</span></li>
                      <li class="text-info mb-1">紫色 = 此進度完成</li>
                      <li>把滑鼠停留在進度上會顯示進度修改日期</li>
                    </ul>
                    <hr>
                    <h3>匯出 / 搜尋功能</h3>
                    <ul>
                      <li>搜尋: 只顯示 含或不含(勾選) 關鍵字的行列</li>
                      <li>匯出: 可匯出 CSV 或 Excel 檔案. 若有搜尋, 則只匯出相關行列</li>
                    </ul>
                  </div>
                </div>
              </template>

              <template #frame-tab-slot>
                <Mytable 
                  id="frameT" 
                  :columns="frameHeader" 
                  :data="frameData" 
                  :formatter="frameFunction">
                </Mytable>
              </template>

              <template #smc-tab-slot>
                <Mytable  
                  id="smcT" 
                  :columns="smcHeader" 
                  :data="smcData" 
                  :formatter="smcFunction">
                </Mytable>
              </template>

              <template #door-tab-slot>
                <Mytable 
                  id="doorT" 
                  :columns="doorHeader" 
                  :data="doorData" 
                  :formatter="doorFunction">
                </Mytable>
              </template>

              <template #main-door-tab-slot>
                <Mytable 
                  id="mainDoorT" 
                  :columns="mainDoorHeader" 
                  :data="mainDoorData" 
                  :formatter="mainDoorFunction">
                </Mytable>
              </template>
            </Tab>

          </template>
        </Tab>

      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.18.5/xlsx.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/3.4.21/vue.global.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.6.8/axios.min.js"></script>
<script type="module">
    import App from "./js/components/App.js";  
    Vue.createApp(App).mount('#app');
</script>


<script>

function changeHighlight(id) {
    var $checkbox = $('#'+id);
    var cls = $('#d'+id).attr('class');
    var $hl = $('.'+id);

    if ($checkbox.prop('checked')) {
        $hl.removeClass('text-muted').addClass(cls);
    } else {
        $hl.addClass('text-muted').removeClass(cls);
    }
}


function getTable(table, progress) {
    $.post('<?=base_url('dashboard/get_progress')?>',{p:progress},function(data) {
        ft = FooTable.init('#'+table,{
            "columns": data['cols'],
            "rows": data['rows'],
            sorting: { enabled: true },
            paging: { enabled: true}
        });
    },'json');
}

function getBoard(table, type) {
    $.post('<?=base_url('dashboard/get_progress_board')?>',{t:type},function(data) {
        ft = FooTable.init('#'+table,{
            "columns": data['cols'],
            "rows": data['rows'],
            sorting: { enabled: true }
        });
        
        let today = new Date();
        $('#'+table+' tbody tr td:nth-child(4)').each(function () {
            let dueDate = new Date($(this).text());
            
            if (today > dueDate || $(this).text() == "0000-00-00") {
                $(this).addClass("bg-danger");
            }
        });
        
        $('#'+table+' thead tr th:nth-child(4)').trigger('click');
    },'json');
}

$(document).ready(function() {
    getTable('table-received','收到訂單');
    getTable('table-shipped','已出貨');
    getTable('table-wait','等排程');
    getTable('table-fold','發包中');
    getTable('table-production','生產中');
    getTable('table-paint','烤漆包裝');
    getTable('table-skin','等SKIN板');
    getTable('table-wait-ship','等出貨');
    getTable('table-response','等客戶回復');
    getTable('table-cancel','取消訂單');

    getBoard('table-progress-frame', 'frame');
    getBoard('table-progress-door', 'door');

});

</script>