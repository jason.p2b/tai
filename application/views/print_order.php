<div id="print_area" style="max-height: 842px;">
    <? foreach($data as $key => $value): ?>
        <table class='print_table' id='print_table_<?=$key?>'>
            <tr>
                <td rowspan='3'><?=$value['df']?></td>
                <th rowspan='2'>型號</th>
                <th colspan='2'>規格</th>
                <th rowspan='2'>內距</th>
                <th rowspan='2'>總數量</th>
                <th colspan='2'>腳鍊方向</th>
                <th rowspan='2'>門鎖</th>
                <th rowspan='2'>鉸鍊</th>
                <th rowspan='2'>其他五金</th>
            </tr>
            <tr>
                <th>W(寬)</th>
                <th>H(高)</th>
                <th>左(L)</th>
                <th>右(R)</th>
            </tr>
            <tr>
                <td><?=$value['model']?><br><?=$value['report_name']?></td>
                <td><?=$value['width']?></td>
                <td><?=$value['length']?></td>
                <td><?=$value['inner_length']?></td>
                <td><?=$value['sum']?></td>
                <td><?=$value['left_amount']?></td>
                <td><?=$value['right_amount']?></td>
                <td style='min-width:150px;'><?=$value['lock_name']?></td>
                <td style='min-width:150px;'><?=$value['hinge_name']?></td>
                <td style='min-width:200px;'><?=$value['hardware']?></td>
            </tr>
            <tr>
                <td colspan='4' class='svg1' style='vertical-align: top;'><?=$value['cut']?></td>
                <td colspan='4' class='svg2' style='vertical-align: top;'><?=$value['bp']?></td>
                <td colspan='3' style='vertical-align: top;'><?=$value['note']?></td>
            </tr>
        </table>
        <div style='page-break-after:always'></div>
        <script type="text/javascript">
            $svg    = $('table#print_table_<?=$key?> .svg1 svg');
            $svgTwo = $('table#print_table_<?=$key?> .svg2 svg');

            $svg.find('#v_total_length').html("<?=$value['v_total_length']?>");
            $svg.find('#v_north_length').html("<?=$value['v_north_length']?>");
            $svg.find('#v_west_length').html("<?=$value['v_west_length']?>");
            $svg.find('#v_south_length').html("<?=$value['v_south_length']?>");
            $svg.find('#v_east_length').html("<?=$value['v_east_length']?>");
            $svg.find('#v_south_east_length').html("<?=$value['v_south_east_length']?>");

            $svgTwo.find('text.frame_width').html("<?=$value['width']?>");
            $svgTwo.find('text.frame_length').html("<?=$value['length']?>");
            $svgTwo.find('text.inner_length').html("<?=$value['inner_length']?>");
            $svgTwo.find('text.beneath_length').html("<?=$value['beneath_length']?>");
            
            <?php if (isset($value['frame'])) { ?>
                $svgTwo.find('.double_door_frame_svg').hide();
                $svgTwo.find('.door_frame_svg .doorComponent').hide();
            
            <?php } else if ($value['door_amount'] != 'single') { ?>
                $svgTwo.find('.door_frame_svg').hide();
            <?php } else { ?>
                $svgTwo.find('.double_door_frame_svg').hide();
            <?php } ?>
        </script>
    <? endforeach; ?>
</div>

<script type="text/javascript">

    $("#print_area").printThis({
       debug: false,
       importCSS: true,
       printContainer: true,
       loadCSS: './css/print.css',
       pageTitle: "",
       removeInline: false
    });

    
</script>
