<style>
  @media print{@page {size: landscape;}}
  -webkit-transform: rotate(-90deg); -moz-transform:rotate(-90deg);
filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

  li[role=group] .select2-results__group {
    color: red;
    background-color: lightgray;
    font-weight: bold;
    font-size: 1.1em;
  }

</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">檢視訂單</div>
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="customer" class="col-sm-4 control-label"><h4>01. 請選擇客戶</h4></label>
                            <div class="col-sm-8">
                                <select name="customer" class="form-control" id="customer">
                                    <option value=""> --- </option>
                                    <?foreach ($customer as $key => $value):?>
                                        <optgroup label="<?=$key?>">
                                          <?foreach ($value as $k => $v):?>
                                          <option value="<?=$k?>"><?=$v?></option>
                                          <?endforeach;?>
                                        </optgroup>
                                    <?endforeach;?>
                                </select>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="site-order" class="col-sm-4 control-label"><h4>02. 請選擇建案</h4></label>
                            <div class="col-sm-8">
                                <select name="site-order" class="form-control" id="site-order"></select>
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div id="order-title" class="card-title"></div>
                <div id="order-match" ></div>
                <div class="table-responsive">
                    <table id="order-table" class="table table-hover table-striped"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="o-tab">
                    <li class="orderNote"><a data-toggle="tab" href="#orderNote">訂單備註</a></li>
                    <li class="active ny"><a data-toggle="tab" href="#original">原訂單</a></li>
                    <li class="ny"><a data-toggle="tab" href="#translate">翻譯訂單</a></li>
                    <li class="trad"><a data-toggle="tab" href="#traditional">訂單</a></li>
                    <li class="history"><a data-toggle="tab" href="#history">歷史紀錄</a></li>
                    <li class="qc"><a data-toggle="tab" href="#qc">品管表</a></li>
                    <li class="upload_order"><a data-toggle="tab" href="#upload_order">上傳訂單</a></li>
                    <?if($editable):?><li><a id="export_me">匯出 <i class="glyphicon glyphicon-export"></i></a></li><?endif;?>
                </ul>
                <div class="tab-content">
                    <div id="orderNote" class="tab-pane fade">
                        <div class="order-item-title card-title"></div>
                        <div class="row">
                            <div class="col-md-9">
                                <textarea class="form-control mt-2" name="orderNote-note" id="orderNote-note" placeholder="這裡可以寫些備註.. (要超過4個字)"></textarea>
                                <button type="button" class="btn btn-primary mt-2" onclick="saveOrderNote();">確定</button>
                            </div>
                        </div>
                        <div id="item-orderNote">
                        </div>
                    </div>
                    <div id="original" class="tab-pane fade in active">
                        <div class="order-item-title card-title"></div>
                        <div class="row">
                            <div class="col-sm-2">
                                <select class="form-control" name="all-ny-option" id="all-ny-option">
                                    <option value="progress">修改進度</option>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <select id="all-ny-progress" type="select" class="form-control">
                                    
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-primary" id="all-ny-submit" type="button" onclick="allSubmit('ny');">確定</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive sticky-table sticky-ltr-cells">
                                    <table id="order-item-table-original" class="pvtTable table table-hover table-striped"></table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div id="translate" class="tab-pane fade">
                        <div class="order-item-title card-title"></div>
                        <div class="table-responsive sticky-table sticky-ltr-cells">
                            <table id="order-item-table-translate" class="pvtTable table table-hover table-striped"></table>
                        </div>
                    </div>
                    <div id="traditional" class="tab-pane fade">
                        <div class="order-item-title card-title"></div>
                        <div class="row">
                            <div class="col-sm-2">
                                <select class="form-control" name="all-trad-option" id="all-trad-option">
                                    <option value="progress">修改進度</option>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <select id="all-trad-progress" type="select" class="form-control">

                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-primary" id="all-trad-submit" type="button" onclick="allSubmit('trad');">確定</button>
                            </div>
                        </div>
                        <div class="table-responsive sticky-table sticky-ltr-cells">
                            <table id="order-item-table-traditional" class="pvtTable table table-hover table-striped"></table>
                        </div>
                    </div>
                    <div id="history" class="tab-pane fade">
                        <div class="order-item-title cart-title"></div>
                        <div class="row">
                            <div class="col-12">
                                <table id="progress-table"></table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div id="item-history"></div>
                            </div>
                        </div>
                    </div>
                    <div id="qc" class="tab-pane fade">
                        <? include "_qc.php"; ?>
                    </div>
                    <div id="upload_order" class="tab-pane fade">
                        <div class="order-item-title card-title"></div>
                        <form id="form1" action="<?=site_url('order/add_upload_order_item')?>" method="post" enctype="multipart/form-data">
                            
                        <div class="row">
                            <div class="col-md-5">
                                <input type="file" class="form-control form-control-lg" id="upload_file" />
                            </div>
                            <div class="col-md-1">
                                <input type="submit" class="form-control btn btn-primary" name="submit" value="上傳" id="upload_file_submit">
                            </div>
                            
                        </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="svg-viewer">
    <div class="col-12">
        <button type="button" class="btn btn-dark hide save-svg" onclick="saveSvg();">下載</button>
        <button type="button" class="btn btn-dark hide save-svg" onclick="printSvg();">列印</button>
        <div id="viewer" class="card" style="width:100%;"> 
            <div id="svg-view" class="card-body"></div>
            <div id="svg-view2" class="card-body" style="margin:auto;"></div>
        </div>
    </div>
</div>
<input type="hidden" id="current_ny_order_id" name="current_ny_order_id" value=""/>

<? include "_fsl_modal.php"; ?>
<? include "_order_modal.php"; ?>
<? include "_order_item_modal.php"; ?>
<? include "_order_trad_modal.php"; ?>

<?if($editable):?>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">進度全部修改 -> <span id="progress-all-text"></span></h4>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" id="modal-btn-yes">確認</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no">取消</button>
        </div>
    </div>
</div>
<?endif;?>
<script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
<script>
const SVGApp = {
    data() {
        return {
          'fA': 100,
          'fB': 20,
          'fC': 45,
          'fD': 68,
          'fE': 0,
          'fdt': 63,
          'max':150,
          'depth':30,
          'height': 2130,
          'lock_height': 1150
        }
  },
  computed: {
    // L Cut Frame
    LCutFrame() {
      return "0,"+this.fB+" 0,0 "+this.fC+",0 "+this.fC+","+(this.fdt+13)+" "+(this.fC+10)+","+(this.fdt+13)+" "+(this.fC+10)+","+this.fdt+" "+this.fD+","+this.fdt+" "+this.fD+","+this.MaxScreenFrameA+" 0,"+this.MaxScreenFrameA+" 0,"+(this.MaxScreenFrameA-this.fB)
    },
    MaxScreenFrameA() {
      if (this.fA > this.max) {
        return this.max
      }
      return this.fA
    },
  }
};
Vue.createApp(SVGApp).mount('#vueSVG');
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
$('#customer').select2();
$('#site-order').select2();
 
var check = false;
<? if ($editable):?>
check = true;
<?endif;?>
var customer_url = "<?=site_url('order/get_site')?>";
var site_url = "<?=site_url('order/get_order')?>";
var progress_url = "<?=site_url('order/update_all_progress')?>";
var o_url = '<?=site_url('order/get_ny_order_item')?>';
var t_url = '<?=site_url('order/get_translate_order')?>';
var tr_url = '<?=site_url('order/get_trad_order_item')?>';
var sf_url = '<?=site_url('svg/get_frame_svg')?>';
var h_url = '<?=site_url('history/get_order_history')?>';
var qc_url = '<?=site_url('qualityControl/get_qc_table')?>';
var showNote_url = '<?=site_url('orderNote')?>';
var createNote_url = '<?=site_url('orderNote/create')?>';

<? if (isset($_GET['get'])) : ?>
$(function() {
    $.ajaxSetup({async:false});
    $.post('<?=site_url("order/get_customer")?>', {id:"<?=$_GET['p']?>"}, function (data) {
        $('#customer').val(data).trigger('change');
        setTimeout(function () {
            $('#site-order').val("<?=$_GET['p']?>").trigger('change');
            getOrderItem('<?=$_GET['o']?>','<?=$_GET['n']?>');
        }, 500);
    },'json');
});
<? endif;?>

function get_order_match() {
    var $id = $("#site-order").val();
    $.post('<?=site_url("order/get_order_match")?>', {id:$id}, function (html) {
        $('#order-match').html(html);
    },'json');
}

$('#site-order').change(function (){
    get_order_match();
});

function allSubmit(table) {
    var progress = $('#all-'+table+'-progress').val();
    var option = $('#all-'+table+'-option').val();
    var vals   = $('.order_items:checkbox:checked').map(function() {
        if ("on" != this.value)
            return this.value;
    }).get();

    if ("progress" == option) {
        jQuery.noConflict();
        jQuery( document ).ready(function( $ ) {
        $.confirm({
            title: "修改進度",
            content: '確認修改進度: ' + progress,
            type: 'orange',
            draggable: true,
            dragWindowGap: 50,
            backgroundDismiss: false,
            backgroundDismissAnimation: 'glow',
            buttons : {
                confirm: {
                    text: "確認",
                    btnClass: 'btn-success',
                    action: function () {
                        changeAllProgress(vals, progress, table);
                    }
                },
                cancel: {
                    text: "取消",
                    btnClass: "btn-danger"
                }
            }
        });
        });
        
    }
}

function changeAllProgress(ids, progress, type) {
    $.post('<?=site_url('order/change_all_progress');?>',{id:ids, p:progress, t:type}, function(res) {
        var $ny_id = $('#current_ny_order_id').val();
        get_order_history($ny_id);
        ids.forEach(function (id) {
            $('input[value=' + id + ']').parent().parent().find('td:nth-child(4)').html(progress);
        });
        $('#order-table tbody tr td:first-child:contains('+$ny_id+')').parent().find('td:nth-child(2)').html(res);
    },'json');
}

$('#upload_file_submit').click(function (event) {
    event.preventDefault();
    
    var fd = new FormData();
    var files = $('#upload_file')[0].files;
        
    // Check file selected or not
    if(files.length > 0 ) {
        fd.append('file',files[0]);
    }
    fd.append('ny_order_id', $('#current_ny_order_id').val());

    $.ajax({
        url : '<?=site_url("order/add_upload_order_item")?>', 
        type : 'POST', 
        data : fd,
        processData : false,
        contentType : false,
        dataType: "json",
        success: function(data) {
            var order_number = $('#original .order-item-title').html().replace('訂單: ', '');
            var order_id = $('#current_ny_order_id').val() + order_number;
            $('#' + order_id).trigger('click');
            $('#o-tab li:first-of-type a').trigger('click');
            $('#upload_file').val('');
        }
    });
});

</script>