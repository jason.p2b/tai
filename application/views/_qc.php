<div id="nav_qc">
    <button type="button" class="btn btn-dark save-svg" onclick="printQC();">列印QC表</button>
</div>
<div id="view_qc">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center">品管表</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table>
                <tr>
                    <td>工地: </td>
                    <td id="qc_site"></td>
                </tr>
                <tr>
                    <td>訂單: </td>
                    <td id="qc_order_number"></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table id="qc_table" class="table table-hover table-striped"></table>
        </div>
    </div>
</div>