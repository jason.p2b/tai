<?if($editable):?>
<!-- Begin Popup Modal -->
<div class="modal fade" id="order-modal" tabindex="-1" role="dialog" aria-labelledby="order-modal-title">

    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="order-editor">
            <div class="modal-header">
                <h4 class="modal-title" id="order-modal-title">新增訂單</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="modal-body">
                <input type="number" id="order_id" name="order_id" class="hidden" value=""/>
                <input type="number" id="project_id" name="project_id" class="hidden" value=""/>
        
                <div class="form-group">
                    <label for="order_number" class="col-sm-3 control-label">訂單編號</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="order_number" name="order_number" placeholder="訂單編號">
                    </div>
                </div>
    
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">副工地</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="副工地">
                    </div>
                </div>

                <div class="form-group required">
                    <label for="customer_due_date" class="col-sm-3 control-label">客戶需要日</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="customer_due_date" name="customer_due_date" placeholder="客戶需要日" required>
                    </div>
                </div>
        
                <div class="form-group required">
                    <label for="factory_due_date" class="col-sm-3 control-label">工廠可出貨日</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="factory_due_date" name="factory_due_date" placeholder="工廠可出貨日" required>
                    </div>
                </div>

                <div class="form-group required">
                    <label for="received_date" class="col-sm-3 control-label">訂單收到日期</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="received_date" name="received_date" placeholder="訂單收到日期" required>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">確認</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </form>
    </div>
</div>
<!-- End Popup Modal -->
<?endif;?>

<script type="text/javascript">

// Used for order
$modal = $('#order-modal');
$editor = $('#order-editor');
$editorTitle = $('#order-modal-title');

<?if($editable):?>
$editor.on('submit', function(e){
    if (this.checkValidity && !this.checkValidity()) return;
    e.preventDefault();

    var row = $modal.data('row'),
    values = {
        ny_order_id: $editor.find('#order_id').val(),
        customer_due_date: $editor.find('#customer_due_date').val(),
        factory_due_date: $editor.find('#factory_due_date').val(),
        name: $editor.find('#name').val(),
        received_date: $editor.find('#received_date').val()
    };

    if (row instanceof FooTable.Row){
        $.post("<?=site_url('order/update_order')?>", values, function (result) {
            row.val(values);
        });
    } else {
        values['project_id'] = $editor.find('#project_id').val();
        values['order_number'] = $editor.find('#order_number').val();

        $.post("<?=site_url('order/add_order')?>", values, function (result) {
            values['progress'] = '收到訂單';
            values['ny_order_id'] = result;
            values['frame'] = 0;
            values['door'] = 0;
            values['order_number'] = '<a href="#" onclick="getOrderItem(\'' + result + '\',\'' + values['order_number'] + '\');">' + values['order_number'] + '</a>';
            ft.rows.add(values);

            getOrderItem(result);
        });
    }
    $modal.modal('hide');
});
<?endif;?>

function orderModal(cols, rows) {
    jQuery(function($) {
        
        ft = FooTable.init('#order-table', {
            "columns": cols,
            "rows": rows,
            sorting: { enabled: true },
            filtering: { enabled: true}<?if($editable):?>,
            editing: {
                enabled: true,
                addRow: function(){
                    $modal.removeData('row');
                    $editor[0].reset();
                    $editorTitle.text('新增訂單');

                    $modal.find('#project_id').val($('#site-order').val());
                    $editor.find('#received_date').val(curday('-'));

                    $modal.modal('show');
                },
                editRow: function(row){
                    var values = row.val();
                    var order_number = values.order_number.replace(/<.+\">/g, '');
                    order_number = order_number.replace('</a>', '');
                    // $editor.find('#id').val(row.$el[0].id);

                    $editor.find('#order_id').val(values.ny_order_id);
                    $editor.find('#project_id').val($('#site-order').val());
                    $editor.find('#order_number').val(order_number);
                    $editor.find('#customer_due_date').val(values.customer_due_date);
                    $editor.find('#factory_due_date').val(values.factory_due_date);
                    $editor.find('#received_date').val(values.received_date);
                    $editor.find('#name').val(values.name);

                    $modal.data('row', row);
                    $editorTitle.text('修改訂單:');
                    jQuery.noConflict(); 
                    $modal.modal('show');
                },
                'alwaysShow': true,
                'allowDelete': false,
                'showText': '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> 新增修改模式',
                'hideText': '取消',
                'addText': '新增訂單'
            }<?endif;?>
        });
        $('span.caret').hide();
        $('.footable-editing').css('text-align','left');
        $('#order-table thead th:last-of-type').remove();
        $('#order-table tbody td:last-of-type').remove();
  });
}

</script>