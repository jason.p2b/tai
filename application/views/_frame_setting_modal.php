<?if($editable):?>
<!-- fs = frame_setting -->
<!-- Begin Popup Modal -->
<style>
    .modal-content {background-color:#d1d1d1;}
    #la .input-group-text, #ta .input-group-text {width:57px;}
</style>
<div class="modal fade" id="fs-modal" tabindex="-1" role="dialog" aria-labelledby="fs-modal">

    <div class="modal-dialog" role="document">
        <div class="modal-content form-horizontal" id="fs-editor">
            <div class="modal-header">
                <h3 class="modal-title text text-info" id="fs-modal-title">門框設定</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="modal-body">
                <input type="hidden" id="fs-id" name="fs-id" value=""/>
                <div class="form-group">
                    <div class="col-sm-6">
                        <svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250 150" style="font-family:Arial;border:2px solid #000;border-radius:5px;background-color:#fff;">
                            <path d="M70,30 50,30 50,100 140,100 140,65 200,65 200,30 180,30" fill="none" stroke="#000" stroke-width="5"/>

                            <path d="M140,105 140,120 M200,120 200,70" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(165,118)" id="sla_a">A</text>

                            <path d="M70,25 70,10 M50,25 50,10" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(55,22)" id="sla_b">B</text>
                            
                            <path d="M205,30 220,30 M205,65 220,65" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(217,52) rotate(-90)" id="sla_c">C</text>
                            
                            <path d="M45,30 30,30 M45,100 30,100" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(42,67) rotate(-90)" id="sla_d">D</text>
                        </svg>
                    </div>
                    <div class="col-sm-6" id="la">
                        <div class="input-group">
                            <div class="input-group-text">A</div>
                            <input type="text" class="form-control" id="la_a" name="la_a" value="" onkeyup="svgText(this.id);">
                        </div>
                        <div class="input-group">
                            <div class="input-group-text">B</div>
                            <input type="text" class="form-control" id="la_b" name="la_b" value="" onkeyup="svgText(this.id);">
                        </div>
                        <div class="input-group">
                            <div class="input-group-text">C</div>
                            <input type="text" class="form-control" id="la_c" name="la_c" value="" onkeyup="svgText(this.id);">
                        </div>
                        <div class="input-group">
                            <div class="input-group-text">D</div>
                            <input type="text" class="form-control" id="la_d" name="la_d" value="" onkeyup="svgText(this.id);">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250 150" style="font-family:Arial;border:2px solid #000;border-radius:5px;background-color:#fff;">
                            <path d="M70,30 50,30 50,65 80,65 80,100 140,100 140,65 200,65 200,30 180,30" fill="none" stroke="#000" stroke-width="5"/>

                            <path d="M140,105 140,120 M200,120 200,70" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(165,118)" id="sta_a">A</text>

                            <path d="M70,25 70,10 M50,25 50,10" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(55,22)" id="sta_b">B</text>
                            
                            <path d="M205,30 220,30 M205,65 220,65" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(217,52) rotate(-90)" id="sta_c">C</text>
                            
                            <path d="M45,30 30,30 M45,100 30,100" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(42,67) rotate(-90)" id="sta_d">D</text>

                            <path d="M50,105 50,120 M80,105 80,120" fill="none" stroke="#f00" stroke-width="2"/>
                            <text fill="#f00" style="font-size:15px;" transform="translate(60,117)" id="sta_e">E</text>
                        </svg>
                    </div>
                    <div class="col-sm-6" id="ta">
                        <div class="input-group">
                            <div class="input-group-text">A</div>
                            <input type="text" class="form-control" id="ta_a" name="ta_a" value="" onkeyup="svgText(this.id);">
                        </div>
                        <div class="input-group">
                            <div class="input-group-text">B</div>
                            <input type="text" class="form-control" id="ta_b" name="ta_b" value="" onkeyup="svgText(this.id);">
                        </div>
                        <div class="input-group">
                            <div class="input-group-text">C</div>
                            <input type="text" class="form-control" id="ta_c" name="ta_c" value="" onkeyup="svgText(this.id);">
                        </div>
                        <div class="input-group">
                            <div class="input-group-text">D</div>
                            <input type="text" class="form-control" id="ta_d" name="ta_d" value="" onkeyup="svgText(this.id);">
                        </div>
                        <div class="input-group">
                            <div class="input-group-text">E</div>
                            <input type="text" class="form-control" id="ta_e" name="ta_e" value="" onkeyup="svgText(this.id);">
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <label for="horizontal_reduce" class="col-sm-6 control-label">橫料縮減</label>
                    <div class="col-sm-6">
                        <div class="input-group mb3">
                            <input type="number" class="form-control" id="horizontal_reduce" name="horizontal_reduce" placeholder="橫料縮減">
                            <div class="input-group-text">mm</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="gap" class="col-sm-6 control-label">下門縫</label>
                    <div class="col-sm-6">
                        <div class="input-group mb3">
                            <input type="number" class="form-control" id="gap" name="gap" placeholder="下門縫">
                            <div class="input-group-text">mm</div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label"><br>鎖高算法</label>
                    <div class="col-sm-3">
                        <div class="form-radio">
                            <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="handle_orientation" id="ho_down" value="0" checked="">從下面算起
                            <i class="input-helper"></i></label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-radio">
                            <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="handle_orientation" id="ho_up" value="1">從上面算起
                            <i class="input-helper"></i></label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label"><br>框焊接方式</label>
                    <div class="col-sm-3">
                        <div class="form-radio">
                            <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="45degree" id="45no" value="0" checked="">一般焊接
                            <i class="input-helper"></i></label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-radio">
                            <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="45degree" id="45yes" value="1">45度焊接
                            <i class="input-helper"></i></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="submitFS();">確認</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- End Popup Modal -->
<?endif;?>

<script type="text/javascript">

function frameInfo(id)
{
  $('#fs-id').val(id);
  $.post('<?=site_url('order/get_fs');?>',{id:id},function(data){
        if (data === undefined || data.length == 0) {
            $('#la_a,#la_b,#la_c,#la_d').val("");
            $('#ta_a,#ta_b,#ta_c,#ta_d,#ta_e').val("");
            $('#sla_a').html('A');
            $('#sla_b').html('B');
            $('#sla_c').html('C');
            $('#sla_d').html('D');
            $('#sta_a').html('A');
            $('#sta_b').html('B');
            $('#sta_c').html('C');
            $('#sta_d').html('D');
            $('#sta_e').html('E');
            $('#gap').val("");
            $('#horizontal_reduce').val("");
            $('#ho_down').trigger('click');
            $('#45no').trigger('click');
        } else {
            $('#la_a').val(data['la_a']).trigger('keyup');
            $('#la_b').val(data['la_b']).trigger('keyup');
            $('#la_c').val(data['la_c']).trigger('keyup');
            $('#la_d').val(data['la_d']).trigger('keyup');
            $('#ta_a').val(data['ta_a']).trigger('keyup');
            $('#ta_b').val(data['ta_b']).trigger('keyup');
            $('#ta_c').val(data['ta_c']).trigger('keyup');
            $('#ta_d').val(data['ta_d']).trigger('keyup');
            $('#ta_e').val(data['ta_e']).trigger('keyup');
            $('#gap').val(data['gap']);
            $('#horizontal_reduce').val(data['horizontal_reduce']);
            if ("0" == data['handle_orientation']) {
                $('#ho_down').trigger('click');
            } else {
                $('#ho_up').trigger('click');
            }
            if ("0" == data['45degree']) {
                $('#45no').trigger('click');
            } else {
                $('#45yes').trigger('click');
            }
        }
  },'json');
}


function svgText(id)
{
    $('#s'+id).text($('#'+id).val());
}

function submitFS() {

    var $val = $('#fs-id').val();

    if ("" != $val)
    {
      var data =  { 'id':$val,
                    'la_a':$('#la_a').val(),
                    'la_b':$('#la_b').val(),
                    'la_c':$('#la_c').val(),
                    'la_d':$('#la_d').val(),
                    'ta_a':$('#ta_a').val(),
                    'ta_b':$('#ta_b').val(),
                    'ta_c':$('#ta_c').val(),
                    'ta_d':$('#ta_d').val(),
                    'ta_e':$('#ta_e').val(),
                    'gap':$('#gap').val(),
                    'horizontal_reduce':$('#horizontal_reduce').val(),
                    '45degree':$('input[name=45degree]:checked').val(),
                    'handle_orientation':$('input[name=handle_orientation]:checked').val()
                  };
      $.post('<?=site_url('order/add_fs');?>',data,function(result){

      },'json');
    }
}
</script>