<div id="tempForm">
<h2>新增訂單</h2>
<div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <span id="order_message"></span>
</div>
<div class="form-horizontal" id="basic">
    <h4>基本資料</h4>
    <hr>
    <div class="col-md-6">
        <div class="col-md-10">
        <div class="form-group">
            <label for="new_project" class="control-label">建案名稱：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
            <select id="new_project" class="form-control" name="project" required>
                <option value=""> --- </option>
                <?php foreach ($projects as $key => $value) : ?>
                    <option value="<?=$key?>"><?=$value?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="new_report" class="control-label">防火報告：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
            <select id="new_report" class="form-control" name="report" required>
                <option value=""> --- </option>
            </select>
        </div>
        </div>
        <div class="non-report col-md-10">
            <div class="form-group">
                <label>訂單類型：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <div class="btn-group" data-toggle="buttons" style="display:block;">
                    <label class="btn btn-default btn-radio order_type" for="order_frame">
                        <input type="radio" class="order_type" name="order_type" id="order_frame" value="frame" required>門框
                    </label>
                    <label class="btn btn-default btn-radio order_type" for="order_door">
                    <input type="radio" class="order_type" name="order_type" id="order_door" value="door" required>門扇
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="new_due_date" class="control-label">出貨日期：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <div class="controls">
                    <div class="input-group">
                        <input id="new_due_date" type="text" class="form-control" name="due_date" required />
                        <label for="new_due_date" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="new_order_name" class="control-label">訂單編號：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <input type="text" id="new_order_name" class="form-control" name="order_name" required>
            </div>
            <div class="door-only form-group">
                <label for="new_same_model" class="control-label">對應門框：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <select id="new_same_model" class="form-control" name="same_model" required>
                    <option value=""> --- </option>
                </select>
            </div>
            <div class="form-group">
                <label for="new_model" class="control-label">圖名：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <input type="text" id="new_model" class="form-control" name="model" required>
            </div>
            <div class="form-group">
                <label>訂單數量：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <div class="input-group">
                    <label for="new_left_amount" class="control-label input-group-addon"><span>左</span></label>
                    <input type="number" id="new_left_amount" class="form-control" name="left" value="0" min="0">
                    <label for="new_right_amount" class="control-label input-group-addon"><span>右</span></label>
                    <input type="number" id="new_right_amount" class="form-control" name="right" value="0" min="0">
                    <label for="new_total_amount" class="control-label input-group-addon"><span>總數</span></label>
                    <input type="text" id="new_total_amount" class="form-control text-right" value="0" min="1" required readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="frame_type" class="control-label">門框：</label>
                <div class="form-control" id="frame_type"></div>
                <input type="hidden" name="frame_type" value="">
            </div>
            <div class="form-group">
                <label for="door_amount" class="control-label">門扇：</label>
                <div class="form-control" id="door_amount"></div>
                <input type="hidden" name="door_amount" value="">
            </div>
        </div>
    </div>
    <div class="non-report col-md-6">
        <div class="form-group">
            <label for="new_note" class="control-label">備註：</label>
            <textarea class="form-control" name="note" id="new_note" rows="15"></textarea>
        </div>
        <div class="form-group">
            <label for="attachment" >附件：</label>
            <input id="attachment" name="file[]" type="file" class="file-loading" multiple>
        </div>
    </div>
</div>
<div class="non-report form-horizontal col-md-12" id="component">
    <h4>框型和五金</h4>
    <hr>
    <div class="col-md-6">
        <div class="col-md-10">
        <div class="form-group">
            <label class="control-label " for="lock_type">門鎖：</label>
            <select class="form-control" name="lock_type" id="lock_type"/>
                <option value="">無</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label " for="handle_type">把手：</label><span class="text-info">請注意 - 有些門鎖已包含把手</span>
            <select class="form-control"name="handle_type" id="handle_type" class="same_hide"/>
                <option value="">無</option>
            </select>
           
        </div>
        <div class="form-group same-model-only">
            <label class="control-label" for="lock_height">把手高度：</label><span class="text-info">（從地平線算起）若無標示，工廠預設 1000 mm</span>
            <div class="input-group">
                <input class="form-control" type="number" step="any" id="lock_height" name="lock_height" value="1000" min="0" >
                <span class="input-group-addon">mm</span>
            </div>
            <span id="lock_height_same" class="same_show"></span>
            
        </div>
        <div class="form-group">
            <label class="control-label" for="hinge_type">鉸鏈：</label>
            <select class="form-control" id="hinge_type" name="hinge_type" class="same_hide">
                <option value="">無</option>
            </select>
            <span class="same_show" id="hinge_type_same"></span>
        </div>
        <div class="form-group">
            <label class="control-label" for="closer_type">門弓器：</label>
            <select class="form-control" name="closer_type" id="closer_type" class="same_hide"/>
                <option value="">無</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" for="bolt_type">天地栓：</label>
            <select class="form-control" name="bolt_type" id="bolt_type" class="same_hide"/>
                <option value="">無</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">其他五金：</label>
            <div class="funkyradio">
                <div class="funkyradio-primary">
                    <input type="checkbox" id='test'>
                    <label for="test">測試</label>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-10">
            <label for="frames">選擇框形：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
            <div id="frames" class="same-model-only">
                <input type="hidden" name="frame_shape" value="" id="frame_shape">
                <ul>

                </ul>
            </div>
            <div class="frame-measure col-md-5">
                <div class="input-group">
                    <span class="input-group-addon">A</span>
                    <input class="form-control" type="number" name="v_total_length" value="0" min="0" onkeyup="changeSVGvalue(this);">
                </div>
            </div>
            <div class="frame-measure col-md-5">
                <div class="input-group">
                    <span class="input-group-addon">B</span>
                    <input class="form-control" type="number" name="v_north_length" value="0" min="0" onkeyup="changeSVGvalue(this);">
                </div>
            </div>
            <div class="frame-measure col-md-5">
                <div class="input-group">
                    <span class="input-group-addon">C</span>
                    <input class="form-control" type="number" name="v_west_length" value="0" min="0" onkeyup="changeSVGvalue(this);">
                </div>
            </div>
            <div class="frame-measure col-md-5">
                <div class="input-group">
                    <span class="input-group-addon">D</span>
                    <input class="form-control" type="number" name="v_east_length" value="0" min="0" onkeyup="changeSVGvalue(this);">
                </div>
            </div>
            <div class="frame-measure frame-se col-md-5">
                <div class="input-group">
                    <span class="input-group-addon">E</span>
                    <input class="form-control" type="number" name="v_south_east_length" value="0" min="0" onkeyup="changeSVGvalue(this);">
                </div>
            </div>
        </div>
        
        <div id="frame_dimension" class="same_hide"></div>
        <div id="frame_shape_same" class="same_show"></div>
    </div>
</div>
<div class="form-horizontal col-md-12 non-report frame-only" id="frame_size">
    <h4>門框尺寸</h4>
    <hr>
    <div class="col-md-6">
        <div class="col-md-10">
        <div class="form-group"> 
            <label class="control-label"for="frame_width">寬度：<span class="asterisk">*</span></label>
            <span class="text-info"></span>
            <span class="help-block with-errors"></span>
            <div class="input-group">
                <input class="form-control" type="number" step="any"  id="frame_width" name="frame_width" value="0" min="0">
                <span class="input-group-addon">mm</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="frame_length">高度：<span class="asterisk">*</span></label>
            <span class="text-info"></span><span class="text-info"> (包插框深度)</span>
            <span class="help-block with-errors"></span>
            <div class="input-group">
                <input id="frame_length" class="form-control" type="number" step="any" name="frame_length" value="0" min="0" >
                <span class="input-group-addon">mm</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="beneath_length">插框深度：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
            <div class="input-group">
                <input class="form-control" type="number" step="any" id="beneath_length" name="beneath_length" value="0" min="0">
                <span class="input-group-addon">mm</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="inner_length">內距：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
            <div class="input-group">
                <input class="form-control" type="number" step="any" id="inner_length" name="inner_length" value="0" min="0">
                <span class="input-group-addon">mm</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="frame_material">材質：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
            <select class="form-control" id="frame_material" name="frame_material" required>
                <option value="">  ---  </option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" for="frame_colour">顏色：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
            <input class="form-control" type="text" id="frame_colour" name="frame_colour" value="" required>
        </div>
        </div>
    </div>
    <div class="col-md-4" id="frame_svg">
        
    </div>
</div>
<div class="form-horizontal col-md-12 non-report door-only" id="door_size">
    <h4>門扇尺寸</h4>
    <hr>
    <div class="col-md-6">
        <div class="col-md-10">
            <div class="form-group"> 
                <label class="control-label" for="door_width">寬度<span class="son">(母)</span>：<span class="asterisk">*</span></label>
                <span class="text-info" id="door_width_range"></span>
                <span class="text-info" id="suggest_width"></span>
                <span class="help-block with-errors"></span>
                <div class="input-group">
                    <input class="form-control" type="number" step="any" id="door_width" name="door_width" value="0" min="0">
                    <span class="input-group-addon">mm</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="door_length">高度<span class="son">(母)</span>：<span class="asterisk">*</span></label>
                <span class="text-info" id="door_length_range"></span>
                <span class="text-info" id="suggest_length"></span>
                <span class="help-block with-errors"></span>
                <div class="input-group">
                    <input class="form-control" type="number" step="any" id="door_length" name="door_length" value="0" min="0">
                    <span class="input-group-addon">mm</span>
                </div>
            </div>
            <div class="form-group son"> 
                <label class="control-label" for="son_width">寬度(子)：<span class="asterisk">*</span></label>
                <span class="text-info"></span>
                <span class="help-block with-errors"></span>
                <div class="input-group">
                    <input class="form-control" type="number" step="any"  id="son_width" name="son_width" value="0" min="0">
                    <span class="input-group-addon">mm</span>
                </div>
            </div>
            <div class="form-group son">
                <label class="control-label" for="son_length">高度(子)：<span class="asterisk">*</span></label>
                <span class="text-info"></span>
                <span class="help-block with-errors"></span>
                <div class="input-group">
                    <input class="form-control" type="number" step="any" name="son_length" value="0" min="0" >
                    <span class="input-group-addon">mm</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="door_material">材質：<span class="asterisk">*</span></label>
                <span class="text-info"></span>
                <span class="help-block with-errors"></span>
                <select class="form-control" id="door_material" name="door_material" required>
                    <option value="">  ---  </option>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label" for="door_colour">顏色：<span class="asterisk">*</span></label>
                <span class="help-block with-errors"></span>
                <input class="form-control" type="text" id="door_colour" name="door_colour" value="" required>
            </div>
        </div>
    </div>
    <div class="col-md-4" id="door_svg">
        
    </div>
</div>
<div class="non-report form-horizontal col-md-12" id="confirmation">
    <div class="form-group funkyradio">
        <div class="funkyradio-primary">
        <input class="form-control" type="checkbox" id="preserve" name="preserve" value="true">
        <label for="preserve">還要繼續下單（下完單會保留現有資料）</label>
        </div>
    </div>
    <hr>
    <input class="btn btn-primary" type="submit" value="確認">
</div>
</div>
