<ul class="nav nav-tabs small">
    <li id="viewTab" class="active"><a href="#viewOrder" data-toggle="tab">現有訂單</a></li>
    <li id="addTab"><a href="#addOrder" data-toggle="tab">增加訂單</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="viewOrder">
        <h2>建案訂單</h2>
        <div class="form">
            <div id="project-div" class="form-group col-md-4">
                <label for="project" class="control-label">建案名稱：</label>
                <select id="project" class="form-control">
                    <option value=""> --- </option>
                    <?php foreach ($projects as $key => $value) : ?>
                        <option value="<?=$key?>"><?=$value?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div id="match"></div>
        <div id="orderTableToolbar">
            <div class="form form-inline">
                <label for="choices" class="control-label">選項：</label>
                <select id="choices" class="form-control">
                    <option value=""> --- </option>
                    <option value="status_option">更改進度</option>
                    <option value="print_option">印列</option>
                    <? if ($this->session->userdata('status') < 3) : ?>
                    <option value="label_option">標籤</option>
                    <? endif; ?>
                </select>
                <div class="form-group" id="status_option">
                    <select id="change-status" class="form-control" disabled>
                        <option value=""> --- </option>
                        <? if ($this->session->userdata('status') < 3) : ?>
                        <option value="發送到工廠">發送到工廠</option>
                        <option value="加工廠裁切">加工廠裁切</option>
                        <option value="備料整理">備料整理</option>
                        <option value="組裝">組裝</option>
                        <option value="烤漆">烤漆</option>
                        <option value="包裝">包裝</option>
                        <option value="等待出貨">等待出貨</option>
                        <option value="出貨">出貨</option>
                        <? endif; ?>
                        <? if ($this->session->userdata('status') >= 3) : ?>
                        <option value="取消訂單">取消訂單</option>
                        <? endif;?>
                    </select>
                </div>
                <div id="print_option">
                    <button id="print" class="btn btn-primary" disabled>
                        <i class="glyphicon glyphicon-print"></i> 印列
                    </button>
                </div>
                <div id="label_option">
                    <button id="label_export" class="btn btn-primary" disabled>
                        <i class="glyphicon glyphicon-export"></i> 產生標籤
                    </button>
                </div>
            </div>
        </div>
        <table id="orderTable"
            data-toolbar="#orderTableToolbar"
            data-show-toggle="true"
            data-show-columns="true"
            data-pagination="true"
            data-search="true"
            data-mobile-responsive="true"
            data-check-on-init="true"
            data-row-style="rowStyle">
            <thead>
                <tr>
                    <th data-checkbox="true" class="col-xs-1"></th>
                    <th class="col-xs-1" data-field="productType" data-sortable="true">框／扇</th>
                    <th class="col-md-1" data-field="orderName" data-sortable="true">訂單編號</th>
                    <th class="col-md-1" data-field="model" data-sortable="true">型號</th>
                    <th class="col-md-1" data-field="frameShape" data-sortable="true">框型</th>
                    <th class="col-md-1" data-field="orderDate" data-sortable="true">下單日期</th>
                    <th class="col-xs-1" data-field="width" data-sortable="true">寬</th>
                    <th class="col-xs-1" data-field="height" data-sortable="true">高</th>
                    <th class="col-xs-1" data-field="total" data-sortable="true">總數</th>
                    <th class="col-xs-1" data-field="left" data-sortable="true">左</th>
                    <th class="col-xs-1" data-field="right" data-sortable="true">右</th>
                    <th class="col-md-2" data-field="hardware" data-sortable="true">門鎖絞鍊</th>
                    <th class="col-md-1" data-field="progress" data-sortable="true">進度</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
    <div class="tab-pane col-md-12" id="addOrder">
        <?include('order_new.php');?>
    </div>
</div>

<!-- Modal for Viewing each Order -->
<div id="viewDetail" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<? include('issue_modal.php'); ?>
<? include('loader.php'); ?>

<div id="printArea"></div>