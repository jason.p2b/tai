<link href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">

<div class="row">
	<div class="col-md-4 mt-4">
		<div class="input-group date p-3">
		  <input type="text" class="form-control" id="datepicker" value="<?=date("Y-m-d", time())?>">
          <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-12 mt-3">
        <h2>英聖 / 泰慶</h2>
        <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="date-progress-table"></table></div>
    </div>
    <div class="col-md-12 mt-3">
        <h2>南亞</h2>
        <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="date-progress-table-ny"></table></div>
    </div>
</div>



<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.zh-TW.min.js" charset="UTF-8"></script>

<script>
$('#datepicker').datepicker({
    format: "yyyy-mm-dd",
    daysOfWeekDisabled: "0",
    daysOfWeekHighlighted: "0,6",
    todayHighlight: true,
    autoclose: true
}).change(function () {
    $trad = $('#date-progress-table');
    $ny = $('#date-progress-table-ny');
    $.post('<?=base_url('dashboard/get_date_progress')?>', {d:$(this).val()}, function(data) {
        $trad.html("");
        ft = FooTable.init('#date-progress-table',{
            "columns": data['cols'],
            "rows": data['rows'],
            sorting: { enabled: true }
        });
    },'json');
    $.post('<?=base_url('dashboard/get_date_progress_ny')?>', {d:$(this).val()}, function(data) {
        $ny.html("");
        ft = FooTable.init('#date-progress-table-ny',{
            "columns": data['cols'],
            "rows": data['rows'],
            sorting: { enabled: true }
        });
    },'json');
}).trigger('change');
</script>