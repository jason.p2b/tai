<ul class="nav nav-tabs small">
    <li id="newOrderTab" class="active"><a href="#newOrder" data-toggle="tab">新訂單&nbsp;&nbsp;<span class="label label-pill label-primary">0</span></a></li>
    <li id="changeOrderTab"><a href="#changeOrder" data-toggle="tab">修改訂單&nbsp;&nbsp;<span class="label label-pill label-primary">0</span></a></li>
    <li id="myIssueTab"><a href="#myIssue" data-toggle="tab">你的追蹤&nbsp;&nbsp;<span class="label label-pill label-primary">0</span></a></li>
    <li id="companyIssueTab"><a href="#companyIssue" data-toggle="tab">公司追蹤&nbsp;&nbsp;<span class="label label-pill label-primary">0</span></a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="newOrder">
        <h2>新訂單資訊</h2>
        <table id="newOrderTable"
            data-toolbar="#newOrderToolbar"
            data-show-toggle="true"
            data-show-columns="true"
            data-pagination="true"
            data-search="true"
            data-mobile-responsive="true"
            data-check-on-init="true">
            <thead>
                <th class="col-md-1" data-field="category" data-sortable="true">屬性</th>
                <th class="col-md-2" data-field="date" data-sortable="true">日期</th>
                <th class="col-md-1" data-field="project_name" data-sortable="true">建案</th>
                <th class="col-md-2" data-field="order_name" data-sortable="true">訂單編號</th>
                <th class="col-md-1" data-field="model" data-sortable="true">型號</th>
                <th class="col-md-1" data-field="name" data-sortable="true">人員</th>
                <th class="col-md-1" data-field="type" data-sortable="true">項目</th>
                <th class="col-md-3" data-field="history" data-sortable="true">內容</th>
            </thead>
            
        </table>
    </div>
    <div class="tab-pane" id="changeOrder">
        <h2>製作中的訂單資訊</h2>
        <table id="oldOrderTable"
            data-toolbar="#oldOrderToolbar"
            data-show-toggle="true"
            data-show-columns="true"
            data-pagination="true"
            data-search="true"
            data-mobile-responsive="true"
            data-check-on-init="true">
            <thead>
                <th data-field="category" data-sortable="true">屬性</th>
                <th data-field="date" data-sortable="true">日期</th>
                <th data-field="project_name" data-sortable="true">建案</th>
                <th data-field="order_name" data-sortable="true">訂單編號</th>
                <th data-field="model" data-sortable="true">型號</th>
                <th data-field="name" data-sortable="true">人員</th>
                <th data-field="type" data-sortable="true">項目</th>
                <th data-field="history" data-sortable="true">內容</th>
            </thead>
            
        </table>
    </div>
    <div class="tab-pane" id="myIssue">
        <h2><?=$this->session->userdata('user')?>的追蹤訊息</h2>
        <table id="myIssueTable"
            data-toolbar="#myIssueToolbar"
            data-show-toggle="true"
            data-show-columns="true"
            data-pagination="true"
            data-search="true"
            data-mobile-responsive="true"
            data-check-on-init="true">
            <thead>
                <tr>
                    <th class="col-md-1" data-field="tracker" data-sortable="true">工地/代號</th>
                    <th class="col-md-2" data-field="date" data-sortable="true">更新日期</th>
                    <th class="col-md-2" data-field="subject" data-sortable="true">主題</th>
                    <th class="col-md-1" data-field="issue_type" data-sortable="true">類別</th>
                    <th class="col-md-6" data-field="conversation" data-sortable="true">內容</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="tab-pane" id="companyIssue">
        <h2>有關公司的追蹤訊息</h2>
        <table id="companyIssueTable"
            data-toolbar="#companyIssueToolbar"
            data-show-toggle="true"
            data-show-columns="true"
            data-pagination="true"
            data-search="true"
            data-mobile-responsive="true"
            data-check-on-init="true">
            <thead>
                <tr>
                    <th class="col-md-1" data-field="tracker" data-sortable="true">工地/代號</th>
                    <th class="col-md-1" data-field="date" data-sortable="true">更新日期</th>
                    <th class="col-md-2" data-field="subject" data-sortable="true">主題</th>
                    <th class="col-md-1" data-field="to" data-sortable="true" data-filter-control="select">收件人</th>
                    <th class="col-md-1" data-field="issue_type" data-sortable="true">類別</th>
                    <th class="col-md-6" data-field="conversation" data-sortable="true">內容</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<? include('issue_modal.php'); ?>