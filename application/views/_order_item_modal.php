<?if($editable):?>
<style>
    .product-status-received {color: blue;}
    .product-status-shipped {color: green;}
</style>

<!-- Begin Popup Modal -->
<div class="modal fade" id="order-item-modal" tabindex="-1" role="dialog" aria-labelledby="order-item-modal-title">
    <style scoped>
        /* provides a red astrix to denote required fields - this should be included in common stylesheet */
        .form-group.required .control-label:after {
            content:"*";
            color:red;
            margin-left: 4px;
        }
        .hide {display:none;}
    </style>

    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="order-item-editor">
            <div class="modal-header">
                <h4 class="modal-title" id="order-item-modal-title">新增訂單內容</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="modal-body">
                <input type="number" id="ny_order_item_id" name="ny_order_item_id" class="hidden" value=""/>
                <input type="number" id="ny_order_id" name="ny_order_id" class="hidden" value=""/>
                <input type="number" id="row_number" name="row_number" class="hidden" value=""/>
    
                <div class="form-row form-group required">
                    <div class="col-sm-12">
                        <label  class="control-label" for="name">品項</label>
                        <div class="input-group">
                            <span id="rank" class="input-group-text">1</span>
                            <input type="text" class="form-control" id="name" name="name" placeholder="品項" required>
                        </div>
                    </div>
                </div>

                <div class="form-row form-group required">
                    <div class="col-sm-6">
                        <label for="progress" class="control-label">進度</label>
                        <select name="progress" id="progress" class="form-control" required>
                            <option value="收到訂單">收到訂單</option>
                            <option value="發包中">發包中</option>
                            <option value="生產中">生產中</option>
                            <option value="烤漆包裝">烤漆包裝</option>
                            <option value="已出貨">已出貨</option>
                            <option value="等排程">等排程</option>
                            <option value="等出貨">等出貨</option>
                            <option value="等SKIN板">等SKIN板</option>
                            <option value="等客戶回復">等客戶回復</option>
                            <option value="取消訂單">取消訂單</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="name" class="control-label">出貨日</label>
                        <input type="text" class="form-control" id="due_date" name="due_date" placeholder="出貨日" required>
                    </div>
                </div>
                

                <div class="form-row form-group required">
                    <div class="col-sm-4">
                        <label for="big_category" class="control-label">大類</label>
                        <select name="big_category" id="big_category" class="form-control" required>
                            <option value=""> --- </option>
                            <option value="BN">BN</option>
                            <option value="BS">BS</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="middle_category" class="control-label">中類</label>
                        <select name="middle_category" id="middle_category" class="form-control" required>
                                <option value=""> --- </option>
                                <option class="frame hide" value="RK">RK</option>
                                <option class="frame hide" value="RF">RF</option>
                                <option class="door hide"  value="L8">L8</option>
                                <option class="door hide"  value="LK">LK</option>
                                <option class="door hide"  value="L7">L7</option>
                            </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="window" class="control-label">上中下窗</label>
                        <input type="text" class="form-control" id="window" name="window" placeholder="上中下窗" required>
                    </div>
                </div>

                <div class="form-row form-group required">
                    <div class="col-sm-4">
                        <label  class="control-label" for="width">寬度</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="width" name="width" placeholder="寬度" required>
                            <span class="input-group-text">mm</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="up_window" class="control-label">上窗高度</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="up_window" name="up_window" placeholder="上窗高度" required>
                            <span class="input-group-text">mm</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="total_height" class="control-label">總高度</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="total_height" name="total_height" placeholder="總高度" required>
                            <span class="input-group-text">mm</span>
                        </div>
                    </div>
                </div>
                
                <div class="form-row form-group required">
                    <div class="col-sm-6">
                        <label  class="control-label" for="wind">風壓</label>
                        <input type="text" class="form-control" id="wind" name="wind" placeholder="風壓" required>
                    </div>
                    <div class="col-sm-6">
                        <label for="colour" class="control-label">色號</label>
                        <input type="text" class="form-control" id="colour" name="colour" placeholder="色號" required>
                    </div>
                </div>

                <div class="form-row form-group required">
                    <div class="col-sm-6">
                        <label  class="control-label" for="amount">數量</label>
                        <input type="number" class="form-control" id="amount" name="amount" placeholder="數量" required>
                    </div>
                    <div class="col-sm-6">
                        <label for="price" class="control-label">單價</label>
                        <div class="input-group">
                            <div class="input-group-text">$</div>
                            <input type="number" class="form-control" id="price" name="price" placeholder="單價" required>
                        </div>
                    </div>
                </div>

                <div class="form-row form-group frame hide">
                    <div class="col-sm-6">
                        <label  class="control-label" for="threshold">門框種類</label>
                        <input type="text" class="form-control" id="threshold" name="threshold" placeholder="門框種類">
                    </div>
                    <div class="col-sm-6">
                        <label  class="control-label" for="depth">埋入深度</label>
                        <input type="text" class="form-control" id="depth" name="depth" placeholder="埋入深度">
                    </div>
                </div>

                <div class="form-row form-group required">
                    <div class="col-sm-6">
                        <label  class="control-label" for="mixed_frame">混合框</label>
                        <input type="text" class="form-control" id="mixed_frame" name="mixed_frame" placeholder="混合框" required>
                    </div>
                    <div class="col-sm-6">
                        <label for="box" class="control-label">BOX色號</label>
                        <input type="text" class="form-control" id="box" name="box" placeholder="BOX色號" required>
                    </div>
                </div>
                
                <div class="form-row form-group door hide">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label  class="control-label" for="l1">L1</label>
                        <input type="number" class="form-control" id="l1" name="l1" placeholder="L1">
                    </div>
                    <div class="col-sm-2">
                        <label  class="control-label" for="l2">L2</label>
                        <input type="number" class="form-control" id="l2" name="l2" placeholder="L2">
                    </div>
                    <div class="col-sm-2">
                        <label  class="control-label" for="l3">L3</label>
                        <input type="number" class="form-control" id="l3" name="l3" placeholder="L3">
                    </div>
                    <div class="col-sm-2">
                        <label  class="control-label" for="l1">L4</label>
                        <input type="number" class="form-control" id="l4" name="l4" placeholder="L4">
                    </div>
                    <div class="col-sm-2">
                        <label  class="control-label" for="l5">L5</label>
                        <input type="number" class="form-control" id="l5" name="l5" placeholder="L5">
                    </div>
                </div>

                <div class="form-row form-group required">
                    <div class="col-sm-6">
                        <label  class="control-label" for="lock">鎖</label>
                        <input type="text" class="form-control" id="lock" name="lock" placeholder="鎖" required>
                    </div>
                    <div class="col-sm-6">
                        <label for="lock_height" class="control-label">鎖高</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="lock_height" name="lock_height" placeholder="鎖高" required>
                            <span class="input-group-text">mm</span>
                        </div>
                        
                    </div>
                </div>
                
                <div class="form-row form-group">
                    <div class="col-sm-3">
                        <label  class="control-label" for="f1">框一</label>
                        <input type="text" class="form-control" id="f1" name="f1" placeholder="框一">
                    </div>
                    <div class="col-sm-3">
                        <label  class="control-label" for="f2">框二</label>
                        <input type="text" class="form-control" id="f2" name="f2" placeholder="框二">
                    </div>
                    <div class="col-sm-3">
                        <label  class="control-label" for="f3">框三</label>
                        <input type="text" class="form-control" id="f3" name="f3" placeholder="框三">
                    </div>
                    <div class="col-sm-3">
                        <label  class="control-label" for="f4">框四</label>
                        <input type="text" class="form-control" id="f4" name="f4" placeholder="框四">
                    </div>
                </div>
            </div>
            

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">確認</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </form>
    </div>
</div>
<!-- End Popup Modal -->
<?endif;?>
<script type="text/javascript">
var curday = function(sp){
    today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //As January is 0.
    var yyyy = today.getFullYear();

    if(dd<10) dd='0'+dd;
    if(mm<10) mm='0'+mm;
    return (yyyy+sp+mm+sp+dd);
};


// Used for order
$modal1 = $('#order-item-modal');
$editor1 = $('#order-item-editor');
$editorTitle1 = $('#order-item-modal-title');

<?if($editable):?>

// Below are for modal
$('#big_category').change(function () {
    var value = $(this).val();

    if ("BN" == value) {
        $('.frame').removeClass('hide');
        $('.door').addClass('hide');
    } else if ("BS" == value) {
        $('.door').removeClass('hide');
        $('.frame').addClass('hide');
    } else {
        $('.frame, .door').addClass('hide');
    }
    $('#middle_category').val("");

});

$editor1.on('submit', function(e){
    if (this.checkValidity && !this.checkValidity()) return;
    e.preventDefault();

    var row = $modal1.data('row'),
    values = {
        ny_order_item_id: $editor1.find('#ny_order_item_id').val(),
        name: $editor1.find('#name').val(),
        progress: $editor1.find('#progress').val(),
        due_date: $editor1.find('#due_date').val(),
        big_category: $editor1.find('#big_category').val(),
        middle_category: $editor1.find('#middle_category').val(),
        window: $editor1.find('#window').val(),
        width: $editor1.find('#width').val(),
        up_window: $editor1.find('#up_window').val(),
        total_height: $editor1.find('#total_height').val(),
        amount: $editor1.find('#amount').val(),
        price: $editor1.find('#price').val(),
        wind: $editor1.find('#wind').val(),
        colour: $editor1.find('#colour').val(),
        mixed_frame: $editor1.find('#mixed_frame').val(),
        Box: $editor1.find('#box').val(),
        lock: $editor1.find('#lock').val(),
        lock_height: $editor1.find('#lock_height').val(),
        depth: $editor1.find('#depth').val(),
        threshold: $editor1.find('#threshold').val(),
        L1: $editor1.find('#l1').val(),
        L2: $editor1.find('#l2').val(),
        L3: $editor1.find('#l3').val(),
        L4: $editor1.find('#l4').val(),
        L5: $editor1.find('#l5').val(),
        f1: $editor1.find('#f1').val(),
        f2: $editor1.find('#f2').val(),
        f3: $editor1.find('#f3').val(),
        f4: $editor1.find('#f4').val()
    };

    if (row instanceof FooTable.Row){
        $.post("<?=site_url('order/update_order_item')?>", values, function (result) {
            row.val(values);
            $('.selectedRow').find('td:nth-of-type(2)').html(result['p']);
            get_order_history($('#current_ny_order_id').val());
        },'json');
    } else {
        values['ny_order_id'] = $editor1.find('#ny_order_id').val();
        values['row_number'] = $editor1.find('#row_number').val();

        $.post("<?=site_url('order/add_order_item')?>", values, function (result) {
            $('#site-order').trigger('change');
            var orderNumber = $('#original>.order-item-title').html().replace("訂單: ", "");
            getOrderItem(values['ny_order_id'], orderNumber);
            $('.selectedRow').find('td:nth-of-type(2)').html(result['p']);
            get_order_history($('#current_ny_order_id').val());
        },'json');
    }
    $modal1.modal('hide');
});
<?endif;?>
function orderItemModal(cols, rows) {
    jQuery(function($) {
        
        ft = FooTable.init('#order-item-table-original', {
            "columns": cols,
            "rows": rows,
            sorting: { enabled: true },
            filtering: { enabled: true}<?if($editable):?>,
            editing: {
                enabled: true,
                addRow: function(){
                    var row_number = $("#order-item-table-original tbody tr").length + 1;
                    if ($('#order-item-table-original tbody tr:first-of-type').hasClass('footable-empty')) {
                        row_number = 1;
                    }
                    $modal1.removeData('row');
                    $editor1[0].reset();
                    $editorTitle1.text('新增訂單明細');

                    $editor1.find('#ny_order_id').val($('#current_ny_order_id').val());
                    $editor1.find('#rank').html(row_number);
                    $editor1.find('#row_number').val(row_number);

                    $modal1.modal('show');
                },
                editRow: function(row){
                    var values = row.val();

                    $editor1.find('#ny_order_id').val($('#current_ny_order_id').val());
                    $editor1.find('#ny_order_item_id').val(values.ny_order_item_id);
                    $editor1.find('#rank').html(values.row_number);
                    $editor1.find('#row_number').val(values.row_number);
                    $editor1.find('#name').val(values.name);
                    $editor1.find('#progress').val(values.progress);
                    $editor1.find('#due_date').val(values.due_date);
                    $editor1.find('#big_category').val(values.big_category).trigger('change');
                    $editor1.find('#middle_category').val(values.middle_category);
                    $editor1.find('#window').val(values.window);
                    $editor1.find('#width').val(values.width);
                    $editor1.find('#up_window').val(values.up_window);
                    $editor1.find('#total_height').val(values.total_height);
                    $editor1.find('#amount').val(values.amount);
                    $editor1.find('#price').val(values.price);
                    $editor1.find('#wind').val(values.wind);
                    $editor1.find('#colour').val(values.colour);
                    $editor1.find('#mixed_frame').val(values.mixed_frame);
                    $editor1.find('#box').val(values.Box);
                    $editor1.find('#lock').val(values.lock);
                    $editor1.find('#lock_height').val(values.lock_height);
                    $editor1.find('#depth').val(values.depth);
                    $editor1.find('#threshold').val(values.threshold);
                    $editor1.find('#l1').val(values.L1);
                    $editor1.find('#l2').val(values.L2);
                    $editor1.find('#l3').val(values.L3);
                    $editor1.find('#l4').val(values.L4);
                    $editor1.find('#l5').val(values.L5);
                    $editor1.find('#f1').val(values.f1);
                    $editor1.find('#f2').val(values.f2);
                    $editor1.find('#f3').val(values.f3);
                    $editor1.find('#f4').val(values.f4);

                    $modal1.data('row', row);
                    $editorTitle1.text('修改訂單明細');
                    jQuery.noConflict(); 
                    $modal1.modal('show');
                },
                'alwaysShow': true,
                'allowDelete': false,
                'showText': '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> 新增修改模式',
                'hideText': '取消',
                'addText': '新增訂單'
            }<?endif;?>
        });
        $('span.caret').hide();
        $('#order-item-table-original tbody td:last-of-type').remove();
        $('#order-item-table-original').append('<div class="row">hi</div>');
        setProgressColour('#order-item-table-original tbody td:nth-of-type(4)');
  });
}

function orderItemModalTranslate(cols, rows) {
    jQuery(function($) {
        
        ft = FooTable.init('#order-item-table-translate', {
            "columns": cols,
            "rows": rows,
            sorting: { enabled: true },
            filtering: { enabled: true
            }
        });
        $('span.caret').hide();
        $('#order-item-table-translate tbody tr td:nth-of-type(3)').each(function () {
            var status = $(this).val();

            $(this).removeClass('product-status-received')
                   .removeClass('product-status-shipped');
            if ("已出貨" == status) {
                $(this).addClass('product-status-shipped');
            } else if ("收到訂單" == status) {
                $(this).addClass('product-status-received');
            }
        });
  });
}
</script>