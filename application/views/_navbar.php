<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center d-flex align-items-top justify-content-center">
          <img src="<?=base_url('img/login/logo.svg');?>" href="#" alt="logo" />
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-right">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item">
            <div class="weather-date-location">
              <p class="text-white">
                <span class="weather-location"><h4><?=date("l", time())?></h4></span>
                <span style="" class="weather-date"><?=date("Y-m-d", time())?></span>
              </p>
            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello <?=$this->session->userdata('user');?></span>
              <!--img class="img-xs rounded-circle" src="<?=base_url('img/login/face.jpg');?>" alt="Profile image"-->
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item" href="<?=site_url('login/password');?>">
                修改密碼
              </a>
              <a class="dropdown-item" href="<?=site_url('login/logout');?>">
                登出
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-right" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>