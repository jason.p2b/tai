<style>
  .tooltip.in {opacity: 1;}
  .tooltip-inner {max-width: 310px;}
  .table td img {
    width:100%;
    height:100%;
    border-radius: 3px;
  }
</style>

<div class="card">
    <div class="card-body">
		<h5 class="card-title">南亞五金表</h2>
		<div class="table-responsive">
		<table id="accessories" class="table table-hover table-striped" 
			data-paging="true" 
			data-filtering="true"
			data-sorting="true">
			<thead>
				<tr>
					<th>南亞代碼</th>
					<th>品牌</th>
					<th>類型</th>
					<th>五金型號</th>
				</tr>
			</thead>
			<tbody>
				<? foreach ($rows as $row) : ?>
					<tr>
						<td><?=$row->skid?></td>
						<td><?=$row->provider?></td>
						<td><?=$row->type?></td>
						<td><?=$row->model?></td>
					</tr>
				<? endforeach ?>
			</tbody>
		</table>
		</div>
    </div>
</div>
<script>
jQuery(function($){
	$('#accessories').footable();
	$('span.caret').hide();
});
</script>