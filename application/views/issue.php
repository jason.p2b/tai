<ul class="nav nav-tabs small">
    <li id="viewTab" class="active"><a href="#viewIssue" data-toggle="tab">現有追蹤訊息</a></li>
    <li id="addTab"><a href="#addIssue" data-toggle="tab">增加追蹤訊息</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="viewIssue">
        <h2>有關公司的追蹤訊息</h2>
        <div class="form">
            <div id="project-div" class="form-group col-md-4">
                <label for="project" class="control-label">建案名稱：</label>
                <select id="project" class="form-control">
                    <option value=""> --- </option>
                    <?php foreach ($projects as $key => $value) : ?>
                        <option value="<?=$key?>"><?=$value?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        
        <table id="companyIssueTable"
            data-show-toggle="true"
            data-show-columns="true"
            data-pagination="true"
            data-search="true"
            data-mobile-responsive="true"
            data-check-on-init="true">
            <thead>
                <tr>
                    <th class="col-md-1" data-field="tracker" data-sortable="true">工地/代號</th>
                    <th class="col-md-1" data-field="date" data-sortable="true">更新日期</th>
                    <th class="col-md-2" data-field="subject" data-sortable="true">主題</th>
                    <th class="col-md-1" data-field="to" data-sortable="true" data-filter-control="select">收件人</th>
                    <th class="col-md-1" data-field="issue_type" data-sortable="true">類別</th>
                    <th class="col-md-6" data-field="conversation" data-sortable="true">內容</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
    <div class="tab-pane" id="addIssue">
        <h2>新增追蹤訊息</h2>
        <div id="tempForm" >
        <!--form data-toggle="validator" method="post" action="<?=site_url('issue/insert');?>"-->
            <div class="form-group">
                <label for="new_project" class="control-label">建案名稱：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <select id="new_project" class="form-control" name="project" required>
                    <option value=""> --- </option>
                    <?php foreach ($projects as $key => $value) : ?>
                        <option value="<?=$key?>"><?=$value?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="new_subject" class="control-label">主題：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <input class="form-control" type="text" id="new_subject" name="subject" value="" data-error="必須填寫" required>
            </div>
            <div class="form-group">
                <label for="new_to" class="control-label">收件人：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <select name="to" id="new_to" class="form-control" required>
                    <option value=""> --- </option>
                    <? foreach ($to as $key => $value) : ?>
                        <option value="<?=$key?>"><?=$value?></option>
                    <? endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label for="type" class="control-label">類別：<span class="asterisk">*</span></label><span class="help-block with-errors"></span>
                <select class="form-control" id="type" name="type" required>
                    <option value=""> --- </option>
                    <option value="訂單修改">訂單修改</option>
                    <option value="工廠製作">工廠製作</option>
                    <option value="工地安裝">工地安裝</option>
                    <option value="其他">其他</option>
                </select>
            </div>
            <div class="form-group">
                <label for="new_attachment" class="control-label">附件：</label>
                <input id="new_attachment" name="new_file[]" type="file" class="file-loading" multiple>
            </div>
            <div class="form-group">
                <label for="new_conversation" class="control-label">內容：</label>
                <textarea class="summernote" name="new_conversation" id="new_conversation" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="確認">
                <input type="reset" class="btn btn-primary" value="清除表單">
            </div>
        </div>
        <!--/form-->
    </div>
</div>

<? include('issue_modal.php'); ?>