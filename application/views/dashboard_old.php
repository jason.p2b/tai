<style>
  /*.past { background-color: red; }*/
  #board1 {max-width: 900px; margin: auto;}
  #calendar {max-width: 900px;border: 5px solid #265a88; border-radius: 5px; padding: 10px; margin: auto;}
    #calendar-title {text-align: center;}
/* Tooltip 容器 */
.mtooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black; /* 悬停元素上显示点线 */
}
.column-in-center{
    float: none;
    margin: 0 auto;
}
/* Tooltip 文本 */
.mtooltip .mtooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    padding: 5px 5px;
    border-radius: 6px;
    bottom: 100%;
    left: 50%; 
    margin-left: -60px;
    /* 定位 */
    position: absolute;
    z-index: 1;
}
 
/* 鼠标移动上去后显示提示框 */
.mtooltip:hover .mtooltiptext {
    visibility: visible;
}

.hide {display: none;}
.mixed {
    color: red;
    font-size: 1.2em;
    animation: animate-color 2s infinite;
    -webkit-animation: animate-color 2s infinite;
}
@-webkit-keyframes animate-color {
  0% { color: white; }
  50% { color: red; }
  100% { color: white; }
}

@keyframes animate-color {
  0% { color: white; }
  50% { color: red; }
  100% { color: white; }
}
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#status">進度總覽</a></li>
                    <li><a data-toggle="tab" href="#not-shipped">未出貨</a></li>
                    <li><a data-toggle="tab" href="#changed">最近進度修改</a></li>
                </ul>
                <div class="tab-content">
                    <div id="not-shipped" class="tab-pane fade">
                        <h3>未出貨建案</h3>
                        <table data-paging="true" data-sorting="true" id="board1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                    <th data-name="progress">進度</th>
                                    <th data-name="site">建案名稱</th>
                                    <th data-name="frame">框</th>
                                    <th data-name="door">扇</th>
                                    <th data-name="customer_due_date">客戶需求日</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?foreach ($board1 as $row):?>
                                    <? if ($row['progress'] == "已出貨") continue; ?>
                                  <tr id="<?=$row['id']?>">
                                    <td><?=$row['factory_due_date'];?></td>

                                    <? if ($row['progress'] == "混合進度"):?>
                                        <td class="mtooltip">
                                            <?=$row['progress'];?>
                                            <span class="mtooltiptext"><?=$mixed_progress[$row['id']]?></span>
                                        </td>
                                    <? endif; ?>

                                    <? if ($row['progress'] != "混合進度"):?>
                                        <td><?=$row['progress'];?></td>
                                    <? endif; ?>

                                    <td><a href="<?=$row['url']?>"><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                    <td><?=$row['frame'];?></td>
                                    <td><?=$row['door'];?></td>
                                    <td><?=$row['customer_due_date'];?></td>
                                  </tr>
                                <?endforeach?>
                            </tbody>
                        </table>
                        <h3 id="calendar-title">預定出貨日</h3>
                        <div id="calendar"></div>
                    </div>
                    <div id="changed" class="tab-pane fade">
                        <div class="order-item-title" class="card-title"></div>
                        <div class="table-responsive sticky-table sticky-ltr-cells">
                            <table id="order-item-table-translate" class="pvtTable table table-hover table-striped"></table>
                        </div>
                    </div>
                    <div id="status" class="tab-pane fade in active">
                        <div class="row card-body">
                            <div class="col-md-8 col-sm-12 column-in-center">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <button id="search-type" class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">建案</button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#" onclick="changeSearchType('建案')">建案</a>
                                            <a class="dropdown-item" href="#" onclick="changeSearchType('框')">框</a>
                                            <a class="dropdown-item" href="#" onclick="changeSearchType('扇')">扇</a>
                                        </div>
                                    </div>
                                    <input type="text" id="search-input" class="form-control" aria-label="quick search" placeholder="快速搜尋">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" id="search-button" type="button">搜尋</button>
                                        <button class="btn btn-outline-secondary" id="clear-button" type="button">清除</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 table-responsive">
                                <div class="panel panel-warning">
                                    <div class="panel-heading">收到訂單</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-received">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "收到訂單" || preg_match("/收到訂單/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 table-responsive">
                                <div class="panel panel-success">
                                    <div class="panel-heading">已出貨</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-shipped">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="DESC">出貨日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "已出貨" || preg_match("/已出貨/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-warning">
                                    <div class="panel-heading">準備中</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-wait">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "等排程" || preg_match("/等排程/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> 

                            <div class="col-md-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading">發包中</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-fold">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "發包中" || preg_match("/發包中/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-md-6 table-responsive">
                                <div class="panel panel-info">
                                    <div class="panel-heading">生產中</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-production">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "生產中" || preg_match("/生產中/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 table-responsive">
                                <div class="panel panel-info">
                                    <div class="panel-heading">烤漆包裝</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-paint">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "烤漆包裝" || preg_match("/烤漆包裝/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 table-responsive">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">等SKIN板</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-skin">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "等SKIN板" || preg_match("/等SKIN板/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 table-responsive">
                                <div class="panel panel-success">
                                    <div class="panel-heading">等出貨</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-wait-ship">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "等出貨" || preg_match("/等出貨/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 table-responsive">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">等客戶回復</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-response">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "等客戶回復" || preg_match("/等客戶回復/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 table-responsive">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">訂單取消</div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="status-cancel">
                                            <thead>
                                                <tr>
                                                    <th data-name="factory_due_date" data-sorted="true" data-direction="ASC">預定完成日</th>
                                                    <th data-name="site">建案名稱</th>
                                                    <th data-name="frame">框</th>
                                                    <th data-name="door">扇</th>
                                                    <th data-name="customer_due_date">客戶需求日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?foreach ($board1 as $row):?>
                                                    <? if ($row['progress'] == "訂單取消" || preg_match("/訂單取消/", $mixed_progress[$row['id']])):?>
                                                        <tr id="a_<?=$row['id']?>">
                                                            <td><?=$row['factory_due_date'];?></td>
                                                            <td><a href="<?=$row['url']?>"><?if('混合進度'==$row['progress']):?><span class="mixed">*</span><?endif;?><?=$row['site'];?> (<?=$row['order_number']?>)</a></td>
                                                            <td><?=$row['frame'];?></td>
                                                            <td><?=$row['door'];?></td>
                                                            <td><?=$row['customer_due_date'];?></td>
                                                        </tr>
                                                    <? endif; ?>
                                                <?endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
jQuery(function($) {
    ft = FooTable.init('#board1', {sorting: {enabled: true}, paging: {enabled: true}});
    ft1 = FooTable.init('#status-received', {sorting: {enabled: true},paging: {enabled: true}});
    ft2 = FooTable.init('#status-shipped', {sorting: {enabled: true},paging: {enabled: true}});
    ft3 = FooTable.init('#status-wait', {sorting: {enabled: true},paging: {enabled: true}});
    ft4 = FooTable.init('#status-fold', {sorting: {enabled: true},paging: {enabled: true}});
    ft5 = FooTable.init('#status-production', {sorting: {enabled: true},paging: {enabled: true}});
    ft6 = FooTable.init('#status-paint', {sorting: {enabled: true},paging: {enabled: true}});
    ft7 = FooTable.init('#status-skin', {sorting: {enabled: true},paging: {enabled: true}});
    ft8 = FooTable.init('#status-wait-ship', {sorting: {enabled: true},paging: {enabled: true}});
    ft9 = FooTable.init('#status-response', {sorting: {enabled: true},paging: {enabled: true}});
    ft10 = FooTable.init('#status-cancel', {sorting: {enabled: true},paging: {enabled: true}});

    $('#calendar').fullCalendar({
        locale: 'zh-TW',
        header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,listMonth'
        },
        themeSystem: 'bootstrap4',
        businessHours: {
            dow: [ 1, 2, 3, 4, 5],
            start: '8:30',
            end: '17:30',
        },
        events:[
            <?foreach ($board1 as $row):?>
                {
                    title: '<?=$row["site"] ."(". $row["frame"] .",". $row["door"] .")"?>',
                    start: '<?=$row["factory_due_date"]?>',
                    color: '<?=$row["colour"]?>',
                    url: '<?=$row["url"]?>'
                },
            <?endforeach;?>
            {}
        ]
      });
    });

  function checkDate(val) {
    var due   = new Date(val);
    var today = new Date();

    if (due < today) {
      return true;
    }

    return false;
  }

  
// 如果有混合進度->進一步顯示狀況
$('#board1 tbody tr td:nth-of-type(2)').hover(function () {
    var txt = $(this).html();
    var id  = $(this).parent().attr('id');
    var url = "<?=site_url('dashboard/get_progress')?>";
    if ("混合進度" == txt) {
        $.post(url, {id: id}, function (result) {
            $(this).addClass('tooltip').append('<span class="tooltiptext">提示文本</span>');
        }, 'json');
    }
});

// 快速搜尋
function findSearch(id, searchText, searchType) {
    let rows = FooTable.get("#" + id).rows.all;

    rows.forEach(function(row) {

        var toMatchText = ""
        if ("建案" == searchType) {
            toMatchText = row.value.site;
        } else if ("框" == searchType) {
            toMatchText = row.value.frame;
        } else {
            toMatchText = row.value.door;
        }

        if (toMatchText != undefined && !toMatchText.match(searchText)) {
            row.$el.addClass('hide');
        } else {
            row.$el.removeClass('hide');
        }
    });
}

function changeSearchType(val) {
    $('#search-type').html(val);
}

$('#clear-button').click(function () {
    $('#search-input').val("");
    $('#status table tbody tr').removeClass('hide');
});

$('#search-button').click(function () {
    var searchText = $('#search-input').val();
    var searchType = $('#search-type').html();
    var position   = "";

    if ("" == searchText) {
        $('#status table tbody tr').removeClass('hide');
        return;
    }

    findSearch("status-received", searchText, searchType);
    findSearch("status-shipped", searchText, searchType);
    findSearch("status-wait", searchText, searchType);
    findSearch("status-fold", searchText, searchType);
    findSearch("status-production", searchText, searchType);
    findSearch("status-paint", searchText, searchType);
    findSearch("status-skin", searchText, searchType);
    findSearch("status-wait-ship", searchText, searchType);
    findSearch("status-response", searchText, searchType);
    findSearch("status-cancel", searchText, searchType);

})
</script>