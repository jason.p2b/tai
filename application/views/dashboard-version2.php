<style>
  /*.past { background-color: red; }*/
  #board1 {max-width: 900px; margin: auto;}
  #calendar {max-width: 900px;border: 5px solid #265a88; border-radius: 5px; padding: 10px; margin: auto;}
    #calendar-title {text-align: center;}
/* Tooltip 容器 */
.mtooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black; /* 悬停元素上显示点线 */
}
.column-in-center{
    float: none;
    margin: 0 auto;
}
/* Tooltip 文本 */
.mtooltip .mtooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    padding: 5px 5px;
    border-radius: 6px;
    bottom: 100%;
    left: 50%; 
    margin-left: -60px;
    /* 定位 */
    position: absolute;
    z-index: 1;
}
 
/* 鼠标移动上去后显示提示框 */
.mtooltip:hover .mtooltiptext {
    visibility: visible;
}

.hide {display: none;}
.mixed {
    color: red;
    font-size: 1.2em;
    animation: animate-color 2s infinite;
    -webkit-animation: animate-color 2s infinite;
}
@-webkit-keyframes animate-color {
  0% { color: white; }
  50% { color: red; }
  100% { color: white; }
}

@keyframes animate-color {
  0% { color: white; }
  50% { color: red; }
  100% { color: white; }
}

#factory-one, #factory-two, #factory-new {
    background-color: #737373;
    border-radius: 5px;
    font-weight: bold;
    color: #ffffff;
    text-align: center;
    margin: 2px;
}
#factory-two {background-color: #7fc224;}
#factory-new {background-color: #e6814d;}
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li><a data-toggle="tab" href="#status">進度總覽</a></li>
                    <li class="active"><a data-toggle="tab" href="#not-shipped">未出貨進度總表</a></li>
                    <li><a data-toggle="tab" href="#date_progress">日期進度表</a></li>
                    <li><a data-toggle="tab" href="#changed">排程表</a></li>
                </ul>
                <div class="tab-content">
                    <div id="status" class="tab-pane fade">
                        <? include "_dashboard_status.php"?>
                    </div>
                    <div id="not-shipped" class="tab-pane fade in active">
                        <? include "_dashboard_not_shipped.php" ?>
                    </div>
                    <div id="date_progress" class="tab-pane fade">
                        <? include "_dashboard_date_progress.php"; ?>
                    </div>
                    <div id="changed" class="tab-pane fade">
                        <div class="row mt-3 mb-3">
                            <div class="col-sm-12">
                                <div class="col-sm-1" id="factory-one">一廠</div>
                                <div class="col-sm-1" id="factory-two">二廠</div>
                                <div class="col-sm-1" id="factory-new">新廠</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=Asia%2FTaipei&amp;src=Y3ZraWhkZWRmcWcyN21ldWJkNGQ3N2pwOThAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=bHZsbGo0YjU4djJqcnQ0NjU1dm45bjVxY2tAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=YmdrM2gwcXFvdjY0cTNqOHA1aWtwMGtwcm9AZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=emgudGFpd2FuI2hvbGlkYXlAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23616161&amp;color=%237CB342&amp;color=%23F4511E&amp;color=%237986CB" style="border-width:0" width="100%" height="800px" frameborder="0" scrolling="no"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

function changeHighlight(id) {
    var $checkbox = $('#'+id);
    var cls = $('#d'+id).attr('class');
    var $hl = $('.'+id);

    if ($checkbox.prop('checked')) {
        $hl.removeClass('text-muted').addClass(cls);
    } else {
        $hl.addClass('text-muted').removeClass(cls);
    }
}


function getTable(table, progress) {
    $.post('<?=base_url('dashboard/get_progress')?>',{p:progress},function(data) {
        ft = FooTable.init('#'+table,{
            "columns": data['cols'],
            "rows": data['rows'],
            sorting: { enabled: true },
            paging: { enabled: true}
        });
    },'json');
}

function getBoard(table, type) {
    $.post('<?=base_url('dashboard/get_progress_board')?>',{t:type},function(data) {
        ft = FooTable.init('#'+table,{
            "columns": data['cols'],
            "rows": data['rows'],
            sorting: { enabled: true }
        });
        
        let today = new Date();
        $('#'+table+' tbody tr td:nth-child(4)').each(function () {
            let dueDate = new Date($(this).text());
            
            if (today > dueDate || $(this).text() == "0000-00-00") {
                $(this).addClass("bg-danger");
            }
        });
        
        $('#'+table+' thead tr th:nth-child(4)').trigger('click');
    },'json');
}

$(document).ready(function() {
    getTable('table-received','收到訂單');
    getTable('table-shipped','已出貨');
    getTable('table-wait','等排程');
    getTable('table-fold','發包中');
    getTable('table-production','生產中');
    getTable('table-paint','烤漆包裝');
    getTable('table-skin','等SKIN板');
    getTable('table-wait-ship','等出貨');
    getTable('table-response','等客戶回復');
    getTable('table-cancel','取消訂單');

    getBoard('table-progress-frame', 'frame');
    getBoard('table-progress-door', 'door');

});

</script>