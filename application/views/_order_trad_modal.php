<?if($editable):?>
<!-- Begin Popup Modal -->
<div class="modal fade" id="trad-item-modal" tabindex="-1" role="dialog" aria-labelledby="trad-item-modal-title">

    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content form-horizontal" id="trad-item-editor">
            <div class="modal-header">
                <h4 class="modal-title" id="trad-item-modal-title">新增訂單內容</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="modal-body">
                <input type="number" id="trad_order_item_id" name="trad_order_item_id" class="hidden" value=""/>
                <input type="number" id="ny_order_id" name="ny_order_id" class="hidden" value=""/>
                <input type="number" id="row_number" name="row_number" class="hidden" value=""/>
    
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-row form-group required">
                            <div class="col-sm-6">
                                <label for="order_name" class="control-label">訂單編號</label>
                                <input type="text" class="form-control" id="order_name" name="order_name" placeholder="訂單編號" required>
                            </div>
                            <div class="col-sm-6">
                                <label  class="control-label" for="name">品項</label>
                                <div class="input-group">
                                    <span id="rank" class="input-group-text">1</span>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="品項" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-row form-group required">
                            <div class="col-sm-4">
                                <label for="progress" class="control-label">進度</label>
                                <select name="progress" id="progress" class="form-control" required>
                                    <option value="收到訂單">收到訂單</option>
                                    <option value="發包中">發包中</option>
                                    <option value="生產中">生產中</option>
                                    <option value="烤漆包裝">烤漆包裝</option>
                                    <option value="已出貨">已出貨</option>
                                    <option value="等排程">等排程</option>
                                    <option value="等出貨">等出貨</option>
                                    <option value="等SKIN板">等SKIN板</option>
                                    <option value="等客戶回復">等客戶回復</option>
                                    <option value="取消訂單">取消訂單</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="name" class="control-label">交貨日</label>
                                <input type="text" class="form-control" id="due_date" name="due_date" placeholder="交貨日" required>
                            </div>
                            <div class="col-sm-4">
                                <label for="standard" class="control-label">防火規範</label>
                                <select name="standar" id="standard" class="form-control" required>
                                    <option value="CNS 11227">CNS 11227</option>
                                    <option value="CNS 11227-1">CNS 11227-1</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-group required">
                            <div class="col-sm-2">
                                <label for="door_frame" class="control-label">框／扇</label>
                                <select name="door_frame" id="door_frame" class="form-control" required>
                                    <option value="框">框</option>
                                    <option value="扇">扇</option>
                                </select>
                            </div>
                            <div id="coupled_change" class="col-sm-4">
                                <label for="coupled" class="control-label">單／雙</label>
                                <select name="coupled" id="coupled" class="form-control" required>
                                    <option value="單開">單開</option>
                                    <option value="雙開">雙開</option>
                                </select>
                            </div>
                            <div id="coupled_mother" class="col-sm-2 hide">
                                <label for="mother_distance" class="control-label">母扇寬(mm)</label>
                                <input type="text" class="form-control" id="mother_distance" name="mother_distance" placeholder="母扇寬度" value="0">
                            </div>
                            <div id="is-mother-div" class="col-sm-2 hide">
                                <label for="is_mother" class="control-label">子/母扇</label>
                                <select name="is_mother" id="is_mother" class="form-control">
                                    <option value=""></option>
                                    <option value="母">母</option>
                                    <option value="子">子</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="type" class="control-label">類型</label>
                                <select name="type" id="type" class="form-control" required>
                                        <option value=""> --- </option>
                                        <option value="防火(60A)">防火(60A)</option>
                                        <option value="防火(60B)">防火(60B)</option>
                                        <option value="四面框(60A)">四面框(60A)</option>
                                        <option value="四面框(60B)">四面框(60B)</option>
                                        <option value="防火(120A)">防火(120A)</option>
                                        <option value="玄關">玄關</option>
                                        <option value="180度">180度</option>
                                        <option value="270度">270度</option>
                                        <option value="台電">台電</option>
                                        <option value="非防火">非防火</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="colour" class="control-label">顏色</label>
                                <input type="text" class="form-control" id="colour" name="colour" placeholder="顏色" required>
                            </div>
                        </div>

                        <div class="form-row form-group required">
                            <div class="col-sm-4">
                                <label  class="control-label" for="width">寬度</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="width" name="width" placeholder="寬度" required>
                                    <span class="input-group-text">mm</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label  class="control-label" for="height">高度(含埋入深度)</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="height" name="height" placeholder="高度" required>
                                    <span class="input-group-text">mm</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label  class="control-label" for="depth">埋入深度</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="depth" name="depth" placeholder="埋入深度" required>
                                    <span class="input-group-text">mm</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-row form-group required">
                            <div class="col-sm-4">
                                <label  class="control-label" for="amount_t">總數量</label>
                                <input type="number" class="form-control" id="amount_t" value="0" disabled>
                                    
                            </div>
                            <div class="col-sm-4">
                                <label for="amount_l" class="control-label">數量 - 左(L)</label>
                                <input type="number" class="form-control" id="amount_l" name="amount_l" placeholder="數量 - 左(L)" value="0" required>
                            </div>
                            <div class="col-sm-4">
                                <label for="amount_r" class="control-label">數量 - 右(R)</label>
                                <input type="number" class="form-control" id="amount_r" name="amount_r" placeholder="數量 - 右(R)" value="0" required>
                            </div>
                        </div>
                        <div class="form-row form-group">
                            <div class="col-sm-4 required">
                                <label for="lock" class="control-label">門鎖</label>
                                <select name="lock" id="lock" class="form-control" required>
                                    <option value=""> --- </option>
                                    <?foreach($lock as $k => $v):?>
                                        <option value="<?=$k?>"><?=$v?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="handle" class="control-label">把手</label>
                                <select name="handle" id="handle" class="form-control">
                                    <option value=""> --- </option>
                                    <?foreach($handle as $k => $v):?>
                                        <option value="<?=$k?>"><?=$v?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                            <div class="col-sm-4 required">
                                <label  class="control-label" for="handle_height">把手高度 <span class="text text-danger">(0 = 授權給工廠決定)</span></label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="handle_height" name="handle_height" placeholder="設'0'為工廠決定"  required>
                                    <span class="input-group-text">mm</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-row form-group">
                            <div class="col-sm-4 required">
                                <label for="hinge" class="control-label">鉸鏈</label>
                                <select name="hinge" id="hinge" class="form-control" required>
                                    <option value=""> --- </option>
                                    <?foreach($hinge as $k => $v):?>
                                        <option value="<?=$k?>"><?=$v?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="closer" class="control-label">門弓器</label>
                                <select name="closer" id="closer" class="form-control">
                                    <option value=""> --- </option>
                                    <?foreach($closer as $k => $v):?>
                                        <option value="<?=$k?>"><?=$v?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="bolt" class="control-label">天地栓</label>
                                <select name="bolt" id="bolt" class="form-control">
                                    <option value=""> --- </option>
                                    <?foreach($bolt as $k => $v):?>
                                        <option value="<?=$k?>"><?=$v?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-row form-group">
                            <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">陽極鎖</label>
                                        <div class="form-check form-check-flat">
                                            <label for="fail_safe_lock" class="form-check-label">
                                                <input type="checkbox" class="form-check-input" id="fail_safe_lock" name="fail_safe_lock" value="yes"> 開孔(母扇)
                                                <i class="input-helper"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 fail-safe-lock-property">
                                        <label for="fail_safe_lock" class="control-label">距離(中心到門扇)</label>
                                        <input type="text" id="fail_safe_lock_distance" class="form-control" name="fail_safe_lock_distance" value="100">
                                    </div>
                                    <div class="col-sm-12 fail-safe-lock-property">
                                        <div class="form-check form-check-flat">
                                            <label for="fail_safe_lock_son" class="form-check-label">
                                                <input type="checkbox" class="form-check-input" id="fail_safe_lock_son" name="fail_safe_lock_son" value="1"> 雙扇都要
                                                <i class="input-helper"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">磁黃孔</label>
                                        <div class="form-check form-check-flat">
                                            <label for="magnetic_hole" class="form-check-label">
                                                <input type="checkbox" class="form-check-input" id="magnetic_hole" name="magnetic_hole" value="yes"> 開孔(母扇)
                                                <i class="input-helper"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 magnetic-hole-property">
                                        <label for="magnetic_hole" class="control-label">距離(門扇算起)</label>
                                        <input type="text" id="magnetic_hole_distance" class="form-control" name="magnetic_hole_distance" value="100">
                                    </div>
                                    <div class="col-sm-12 magnetic-hole-property">
                                        <div class="form-check form-check-flat">
                                            <label for="magnetic_hole_son" class="form-check-label">
                                                <input type="checkbox" class="form-check-input" id="magnetic_hole_son" name="magnetic_hole_son" value="1"> 雙扇都要
                                                <i class="input-helper"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">下降條</label>
                                        <select name="falling_strip" id="falling_strip" class="form-control">
                                            <option value="-"> --- </option>
                                            <option value="外掛式">外掛式</option>
                                            <option value="隱藏式">隱藏式</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">龍吐珠</label>
                                        <div class="form-check form-check-flat">
                                            <label for="ball" class="form-check-label">
                                                <input type="checkbox" class="form-check-input" id="ball" name="ball" value="yes"> 加龍吐珠
                                                <i class="input-helper"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row form-group">
                            <div class="col-sm-6">
                                <label  class="control-label" for="material">材質</label>
                                <div class="input-group">
                                    <select name="material" class="form-control" id="material">
                                        <option value="SECC 1.0t">SECC 1.0t</option>
                                        <option value="SECC 1.2t">SECC 1.2t</option>
                                        <option value="SECC 1.6t">SECC 1.6t</option>
                                        <option value="SUS 1.2t">SUS 1.2t</option>
                                        <option value="SUS 1.0t">SUS 1.0t</option>
                                        <option value="SUS 1.5t">SUS 1.5t</option>
                                        <option value="SUS 1.5t(2B)">SUS 1.5t(2B)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label  class="control-label" for="door_thickness">門厚(裸扇)</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="door_thickness" name="door_thickness" placeholder="門厚" value="49" required>
                                    <span class="input-group-text">mm</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-row form-group" id="frame_info">
                            <div class="col-md-12 col-lg-8">
                                <label class="control-label" for="frame_type">框型</label>
                                <div id="frames">
                                    <input type="hidden" name="frame_shape" value="la_frame" id="frame_shape" >
                                    <ul>
                                        <li id="la_frame" class="my-frame frames_active" style="display: block;" onclick="selectFrame('la_frame');">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="la_frame" style="height:160px;">
                                            <marker id="arrowStart1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000 !important;"></path>
                                            </marker>
                                            <marker id="arrowEnd1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto-start-reverse">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000 !important;"></path>
                                            </marker>
                                            <g class="vertical_cut">
                                                <!-- SHAPE -->
                                                <path class="shapes" d="M76,59 76,56 56,56 56,106 151,106 151,119 136,119 136,136 256,136 256,56 236,56
                                                            236,59 253,59 253,133 139,133 139,122 154,122 154,103 59,103 59,59 76,59Z"></path>

                                                        <!-- LABEL PATHS -->                   
                                                <path class="labels" d="M56,53 56,41 M76,53 76,41
                                                                        M56,38 56,20 M256,53 256,20
                                                                        M33,56 53,56 M33,106 53,106
                                                                        M259,56 279,56 M259,136 279,136
                                                                        M136,100 136,85 M151,100 151,85"></path>

                                                <!--path class="labels" d="M 56,109  56,159 M136,139 136,159" /-->

                                                <path class="arrows1" d="M60,46 72,46"></path>
                                                <path class="arrows1" d="M60,25 252,25"></path>
                                                <path class="arrows1" d="M36,60 36,102"></path>
                                                <!--path class="arrows1" d="M60,156 132,156" /-->
                                                <path class="arrows1" d="M276,60 276,132"></path>
                                                <path class="arrows1" d="M140,89 147,89"></path>

                                                <!-- LABEL TEXT -->
                                                <text class="svg_text1" id="v_north_length" x="66" y="43">B</text>
                                                <text class="svg_text1" id="v_total_length" x="156" y="22">A</text>
                                                <text class="svg_text1" id="v_west_length" x="33" y="81" transform="rotate(270 33,81)">C</text>
                                                <!--text class="svg_text1" x="96" y="153">55</text-->
                                                <text class="svg_text1" id="v_east_length" x="273" y="96" transform="rotate(270 273,96)">D</text>
                                                <text class="svg_text1" x="143" y="85">13</text>
                                                    </g>
                                            </svg>
                                        </li>
                                        <li id="l_frame" class="my-frame" style="display: block;" onclick="selectFrame('l_frame');">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="l_frame" style="height:160px;">
                                            <marker id="arrowStart1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;"></path>
                                            </marker>
                                            <marker id="arrowEnd1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto-start-reverse">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;"></path>
                                            </marker>
                                            <g class="vertical_cut">
                                                <!-- SHAPE -->
                                                <path class="shapes" d="M 76,59  76,56  56, 56  56,106 136,106 136,136 256,136 256,56 236,56
                                                            236,59 253,59 253,133 139,133 139,103  59,103  59, 59  76,59Z"></path>

                                                        <!-- LABEL PATHS -->                   
                                                <path class="labels" d="M 56, 53  56, 41 M 76, 53  76, 41
                                                                        M 56, 38  56, 20 M256, 53 256, 20
                                                                        M 33, 56  53, 56 M 33,106  53,106
                                                                        M259, 56 279, 56 M259,136 279,136"></path>

                                                <!--path class="labels" d="M 56,109  56,159 M136,139 136,159" /-->

                                                <path class="arrows1" d="M 60, 46  72, 46"></path>
                                                <path class="arrows1" d="M 60, 25 252, 25"></path>
                                                <path class="arrows1" d="M 36, 60  36,102"></path>
                                                <!--path class="arrows1" d="M 60,156 132,156" /-->
                                                <path class="arrows1" d="M276, 60 276,132"></path>

                                                <!-- LABEL TEXT -->
                                                <text class="svg_text1" id="v_north_length" x="66" y="43">B</text>
                                                <text class="svg_text1" id="v_total_length" x="156" y="22">A</text>
                                                <text class="svg_text1" id="v_west_length" x="33" y="81" transform="rotate(270 33,81)">C</text>
                                                <!--text class="svg_text1" x="96" y="153">55</text-->
                                                <text class="svg_text1" id="v_east_length" x="273" y="96" transform="rotate(270 273,96)">D</text>
                                            </g>
                                            </svg>
                                        </li>
                                        <li id="ta_frame" class="my-frame" style="display: block;" onclick="selectFrame('ta_frame');">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="ta_frame" style="height:160px;">
                                            <marker id="arrowStart1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;"></path>
                                            </marker>
                                            <marker id="arrowEnd1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto-start-reverse">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;"></path>
                                            </marker>
                                            <g class="vertical_cut">
                                                <!-- SHAPE -->
                                                <path class="shapes" d="M76,59 76,56 56,56 56,106 151,106 151,119 136,119 136,136 226,136 226,106 256,106 256,56 236,56
                                                            236,59 253,59 253,103 223,103 223,133 139,133 139,122 154,122 154,103 59,103 59,59 76,59Z"></path>

                                                        <!-- LABEL PATHS -->                   
                                                <path class="labels" d="M56,53 56,41 M76,53 76,41
                                                                        M56,38 56,20 M256,53 256,20
                                                                        M33,56 53,56 M33,106 53,106
                                                                        M259,56 279,56 M259,136 279,136
                                                                        M136,100 136,85 M151,100 151,85
                                                                        M226,139 226,159 M256,139 256,159"></path>

                                                <!--path class="labels" d="M 56,109  56,159 M136,139 136,159" /-->

                                                <path class="arrows1" d="M60,46 72,46"></path>
                                                <path class="arrows1" d="M60,25 252,25"></path>
                                                <path class="arrows1" d="M36,60 36,102"></path>
                                                <!--path class="arrows1" d="M60,156 132,156" /-->
                                                <path class="arrows1" d="M276,60 276,132"></path>
                                                <path class="arrows1" d="M140,89 147,89"></path>
                                                <path class="arrows1" d="M230,156 252,156"></path>

                                                <!-- LABEL TEXT -->
                                                <text class="svg_text1" id="v_north_length" x="66" y="43">B</text>
                                                <text class="svg_text1" id="v_total_length" x="156" y="22">A</text>
                                                <text class="svg_text1" id="v_west_length" x="33" y="81" transform="rotate(270 33,81)">C</text>
                                                <!--text class="svg_text1" x="96" y="153">55</text-->
                                                <text class="svg_text1" id="v_east_length" x="273" y="96" transform="rotate(270 273,96)">D</text>
                                                <text class="svg_text1" x="143" y="85">13</text>
                                                <text class="svg_text1" id="v_south_east_length" x="241" y="153">E</text>
                                            </g>
                                            </svg>  
                                        </li>
                                        <li id="t_frame" class="my-frame" style="display: block;" onclick="selectFrame('t_frame');">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="t_frame" style="height:160px;">
                                            <marker id="arrowStart1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;"></path>
                                            </marker>
                                            <marker id="arrowEnd1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto-start-reverse">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;"></path>
                                            </marker>
                                            <g class="vertical_cut">
                                                <!-- SHAPE -->
                                                <path class="shapes" d="M76,59 76,56 56,56 56,106 136,106 136,136 226,136 226,106 256,106 256,56 236,56
                                                            236,59 253,59 253,103 223,103 223,133 139,133 139,122 139,103 59,103 59,59 76,59Z"></path>
                                                
                                                        
                                                <!-- LABEL PATHS -->                   
                                                <path class="labels" d="M56,53 56,41 M76,53 76,41
                                                                        M56,38 56,20 M256,53 256,20
                                                                        M33,56 53,56 M33,106 53,106
                                                                        M259,56 279,56 M259,136 279,136
                                                                        M226,139 226,159 M256,139 256,159"></path>

                                                <!--path class="labels" d="M 56,109  56,159 M136,139 136,159" /-->

                                                <path class="arrows1" d="M60,46 72,46"></path>
                                                <path class="arrows1" d="M60,25 252,25"></path>
                                                <path class="arrows1" d="M36,60 36,102"></path>
                                                <!--path class="arrows1" d="M60,156 132,156" /-->
                                                <path class="arrows1" d="M276,60 276,132"></path>
                                                <path class="arrows1" d="M230,156 252,156"></path>

                                                <!-- LABEL TEXT -->
                                                <text class="svg_text1" id="v_north_length" x="66" y="43">B</text>
                                                <text class="svg_text1" id="v_total_length" x="156" y="22">A</text>
                                                <text class="svg_text1" id="v_west_length" x="33" y="81" transform="rotate(270 33,81)">C</text>
                                                <!--text class="svg_text1" x="96" y="153">55</text-->
                                                <text class="svg_text1" id="v_east_length" x="273" y="96" transform="rotate(270 273,96)">D</text>
                                                <text class="svg_text1" id="v_south_east_length" x="241" y="153">E</text>
                                            </g>
                                            </svg>
                                        </li>
                                        <li id="120_frame" class="my-frame" style="display:block" onclick="selectFrame('120_frame');">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  class="120_frame" style="height:160px;">
                                                <marker id="arrowStart1" markerWidth="7" markerHeight="7" refx="2" refy="4" orient="auto">
                                                    <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;" />
                                                </marker>
                                                <marker id="arrowEnd1" markerWidth="7" markerHeight="7" refx="2" refy="4" orient="auto-start-reverse">
                                                    <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;" />
                                                </marker>

                                                <g class="vertical_cut" >
                                                    <!-- SHAPE -->
                                                    <path   class="shapes"
                                                            d="M  76, 59  76, 56  56, 56  56,81 70,81 70,106 136,106 136,121 151,121 151,128 136,128 
                                                                 136,136 256,136 256, 56 236, 56 236, 59 253, 59 253,133 139,133 139,131 
                                                                 154,131 154,118 139,118 139,103  73,103 73,78 59,78 59, 59  76,59Z" />
                                                    <!-- LABEL PATHS -->                   
                                                    <path class="labels" d="M 56, 53  56, 41 M 76, 53  76, 41
                                                                            M 56, 38  56, 20 M256, 53 256, 20
                                                                            M 33, 56  53, 56 M 33,106  53,106
                                                                            M259, 56 279, 56 M259,136 279,136
                                                                            M 33,121 133,121 M 33,136 133,136
                                                                            M136,100 136, 90 M151,115 151, 90" />

                                                    <path class="arrows1" d="M 60, 46  72, 46" />
                                                    <path class="arrows1" d="M 60, 25 252, 25" />
                                                    <path class="arrows1" d="M 36, 60  36,102" />
                                                    <path class="arrows1" d="M276, 60 276,132" />
                                                    <path class="arrows1" d="M 36,124  36,133" />
                                                    <path class="arrows1" d="M140, 95 147, 95" />

                                                    <!-- LABEL TEXT -->
                                                    <text class="svg_text1" x="35" y="10">防火(120A)框</text>
                                                    <text class="svg_text1" id="v_north_length" x="66"  y="43">B</text>
                                                    <text class="svg_text1" id="v_total_length" x="156" y="22">A</text>
                                                    <text class="svg_text1" id="v_west_length"  x="33"  y="81" transform="rotate(270 33,81)">C</text>
                                                    <text class="svg_text1" id="v_west_length"  x="33"  y="129" transform="rotate(270 33,129)">15</text>
                                                    <text class="svg_text1" id="v_east_length"  x="273" y="96" transform="rotate(270 273,96)">D</text>
                                                    <text class="svg_text1" x="143" y="90">13</text>
                                                </g>
                                            </svg>
                                        </li>
                                        <li id="entrance_frame" class="my-frame" style="display: block;" onclick="selectFrame('entrance_frame');">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="entrance_frame" style="height:160px;">
                                            <marker id="arrowStart1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;"></path>
                                            </marker>
                                            <marker id="arrowEnd1" markerWidth="7" markerHeight="7" refX="2" refY="4" orient="auto-start-reverse">
                                                <path d="M2,2 L2,6 L5,4 L2,2" style="fill: #ff0000;"></path>
                                            </marker>
                                            <g class="vertical_cut">
                                                <!-- SHAPE -->
                                                <path class="shapes" d="M76,59 76,56 56,56 56,70 66,70 66,106 151,106 151,119 136,119 136,136 226,136 226,106 256,106 256,56 236,56
                                                            236,59 253,59 253,103 223,103 223,133 139,133 139,122 154,122 154,103 69,103 69,67 59,67 59,59 76,59Z"></path>

                                                        <!-- LABEL PATHS -->                   
                                                <path class="labels" d="M56,53 56,41 M76,53 76,41
                                                                        M56,38 56,20 M256,53 256,20
                                                                        M33,56 53,56 M33,106 53,106
                                                                        M259,56 279,56 M259,136 279,136
                                                                        M136,100 136,85 M151,100 151,85
                                                                        M226,139 226,159 M256,139 256,159"></path>

                                                <!--path class="labels" d="M 56,109  56,159 M136,139 136,159" /-->

                                                <path class="arrows1" d="M60,46 72,46"></path>
                                                <path class="arrows1" d="M60,25 252,25"></path>
                                                <path class="arrows1" d="M36,60 36,102"></path>
                                                <!--path class="arrows1" d="M60,156 132,156" /-->
                                                <path class="arrows1" d="M276,60 276,132"></path>
                                                <path class="arrows1" d="M140,89 147,89"></path>
                                                <path class="arrows1" d="M230,156 252,156"></path>

                                                <!-- LABEL TEXT -->
                                                <text class="svg_text1" x="20" y="10">玄關框</text>
                                                <text class="svg_text1" x="66" y="43">30</text>
                                                <text class="svg_text1" id="v_total_length" x="156" y="22">A</text>
                                                <text class="svg_text1" id="v_west_length" x="33" y="81" transform="rotate(270 33,81)">C</text>
                                                <!--text class="svg_text1" x="96" y="153">55</text-->
                                                <text class="svg_text1" id="v_east_length" x="273" y="96" transform="rotate(270 273,96)">D</text>
                                                <text class="svg_text1" x="143" y="85">13</text>
                                                <text class="svg_text1" x="241" y="153">56</text>
                                            </g>
                                            </svg>  
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label"> </label>
                                <div class="input-group">
                                    <span class="input-group-text">A</span>
                                    <input class="form-control" type="number" id="v_total_length" name="v_total_length" value="" min="" onkeyup="changeSVGvalue(this);">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-text">B</span>
                                    <input class="form-control" type="number" id="v_north_length" name="v_north_length" value="" min="" onkeyup="changeSVGvalue(this);">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-text">C</span>
                                    <input class="form-control" type="number" id="v_west_length" name="v_west_length" value="" min="0" onkeyup="changeSVGvalue(this);">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-text">D</span>
                                    <input class="form-control" type="number" id="v_east_length" name="v_east_length" value="" min="0" onkeyup="changeSVGvalue(this);">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-text">E</span>
                                    <input class="form-control" type="number" id="v_south_east_length" name="v_south_east_length" value="" min="0" onkeyup="changeSVGvalue(this);">
                                </div>
                            </div>
                        </div>
                        <div class="form-row form-group">
                            <div class="col-sm-12">
                                <label for="note" class="control-label">備註</label>
                                <textarea class="form-control rounded-0" name="note" id="note" rows="2"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">確認</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </form>
    </div>
</div>
<!-- End Popup Modal -->
<?endif;?>

<script>
<?if($editable):?>
function tradModalCheckboxToTable(val) {
    if ("1" == val) {
        return "yes";
    } else {
        return "";
    }
}

function checkTradModalCheckbox(id, val) {
    if ("yes" == val) {
        // $('#' + id).attr('checked', 'true');
        $('#' + id).trigger('click');
    }// else {
    //     $('#' + id).removeAttr('checked');
    // }
}

function getCheckBoxValue(id) {
    if ($('#'+id).is(":checked")) {
        return "1";
    }
    return "0"
}

$('#type').change(function() {
    var $val = $(this).val();
    var $thickness = 49;

    if ("防火(60B)" == $val) {
        $thickness = 42;
    } else if ("四面框(60B)" == $val) {
        $thickness = 42;
    } else if ("非防火" == $val) {
        $thickness = 42;
    } else if ("防火(120A)" == $val) {
        $thickness = 61;
    }
    $('#door_thickness').val($thickness);
});

$('#door_frame, #coupled').change(function () {
    var $val = $('#coupled').val();

    if ("雙開" == $val && $('#door_frame').val() == "框") {
        $('#coupled_change').removeClass('col-sm-4').addClass('col-sm-2');
        $('#is-mother-div').addClass('hide');
        $('#coupled_mother').removeClass('hide').find('input').trigger('select');
    } else if ("雙開" == $val && $('#door_frame').val() == "扇") {
        $('#coupled_change').removeClass('col-sm-4').addClass('col-sm-2');
        $('#is-mother-div').removeClass('hide');
        $('#coupled_mother').addClass('hide')
    } else {
        $('#is-mother-div').addClass('hide');
        $('#coupled_mother').addClass('hide');
        $('#coupled_change').removeClass('col-sm-2').addClass('col-sm-4');
    }
});

// $('#door_frame, #coupled').change(function () {
//     var $val = $('#coupled').val();

//     if ("雙開" == $val && $('#door_frame').val() == "扇") {
//         $('#coupled_change').removeClass('col-sm-4').addClass('col-sm-2');
//         $('#is-mother-div').removeClass('hide');
//     } else {
//         $('#is-mother-div').addClass('hide');
//         $('#coupled_change').removeClass('col-sm-2').addClass('col-sm-4');
//     }
// });

$('#magnetic_hole').click(function () {
    var $v = $(this).is(':checked');
    if ($v) {
        $('.magnetic-hole-property').slideDown();
        $('#magnetic_hole_distance').trigger('select');
    } else {
        $('.magnetic-hole-property').slideUp();
    }
});
$('#fail_safe_lock').click(function () {
    var $v = $(this).is(':checked');
    if ($v) {
        $('.fail-safe-lock-property').slideDown();
        $('#fail_safe_lock_distance').trigger('select');
    } else {
        $('.fail-safe-lock-property').slideUp();
    }
});

<?endif;?>
// Used for footable (traditional order modal)
$trad_m = $('#trad-item-modal');
$trad_e = $('#trad-item-editor');
$trad_t = $('#trad-item-modal-title');

<?if($editable):?>
$trad_e.on('submit', function(e){
    if (this.checkValidity && !this.checkValidity()) return;
    e.preventDefault();

    var row = $trad_m.data('row'),
    values = {
        trad_order_item_id: $trad_e.find('#trad_order_item_id').val(),
        order_name: $trad_e.find('#order_name').val(),
        name: $trad_e.find('#name').val(),
        progress: $trad_e.find('#progress').val(),
        due_date: $trad_e.find('#due_date').val(),
        standard: $trad_e.find('#standard').val(),
        door_frame: $trad_e.find('#door_frame').val(),
        coupled: $trad_e.find('#coupled').val(),
        mother_distance: $trad_e.find('#mother_distance').val(),
        type: $trad_e.find('#type').val(),
        colour: $trad_e.find('#colour').val(),
        width: $trad_e.find('#width').val(),
        height: $trad_e.find('#height').val(),
        depth: $trad_e.find('#depth').val(),
        amount_l: $trad_e.find('#amount_l').val(),
        amount_r: $trad_e.find('#amount_r').val(),
        lock: $trad_e.find('#lock').val(),
        handle: $trad_e.find('#handle').val(),
        handle_height: $trad_e.find('#handle_height').val(),
        hinge: $trad_e.find('#hinge').val(),
        closer: $trad_e.find('#closer').val(),
        bolt: $trad_e.find('#bolt').val(),
        door_thickness: $trad_e.find('#door_thickness').val(),
        v_total_length: $trad_e.find('input[name=v_total_length]').val(),
        v_north_length: $trad_e.find('input[name=v_north_length]').val(),
        v_east_length: $trad_e.find('input[name=v_east_length]').val(),
        v_west_length: $trad_e.find('input[name=v_west_length]').val(),
        v_south_east_length: $trad_e.find('input[name=v_south_east_length]').val(),
        note: $trad_e.find('#note').val(),
        frame_shape: $trad_e.find('#frame_shape').val(),
        fail_safe_lock: getCheckBoxValue('fail_safe_lock'),
        fail_safe_lock_distance: $trad_e.find('#fail_safe_lock_distance').val(),
        fail_safe_lock_son: getCheckBoxValue('fail_safe_lock_son'),
        magnetic_hole: getCheckBoxValue('magnetic_hole'),
        magnetic_hole_distance: $trad_e.find('#magnetic_hole_distance').val(),
        magnetic_hole_son: getCheckBoxValue('magnetic_hole_son'),
        falling_strip: $trad_e.find('#falling_strip').val(),
        ball: getCheckBoxValue('ball'),
        material: $trad_e.find('#material').val(),
        is_mother: $trad_e.find('#is_mother').val()
    };

    if (row instanceof FooTable.Row){
        $.post("<?=site_url('order/update_trad_order_item')?>", values, function (result) {
            values.fail_safe_lock = tradModalCheckboxToTable(values.fail_safe_lock);
            values.fail_safe_lock_son = tradModalCheckboxToTable(values.fail_safe_lock_son);
            values.magnetic_hole = tradModalCheckboxToTable(values.magnetic_hole);
            values.magnetic_hole_son = tradModalCheckboxToTable(values.magnetic_hole_son);
            values.ball = tradModalCheckboxToTable(values.ball);

            if (values.frame_shape == "l_frame") {
                values.frame_shape = "L型框";
            } else if (values.frame_shape == "la_frame") {
                values.frame_shape = "L型氣密框";
            } else if (values.frame_shape == "t_frame") {
                values.frame_shape = "凸型框";
            } else if (values.frame_shape == "ta_frame") {
                values.frame_shape = "凸型氣密框";
            } else if (values.frame_shape == "120_frame") {
                values.frame_shape = "120A框";
            } else if (values.frame_shape == "entrance_frame") {
                values.frame_shape = "玄關框";
            }

            row.val(values);
            $('.selectedRow').find('td:nth-of-type(2)').html(result['p']);
            get_order_match();
            get_order_history($('#current_ny_order_id').val());
        },'json');
    } else {
        values['ny_order_id'] = $trad_e.find('#ny_order_id').val();
        values['row_number'] = $trad_e.find('#row_number').val();

        $.post("<?=site_url('order/add_trad_order_item')?>", values, function (result) {
            $('#site-order').trigger('change');
            var orderNumber = $('#traditional>.order-item-title').html().replace("訂單: ", "");
            getOrderItem(values['ny_order_id'], orderNumber);
            $('.selectedRow').find('td:nth-of-type(2)').html(result['p']);
            get_order_match();
            get_order_history($('#current_ny_order_id').val());
        },'json');
    }
    $trad_m.modal('hide');
});
<?endif;?>

function orderItemModalTraditional(cols, rows) {
    jQuery(function($) {
        
        ft = FooTable.init('#order-item-table-traditional', {
            "columns": cols,
            "rows": rows,
            sorting: { enabled: true},
            filtering: { enabled: true}<?if($editable):?>,
            editing: {
                enabled: true,
                addRow: function(){
                    var row_number = $("#order-item-table-traditional tbody tr").length + 1;
                    if ($('#order-item-table-traditional tbody tr:first-of-type').hasClass('footable-empty')) {
                        row_number = 1;
                    }
                    $trad_m.removeData('row');
                    $trad_e[0].reset();
                    $trad_e.find('#coupled').trigger('change');
                    $trad_e.find('.fail-safe-lock-property').slideUp();
                    if ($trad_e.find('#magnetic_hole').is(':checked')) {
                        $trad_e.find('#magnetic_hole').trigger('click');
                    }
                    $trad_e.find('.magnetic-hole-property').slideUp();
                    $trad_t.text('新增訂單明細');

                    $trad_e.find('#ny_order_id').val($('#current_ny_order_id').val());
                    $trad_e.find('#rank').html(row_number);
                    $trad_e.find('#row_number').val(row_number);

                    $trad_m.modal('show');
                },
                editRow: function(row){
                    var values = row.val();

                    $trad_e[0].reset();
                    $trad_e.find('#coupled').trigger('change');
                    $trad_e.find('.fail-safe-lock-property').slideUp();
                    if ($trad_e.find('#magnetic_hole').is(':checked')) {
                        $trad_e.find('#magnetic_hole').trigger('click');
                    }
                    $trad_e.find('.magnetic-hole-property').slideUp();

                    $trad_e.find('#ny_order_id').val($('#current_ny_order_id').val());
                    $trad_e.find('#trad_order_item_id').val(values.trad_order_item_id);
                    $trad_e.find('#rank').html(values.row_number);
                    $trad_e.find('#row_number').val(values.row_number);
                    $trad_e.find('#order_name').val(values.order_name);
                    $trad_e.find('#name').val(values.name);
                    $trad_e.find('#progress').val(values.progress);
                    $trad_e.find('#due_date').val(values.due_date);
                    $trad_e.find('#standard').val(values.standard);
                    $trad_e.find('#door_frame').val(values.door_frame).trigger('change');
                    $trad_e.find('#coupled').val(values.coupled).trigger('change');
                    $trad_e.find('#type').val(values.type).trigger('change');
                    $trad_e.find('#width').val(values.width);
                    $trad_e.find('#height').val(values.height);
                    $trad_e.find('#depth').val(values.depth);
                    $trad_e.find('#colour').val(values.colour);
                    $trad_e.find('#amount_t').val(values.amount_t);
                    $trad_e.find('#amount_l').val(values.amount_l);
                    $trad_e.find('#amount_r').val(values.amount_r);
                    $trad_e.find('#lock').val(values.lock);
                    $trad_e.find('#handle').val(values.handle);
                    $trad_e.find('#handle_height').val(values.handle_height);
                    $trad_e.find('#hinge').val(values.hinge);
                    $trad_e.find('#closer').val(values.closer);
                    $trad_e.find('#bolt').val(values.bolt);
                    $trad_e.find('#door_thickness').val(values.door_thickness);
                    $trad_e.find('input[name=v_total_length]').val(values.v_total_length);
                    $trad_e.find('input[name=v_north_length]').val(values.v_north_length);
                    $trad_e.find('input[name=v_west_length]').val(values.v_west_length);
                    $trad_e.find('input[name=v_east_length]').val(values.v_east_length);
                    $trad_e.find('input[name=v_south_east_length]').val(values.v_south_east_length);
                    $trad_e.find('#note').val(values.note);
                    $trad_e.find('#material').val(values.material);
                    $trad_e.find('#fail_safe_lock_distance').val(values.fail_safe_lock_distance);
                    $trad_e.find('#mother_distance').val(values.mother_distance);
                    $trad_e.find('#is_mother').val(values.is_mother);
                    checkTradModalCheckbox("fail_safe_lock", values.fail_safe_lock);
                    checkTradModalCheckbox("fail_safe_lock_son", values.fail_safe_lock_son);
                    checkTradModalCheckbox("magnetic_hole", values.magnetic_hole);
                    $trad_e.find('#falling_strip').val(values.falling_strip);
                    checkTradModalCheckbox("magnetic_hole_son", values.magnetic_hole_son);
                    checkTradModalCheckbox("ball", values.ball);
                    if (values.frame_shape == "L型框") {
                        $('#frame_shape').val("l_frame");
                        selectFrame("l_frame");
                    } else if (values.frame_shape == "L型氣密框") {
                        $('#frame_shape').val("la_frame");
                        selectFrame("la_frame");
                    } else if (values.frame_shape == "凸型框") {
                        $('#frame_shape').val("t_frame");
                        selectFrame("t_frame");
                    } else if (values.frame_shape == "凸型氣密框") {
                        $('#frame_shape').val("ta_frame");
                        selectFrame("ta_frame");
                    } else if (values.frame_shape == "120A框") {
                        $('#frame_shape').val("120_frame");
                        selectFrame("120_frame");
                    } else if (values.frame_shape == "玄關框") {
                        $('#frame_shape').val("entrance_frame");
                        selectFrame("entrance_frame");
                    }

                    $trad_m.data('row', row);
                    $trad_t.text('修改訂單明細');
                    jQuery.noConflict(); 
                    $trad_m.modal('show');
                },
                'alwaysShow': true,
                'allowDelete': false,
                'showText': '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> 新增修改模式',
                'hideText': '取消',
                'addText': '新增訂單'
            }<?endif;?>
        });
        $('span.caret').hide();
        setProgressColour('#order-item-table-traditional tbody td:nth-of-type(4)');
        setDoorColour('#order-item-table-traditional tbody td:nth-of-type(8)');
        setDoubleDoorColour('#order-item-table-traditional tbody td:nth-of-type(9)');
        // $('#order-item-table-traditional .footable-filtering th .form-inline').prepend('<h2>hello</h2>');
        $('.footable-editing').css('text-align','left');
        $('#order-item-table-traditional tbody td:last-of-type').remove();
  });
}
</script>