<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <!--div class="profile-image">
                  <img src="<?=base_url('img/login/face.jpg');?>" alt="profile">
                </div-->
                <div class="text-wrapper">
                  <p class="profile-name"><?=$this->session->userdata('user');?></p>
                  <div>
                    <small class="designation text-muted"><?=$this->session->userdata('position');?></small>
                    <!--span class="status-indicator online"></span-->
                  </div>
                </div>
              </div>
              <!--button class="btn btn-success btn-block">New Project
                <i class="mdi mdi-plus"></i>
              </button-->
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('dashboard')?>">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">看板</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('customer/view_customer');?>">
              <i class="menu-icon mdi mdi-account-multiple-outline"></i>
              <span class="menu-title">客戶</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('project');?>">
              <i class="menu-icon mdi mdi-forklift"></i>
              <span class="menu-title">建案</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('order');?>">
              <i class="menu-icon mdi mdi-briefcase"></i>
              <span class="menu-title">訂單</span>
            </a>
          </li>
          <? if ($editable): ?>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('stock');?>">
              <i class="menu-icon mdi mdi-cards-outline"></i>
              <span class="menu-title">庫存</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('ny_accessories');?>">
              <i class="menu-icon mdi mdi-table"></i>
              <span class="menu-title">南亞五金表</span>
            </a>
          </li>
          <?endif;?>
          <!--li class="nav-item">
            <a class="nav-link" href="pages/tables/basic-table.html">
              <i class="menu-icon mdi mdi-table"></i>
              <span class="menu-title">Tables</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages/icons/font-awesome.html">
              <i class="menu-icon mdi mdi-sticker"></i>
              <span class="menu-title">Icons</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title">User Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="pages/samples/blank-page.html"> Blank Page </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="pages/samples/login.html"> Login </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="pages/samples/register.html"> Register </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="pages/samples/error-404.html"> 404 </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="pages/samples/error-500.html"> 500 </a>
                </li>
              </ul>
            </div>
          </li-->
        </ul>
      </nav>