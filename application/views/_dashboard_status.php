<div class="row">
  <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-warning">
          <div class="panel-heading">01. 收到訂單</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-received"></table></div>
        </div>
    </div>
          </div>
          <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-success">
          <div class="panel-heading">07. 已出貨 (最近60天)</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-shipped"></table></div>
        </div>
    </div>
          </div>
          <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-warning">
          <div class="panel-heading">02. 備料中</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-wait"></table></div>
        </div>
    </div>
          </div>
          <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-info">
          <div class="panel-heading">03. 發包中</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-fold"></table></div>
        </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-info">
          <div class="panel-heading">04. 組裝中</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-production"></table></div>
        </div>
    </div>
          </div>
          <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-info">
          <div class="panel-heading">05. 烤漆包裝</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-paint"></table></div>
        </div>
    </div>
          </div>
          <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-success">
          <div class="panel-heading">06. 等出貨</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-wait-ship"></table></div>
        </div>
    </div>
          </div>
          <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-danger">
          <div class="panel-heading">等SKIN板</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-skin"></table></div>
        </div>
    </div>
          </div>
          <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-danger">
          <div class="panel-heading">等客戶回復</div>
          <div class="panel-body table-respo nsive"><table class="table table-striped table-hover" id="table-response"></table></div>
        </div>
    </div>
          </div>
          <div class="col-md-6">
    <div class="panel-group">
        <div class="panel panel-danger">
          <div class="panel-heading">取消訂單 (最近60天)</div>
          <div class="panel-body table-responsive"><table class="table table-striped table-hover" id="table-cancel"></table></div>
        </div>
    </div>
  </div>
</div>