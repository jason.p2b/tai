<form id="order_form" method="post" action="<?=site_url('report/insert');?>" acceptcharset="utf-8" enctype="multipart/form-data">

<fieldset>
    <legend>防火報告</legend>
    <p>
        <label for="report">防火報告：</label>
        <input type="text" id="report" name="report" vlaue="" required />
    </p>
    <p>
        <label for="m_report">屬同型式：</label>
        <input type="checkbox" id="m_report" name="m_report" value="true"><label class="ignore_label" for="m_report">是　</label>
        <select name="main_report" id="main_report">
            <option value=""> --- </option>
            <?php foreach ($fireproof_report as $index => $obj) : ?>
                <option value="<?=$obj->id?>"><?=$obj->name?></option>
            <?php endforeach; ?>
        </select>
    </p>
</fieldset>
<fieldset>
    <legend>門框</legend>
    <p>
        <label for="width">門寬度：</label>
        小<input type="number" step="any" id="min_frame_width" name="min_frame_width" value="0" min="0" required /> mm ~ 
        <input type="number" step="any" id="max_frame_width" name="max_frame_width" value="0" min="0" required /> mm 大
    </p>
    <p>
        <label for="width">門長度：</label>
        小<input type="number" step="any" id="min_frame_length" name="min_frame_length" value="0" min="0" required /> mm ~ 
        <input type="number" step="any" id="max_frame_length" name="max_frame_length" value="0" min="0" required /> mm 大   
    </p>
    <p>
        <label>種類：</label>
        <input type="radio" id="normal" name="frame_type" value="normal"><label class="ignore_label" for="normal">普通框　</label>
        <input type="radio" id="four-side" name="frame_type" value="four-side"><label class="ignore_label" for="four-side">四面框</label>
    </p>
    <p>
        <label>形狀：</label>
        <!-- frame shape -->
        <div id="svg_box">
            <?php foreach ($frame_shape as $index => $obj) :?>
            <span class="svg_class">
                <input type="checkbox" id="<?=$obj->html_id?>" name="frame_shape[]" value="<?=$obj->frame_shape_id?>">
                <label class="ignore_label" for="<?=$obj->html_id?>"><?php include "./svg/" . $obj->file; ?></label>
            </span>
            <? endforeach; ?>
        </div>
    </p>
    <p>
        <label>材質：</label>
        <?php foreach ($frame_material as $index => $obj) : ?>
            <span class="box">
                <input type="checkbox" name="frame_material[]" value="<?=$obj->frame_material_id?>" id="fm<?=$obj->frame_material_id?>">
                <label class="fm" for="fm<?=$obj->frame_material_id?>"><?=$obj->name?></label>
            </span>
        <?php endforeach;?>
        <span class="new_items"></span>
        <span class="add_img" onclick="add_item(this,'new_frame_material', 'fm', '填寫新材質', '已有相同材質');">
            <img src="<?=base_url()?>/img/add_item.png" alt="加材質">
            <span>加材質<span>
        </span>
    </p>
</fieldset>
<fieldset>
    <legend>門扇</legend>
    <p>
        <label for="width">門扇寬度：</label>
        小<input type="number" step="any" id="min_door_width" name="min_door_width" value="0" min="0" required />mm ~ 
        <input type="number" step="any" id="max_door_width" name="max_door_width" value="0" min="0" required />mm 大
    </p>
    <p>
        <label for="width">門扇長度：</label>
        小<input type="number" step="any" id="min_door_length" name="min_door_length" value="0" min="0" required />mm ~ 
        <input type="number" step="any" id="max_door_length" name="max_door_length" value="0" min="0" required />mm 大   
    </p>
    <p>
        <label for="door_thickness">門扇厚度：</label><input type="number" step="any" id="door_thickness" name="door_thickness" value="0"> mm
    </p>
    <p>
        <label>門扇數量：</label>
        <input type="radio" id="single" name="door_amount" value="1"><label class="ignore_label" for="single">單扇　</label>
        <input type="radio" id="double" name="door_amount" value="0"><label class="ignore_label" for="double">雙扇</label>
    </p>
    <p>
        <label>玻璃</label>
        <input type="radio" id="glass_yes" name="glass" value="yes"><label class="ignore_label" for="glass_yes">是　</label>
        <input type="radio" id="glass_no" name="glass" value="no"><label class="ignore_label" for="glass_no">否　</label>
        <input type="radio" id="glass_both" name="glass" value="both"><label class="ignore_label" for="glass_both">都可以</label>
        <span class="hide" id="glass_dimension">
            　寬：<input type="number" step="any" id="glass_width" name="glass_width" value="0"> mm ~ 
            長：<input type="number" step="any" id="glass_length" name="glass_length" value="0"> mm
        </span>
    </p>
    <p>
        <label>材質：</label>
        <?php foreach ($door_material as $index => $obj) : ?>
            <span class="box">
                <input type="checkbox" name="door_material[]" value="<?=$obj->door_material_id?>" id="dm<?=$obj->door_material_id?>">
                <label class="dm" for="dm<?=$obj->door_material_id?>"><?=$obj->name?></label>
            </span>
        <?php endforeach;?>
        <span class="new_items"></span>
        <span class="add_img" onclick="add_item(this,'new_door_material', 'dm', '填寫新材質', '已有相同材質');">
            <img src="<?=base_url()?>/img/add_item.png" alt="加材質">
            <span>加材質<span>
        </span>
    </p>
</fieldset>
<fieldset>
    <legend>五金</legend>
    <p>
        <label>門鎖：</label>
        <?php foreach ($lock as $index => $obj) : ?>
            <span class="box">
                <input type="checkbox" name="lock[]" value="<?=$obj->lock_id?>" id="lock<?=$obj->lock_id?>">
                <label class="lock ignore_label" for="lock<?=$obj->lock_id?>"><?=$obj->model?><span class="restriction"><?=$obj->restriction?></span></label>
            </span>
        <?php endforeach;?>
        <span class="new_items"></span>
        <span class="add_img" onclick="add_item(this,'new_lock', 'lock', '填寫新門鎖', '已有相同門鎖');">
            <img src="<?=base_url()?>/img/add_item.png" alt="加門鎖">
            <span>加門鎖<span>
        </span>
    </p>
    <p>
        <label>鉸鏈：</label>
        <?php foreach ($hinge as $index => $obj) : ?>
            <span class="box">
                <input type="checkbox" name="hinge[]" value="<?=$obj->hinge_id?>" id="hinge<?=$obj->hinge_id?>">
                <label class="hinge ignore_label" for="hinge<?=$obj->hinge_id?>"><?=$obj->model?><span class="restriction"><?=$obj->restriction?></span></label>
            </span>
        <?php endforeach;?>
        <span class="new_items"></span>
        <span class="add_img" onclick="add_item(this,'new_hinge', 'hinge', '填寫新鉸鏈', '已有相同鉸鏈');">
            <img src="<?=base_url()?>/img/add_item.png" alt="加鉸鏈">
            <span>加鉸鏈<span>
        </span>
    </p>
    <p>
        <label>把手：</label>
        <?php foreach ($handle as $index => $obj) : ?>
            <span class="box">
                <input type="checkbox" name="handle[]" value="<?=$obj->handle_id?>" id="handle<?=$obj->handle_id?>">
                <label class="handle ignore_label" for="handle<?=$obj->handle_id?>"><?=$obj->model?><span class="restriction"><?=$obj->restriction?></span></label>
            </span>
        <?php endforeach;?>
        <span class="new_items"></span>
        <span class="add_img" onclick="add_item(this,'new_handle', 'handle', '填寫新把手', '已有相同把手');">
            <img src="<?=base_url()?>/img/add_item.png" alt="加把手">
            <span>加把手<span>
        </span>
    </p>
    <p>
        <label>門弓器：</label>
        <?php foreach ($closer as $index => $obj) : ?>
            <span class="box">
                <input type="checkbox" name="closer[]" value="<?=$obj->closer_id?>" id="closer<?=$obj->closer_id?>">
                <label class="closer ignore_label" for="closer<?=$obj->closer_id?>"><?=$obj->model?><span class="restriction"><?=$obj->restriction?></span></label>
            </span>
        <?php endforeach;?>
        <span class="new_items"></span>
        <span class="add_img" onclick="add_item(this,'new_closer', 'closer', '填寫新門弓器', '已有相同門弓器');">
            <img src="<?=base_url()?>/img/add_item.png" alt="加門弓器">
            <span>加門弓器<span>
        </span>
    </p>
    <p>
        <label>天地栓：</label>
        <?php foreach ($bolt as $index => $obj) : ?>
            <span class="box">
                <input type="checkbox" name="bolt[]" value="<?=$obj->bolt_id?>" id="bolt<?=$obj->bolt_id?>">
                <label class="bolt ignore_label" for="bolt<?=$obj->bolt_id?>"><?=$obj->model?><span class="restriction"><?=$obj->restriction?></span></label>
            </span>
        <?php endforeach;?>
        <span class="new_items"></span>
        <span class="add_img" onclick="add_item(this,'new_bolt', 'bolt', '填寫新天地栓', '已有相同天地栓');">
            <img src="<?=base_url()?>/img/add_item.png" alt="加天地栓">
            <span>加天地栓<span>
        </span>
    </p>
    <p>
        <label>其他五金：</label>
        <?php foreach ($hardware as $index => $obj) : ?>
            <span class="box">
                <input type="checkbox" name="hardware[]" value="<?=$obj->hardware_id?>" id="hardware<?=$obj->hardware_id?>">
                <label class="hardware ignore_label" for="hardware<?=$obj->hardware_id?>"><?=$obj->model?><span class="restriction"><?=$obj->restriction?></span></label>
            </span>
        <?php endforeach;?>
        <span class="new_items"></span>
        <span class="add_img" onclick="add_item(this,'new_hardware', 'hardware', '填寫新五金', '已有相同五金');">
            <img src="<?=base_url()?>/img/add_item.png" alt="加五金">
            <span>加五金<span>
        </span>
    </p>
</fieldset>
<div id="confirmation">
    <input type="submit" value="確認並結束">
</div id="confirmation">
</form>