<!-- Begin Popup Modal -->
<div class="modal fade" id="material-modal" tabindex="-1" role="dialog" aria-labelledby="material-modal-title">

    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="material-editor" enctype="multipart/form-data">
            <div class="modal-header">
                <h4 class="modal-title" id="material-modal-title">新增型號</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="modal-body">
                <div class="alert alert-danger">
                  <strong>已經有這個品名</strong>
                    <button type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="number" id="mid" name="mid" class="hidden" value=""/>
        
                <div class="form-group">
                    <label for="type" class="col-sm-3 control-label">類型</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="type" name="type" placeholder="類型">
                    </div>
                </div>
    
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">品名</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="品名">
                    </div>
                </div>

                <div class="form-group">
                    <label for="size" class="col-sm-3 control-label">尺寸</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="size" name="size" placeholder="尺寸">
                    </div>
                </div>

                <div class="form-group required">
                    <label for="unit" class="col-sm-3 control-label">單位</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="unit" name="unit" placeholder="單位" required>
                    </div>
                </div>
                
                <div class="form-group required">
                    <label for="undersupply" class="col-sm-3 control-label">庫存線</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="undersupply" name="undersupply" placeholder="庫存線" required>
                    </div>
                </div>


                <div class="form-group required">
                    <label for="used_in" class="col-sm-3 control-label">使用處</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="used_in" name="used_in" placeholder="使用處" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="note" class="col-sm-3 control-label">備註</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="note" name="note" placeholder="備註">
                    </div>
                </div>

                <div class="form-group">
                  <label for="photo" class="col-sm-3 control-label">圖片</label>
                  <div class="col-sm-9">
                    <input type="file" class="form-control" id="photo" name="note" placeholder="圖片">
                  </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">確認</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </form>
    </div>
</div>
<!-- End Popup Modal -->

<script>
    var $modal_m  = $('#material-modal');
    var $editor_m = $('#material-editor');
    var $title_m  = $('#material-modal-title');
    $('.alert').hide();

    $editor_m.on('submit', function(e){
        if (this.checkValidity && !this.checkValidity()) return;
        e.preventDefault();

        var fData = new FormData();
        fData.append('photo', $('#photo')[0].files[0]);
        fData.append('mid', $editor_m.find('#mid').val());
        fData.append('type', $editor_m.find('#type').val());
        fData.append('name', $editor_m.find('#name').val());
        fData.append('size', $editor_m.find('#size').val());
        fData.append('undersupply', $editor_m.find('#undersupply').val());
        fData.append('unit', $editor_m.find('#unit').val());
        fData.append('used_in', $editor_m.find('#used_in').val());
        fData.append('note', $editor_m.find('#note').val());


        var row = $modal_m.data('row'),
        values = {
            mid: $editor_m.find('#mid').val(),
            type: $editor_m.find('#type').val(),
            name: $editor_m.find('#name').val(),
            size: $editor_m.find('#size').val(),
            undersupply: $editor_m.find('#undersupply').val(),
            unit: $editor_m.find('#unit').val(),
            used_in: $editor_m.find('#used_in').val(),
            note: $editor_m.find('#note').val()
        };

        $.post("<?=site_url('stock/check_material')?>",{name: values['name']}, function(result) {
            // if (result) {
            //     $('.alert').show();
            //     return false;
            

            if (row instanceof FooTable.Row){
                // $.post("<?=site_url('stock/update_material')?>", values, function (result) {
                //     row.val(values);
                // });
                $.ajax({
                    url: '<?=site_url('stock/update_material')?>',
                    type: 'POST',
                    cache: false,
                    data: fData,
                    processData: false,
                    contentType: false
                }).done(function(res) {
                  row.val(values);
                }).fail(function(res) {});
              } else {
                // $.post("<?=site_url('stock/add_material')?>", values, function (result) {
                //     values['mid'] = result;
                //     ft.rows.add(values);
                // });
                $.ajax({
                    url: '<?=site_url('stock/add_material')?>',
                    type: 'POST',
                    cache: false,
                    data: fData,
                    processData: false,
                    contentType: false
                }).done(function(res) {
                  values['mid'] = result;
                  ft.rows.add(values);
                }).fail(function(res) {});
            }
            $modal_m.modal('hide'); 

            material_select();
        },'json');

        
    });

    $.post('<?=site_url('stock/get_material')?>', {}, function (result) {
        ft = FooTable.init('#material-table', {
                "columns": result['col'],
                "rows": result['row'],
                editing: {
                    enabled: true,
                    addRow: function(){
                        $modal_m.removeData('row');
                        $editor_m[0].reset();
                        $title_m.text('新增訂單');
                        $modal_m.modal('show');
                    },
                    editRow: function(row){
                        var values = row.val();

                        $editor_m.find('#mid').val(values.mid);
                        $editor_m.find('#type').val(values.type);
                        $editor_m.find('#name').val(values.name);
                        $editor_m.find('#size').val(values.size);
                        $editor_m.find('#undersupply').val(values.undersupply);
                        $editor_m.find('#unit').val(values.unit);
                        $editor_m.find('#used_in').val(values.used_in);
                        $editor_m.find('#note').val(values.note);

                        $modal_m.data('row', row);
                        $title_m.text('修改訂單:');
                        jQuery.noConflict(); 
                        $modal_m.modal('show');
                    },
                    'alwaysShow': true,
                    'allowDelete': false,
                    'addText': '新增訂單'
                },
                sorting: {enabled: true},
                filtering: {enabled: true},
                paging: {
                    enabled: true,
                    size: "20"
                }
                
        });
        $('span.caret').hide();
    },'json');


    $('.alert button').click(function () {
        $('.alert').hide();
    });
    

</script>