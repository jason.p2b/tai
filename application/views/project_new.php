
<form method="post" action="<?=site_url('project/insert');?>">
<fieldset>
    <legend>新增工地</legend>
    <? if ($user_status < 3) : ?>
    <p>
        <label for="restriction">屬性：<span class="asterisk">*</span></label>
        <select name="restriction" id="restriction">
            <option value=""> --- </option>
            <? foreach ($all_status as $key => $value) : ?>
                <option value="<?=$key?>"><?=$value?></option>
            <? endforeach; ?>
        </select>
    </p>
    <? endif; ?>
    <? if ($user_status >= 3) : ?>
        <input type="hidden" name="restriction" id="restriction" value="<?=$user_status?>">
    <? endif; ?>
    <p>
        <label for="customer">客戶名稱：<span class="asterisk">*</span></label>
        <input type="text" id="customer" name="customer" value="">
    </p>
    <p>
        <label for="site">工地名稱：<span class="asterisk">*</span></label>
        <input type="text" id="site" name="site" value="">
    </p>
    <p>
        <label for="address">送貨地址：<span class="asterisk">*</span></label>
        <input type="text" id="address" name="address" value="">
    </p>
    <p>
        <label for="contact_name">工地聯絡人：<span class="asterisk">*</span></label>
        <input type="text" id="contact_name" name="contact_name" value="">
    </p>
    <p>
        <label for="phone">工地聯絡電話：<span class="asterisk">*</span></label>
        <input type="text" id="phone" name="phone" value="">
    </p>
    <p>
        <label for="user">工務：<span class="asterisk">*</span></label>
        
            <select id="user" name="user">
                <option value=""> --- </option>
                <?php foreach ($staff as $obj) : ?>
                    <option value="<?=$obj->uid?>"><?=$obj->name?></option>
                <?php endforeach; ?>
            </select>
            <span id="staff_error">請選擇此案的工務</span>
        
    </p>
    <p>
        <label for="note">備註：</label>
        <textarea name="note" id="note"></textarea>
    </p>
    <p>
        <label>防火報告：<span class="asterisk">*</span></label>
        
            <span id="report_error"></span>
            <?php $count = 1; ?>
            <?php foreach($report as $name => $source) : ?>
                <span class="report_span">
                <input class="require-one" type="checkbox" id="report_<?=$source['id']?>" name="report[]" value="<?=$source['id']?>">
                <label class="report" for="report_<?=$source['id']?>">
                <?php 
                    if ($source['sub'] != "")
                    {
                        echo "<a class='report_tooltip' title=\"同型式: " . $source['sub'] . "\">$name</a>";
                    }
                    else
                    {
                        echo $name;
                    }

                ?>
                </label>
                </span>
            <?php endforeach;?>
        
    </p>
    <p>
        <input type="submit" value="確認">
        <input type="reset" value="清除表單">
    </p>
</fieldset>

</form>