<style>
    #my-block{margin:auto;}
    .alert{display:none;}
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            	<div class="row">
            		<div class="col-md-6" id="my-block">
            			<h4 class="card-title">修改密碼</h4>
                            <div class="alert" role="alert"></div>
		                    <div class="form-group required">
		                        <label for="old_pw">舊密碼</label>
		                       	<input type="password" class="form-control" id="old_pw" placeholder="舊密碼" required>
		                    </div>
		                    <div class="form-group required">
		                        <label for="pw">新密碼</label>
		                        <input type="password" class="form-control" id="pw" placeholder="新密碼" required>
		                    </div>
		                    <div class="form-group required">
		                        <label for="repeat_pw">重複新密碼</label>
		                        <input type="password" class="form-control" id="repeat_pw" placeholder="重複新密碼" required>
		                    </div>
		                    <button type="submit" class="btn btn-success mr-2">確認</button>
		                    <button type="reset" class="btn btn-primary">取消</button>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>

<script>
    $('button[type=submit]').click(function () {
        $msg = $('.alert');
        $opw = $('#old_pw').val();
        $pw = $('#pw').val();
        $rpw = $('#repeat_pw').val();

        if ($pw.length < 8 ) {
            $msg.html("新密碼需要最少８位元")
                .addClass('alert-danger')
                .removeClass('alert-success')
                .show();

        } else if ($pw != $rpw) {
            $msg.html("重複新密碼和新密碼不一樣")
                .addClass('alert-danger')
                .removeClass('alert-success')
                .show();

        } else {
            $.post('<?=site_url('login/change_password')?>',{opw:$opw, pw:$pw, rpw:$rpw}, function (result) {
                if (result.success)
                {
                    $msg.html("密碼修改成功")
                        .addClass('alert-success')
                        .removeClass('alert-danger')
                        .show();
                    return true;
                }

                $msg.html("密碼修改失敗")
                    .addClass('alert-danger')
                    .removeClass('alert-success')
                    .show();
            },'json');
        }
        return false;
    });
</script>