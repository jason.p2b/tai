<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');

function OutputLabel($data)
{
    $excel = new PHPExcel();
    $sheet = $excel->getActiveSheet();

    // Set Cell Height & Width
    // $sheet->getDefaultColumnDimension()->setWidth('20');
    $sheet->getColumnDimension('A')->setWidth('4.33');
    $sheet->getColumnDimension('B')->setWidth('8.89');
    $sheet->getColumnDimension('C')->setWidth('3.89');
    $sheet->getColumnDimension('D')->setWidth('3.89');
    $sheet->getColumnDimension('E')->setWidth('6.89');
    $sheet->getColumnDimension('F')->setWidth('3.89');
    $sheet->getColumnDimension('G')->setWidth('6.89');
    $sheet->getColumnDimension('H')->setWidth('12.89');

    $rowCount = 0;

    foreach ($data as $key => $value)
    {
        $info = array();
        $info['model'] = $value['other_model'];
        $info['site']  = $value['customer'] . $value['site'];
        $info['width'] = $value['width'];
        $info['height'] = $value['height'];

        for ($i=0; $i<$value['left']; $i++)
        {
            $info['direction'] = "L";

            if ($value['cType'] == "框")
            {
                $info['amount'] = "";
            }
            else ($value['door_amount'] == "single")
            {
                $info['amount'] = "單";

            }
            else
            {
                $info['amount'] = "親";

                $son = $info;
                $son['width']  = $value['son_width'];
                $son['height'] = $value['son_height'];
                $son['amount'] = "子";
                $son['direction'] = "R";
                setLabel($sheet, $rowCount, $info);
                $rowCount += 5;
            }

            setLabel($sheet, $rowCount, $info);
            $rowCount += 5;
        }
        
        for ($i=0; $i<$value['right']; $i++)
        {
            $info['direction'] = "R";

            if ($value['cType'] == "框")
            {
                $info['amount'] = "";
            }
            else ($value['door_amount'] == "single")
            {
                $info['amount'] = "單";

            }
            else
            {
                $info['amount'] = "親";

                $son = $info;
                $son['width']  = $value['son_width'];
                $son['height'] = $value['son_height'];
                $son['amount'] = "子";
                $son['direction'] = "L";
                setLabel($sheet, $rowCount, $info);
                $rowCount += 5;
            }

            setLabel($sheet, $rowCount, $info);
            $rowCount += 5;
        }
    }

    // Create heading
    // $sheet->getRowDimension('1')->setRowHeight('45.0680');
    // $styleArray = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 24,
    //         'name'  => '新細明體',
    //     ),
    //     'alignment' => array(
    //         'wrap'       => true,
    //         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    //         'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
    //     ));
    // $sheet->getCell('A1')->setValue('泰慶興業有限公司');
    // $sheet->getStyle('A1')->applyFromArray($styleArray);
    // $sheet->mergeCells('A1:H1');

    // // Create 估價單
    // $sheet->getRowDimension('2')->setRowHeight('35.9977');
    // $styleArray = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 20,
    //         'name'  => '新細明體',
    //     ),
    //     'alignment' => array(
    //         'wrap'       => true,
    //         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    //         'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
    //     ));
    // $sheet->getCell('A2')->setValue('估   價   單');
    // $sheet->getStyle('A2')->applyFromArray($styleArray);
    // $sheet->mergeCells('A2:H2');

    // // Create ********
    // $sheet->getRowDimension('3')->setRowHeight('15.0227');
    // $styleArray = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 10,
    //         'name'  => '新細明體',
    //     ),
    //     'alignment' => array(
    //         'wrap'       => true,
    //         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    //         'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
    //     ));
    // $sheet->getCell('A3')->setValue('**************************');
    // $sheet->getStyle('A3')->applyFromArray($styleArray);
    // $sheet->mergeCells('A3:H3');

    // // Create Company Details
    // $sheet->getRowDimension('4')->setRowHeight('24.093');
    // $sheet->getRowDimension('5')->setRowHeight('24.093');
    // $sheet->getRowDimension('6')->setRowHeight('24.093');
    // $sheet->getRowDimension('7')->setRowHeight('24.093');
    // $sheet->getRowDimension('8')->setRowHeight('24.093');

    // $styleArray = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 12,
    //         'name'  => '新細明體',
    //     ));
    
    // $sheet->getCell('A4')->setValue('客戶名稱 : ' . $companyInfo->company);
    // $sheet->getStyle('A4')->applyFromArray($styleArray);

    // $sheet->getCell('A5')->setValue('工程名稱 : ' . $companyInfo->construction_site);
    // $sheet->getStyle('A5')->applyFromArray($styleArray);

    // $sheet->getCell('A6')->setValue('連  絡  人 : ' . $companyInfo->contact_person);
    // $sheet->getStyle('A6')->applyFromArray($styleArray);

    // $sheet->getCell('A7')->setValue('TEL : ' . $companyInfo->phone);
    // $sheet->getStyle('A7')->applyFromArray($styleArray);

    // $sheet->getCell('A8')->setValue('FAX : ' . $companyInfo->fax);
    // $sheet->getStyle('A8')->applyFromArray($styleArray);

    // $sheet->getCell('F8')->setValue('日        期 :');
    // $sheet->getStyle('F8')->applyFromArray($styleArray);

    // list($year, $month, $day) = explode("-", $companyInfo->date);
    // $year = $year - 1911;

    // $sheet->getCell('G8')->setValue($year . '  年   ' . $month . '   月  ' . $day . '   日');
    // $sheet->getStyle('G8')->applyFromArray($styleArray);

    // // Create Estimate Table
    // $sheet->getRowDimension('9')->setRowHeight('35.9978');
    // $styleArray = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 12,
    //         'name'  => '新細明體',
    //     ),
    //     'borders' => array(
    //         'allborders' => array(
    //           'style' => PHPExcel_Style_Border::BORDER_THIN
    //         )
    //     ),
    //     'alignment' => array(
    //         'wrap'       => true,
    //         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    //         'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
    //     ));
    // $sheet->getCell('A9')->setValue('品   名   型   號');
    // $sheet->mergeCells('A9:C9');
    // $sheet->getStyle('A9')->applyFromArray($styleArray);

    // $sheet->getCell('D9')->setValue('單位');
    // $sheet->getStyle('D9')->applyFromArray($styleArray);

    // $sheet->getCell('E9')->setValue('數量');
    // $sheet->getStyle('E9')->applyFromArray($styleArray);

    // $sheet->getCell('F9')->setValue('單價');
    // $sheet->getStyle('F9')->applyFromArray($styleArray);

    // $sheet->getCell('G9')->setValue('複價');
    // $sheet->getStyle('G9')->applyFromArray($styleArray);

    // $sheet->getCell('H9')->setValue('備註');
    // $sheet->getStyle('H9')->applyFromArray($styleArray);

    // // Create Table Content
    // $styleArrayCenter = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 12,
    //         'name'  => '新細明體',
    //     ),
    //     'borders' => array(
    //         'allborders' => array(
    //           'style' => PHPExcel_Style_Border::BORDER_THIN
    //         )
    //     ),
    //     'alignment' => array(
    //         'wrap'       => true,
    //         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    //         'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
    //     ));

    // $styleArrayLeft = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 12,
    //         'name'  => '新細明體',
    //     ),
    //     'borders' => array(
    //         'allborders' => array(
    //           'style' => PHPExcel_Style_Border::BORDER_THIN
    //         )
    //     ),
    //     'alignment' => array(
    //         'wrap'       => true,
    //         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    //         'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
    //     ));

    // $styleArrayRight = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 12,
    //         'name'  => '新細明體',
    //     ),
    //     'borders' => array(
    //         'allborders' => array(
    //           'style' => PHPExcel_Style_Border::BORDER_THIN
    //         ),
    //     ),
    //     'alignment' => array(
    //         'wrap'       => true,
    //         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    //         'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
    //     ));

    // $line = 10;
    // $grand_total = 0;

    // foreach ($content as $key => $info)
    // {
    //     $sheet->getRowDimension($line)->setRowHeight('35.9978');
    //     if (!is_null($info->pid))
    //     {
    //         $sheet->getCell('A' . $line)->setValue($info->product);
    //         $sheet->mergeCells('A' . $line . ':C' . $line);
    //         $sheet->getStyle('A' . $line)->applyFromArray($styleArrayLeft);

    //         $sheet->getCell('D' . $line)->setValue($info->unit);
    //         $sheet->getStyle('D' . $line)->applyFromArray($styleArrayCenter);

    //         $sheet->getCell('E' . $line)->setValue($info->amount);
    //         $sheet->getStyle('E' . $line)->applyFromArray($styleArrayCenter);

    //         $sheet->getCell('F' . $line)->setValue($info->price);
    //         $sheet->getStyle('F' . $line)->applyFromArray($styleArrayRight);

    //         $total = $info->price * $info->amount;
    //         $grand_total += $total;
    //         $sheet->getCell('G' . $line)->setValue($total);
    //         $sheet->getStyle('G' . $line)->applyFromArray($styleArrayRight);

    //         $sheet->getCell('H' . $line)->setValue($info->comment);
    //         $sheet->getStyle('H' . $line)->applyFromArray($styleArrayLeft);
    //     }
    //     else
    //     {
    //         $sheet->getCell('A' . $line)->setValue($info->comment);
    //         $sheet->mergeCells('A' . $line . ':H' . $line);
    //         $sheet->getStyle('A' . $line)->applyFromArray($styleArrayLeft);
    //         $sheet->getStyle('I' . $line)->applyFromArray(array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
    //     }

    //     $line++;
    // }

    // $sheet->getRowDimension($line)->setRowHeight('41.9501');

    // $sheet->getCell('F' . $line)->setValue('合計:NT$');
    // $sheet->getStyle('F' . $line)->applyFromArray($styleArrayCenter);

    // $sheet->getCell('G' . $line)->setValue($grand_total);
    // $sheet->getStyle('G' . $line)->applyFromArray($styleArrayRight);

    // // Footer Info
    // $styleArray = array(
    //     'font'  => array(
    //         'bold'  => false,
    //         'size'  => 12,
    //         'name'  => '新細明體',
    //     ));
    // $sheet->getCell('A' . $line)->setValue('說 明 : 1. 報價均不含營業稅。');
    // $sheet->getStyle('A' . $line)->applyFromArray($styleArray);

    // $line++;
    // $sheet->getRowDimension($line)->setRowHeight('24.0930');
    // $sheet->getCell('A' . $line)->setValue('            2. 門鎖配 M.K , 費用每組另加 80 元。');
    // $sheet->getStyle('A' . $line)->applyFromArray($styleArray);

    // $line++;
    // $sheet->getRowDimension($line)->setRowHeight('24.0930');
    // $sheet->getCell('A' . $line)->setValue('            3. 估價單有效期限 30 天。');
    // $sheet->getStyle('A' . $line)->applyFromArray($styleArray);

    // $line++;
    // $sheet->getRowDimension($line)->setRowHeight('24.0930');
    // $sheet->getCell('A' . $line)->setValue('台北市松山區 ( 105 ) 敦化北路 244 巷 11 號');
    // $sheet->getStyle('A' . $line)->applyFromArray($styleArray);
    // $sheet->getCell('G' . $line)->setValue('經 辦 人 : 林  文  堂');
    // $sheet->getStyle('G' . $line)->applyFromArray($styleArray);

    // $line++;
    // $sheet->getRowDimension($line)->setRowHeight('24.0930');
    // $sheet->getCell('A' . $line)->setValue('TEL : (02) 2713-5966       FAX : (02) 2715-2747');
    // $sheet->getStyle('A' . $line)->applyFromArray($styleArray);
    // $sheet->getCell('G' . $line)->setValue('0933-125-118');
    // $sheet->getStyle('G' . $line)->applyFromArray($styleArray);

    // We'll be outputting an excel file
    // header('Content-type: application/vnd.ms-excel');

    // // It will be called file.xlsx
    // header('Content-Disposition: attachment; filename="file.xlsx"');

    // // Write file to the browser
    // $objWriter = PHPExcel_IOFactory::createWriter($excel , 'Excel2007');
    // $objWriter->save('php://output');

    return $excel;
}

function setLabel($sheet, $rowCount, $data)
{
    $r1 = $rowCount + 1;
    $r2 = $rowCount + 2;
    $r3 = $rowCount + 3;
    $r4 = $rowCount + 4;
    $r5 = $rowCount + 5;

    $sheet->getRowDimension($r1)->setHeight('20.1');
    $sheet->getRowDimension($r2)->setHeight('20.1');
    $sheet->getRowDimension($r3)->setHeight('24');
    $sheet->getRowDimension($r4)->setHeight('18');
    $sheet->getRowDimension($r5)->setHeight('33');

    $sheet->getActiveSheet()->mergeCells('B' . $r1 . ":B" . $r2);
    $sheet->getActiveSheet()->mergeCells('C' . $r1 . ":G" . $r1);
    $sheet->getActiveSheet()->mergeCells('C' . $r2 . ":D" . $r2);
    $sheet->getActiveSheet()->mergeCells('B' . $r3 . ":B" . $r4);
    $sheet->getActiveSheet()->mergeCells('C' . $r4 . ":G" . $r4);
    $sheet->getActiveSheet()->mergeCells('H' . $r1 . ":H" . $r4);

    $sheet->getCell('B' . $r1)->setValue($data['model']);
    $sheet->getCell('C' . $r1)->setValue($data['site']);
    $sheet->getCell('C' . $r2)->setValue('開向');
    $sheet->getCell('E' . $r2)->setValue($data['amount']);
    $sheet->getCell('F' . $r2)->setValue($data['direction']);
    $sheet->getCell('C' . $r3)->setValue('尺寸');
    $sheet->getCell('D' . $r3)->setValue('W');
    $sheet->getCell('E' . $r3)->setValue($data['width']);
    $sheet->getCell('F' . $r3)->setValue('H');
    $sheet->getCell('G' . $r3)->setValue($data['height']);
}