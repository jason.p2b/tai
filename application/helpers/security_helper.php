<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');

function checkUser()
{
    $MODEL =& get_instance();
    $MODEL->load->model('login_model');

    $SESS =& get_instance();
    $SESS->load->library('session');

    $EN =& get_instance();
    $EN->load->library('encrypt');

    $uid = $EN->encrypt->decode($SESS->session->userdata('uid'));
    $username = $EN->encrypt->decode($SESS->session->userdata('username'));
    $isLogged = $SESS->session->userdata('logged_in');

    if ("" == $uid || "" == $username)
    {
        redirect('login');
    }

    $state = $MODEL->login_model->checkUserSession($username, $uid);

    if (!($state && $isLogged))
    {
        redirect('login');
    }
}