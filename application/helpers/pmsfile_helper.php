<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');

function add_pms_file($file, $index, $id_type, $id)
{
    if ($_FILES[$file]['error'][$index] != "0")
    {
        return array();
    }

    $path_part = pathinfo($_FILES[$file]['name'][$index]);
    $target = './files/' . $id . '_' . time() . '_' . rand() . '.' . $path_part['extension'];
    move_uploaded_file($_FILES[$file]['tmp_name'][$index], $target);

    $data = array(
                $id_type => $id,
                'filename' => $_FILES[$file]['name'][$index],
                'type' => $_FILES[$file]['type'][$index],
                'location' => $target
            );

    return $data;
}