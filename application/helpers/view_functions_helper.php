<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');

function load_customer_option($company)
{
    $og = 0;
    foreach ($company as $num => $info)
    {
        if (!in_array($info->cid, array(1,23)) && $og != $info->char_num)
        {
            echo "</optgroup>\n";
            $og = $info->char_num;
            echo "<optgroup label='" . $og . "筆劃'>\n";
        }
        else if ($info->cid == "1")
        {
            echo "<optgroup label='本企業'>";
        }
        echo "<option value='" . $info->cid . "'>" . $info->name . "</option>\n";
    }
    echo "</optgroup>\n";
}