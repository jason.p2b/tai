<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');

/*
 *      This helper file covert taiwanese date <=> ad date
 */


function convertToTaiwanDate($date)
{
    list($year, $month, $day) = explode("-", $date);
    $tpe_year = $year - 1911;
    $tpe_date = $tpe_year . "-" . $month . "-" . $day;

    return $tpe_date;
}

function convertToADDate($date)
{
    $newdate = new DateTime($date);
    $ad_date = date_add($newdate, date_interval_create_from_date_string('1911 years'));

    return date_format($ad_date, 'Y-m-d');
}

function convertToTaiwanYear($year)
{
    return $year - 1911;
}

function convertToADYear($year)
{
    return $year + 1911;
}