<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : Jason Liu
 *  License    : Free 
 *  Email      : jylp2b@yahoo.com
 *    
 *  ======================================= 
 */  
require_once APPPATH."/third_party/TCPDF/TCPDF.php"; 
 
class Pdf extends TCPDF { 
    public function __construct() { 
        parent::__construct(); 
    } 
}