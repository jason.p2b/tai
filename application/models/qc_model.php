<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the common functions used across entire website
 *
 */

class qc_model extends CI_Model
{
    function getQCTable($id)
    {
        $ids = array();
        $data = array();

        // $sql = "SELECT n.ny_order_item_id AS 'id', n.name, n.amount, n.big_category, n.window, n.box, h.date
        //         FROM ny_order_item n INNER JOIN ny_order_item_history h ON n.ny_order_item_id = h.ny_order_item_id
        //         WHERE ny_order_id = $id
        //         AND new_value IN (\"等出貨\", \"已出貨\")
        //         order by h.date";
        $sql = "SELECT u.name, c.*
                FROM ny_order n INNER JOIN quality_control c ON n.ny_order_id = c.ny_order_id
                    INNER JOIN user u ON c.user_id = u.uid
                WHERE n.ny_order_id = $id";
        $q   = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            $col['date'] = $row->date;
            $col['row_number'] = $row->row_number;
            $col['amount'] = $row->amount;
            $col['width'] = $row->width;
            $col['height'] = $row->height;
            $col['thickness'] = $row->thickness;
            $col['diagonal'] = $row->diagonal;
            $col['side'] = $row->side;
            $col['lock_height'] = $row->lock_height;
            $col['fake_handle'] = ($row->fake_handle == 1) ? "V" : "";
            $col['side_connect'] = $row->side_connect;
            $col['side_tightness'] = $row->side_tightness;
            $col['colour'] = ($row->colour == 1) ? "V" : "";
            $col['exterior'] = ($row->exterior == 1) ? "V" : "";
            $col['hardware'] = ($row->hardware == 1) ? "V" : "";
            $col['adhesion'] = $row->adhesion;
            $col['flatness'] = $row->flatness;
            $col['hinge_1'] = ($row->hinge_1 == 1) ? "V" : "";
            $col['hinge_2'] = ($row->hinge_2 == 1) ? "V" : "";
            $col['hinge_3'] = ($row->hinge_3 == 1) ? "V" : "";
            $col['hinge_4'] = ($row->hinge_4 == 1) ? "V" : "";
            $col['glass_top'] = ($row->glass_top == 1) ? "V" : "";
            $col['glass_side'] = ($row->glass_side == 1) ? "V" : "";
            $col['glass_width'] = ($row->glass_width == 1) ? "V" : "";
            $col['glass_height'] = ($row->glass_height == 1) ? "V" : "";
            $col['user'] = $row->name;

            $data[] = $col;
        }

        // if ($q->num_rows() > 0)
        // {
        //     foreach($q->result() as $row)
        //     {
        //         if (!in_array($row->id, $ids))
        //         {
        //             $col = array();
        //             array_push($ids, $row->id);

        //             $col['name'] = $row->name;
        //             $col['type'] = ("BS" == $row->big_category) ? "扇" : "框";
        //             $col['amount'] = $this->_getAmount($row->amount, $row->window);
        //             $col['color'] = "OK";
        //             $col['size'] = "OK";
        //             $col['quantity'] = "OK";
        //             $col['lock'] = "OK";
        //             $col['hinge'] = "OK";
        //             $col['other'] = "OK";
        //             $col['package'] = "OK";
        //             $col['date'] = Date("Y-m-d", strtotime($row->date));
        //             $col['user'] = $this->_getUser($col['date']);

        //             $data[] = $col;
        //         }
        //     }
        // }
        // else
        // {
        //     $q->free_result();
        //     $ids = array();

        //     $sql = "SELECT n.trad_order_item_id AS 'id', n.name, n.amount_l, n.amount_r, n.door_frame, h.date
        //             FROM trad_order_item n INNER JOIN trad_order_item_history h ON n.trad_order_item_id = h.trad_order_item_id
        //             WHERE ny_order_id = $id
        //             AND new_value IN (\"等出貨\", \"已出貨\")
        //             order by h.date";
        //     $q   = $this->db->query($sql);
        //     foreach($q->result() as $row)
        //     {
        //         if (!in_array($row->id, $ids))
        //         {
        //             $col = array();
        //             array_push($ids, $row->id);

        //             $col['name'] = $row->name;
        //             $col['type'] = $row->door_frame;
        //             $col['amount'] = $row->amount_l + $row->amount_r;
        //             $col['color'] = "OK";
        //             $col['size'] = "OK";
        //             $col['quantity'] = "OK";
        //             $col['lock'] = "OK";
        //             $col['hinge'] = "OK";
        //             $col['other'] = "OK";
        //             $col['package'] = "OK";
        //             $col['date'] = Date("Y-m-d", strtotime($row->date));
        //             $col['user'] = $this->_getUser($col['date']);

        //             $data[] = $col;
        //         }
        //     }
        // }

        $q->free_result();

        return $data;
    }

    function _getAmount($amount, $window)
    {
        if ("3" == substr($window, -1))
        {
            return "R" . $amount;
        }
        else
        {
            return "L" . $amount;
        }
    }

    function _getUser($date)
    {
        if ($date < "2019-02-11")
        {
            return "小柔";
        }
        else if ($date >= "2019-02-11" AND $date < "2019-07-01")
        {
            return "小戴";
        }
        else if ($date >= "2019-07-01" AND $date < "2020-02-01")
        {
            return "蕭國彬";
        }
        else
        {
            return "黃廷宇";
        }
    }
}