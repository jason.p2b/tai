<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class issue_model extends CI_Model
{
    function getAvailableUsers($status)
    {
        $data = array();

        $sql = "SELECT u.uid, u.name 
                FROM user u INNER JOIN user_status us ON u.uid = us.uid
                WHERE u.uid != " . $this->session->userdata('uid');
        if ($status > 2)
        {
            $sql .= " AND status_id IN (1,2,$status)";
        }
        $sql .= " ORDER BY us.status_id";

        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->uid] = $row->name;
        }
        $q->free_result();

        return $data;
    }

    function getIssueSequence($project_id)
    {
        $sql = "SELECT max(sequence) as 'seq' FROM issue WHERE project_id = $project_id";
        $q = $this->db->query($sql);
        $row = $q->first_row('array');

        return $row['seq'];
    }

    function getOrders($project_id)
    {
        $data = array();

        $sql = "SELECT o.order_id, o.order_name, o.model, f.model as 'f_model', f.status
                FROM `order` o INNER JOIN order_frame f ON o.order_id = f.order_id
                WHERE project_id = $project_id";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            if ($row->model != $row->f_model)
            {
                $data['frame_' . $row->order_id] = "門框：" . $row->order_name . " / " . $row->model . "(" . $row->f_model . ") / " . $row->status;
            }
            else
            {
                $data['frame_' . $row->order_id] = "門框：" . $row->order_name . " / " . $row->model . " / " . $row->status;
            }
        }
        $q->free_result();

        $sql = "SELECT o.order_id, o.order_name, o.model, f.model as 'f_model', f.status
                FROM `order` o INNER JOIN order_door f ON o.order_id = f.order_id
                WHERE project_id = $project_id";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            if ($row->model != $row->f_model)
            {
                $data['door_' . $row->order_id] = "門扇：" . $row->order_name . " / " . $row->model . "(" . $row->f_model . ") / " . $row->status;
            }
            else
            {
                $data['door_' . $row->order_id] = "門扇：" . $row->order_name . " / " . $row->model . " / " . $row->status;
            }
        }
        $q->free_result();
        
        return $data;
    }

    function getLastConversation($col="", $val="", $time="")
    {
        $sub_sql = ($col != "") ? " AND $col = '$val' " : "";

        date_default_timezone_set('Asia/Taipei');
        $today = new DateTime();
        $today->setTime(0,0,0);

        $files = $this->_getConversationFiles();

        $data = array();
        $sql = "SELECT c.conversation_id, 
                    CONCAT(p.customer, p.site) as 'project' , 
                    i.sequence, 
                    i.issue_id, 
                    i.subject, 
                    i.issue_type, 
                    tu.name as 't_user', 
                    u.name as 'o_user', 
                    c.create_date, 
                    c.conversation
                FROM issue i INNER JOIN conversation c ON i.issue_id = c.issue_id
                    INNER JOIN user u ON c.uid = u.uid
                    INNER JOIN user tu ON c.to_uid = tu.uid
                    INNER JOIN project p ON p.project_id = i.project_id
                WHERE c.conversation_id IN (
                    SELECT MAX( c.conversation_id ) 
                    FROM issue i
                    INNER JOIN conversation c ON i.issue_id = c.issue_id
                    GROUP BY c.issue_id)
                    $sub_sql
                ORDER BY c.create_date DESC";
        $q = $this->db->query($sql);
        foreach ($q->result_array() as $row)
        {
            $row['new'] = ("" != $time && $row['create_date'] >= $time) ? true : false;
            $row['files'] = (!isset($files[$row['conversation_id']])) ? array() : $files[$row['conversation_id']];
            $row['create_date'] = $this->_changeDate($today, $row['create_date']);
            $data[] = $row;
        }
        $q->free_result();

        return $data;
    }

    function _getConversationFiles()
    {
        $files = array();
        $sql = "SELECT * FROM conversation_file";
        $q = $this->db->query($sql);
        foreach ($q->result_array() as $row)
        {
            if (preg_match('/image/', $row['type']))
            {
                $image =  base_url() . $row['location'];
            }
            else if (preg_match('/pdf/', $row['type']))
            {
                $image =  base_url() . '/img/pdf_image.png';
            }
            else if (preg_match('/\.doc/', $row['filename']))
            {
                $image =  base_url() . '/img/word_image.png';
            }
            else if (preg_match('/\.xls/', $row['filename']))
            {
                $image =  base_url() . '/img/excel.png';
            }
            else
            {
                $image = base_url() . '/img/document_image.png';
            }

            $files[$row['conversation_id']][] = array(
                                                    'filename' => $row['filename'],
                                                    'location' => $row['location'],
                                                    'file_id'  => $row['conversation_file_id'],
                                                    'img' => $image
                                                );
        }
        $q->free_result();
        return $files;
    }

    function _changeDate($today, $dateTime)
    {
        date_default_timezone_set('Asia/Taipei');
        $date = new DateTime($dateTime);
        $date->setTime(0,0,0);

        $diff = $today->diff( $date );
        $diffDays = (integer) $diff->format( "%R%a" );

        if ($diffDays == 0)
        { 
            $time = time() - strtotime($dateTime);
            if ($time < 60)
            {
                return $time . "秒前";
            }
            else if ($time < 3600)
            {
                return round($time/60) . "分鐘前";
            }
            else if ($time < 432600)
            {
                return "約" . round($time/3600) . "小時前";
            }
            else
            {
                return "今天 " . date("g:i A", strtotime($dateTime));
            } 

        }
        else if ($diffDays == -1)
        {
            return "昨天 " . date("g:i A", strtotime($dateTime));
        }
        else if ($diffDays == -2)
        {
            return "前天 " . date("g:i A", strtotime($dateTime));
        }
        else if ($diffDays > -7)
        {
            return abs($diffDays) . "天前 " . date("g:i A", strtotime($dateTime));
            //setlocale(LC_ALL, 'zh-Hant-TW');
            //return strftime('%A', strtotime($dateTime)) . ' ' . date("g:ia", strtotime($dateTime));
        }
        else
        {
            return date("Y-m-d g:i A", strtotime($dateTime));
        }
    }

    function getConversation($issue_id)
    {
        date_default_timezone_set('Asia/Taipei');
        $today = new DateTime();
        $today->setTime(0,0,0);

        $files = $this->_getConversationFiles();
        $sql = "SELECT DISTINCT c.conversation_id, i.subject, i.issue_type, c.create_date, tu.name as 't_user', u.name as 'o_user', c.uid, c.conversation
                FROM conversation c INNER JOIN issue i ON c.issue_id = i.issue_id
                    INNER JOIN user u ON c.uid = u.uid
                    INNER JOIN user tu ON c.to_uid = tu.uid
                WHERE c.issue_id = $issue_id
                ORDER BY c.conversation_id";
        $q = $this->db->query($sql);
        foreach ($q->result_array() as $row)
        {
            $row['files'] = (!isset($files[$row['conversation_id']])) ? array() : $files[$row['conversation_id']];
            $row['create_date'] = $this->_changeDate($today, $row['create_date']);
            $data[] = $row;
        }
        $q->free_result();

        return $data;
    }

    function download($conversation_id, $conversation_file_id)
    {
        $sql = "SELECT filename, location
                FROM conversation_file
                WHERE conversation_id = $conversation_id
                    AND conversation_file_id = $conversation_file_id";
        $q = $this->db->query($sql);
        $data = $q->first_row('array');
        $q->free_result();

        return $data;
    }

    function get_company_tracker()
    {
        
    }
}