<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the common functions used across entire website
 *
 */

class svg_model extends CI_Model
{
    public function getTradFrameSVG($item_id)
    {
        $svg = array();
        $sql = "SELECT * FROM trad_order_item WHERE trad_order_item_id = $item_id";
        $q   = $this->db->query($sql);
        $row = $q->first_row('array');
        $q->free_result();

        $svg = $row;
        $row['set'] = $this->_calculateSet($row);
        $row['half']  = floor($row['set']/2);
        $svg['site']  = $this->_getProject($row['ny_order_id']);
        $svg['main']  = $this->_getMain($row['frame_shape']);
        $svg['frame'] = $this->_getFrame($row);
        $svg['hinge_svg'] = $this->_getHinge($row);
        $svg['lock_svg']  = $this->_getLock($row);
        $svg['magnetic_hole_svg'] = $this->_getMagneticHole($row);
        $svg['fail_safe_lock_svg'] = $this->_getFailSafeLock($row);
        $svg['closer_svg'] = $this->_getCloser($row);

        return $svg;
    }

    function _getProject($ny_order_id)
    {
        $sql = "SELECT CONCAT(p.site, ' (', o.order_number,')') AS 'site' 
                FROM ny_order o INNER JOIN project p ON o.project_id = p.project_id
                WHERE o.ny_order_id = $ny_order_id";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $q->free_result();

        return $row->site;
    }

    function _getMain($frame_shape)
    {   
        $id = 1;

        if (in_array($frame_shape, array("entrance_frame","180度","270度")))
        {
            $id = 2;
        }

        $sql = "SELECT svg FROM svg WHERE id=$id";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $q->free_result();
        return $row->svg;
    }

    function _getFrame($data)
    {
        $svg = "";
        $s = array();
        $r = array();

        $set = $data['set'];
        $field = (preg_match('/四面框/', $data['type'])) ? "svg_4side" : "svg_normal";
        $sql = "SELECT $field AS 'svg' FROM svg_frame WHERE name = '" . $data['frame_shape'] . "'";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $svg = $row->svg;
        $q->free_result();

        if ($data['v_east_length'] - $data['v_west_length'] <= 21) {
            $data['v_east_length'] += 0.5;
        }
        switch ($data['frame_shape']) 
        {
            case 'l_frame':
            case 'la_frame':
                $s = array('$A$','$A-6$','$B$','$C$','$D$','$HEIGHT$','$WIDTH-2*C$','$SET$','$SET-3$','25-3');
                $r = array($data['v_total_length'],$data['v_total_length']-6,$data['v_north_length'],$data['v_west_length'],$data['v_east_length'],$data['height'],$data['width'] - 2*$data['v_west_length'],$set,$set-3,22);
                break;
            case 't_frame':
            case 'ta_frame':
                $s = array('$A$','$A-6$','$B$','$C$','$D$','$HEIGHT$','$WIDTH-2*C$','$SET$','$SET-3$','25-3','$E$','$E-3$');
                $r = array($data['v_total_length'],$data['v_total_length']-6,$data['v_north_length'],$data['v_west_length'],$data['v_east_length'],$data['height'],$data['width'] - 2*$data['v_west_length'],$set,$set-3,22,$data['v_south_east_length'],$data['v_south_east_length']-3);
                break;
            case '120_frame':
                $s = array('$WIDTH-2*C$','$WIDTH-2*D$','$HEIGHT$','$B$');
                $r = array($data['width']-2*$data['v_west_length']+30, $data['width']-2*$data['v_east_length'],$data['height'],$data['v_north_length']);
                break;
            case 'entrance_frame':
                $s = array('$A$','$A-102$','$HEIGHT$','$WIDTH$','$A-3$');
                $r = array($data['v_total_length'],$data['v_total_length']-128,$data['height'],$data['width']-110,$data['v_total_length']-3);
                break;
            case '180度':
                $s = array('$HEIGHT$','$WIDTH-120$');
                $r = array($data['height'],$data['width']-120);
                break;
            case '270度':
                $s = array('$HEIGHT$','$WIDTH$');
                $r = array($data['height'],$data['width']);
                break;
        }
        $svg = str_replace($s, $r, $svg);
        return $svg;
    }

    function _getLock($data)
    {
        if ("單開" != $data['coupled'])
        {
            return "";
        }

        $sql = "";
        $gap   = (!preg_match('/四面框/', $data['type'])) ? 15 : 5;
        $depth = (!preg_match('/四面框/', $data['type'])) ? $data['depth'] : $data['v_west_length'];

        $set = $this->_calculateSet($data);

        // $set   = (preg_match('/120A/', $data['type'])) ? 68 : 55;
        $half  = $data['half'];
        $hh    = 0;
        $model = $data['lock'];

        if ("0" == $data['handle_height'] && !preg_match('/四面框/', $data['type']))
        {
            $hh = 1000;
        }
        else if ("0" == $data['handle_height'])
        {
            $hh = ($data['height'] - 2*$data['v_west_length']) / 2;
        }
        else if (preg_match('/四面框/', $data['type']))
        {
            $hh = $data['handle_height'] - $data['v_west_length'];
        }
        else
        {
            $hh = $data['handle_height'];
        }

        switch ($model) {
            case 'CN-605 平頭鎖':  // 斜舌
            case 'CN-607 平頭鎖':  // 平舌
                $hh += -26;
                break;
            case 'CN-1800 平推鎖':
            case 'CN-1800 平推鎖 + CN-K106 外側把手':
            case 'CN-26K 水平鎖':
                $hh += 2;
                break;
            case 'CN-21-01S 水平鎖':
                break;
            case '華豫寧WEL-3600電子鎖':
                $hh += 50;
                break;
        }

        switch ($data['frame_shape']) {
            case '120_frame':
                $sql = "SELECT single_svg AS 'svg' FROM special_svg WHERE model = '$model' AND `type` = '120_frame'";
                break;
            case 'entrance_frame':
                $sql = "SELECT single_svg AS 'svg' FROM special_svg WHERE model = '$model' AND `type` = 'entrance_frame'";
                break;
            case '180度':
                $sql = "SELECT single_svg AS 'svg' FROM special_svg WHERE model = '$model' AND `type` = '180度'";
                break;
            case '270度':
                $sql = "SELECT single_svg AS 'svg' FROM special_svg WHERE model = '$model' AND `type` = '270度'";
                break;
            default:
                $sql = "SELECT svg FROM `lock` WHERE model = '$model'";
            break;
        }

        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $svg = $row->svg;
        $q->free_result();

        $search  = array('$GAP$','$DEPTH$', '$HALF$', '$HH$');
        $replace = array($gap, $depth, $half, $hh);
        $svg = str_replace($search, $replace, $svg);

        return $svg;
    }

    function _getHinge($data)
    {
        $sql = "";
        $gap   = (!preg_match('/四面框/', $data['type'])) ? 15 : 5;
        $depth = (!preg_match('/四面框/', $data['type'])) ? $data['depth'] : $data['v_west_length'];
        $topFlag = 168.5 + $data['v_west_length'];
        $topButterfly = 205 + $data['v_west_length']; // used in 蝴蝶鉸練
        $half  = $data['half'];
        $middle= ($data['height'] - $data['v_west_length'] - $depth - 5 - $gap) / 2 + $data['v_west_length'] + 5; // used in GD902 & 1050L
        $dist  = round(floor(($data['height'] - 50 - 5 - 167 - 133 - 15 - $depth) / 3),-1); // used in 1050L
        $avg   = floor(($data['height'] - 210 - 160 - $depth) / 4); // used in entrance_frame CN127
        $field = ("單開" == $data['coupled']) ? "single_svg" : "double_svg";
        $model = $data['hinge'];
        $hinge_gap = $data['height'] - $gap - $topFlag - $depth - 136.5; // used in Flag Hinge e.g 1050A
        
        switch ($model) {
            case 'CN-1050A 旗形鉸鏈 * 4':
                $hinge_gap -= 600;
                break;
            case 'CN-1050A 旗形鉸鏈 * 3':
                $hinge_gap -= 200;
                break;
            default:
                break;
        }
        
        switch ($data['frame_shape']) {
            case 'entrance_frame':
                $sql = "SELECT $field AS svg FROM special_svg WHERE model = '$model' AND `type` = 'entrance_frame'";
                break;
            case '180度':
                $sql = "SELECT $field AS svg FROM special_svg WHERE model = '$model' AND `type` = '180度'";
                break;
            case '270度':
                $sql = "SELECT $field AS svg FROM special_svg WHERE model = '$model' AND `type` = '270度'";
                break;
            default:
                $sql = "SELECT $field AS svg FROM hinge WHERE model = '$model'";
            break;
        }

        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $svg = $row->svg;
        $q->free_result();

        $search  = array('$GAP$','$DEPTH$', '$168.5+C$', '$MIDDLE$', '$HALF$', '$205+C$', '$DIST$','$HALL$', '$HINGE_GAP$');
        $replace = array($gap, $depth, $topFlag, $middle, $half, $topButterfly, $dist, $avg, $hinge_gap);
        $svg = str_replace($search, $replace, $svg);

        return $svg;
    }

    function _getMagneticHole($data)
    {
        if ("1" != $data['magnetic_hole'])
        {
            return "";
        }

        $sql = "";
        $svg = "";
        $set = $data['set'];
        $field = ("單開" == $data['coupled']) ? "single_svg" : "double_svg";

        $half  = $data['half'] - 3;
        $distanceS = $data['magnetic_hole_distance'] + 5;
        $distanceD = $data['magnetic_hole_distance'] + 3;
        $mother    = $data['mother_distance'] + 5 + 4;

        switch ($data['frame_shape']) 
        {
            case '120_frame':
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'magnetic_hole' AND `type` = '120_frame'";
                break;
            case 'entrance_frame':
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'magnetic_hole' AND `type` = 'entrance_frame'";
                break;
            case '180度':
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'magnetic_hole' AND `type` = '180度'";
                break;
            case '270度':
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'magnetic_hole' AND `type` = '270度'";
                break;
            default:
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'magnetic_hole' AND `type` = 'normal_frame'";
            break;
        }

        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $svg = $row->svg;
        $q->free_result();

        $search  = array('$HALF-3$','$DIST+5$', '$DIST+3$', '$MOTHER$');
        $replace = array($half, $distanceS, $distanceD, $mother);

        if ("1" != $data['magnetic_hole_son'])
        {
            $search[]  = 'id="magnetic-hole-son"';
            $replace[] = 'style="display:none;"';
        }
        $svg = str_replace($search, $replace, $svg);

        return $svg;
    }

    function _getFailSafeLock($data)
    {
        if ("" == $data['fail_safe_lock'] || 0 == $data['fail_safe_lock'])
        {
            return "";
        }

        $sql = "";
        $svg = "";
        // $set = (preg_match('/60B/', $data['type'])) ? 48 : 55;
        $set = $data['set'];
        $field = ("單開" == $data['coupled']) ? "single_svg" : "double_svg";

        $half  = $data['half'] - 3;
        $distanceS = $data['fail_safe_lock_distance'] + 5;
        $distanceD = $data['fail_safe_lock_distance'] + 3;
        $mother    = $data['mother_distance'] + 5 + 4;

        switch ($data['frame_shape']) 
        {
            case '120_frame':
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'fail_safe_lock' AND `type` = '120_frame'";
                break;
            case 'entrance_frame':
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'fail_safe_lock' AND `type` = 'entrance_frame'";
                break;
            case '180度':
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'fail_safe_lock' AND `type` = '180度'";
                break;
            case '270度':
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'fail_safe_lock' AND `type` = '270度'";
                break;
            default:
                $sql = "SELECT $field AS 'svg' FROM special_svg WHERE model = 'fail_safe_lock' AND `type` = 'normal_frame'";
            break;
        }

        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $svg = $row->svg;
        $q->free_result();

        $sql = "SELECT DISTINCT f.* 
                FROM fsl_info f INNER JOIN ny_order n ON f.project_id = n.project_id
                WHERE n.ny_order_id = " . $data['ny_order_id'];
        $q   = $this->db->query($sql);
        $res = $q->first_row();
        $q->free_result();

        $search  = array('$HALF-3$','$DIST+5$', '$DIST+3$', '$MOTHER$','$A$','$B$','$C$','$D$','$E$','$F$','$W$','$L$');
        $replace = array($half, $distanceS, $distanceD, $mother, $res->fsl_a, $res->fsl_b, $res->fsl_c, $res->fsl_d, $res->fsl_w/2, ($res->fsl_w - $res->fsl_b)/2, $res->fsl_w, $res->fsl_l);

        switch ($res->type) {
            case '1-hole':
                $search[]  = 'class="2-hole';
                $replace[] = 'style="display:none;" "';
                break;
            case '2-hole':
                $search[]  = 'class="1-hole';
                $replace[] = 'style="display:none;" "';
                break;
        }

        if ("1" != $data['fail_safe_lock_son'])
        {
            $search[]  = 'id="fsl-son"';
            $replace[] = 'style="display:none;"';
        }

        $svg = str_replace($search, $replace, $svg);
        return $svg;
    }

    function _getCloser($data)
    {
        $sql = "";
        $svg = "";
        $model = $data['closer'];
        $set = $data['set'];
        $field = ("單開" == $data['coupled']) ? "single_svg" : "double_svg";
        $half  = $data['half'] - 3;

        $sql = "SELECT $field AS 'svg' FROM closer WHERE model='$model'";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $svg = $row->svg;
        $q->free_result();

        $svg = str_replace('$HALF-3$', $half, $svg);

        return $svg;
    }

    function _calculateSet ($data)
    {
        $set = "";

        if (preg_match('/120A/', $data['type'])) 
        {
            $set = 68;
        }
        else if ($data['standard'] == "CNS 11227-1")
        {
            $set = 57;
        }
        else if (preg_match('/CN-180/', $data['hinge']) && $data['falling_strip'] == "隱藏式")
        {
            $set = 57;
        }
        else
        {
            $set = 55;
        }

        return $set;
    }


    /*******************************************************************************************************************************
    *
    *  BELOW FUNCTIONS ARE FOR 南亞訂單
    *
    * ******************************************************************************************************************************/

    public function getNYFrameSVG($item_id)
    {
        $svg = array();
        $sql = "SELECT i.*, f.*
                FROM ny_order_item i INNER JOIN ny_order n ON i.ny_order_id = n.ny_order_id
                    INNER JOIN frame_setting f ON n.project_id = f.project_id
                WHERE i.ny_order_item_id = $item_id";
        $q   = $this->db->query($sql);
        $row = $q->first_row('array');
        $q->free_result();

        switch ($row['wind']) {
            case 'F1':
                $row['frame'] = "l_frame";
                $row['set']   = $row['la_a'];
                $row['b'] = $row['la_b'];
                $row['c'] = $row['la_c'];
                $row['d'] = $row['la_d'];
                $row['e'] = 0;
                break;
            case 'F2':
                $row['frame'] = "la_frame";
                $row['set']   = $row['la_a'];
                $row['b'] = $row['la_b'];
                $row['c'] = $row['la_c'];
                $row['d'] = $row['la_d'];
                $row['e'] = 0;
                break;
            case 'F3':
                $row['frame'] = "ta_frame";
                $row['set']   = $row['ta_a'];
                $row['b'] = $row['ta_b'];
                $row['c'] = $row['ta_c'];
                $row['d'] = $row['ta_d'];
                $row['e'] = $row['ta_e'];
                break;
        }

        $svg = $row;
        $svg['site']  = $this->_getProject($row['ny_order_id']);

        // get amount
        $svg['amount'] = $this->_getNYAmount($row['ny_order_id']);

        // get paper setting
        $svg['main']  = $this->_getMain('ny_frame_all_vertical');

        // get frame style
        $svg['frame'] = $this->_getNYFrame($row);

        // get hinge
        // $svg['hinge_svg'] = $this->_getNYHinge($row);
        $svg['lock_svg']  = $this->_getNYLock($row);
        // $svg['magnetic_hole_svg'] = $this->_getMagneticHole($row);
        // $svg['fail_safe_lock_svg'] = $this->_getFailSafeLock($row);
        // $svg['closer_svg'] = $this->_getCloser($row);

        return $svg;
    }

    public function _getNYFrame($data)
    {
        $frame = $data['frame'];
        $svg = "";
        $s = array();
        $r = array();
        $set = $data['set'];
        $b = $data['b'];
        $c = $data['c'];
        $d = $data['d'];
        $e = $data['e'];
        $reduce = $data['horizontal_reduce'];

        $field = ("" != $data['threshold']) ? "svg_4side" : "svg_normal";
        $sql = "SELECT $field AS 'svg' FROM svg_frame WHERE name = '$frame'";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $svg = $row->svg;
        $q->free_result();

        switch ($frame) 
        {
            case 'l_frame':
            case 'la_frame':
                $s = array('$A$','$A-6$','$B$','$C$','$D$','$HEIGHT$','$WIDTH-2*C$','$SET$','$SET-3$','25-3');
                $r = array($data['up_window'],$data['up_window']-$reduce*2,$b,$c,$d,$data['total_height'],$data['width'] - 2*$c,$set,$set-$reduce,25-$reduce);
                break;
            case 't_frame':
            case 'ta_frame':
                $s = array('$A$','$A-6$','$B$','$C$','$D$','$HEIGHT$','$WIDTH-2*C$','$SET$','$SET-3$','25-3','$E$','$E-3$');
                $r = array($data['up_window'],$data['up_window']-$reduce*2,$b,$c,$d,$data['total_height'],$data['width'] - 2*$c,$set,$set-$reduce,25-$reduce,$e,$e-3);
                break;
        }
        $svg = str_replace($s, $r, $svg);
        return $svg;
    }

    function _getNYLock($data)
    {
        if (5 <= (int) substr($data['window'],-1))
        {
            return "";
        }

        $gap   = ("" != $data['threshold']) ? $data['gap'] : 5;
        $depth = ("" != $data['threshold']) ? $data['depth'] : $data['c'];
        $set   = $data['set'];
        $half  = floor(($set-2)/2);
        $hh    = 0;
        $model = $data['lock'];

        if (!$data['handle_orientation'])
        {
            $hh = $data['total_height'] - $data['lock_height'] - $depth - 5 - $data['c'];
        }
        else
        {
            $hh = $data['lock_height'];
        }

        $sql = "SELECT svg FROM `五金` WHERE skid = '" . $data['lock'] . "'";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $svg = $row->svg;
        $q->free_result();

        $search  = array('$GAP$','$DEPTH$', '$HALF$', '$HH$');
        $replace = array($gap, $depth, $half, $hh);
        $svg = str_replace($search, $replace, $svg);

        return $svg;
    }

    function _getNYHinge($data)
    {
        // $sql = "";
        // $gap   = (!preg_match('/四面框/', $data['type'])) ? 15 : 5;
        // $depth = (!preg_match('/四面框/', $data['type'])) ? $data['depth'] : $data['v_west_length'];
        // $topFlag = 168.5 + $data['v_west_length'];
        // $topButterfly = 205 + $data['v_west_length']; // used in 蝴蝶鉸練
        // $half  = 55 / 2;
        // $middle= ($data['height'] - $data['v_west_length'] - $depth - 5 - $gap) / 2 + $data['v_west_length'] + 5; // used in GD902 & 1050L
        // $dist  = round(floor(($data['height'] - 50 - 5 - 167 - 133 - 15 - $depth) / 3),-1); // used in 1050L
        // $avg   = floor(($data['height'] - 210 - 160 - $depth) / 4); // used in entrance_frame CN127
        // $field = ("單開" == $data['coupled']) ? "single_svg" : "double_svg";
        // $model = $data['hinge'];

        // switch ($data['frame_shape']) {
        //     case 'entrance_frame':
        //         $sql = "SELECT $field AS svg FROM special_svg WHERE model = '$model' AND `type` = 'entrance_frame'";
        //         break;
        //     case '180度':
        //         $sql = "SELECT $field AS svg FROM special_svg WHERE model = '$model' AND `type` = '180度'";
        //         break;
        //     case '270度':
        //         $sql = "SELECT $field AS svg FROM special_svg WHERE model = '$model' AND `type` = '270度'";
        //         break;
        //     default:
        //         $sql = "SELECT $field AS svg FROM hinge WHERE model = '$model'";
        //     break;
        // }

        // $q   = $this->db->query($sql);
        // $row = $q->first_row();
        // $svg = $row->svg;
        // $q->free_result();

        // $search  = array('$GAP$','$DEPTH$', '$168.5+C$', '$MIDDLE$', '$HALF$', '$205+C$', '$DIST$','$HALL$');
        // $replace = array($gap, $depth, $topFlag, $middle, $half, $topButterfly, $dist, $avg);
        // $svg = str_replace($search, $replace, $svg);

        // return $svg;
    }
}