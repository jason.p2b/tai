<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the common functions used across entire website
 *
 */

class customer_model extends CI_Model
{   
    public function __construct() {
        parent::__construct();
        $this->load->model('status_model');
    }

    // get customer table
    function get($uid)
    {
        $where = $this->status_model->getWhere($uid, 'customer_id', 'customer_id');
        $data = array();
        $sql = "SELECT s.company as 'main', c.company, c.customer_id, c.code
                FROM customer c INNER JOIN status s ON c.status_id = s.status_id
                WHERE $where
                ORDER BY c.status_id DESC, c.code ASC";
        $q   = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->main][$row->customer_id] = $row->code . ' - ' . $row->company;
        }
        $q->free_result();

        return $data;
    }

    function getCustomer($uid)
    {
        $where = $this->status_model->getWhere($uid, 'customer_id', 'customer_id');
        $data = array();
        $sql = "SELECT * FROM customer WHERE $where";
        $q = $this->db->query($sql);
        $rows = $q->result();
        $q->free_result();
        return $rows;
    }

    function update($data, $id)
    {
        $this->db->where('customer_id', $id);
        $this->db->update('customer', $data);
        return true;
    }
}