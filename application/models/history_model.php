<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the common functions used across entire website
 *
 */

class history_model extends CI_Model
{
    function getOrderHistory($ny_order_id)
    {
        $diary = array();

        $sql = "SELECT * 
                FROM (
                    SELECT i.row_number, u.name AS 'user', i.name, tr.name_tw AS 'col', ih.date, ih.old_value, ih.new_value
                    FROM ny_order n INNER JOIN ny_order_item i ON n.ny_order_id = i.ny_order_id
                        INNER JOIN ny_order_item_history ih ON i.ny_order_item_id = ih.ny_order_item_id
                        INNER JOIN user u ON u.uid = ih.user_id
                        INNER JOIN translation tr ON tr.field = ih.field
                    WHERE n.ny_order_id = $ny_order_id
                    UNION
                    SELECT t.row_number, u.name AS 'user', CONCAT(t.name,'(', IF(t.door_frame='扇' AND t.coupled='雙開',t.is_mother,''),t.door_frame,')') AS 'name', tr.name_tw AS 'col', ih.date, ih.old_value, ih.new_value
                    FROM ny_order n INNER JOIN trad_order_item t ON n.ny_order_id = t.ny_order_id
                        INNER JOIN trad_order_item_history ih ON t.trad_order_item_id = ih.trad_order_item_id
                        INNER JOIN user u ON u.uid = ih.user_id
                        INNER JOIN translation tr ON tr.field = ih.field
                    WHERE n.ny_order_id = $ny_order_id
                ) a 
                ORDER BY date, `row_number`";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $dt   = new DateTime($row->date);
            $date = $dt->format('Y/m/d, l');
            $time = $dt->format('h:i a');
            $text = (!("進度" == $row->col)) ? '<span class="text text-info">' . $row->col . '</span>' : '<span class="text text-warning">' . $row->col . '</span>';
            $old  = ("" != $row->old_value) ? $row->old_value : "無";
            $new  = ("" != $row->new_value) ? $row->new_value : "無";

            if ("新訂單" != $row->col)
            {
                $diary[$date][$time][] = "$row->user 修改 <span class='text text-success'><i> (" . $row->row_number . ") " . $row->name . "</i></span> $text: $old -> $new";
            }
            else
            {
                $diary[$date][$time][] = "<span class='glyphicon glyphicon-star'></span>$row->user <span class='text text-success'><strong>新加訂單</strong> <i>(" . $row->row_number . ") " . $row->name . "</i></span>";
            }
            
        }
        $q->free_result();

        return $diary;
    }
}