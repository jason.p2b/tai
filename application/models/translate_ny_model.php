<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the common functions used across entire website
 *
 */

class translate_ny_model extends CI_Model
{

    public function getTranslateOrderItem($ny_order_id)
    {
        $data = array();
        $sql = "SELECT * FROM ny_order_item WHERE ny_order_id = $ny_order_id";
        $q   = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            $col = array();
            $col['ny_order_item_id'] = $row['ny_order_item_id'];
            $col['row_number'] = $row['row_number'];
            $col['progress'] = $row['progress'];
            $col['due_date'] = $row['due_date'];
            $col['name'] = $row['name'];
            $col['類別'] = ("BS" == $row['big_category']) ? "扇" : "框";
            $col['類型'] = $this->_getMiddle($row['middle_category']);
            $col['開向'] = $this->_getWindow($row['window']);
            if ("框" == $col['類別'])
            {
                $col["框寬"] = $row['width'];
                $col["框深"] = $row['up_window'];
                $col["框高"] = $row['total_height'];
                $col['框型'] = $this->_getWind($row['wind']);
            }
            else
            {
                $col["門寬"] = $row['width'];
                $col["門厚"] = $row['up_window'];
                $col["門高"] = $row['total_height'];
            }
            $col['skin'] = $this->_getSkin($row['colour'], $row['big_category'], $row['wind']);
            $col['數量'] = $row['amount'];
            $col['單價'] = $row['price'];
            $col['總價'] = $row['amount'] * $row['price'];
            $col['門檻'] = $row['threshold'];
            $col['封邊材質'] = $this->_getMaterial($row['big_category'], substr($row['window'], 1, 1), substr($row['mixed_frame'], 0, 1));
            $col['鉸鏈'] = $this->_getHinge($row['mixed_frame']);
            $col['防橇栓'] = $this->_getBolt($row['mixed_frame']);
            $col['上加工'] = $this->_getUpBox(substr($row['Box'], 1, 1));
            $col['下加工'] = $this->_getDownBox(substr($row['Box'],-1),$row['big_category']);
            $col['L1'] = $row['L1'];
            $col['L2'] = $row['L2'];
            $col['L3'] = $row['L3'];
            $col['L4'] = $row['L4'];
            $col['L5'] = $row['L5'];
            $col['埋入深度'] = $row['depth'];
            $col['鎖'] = $this->_getLock($row['lock']);
            $col['鎖高'] = $row['lock_height'];
            $col['f1'] = $row['f1'];
            $col['f2'] = $row['f2'];
            $col['f3'] = $row['f3'];
            $col['f4'] = $row['f4'];
            $col['把手'] = $this->_getExtraHandle($row['f1']);

            $data[] = $col;
        }
        $q->free_result();

        return $data;
    }

    // 材質
    function _getMaterial($big, $window, $mixed_frame)
    {
        if ("BS" == $big) // 扇
        {
            switch ($mixed_frame) {
                case 'T':
                    return "SECC 1.2t";

                case 'S':
                    return "SUS 1.2t";

                default:
                    return "找不到";
            }
        }
        else
        {
            switch ($window) {
                case 'T':
                    return "SECC 1.6t";

                case 'S':
                    return "SUS 1.5t";

                default:
                    return "找不到";
            }
        }
    }

    // 中類 -> 類型
    function _getMiddle($code)
    {
        $result = "";

        switch ($code) {
            case 'RK':
                $result = "玄關框";
                break;

            case 'RF':
                $result = "防火框";
                break;

            case 'LK':
                $result = "玄關門";
                break;
            
            case 'L8':
                $result = "防火門";
                break;
            
            case 'L7':
                $result = "非防火";
                break;

            default:
                $result = "找不到";
                break;
        }

        return $result;
    }

    // 上中下窗 -> 開向
    function _getWindow($code)
    {
        $data = "";
        $direction = substr(str_replace(' ', '', $code), 1);
        $sql = "SELECT name FROM `上中下窗` WHERE code = '$direction'";
        $q = $this->db->query($sql);
        $data = $q->first_row();
        $q->free_result();

        $data = (empty($data)) ? "找不到" : $data->name;

        switch (substr($code,0,1)) {
            case 'T':
                $data .= "(雅雕CT)";
                break;
            case 'P':
                $data .= "(經典PD)";
                break;
            case 'C':
                $data .= "(雅鑲CP)";
                break;
            case 'F':
                $data .= "(全鏽鋁FP)";
                break;
        }

        return $data;
    }

    // 風壓 -> 框型
    function _getWind($code)
    {
        $data = "";
        $sql = "SELECT name FROM `框型` WHERE code = '$code'";
        $q = $this->db->query($sql);
        $data = $q->first_row();
        $q->free_result();

        $data = (empty($data)) ? "找不到" : $data->name;

        return $data;
    }

    // 色號 -> Skin
    function _getSkin($code, $big_category, $wind)
    {
        $data = "";
        $sql = "SELECT * FROM `色號` WHERE code = '$code'";
        $q = $this->db->query($sql);
        $data = $q->first_row();
        $q->free_result();

        $data = (empty($data)) ? "找不到" : $data->order;

        if ("BS" == $big_category && preg_match("/^T/", $wind))
        {
            $data .= " (刻溝$wind)";
        }

        return $data . " / $code";
    }

    // 混合框(2,3 char) -> 鉸練
    function _getHinge($string) 
    {
        $data = "";
        $sql = "SELECT name FROM `鉸鏈` WHERE code = '" . substr($string,1,1) . "'";
        $q = $this->db->query($sql);
        $data = $q->first_row();
        $q->free_result();

        $data = (empty($data)) ? "找不到" : $data->name;
        preg_match('/^\w\w(\d+)/', $string, $match);

        return $match[1] . " X " . $data;
    }

    // 混合框(3,4 char) -> 防橇栓
    function _getBolt($string) 
    {
        // $result = "";
        preg_match('/^\w\d+(\w+)(\d+)$/', $match);
        $amount = substr($string, -1);
        $code = substr($string, -2, 1);
// echo "$string - code:" . $code . "<BR>\n";
        $sql = "SELECT name FROM `防橇栓` WHERE code = '$code'";
        $q = $this->db->query($sql);
        $data = $q->first_row();
        $q->free_result();

        $data = (empty($data)) ? "找不到" : $amount . "X" . $data->name;

        return $data;
    }

    // Box色系 -> 上加工
    function _getUpBox($code) 
    {
        $data = "";
        $sql = "SELECT name FROM `上加工` WHERE code = '$code'";
        $q = $this->db->query($sql);
        $data = $q->first_row();
        $q->free_result();

        $data = (empty($data)) ? "找不到" : $data->name;

        return $data;
    }

    // Box色系 -> 下加工
    function _getDownBox($code, $big) 
    {
        $data = "";
        $sql = "SELECT name FROM `下加工` WHERE code = '$code'";
        $q = $this->db->query($sql);
        $data = $q->first_row();
        $q->free_result();

        if ($code == "1")
        {
            $data = ("BS" == $big) ? preg_replace('/^(.+)\//', '', $data->name) : preg_replace('/\/(.+)$/', '', $data->name);
        }
        else 
        {
            $data = (empty($data)) ? "找不到" : $data->name;
        }

        return $data;
    }

    // 鎖 -> 鎖
    function _getLock($code)
    {
        $data = "";
        $sql = "SELECT * FROM `五金` WHERE skid = '$code'";
        $q = $this->db->query($sql);
        $data = $q->first_row();
        $q->free_result();

        $data = (empty($data)) ? "找不到" : $data->provider . $data->model;

        return $data;
    }

    function _getExtraHandle($code)
    {
        if ($code == "" || $code == 0)
        {
            return "";
        }

        $data = "";
        $code = str_replace("SK", "", strtoupper($code));
        $c    = substr($code, 0, 3);


        $height = preg_replace("/^$c/", "", $code);

        $handle = $this->_getLock("SK" . $c);
        return $handle . " / " . $height;
    }
}