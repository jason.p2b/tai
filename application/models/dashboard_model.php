<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the common functions used across entire website
 *
 */

class dashboard_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->model('status_model');
    }

    function getJob($uid)
    {
        $where = $this->status_model->getWhere($uid, 'p.customer_id', 'customer_id');
        $show = $this->status_model->getHideNYOrder($uid);

        $data = array();
        $sql = "SELECT o.ny_order_id, p.site, p.project_id, o.order_number, o.ny_order_id, o.customer_due_date, o.progress, o.factory_due_date, i.big_category, sum(i.amount) as 'amount', cus.colour
                FROM project p INNER JOIN ny_order o ON p.project_id = o.project_id
                    INNER JOIN ny_order_item i ON o.ny_order_id = i.ny_order_id
                    INNER JOIN customer cus ON p.customer_id = cus.customer_id
                WHERE $where $show
                GROUP BY i.ny_order_id, i.big_category
                ORDER BY o.factory_due_date";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            if (!isset($data[$row['ny_order_id']]))
            {
                $r = array('door'=>0, 'frame'=>0);
                $r['id'] = $row['ny_order_id'];
                $r['colour'] = $row['colour'];
                $r['site'] = $row['site'];
                $r['url'] = site_url('order') . "?get=1&p=" . $row['project_id'] . "&o=" . $row['ny_order_id'] . "&n=" . $row['order_number'];
                $r['progress'] = $row['progress'];
                $r['customer_due_date'] = $row['customer_due_date'];
                $r['factory_due_date']  = ($row['progress'] == "已出貨") ? $this->_getShippedDate($row['ny_order_id']):$row['factory_due_date'];
                $r['order_number'] = $row['order_number'];

                if ("BS" == $row['big_category'])
                {
                    $r['door'] += $row['amount'];
                }
                else
                {
                    $r['frame'] += $row['amount'];
                }
                $data[$row['ny_order_id']] = $r;
            }
            else
            {
                if ("BS" == $row['big_category'])
                {
                    $data[$row['ny_order_id']]['door'] += $row['amount'];
                }
                else
                {
                    $data[$row['ny_order_id']]['frame'] += $row['amount'];
                }
            }
        }
        $q->free_result();

        $sql = "SELECT o.ny_order_id, p.site, p.project_id, o.order_number, o.ny_order_id, o.customer_due_date, o.progress, o.factory_due_date, i.door_frame, (sum(amount_l)+sum(amount_r)) as 'amount', cus.colour
                FROM project p INNER JOIN ny_order o ON p.project_id = o.project_id
                    INNER JOIN trad_order_item i ON o.ny_order_id = i.ny_order_id
                    INNER JOIN customer cus ON p.customer_id = cus.customer_id
                WHERE $where
                GROUP BY i.ny_order_id, i.door_frame
                ORDER BY o.factory_due_date";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            if (!isset($data[$row['ny_order_id']]))
            {
                $r = array('door'=>0, 'frame'=>0);
                $r['id'] = $row['ny_order_id'];
                $r['colour'] = $row['colour'];
                $r['site'] = $row['site'];
                $r['url'] = site_url('order') . "?get=1&p=" . $row['project_id'] . "&o=" . $row['ny_order_id'] . "&n=" . $row['order_number'];
                $r['progress'] = $row['progress'];
                $r['customer_due_date'] = $row['customer_due_date'];
                $r['factory_due_date']  = $row['factory_due_date'];
                $r['order_number'] = $row['order_number'];

                if ("扇" == $row['door_frame'])
                {
                    $r['door'] += $row['amount'];
                }
                else
                {
                    $r['frame'] += $row['amount'];
                }
                $data[$row['ny_order_id']] = $r;
            }
            else
            {
                if ("扇" == $row['door_frame'])
                {
                    $data[$row['ny_order_id']]['door'] += $row['amount'];
                }
                else
                {
                    $data[$row['ny_order_id']]['frame'] += $row['amount'];
                }
            }
            
        }
        $q->free_result();

        return $data;
    }

    function getMixedProgress($uid)
    {
        $data = array();
        $show = $this->status_model->getHideNYOrder($uid);

        $sql = "SELECT i.ny_order_id, i.progress, sum(i.amount) as amount 
                FROM ny_order_item i INNER JOIN ny_order o 
                WHERE o.ny_order_id = i.ny_order_id
                $show
                GROUP BY i.ny_order_id, i.progress";
        $q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            if (!isset($data[$row->ny_order_id]))
            {
                $data[$row->ny_order_id] = $row->progress . "(" . $row->amount . ")";
            }
            else
            {
                $data[$row->ny_order_id] .= "<br>" . $row->progress . "(" . $row->amount . ")";
            }
        }

        $sql = "SELECT ny_order_id, progress, (sum(amount_l)+sum(amount_r)) as amount 
                FROM trad_order_item 
                GROUP BY ny_order_id, progress";
        $q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            if (!isset($data[$row->ny_order_id]))
            {
                $data[$row->ny_order_id] = $row->progress . "(" . $row->amount . ")";
            }
            else
            {
                $data[$row->ny_order_id] .= "<br>" . $row->progress . "(" . $row->amount . ")";
            }
        }

        return $data;
    }

    function _getShippedDate($ny_order_id)
    {
        $date = '';
        $sql = "SELECT max(h.date) as 'date'
                FROM ny_order_item n INNER JOIN ny_order_item_progress_history h ON n.ny_order_item_id = h.ny_order_item_id
                WHERE n.ny_order_id = $ny_order_id
                    AND new_progress = '已出貨'";
        $q = $this->db->query($sql);
        $r = $q->first_row();
        $q->free_result();

        $date = preg_replace('/\s.+/','', $r->date);
        return $date;
    }

    function getProgress($progress, $uid)
    {
        $where = $this->status_model->getWhere($uid, 'p.customer_id', 'customer_id');
        $show = $this->status_model->getHideNYOrder($uid);
        $total = $this->_getTotalAmount();
        $data  = array();
        $interval = ("已出貨" == $progress || "取消訂單" == $progress) ? " AND h.date BETWEEN NOW() - INTERVAL 180 DAY AND NOW()" : "";

        $sql = "SELECT DISTINCT p.project_id, p.site, p.order_type, n.ny_order_id, n.order_number, i.big_category, i.amount, MAX(h.date) AS 'date', i.ny_order_item_id
                FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
                    INNER JOIN ny_order_item i ON n.ny_order_id = i.ny_order_id
                    INNER JOIN ny_order_item_history h ON i.ny_order_item_id = h.ny_order_item_id
                    AND i.progress = h.new_value
                WHERE i.progress = '$progress' $interval
                GROUP BY i.ny_order_item_id
                ORDER BY h.date";
        
        // $sql = "SELECT DISTINCT p.project_id, p.site, p.order_type, n.ny_order_id, n.order_number, i.big_category, sum(i.amount), DATE_FORMAT(h.date, '%Y-%m-%d') AS 'date'
        //         FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
        //             INNER JOIN ny_order_item i ON n.ny_order_id = i.ny_order_id
        //             INNER JOIN ny_order_item_history h ON i.ny_order_item_id = h.ny_order_item_id
        //             AND i.progress = h.new_value 
        //         WHERE i.progress = '$progress' $interval
        //         GROUP BY p.project_id, p.site, p.order_type, n.ny_order_id, n.order_number, i.big_category, date
        //         ORDER BY n.order_number";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            if (!isset($data[$row['ny_order_id']]))
            {
                $r = array('door'=>0, 'frame'=>0);
                $d = new DateTime($row['date']);
                $url = site_url('order') . "?get=1&p=" . $row['project_id'] . "&o=" . $row['ny_order_id'] . "&n=" . $row['order_number'];

                $r['dates'] = $d->format('Y-m-d');
                $r['order_type'] = $row['order_type'];
                $r['name'] = '<a href="' . $url . '">' . $row['site'] . '('. $row['order_number'] . ')</a>';

                if ("BS" == $row['big_category'])
                {
                    $r['door'] += $row['amount'];
                }
                else
                {
                    $r['frame'] += $row['amount'];
                }

                $data[$row['ny_order_id']] = $r;
            }
            else
            {
                if ("BS" == $row['big_category'])
                {
                    $data[$row['ny_order_id']]['door'] += $row['amount'];
                }
                else
                {
                    $data[$row['ny_order_id']]['frame'] += $row['amount'];
                }
            }
        }
        $q->free_result();

        $sql = "SELECT DISTINCT p.project_id, p.site, p.order_type, n.ny_order_id, n.order_number, i.door_frame, i.amount_l + i.amount_r AS 'amount', MAX(h.date) AS 'date', i.trad_order_item_id
                FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
                    INNER JOIN trad_order_item i ON n.ny_order_id = i.ny_order_id
                    INNER JOIN trad_order_item_history h ON i.trad_order_item_id = h.trad_order_item_id
                    AND i.progress = h.new_value
                WHERE i.progress = '$progress' $interval
                GROUP BY i.trad_order_item_id
                ORDER BY h.date";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            if (!isset($data[$row['ny_order_id']]))
            {
                $r = array('door'=>0, 'frame'=>0);
                $d = new DateTime($row['date']);
                $url = site_url('order') . "?get=1&p=" . $row['project_id'] . "&o=" . $row['ny_order_id'] . "&n=" . $row['order_number'];

                $r['dates'] = $d->format('Y-m-d');
                $r['order_type'] = $row['order_type'];
                $r['name'] = '<a href="' . $url . '">' . $row['site'] . '('. $row['order_number'] . ')</a>';

                if ("扇" == $row['door_frame'])
                {
                    $r['door'] += $row['amount'];
                }
                else
                {
                    $r['frame'] += $row['amount'];
                }   
                $data[$row['ny_order_id']] = $r;
            }
            else
            {
                switch ($row['door_frame']) 
                {
                    case '扇':
                        $data[$row['ny_order_id']]['door'] += $row['amount'];
                        break;
                    default:
                        $data[$row['ny_order_id']]['frame'] += $row['amount'];
                        break;
                }
            }
        }
        $q->free_result();

        $info = array();
        foreach ($data as $id => $v)
        {
            if (isset($total[$id]['frame']))
            {
                $colour = ($v['frame'] != $total[$id]['frame']) ? "text-danger" : "text-success";
                $v['frame'] = "<span class='text $colour'>" . $v['frame'] . "/" . $total[$id]['frame'] . '</span>';
            }

            if (isset($total[$id]['door']))
            {
                $colour = ($v['door'] != $total[$id]['door']) ? "text-danger" : "text-success";
                $v['door'] = "<span class='text $colour'>" . $v['door'] . "/" . $total[$id]['door'] . '</span>';
            }

            $info[] = $v;
        }

        return $info;
    }

    function _getTotalAmount()
    {
        $total = array();
        $sql = "SELECT ny_order_id, IF(door_frame = '框', 'frame', 'door') AS 'type', sum(amount_l+amount_r) AS 'amount'
                FROM trad_order_item 
                GROUP BY ny_order_id, door_frame
                UNION
                SELECT ny_order_id, IF(big_category = 'BN', 'frame', 'door') AS 'type', sum(amount) AS 'amount'
                FROM ny_order_item
                GROUP BY ny_order_id, big_category";
        $q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            $total[$row->ny_order_id][$row->type] = $row->amount;
        }
        $q->free_result();
        return $total;
    }

    function getProgressBoard($type, $uid)
    {
        $data = array();
        $total = $this->_getTotalAmount();
        $today = new DateTime();

        if ($type == "frame")
        {
            $ntype = "BN";
            $ttype = "框";
        }
        else
        {
            $ntype = "BS";
            $ttype = "扇";
        }

        $sql = "SELECT DISTINCT p.project_id, p.site, n.progress, n.customer_due_date, p.order_type, n.ny_order_id, n.order_number, i.amount, h.date, i.ny_order_item_id AS 'id', h.new_value
                FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
                    INNER JOIN ny_order_item i ON n.ny_order_id = i.ny_order_id
                    INNER JOIN ny_order_item_history h ON i.ny_order_item_id = h.ny_order_item_id
                WHERE i.big_category = '$ntype' AND h.field = 'progress' AND i.progress NOT IN ('已出貨','取消訂單')
                UNION
                SELECT DISTINCT p.project_id, p.site,  n.progress, n.customer_due_date, p.order_type, n.ny_order_id, n.order_number, i.amount_l + i.amount_r AS 'amount', h.date, i.trad_order_item_id AS 'id', h.new_value
                FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
                    INNER JOIN trad_order_item i ON n.ny_order_id = i.ny_order_id
                    INNER JOIN trad_order_item_history h ON i.trad_order_item_id = h.trad_order_item_id
                WHERE i.door_frame = '$ttype' AND h.field = 'progress' AND i.progress NOT IN ('已出貨','取消訂單')";
        $q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            if (!isset($data[$row->ny_order_id]))
            {
                $url = site_url('order') . "?get=1&p=" . $row->project_id . "&o=" . $row->ny_order_id . "&n=" . $row->order_number;
                $data[$row->ny_order_id]['df'] = $ttype;
                $data[$row->ny_order_id]['name']  = '<a href="' . $url . '">' . $row->site . '('. $row->order_number . ')</a>';
                $data[$row->ny_order_id]['total'] = ($type == "frame") ? $total[$row->ny_order_id]['frame'] : $total[$row->ny_order_id]['door'];
                // $data[$row->ny_order_id]['customer_due_date'] = (date("Y-m-d") > $row->customer_due_date) ? "<span class='bg-danger'>" . $row->customer_due_date . "</span>" : $row->customer_due_date;
                $data[$row->ny_order_id]['customer_due_date'] = $row->customer_due_date;
                $data[$row->ny_order_id]['progress']  = ($row->progress == "等出貨") ? "<span class='bg-success'>" . $row->progress . "</span>" : $row->progress;
                $data[$row->ny_order_id]['received'] = "";
                $data[$row->ny_order_id]['shipped'] = "";
                $data[$row->ny_order_id]['fold'] = "";
                $data[$row->ny_order_id]['wait'] = "";
                $data[$row->ny_order_id]['production'] = "";
                $data[$row->ny_order_id]['paint'] = "";
                $data[$row->ny_order_id]['skin'] = "";
                $data[$row->ny_order_id]['wait_ship'] = "";
                $data[$row->ny_order_id]['response'] = "";
            }

            $d = new DateTime($row->date);

            if ($d->format('Y') == $today->format('Y')) {
                $date = $d->format('m/d');
            } else {
                $date = $d->format('Y-m-d');
            }

            switch ($row->new_value) {
                case '收到訂單':
                    $data[$row->ny_order_id]['received'][$date] += $row->amount;
                    break;
                case '已出貨':
                    $data[$row->ny_order_id]['shipped'][$date] += $row->amount;
                    break;
                case '發包中':
                    $data[$row->ny_order_id]['fold'][$date] += $row->amount;
                    break;
                case '等排程':
                    $data[$row->ny_order_id]['wait'][$date] += $row->amount;
                    break;
                case '生產中':
                    $data[$row->ny_order_id]['production'][$date] += $row->amount;
                    break;
                case '烤漆包裝':
                    $data[$row->ny_order_id]['paint'][$date] += $row->amount;
                    break;
                case '等SKIN板':
                    $data[$row->ny_order_id]['skin'][$date] += $row->amount;
                    break;
                case '等出貨':
                    $data[$row->ny_order_id]['wait_ship'][$date] += $row->amount;
                    break;
                case '等客戶回復':
                    $data[$row->ny_order_id]['response'][$date] += $row->amount;
                    break;
            }
        }

        $info = array();
        foreach ($data as $id => $array)
        {
            $row = array();
            $row['df']    = $array['df'];
            $row['name']  = $array['name'];
            $row['total'] = $array['total'];
            $row['customer_due_date'] = $array['customer_due_date'];
            $row['progress'] = $array['progress'];
            $row['received'] = $this->_arrayToLine($array['received']);
            $row['shipped'] = $this->_arrayToLine($array['shipped']);
            $row['fold'] = $this->_arrayToLine($array['fold']);
            $row['wait'] = $this->_arrayToLine($array['wait']);
            $row['production'] = $this->_arrayToLine($array['production']);
            $row['paint'] = $this->_arrayToLine($array['paint']);
            $row['skin'] = $this->_arrayToLine($array['skin']);
            $row['wait_ship'] = "<b>" . $this->_arrayToLine($array['wait_ship']) . "</b>";
            $row['response'] = $this->_arrayToLine($array['response']);
            $info[] = $row;
        }

        return $info;
    }

    function getDateProgress($date)
    {
        $data = array();

        $sql = "SELECT th.date, tr.name_tw, th.old_value, th.new_value, u.name as 'userName',
                    p.site, n.order_number,
                    t.name as 'model', t.door_frame, t.amount_l, t.amount_r, t.width, t.height
                FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
                    INNER JOIN trad_order_item t ON n.ny_order_id = t.ny_order_id
                    INNER JOIN trad_order_item_history th ON t.trad_order_item_id = th.trad_order_item_id
                    INNER JOIN user u ON th.user_id = u.uid
                    INNER JOIN translation tr ON tr.field = th.field
                WHERE th.date  like '%$date%'
                ORDER BY th.date";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            $items = array();

            $d = new DateTime($row['date']);
            $dt_colour = ($row['door_frame'] != "框") ? "<span class='text-danger'>" . $row['door_frame'] . "</span>" : $row['door_frame'];

            $items['time'] = $d->format("H:i");
            $items['field'] = ($row['name_tw'] != "進度") ? "<span class='text-danger'>" . $row['name_tw'] . "</span>" : $row['name_tw'];
            $items['old_value'] = $row['old_value'];
            $items['new_value'] = $this->_progressColour($row['new_value']);
            $items['site'] = $row['site'];
            $items['order'] = $row['order_number'];
            $items['model'] = '(' . $dt_colour . ') ' . $row['model'];
            $items['amount'] = "L" . $row['amount_l'] . " R" . $row['amount_r'];
            $items['size'] = $row['width'] . " x " . $row['height'];
            $items['user'] = $row['userName'];

            $data[] = $items;
        }
        $q->free_result();

        return $data;
    }


    function getDateProgressNY($date)
    {
        $data = array();

        $sql = "SELECT th.date, tr.name_tw, th.old_value, th.new_value, u.name as 'userName',
                    p.site, n.order_number,
                    t.name as 'model', t.big_category, t.amount, t.width, t.total_height
                FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
                    INNER JOIN ny_order_item t ON n.ny_order_id = t.ny_order_id
                    INNER JOIN ny_order_item_history th ON t.ny_order_item_id = th.ny_order_item_id
                    INNER JOIN user u ON th.user_id = u.uid
                    INNER JOIN translation tr ON tr.field = th.field
                WHERE th.date  like '%$date%'
                ORDER BY th.date";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            $items = array();

            $d = new DateTime($row['date']);
            $items['time'] = $d->format("H:i");
            $items['field'] = ($row['name_tw'] != "進度") ? "<span class='text-danger'>" . $row['name_tw'] . "</span>" : $row['name_tw'];
            $items['old_value'] = $row['old_value'];
            $items['new_value'] = $this->_progressColour($row['new_value']);
            $items['site'] = $row['site'];
            $items['order'] = $row['order_number'];
            $items['model'] = '(' . (($row['big_category'] != "BS") ? "框" : "<span class='text-danger'>扇</span>") . ') ' . $row['model'];
            $items['amount'] = $row['amount'];
            $items['size'] = $row['width'] . " x " . $row['total_height'];
            $items['user'] = $row['userName'];

            $data[] = $items;
        }
        $q->free_result();

        return $data;
    }

    function _progressColour($value)
    {
        switch ($value) {
            case '收到訂單':
                $value = "<span class='text-primary'>$value</span>";
                break;

            case '等出貨':
                $value = "<span class='text-warning'>$value</span>";
                break;

            case '已出貨':
                $value = "<span class='text-success'>$value</span>";
                break;

            case '等SKIN板':
                $value = "<span class='text-danger'>$value</span>";
                break;

            case '等客戶回復':
                $value = "<span class='text-danger'>$value</span>";
                break;

            default:
                break;
        }

        return $value;
    }

    function _arrayToLine($array)
    {
        $today = date("Y-m-d");
        $arr = array();

        if (!is_array($array))
        {
            return "";
        }

        foreach ($array as $k => $v)
        {
            $days = round(abs(strtotime($today)-strtotime($k))/86400);

            if (0 == $days)
            {
                $arr[] = "<span class='p-1 bg-success text-light d-0'>今天 ($v)</span>";
            }
            else if (1 >= $days)
            {
                $arr[] = "<span class='bg-warning text-light d-2'>昨天 ($v)</span>";
            }
            else if (2 >= $days)
            {
                $arr[] = "<span class='bg-warning text-light d-2'>前天 ($v)</span>";
            }
            else if (7 >= $days)
            {
                $arr[] = "<span class='bg-info text-light d-7'>$k ($v)</span>";
            }
            else if (30 >= $days)
            {
                $arr[] = "<span class='text-muted d-30'>$k ($v)</span>";
            }
            else 
            {
                 $arr[] = "<span class='text-muted'>$k ($v)</span>";
            }
        }

        return implode('<br>', $arr);
    }

    function getTableHeader($type) {
        $data = array();
        $data[] = array('name' => 'site', 'title' => '工地', 'sortable' => 1,'formatter' => true);
        $data[] = array('name' => 'order', 'title' => '訂單', 'sortable' => 1, 'url' => true,'formatter' => false);
        $data[] = array('name' => 'amount', 'title' => '數量', 'sortable' => 1, 'sumColumn' => true,'formatter' => false, 'align' => 'text-right');
        $data[] = array('name' => 'due_date', 'title' => '客需日', 'sortable' => 1, 'formatter' => true);
        $data[] = array('name' => 'current_progress', 'title' => '目前進度', 'sortable' => 1,'formatter' => true);
        $data[] = array('name' => 'received', 'title' => '收單日', 'sortable' => 1,'formatter' => false);

        $sql = "SELECT DISTINCT progress_category FROM stage WHERE product_category = '$type' ORDER BY step";
        $q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            $data[] = array('name' => $row->progress_category, 'title' => $row->progress_category, 'sortable' => 1,'formatter' => true, 'align' => 'text-center');
        }
        $q->free_result();

        $data[] = array('name' => 'other_progress', 'title' => '其他進度', 'sortable' => 1, 'align' => 'text-center','formatter' => true);
        
        return $data;
    }

    function _getOrders($category) {
        $data = array();
        $condition = "";
        $orderType = "";
        $amountType = "";

        if ("BN" == $category || "BS" == $category) {
            $orderType = "ny";
            $amountType = "i.amount";
            $condition = "big_category = '$category'";
        } else {
            $orderType = "trad";
            $amountType = "i.amount_l + i.amount_r";
            
            if ("框" == $category) {
                $condition = "i.door_frame = '框'";
            } else if ("鐵門" == $category) {
                $condition = "i.door_frame = '扇' AND i.type NOT IN ('玄關')";
            } else if ("玄關門" == $category) {
                $condition = "i.door_frame = '扇' AND i.type = '玄關'";
            }
        }

        $sql = "SELECT p.project_id, n.ny_order_id, p.site, n.order_number, n.customer_due_date, n.progress, DATE(h.date) AS 'date', h.new_value
                FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
                    INNER JOIN " . $orderType . "_order_item i ON n.ny_order_id = i.ny_order_id
                    INNER JOIN " . $orderType . "_order_item_history h ON i." . $orderType . "_order_item_id = h." . $orderType . "_order_item_id
                WHERE i.progress NOT IN ('已出貨','取消訂單') AND h.field = 'progress' AND $condition
                ORDER BY h.date ASC, n.ny_order_id ASC";
        $q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            if (!isset($data[$row->ny_order_id])) {
                $data[$row->ny_order_id] = array();

                for($i=0; $i<count($header); $i++) {
                    $data[$row->ny_order_id][$header[$i]['name']] = " ";
                }

                $data[$row->ny_order_id]['site'] = $row->site;
                $data[$row->ny_order_id]['order'] = $row->order_number;
                $data[$row->ny_order_id]['amount'] = 0;
                $data[$row->ny_order_id]['due_date'] = $row->customer_due_date;
                $data[$row->ny_order_id]['current_progress'] = $row->progress;
                $data[$row->ny_order_id]['url'] = site_url('order') . "?get=1&p=" . $row->project_id . "&o=" . $row->ny_order_id . "&n=" . $row->order_number;
            }

            $progress = explode("-", $row->new_value);
            if ("收到訂單" == $progress[0]) {
                // $data[$row->ny_order_id]['amount'] += $row->amount;
                $data[$row->ny_order_id]['received'] = $row->date;
            } else if (count($progress) == 1) {
                $data[$row->ny_order_id]['other_progress'] = $progress[0];
            } else {
                $data[$row->ny_order_id][$progress[0]] = $progress[1];
            }
        }
        $q->free_result();

        // Following query is for getting the "history" of each progress column. It will be displayed as tooltip
        $titles = array();
        $sql = "SELECT i.ny_order_id, sum($amountType) as 'amount', latestHistory.new_value, MAX(DATE(latestHistory.date)) as 'date'
                FROM " . $orderType . "_order_item i INNER JOIN
                (
                    SELECT " . $orderType . "_order_item_id, new_value, max(date) as 'date'
                    FROM " . $orderType . "_order_item_history oih
                    WHERE field = 'progress'
                    GROUP BY oih." . $orderType . "_order_item_id, oih.`new_value`
                ) latestHistory ON i." . $orderType . "_order_item_id = latestHistory." . $orderType . "_order_item_id
                WHERE i.progress NOT IN ('已出貨','取消訂單') AND $condition
                GROUP BY i.ny_order_id, latestHistory.new_value
                ORDER BY i.ny_order_id, latestHistory.date";
        $q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            if (!isset($titles[$row->ny_order_id])) {
                $titles[$row->ny_order_id] = array();
            }
            $progress = explode("-", $row->new_value);
            
            if ("收到訂單" == $progress[0]) {
                $titles[$row->ny_order_id]['received'] = $row->date . " (" . $row->amount . ")";
                if (isset($data[$row->ny_order_id])) {
                    $data[$row->ny_order_id]['amount'] = (int) $row->amount;
                }
            } else if (count($progress) == 1) {
                $titles[$row->ny_order_id]['other_progress'] .= $row->date . ": " . $progress[0] . " (" . $row->amount . ")\r\n";
            } else {
                $titles[$row->ny_order_id][$progress[0]] .= $row->date . ": " . $progress[1] . " (" . $row->amount . ")\r\n";
            }
        }
        $q->free_result();

        foreach ($titles as $key => $rows) 
        {
            if (isset($data[$key])) 
            {
                $data[$key]['titles'] = $rows;
            }
        }
        return $data;
    }

    function getTableFrame() {
        $header = $this->getTableHeader("框"); 
        $ny   = $this->_getOrders("BN");
        $trad = $this->_getOrders("框");
        $data = array_merge($ny, $trad);

        return array($header, array_values($data));
    }

    function getTableSMC() {
        $header = $this->getTableHeader("BS");
        $data = $this->_getOrders("BS");

        return array($header, array_values($data));
    }

    function getTableDoor() {
        $header = $this->getTableHeader("鐵門");
        $data = $this->_getOrders("鐵門");
        return array($header, array_values($data));
    }

    function getTableMainDoor() {
        $header = $this->getTableHeader("玄關門");
        $data = $this->_getOrders("玄關門");
        return array($header, array_values($data));
    }

    function getShipped($uid) {
        $data = array();

        $sql = "WITH summed_trad_order AS (
                    SELECT
                        ny_order_id,
                        door_frame,
                        SUM(amount_l + amount_r) AS total_amount
                    FROM
                        trad_order_item
                    WHERE
                        progress != '取消訂單'
                    GROUP BY
                        ny_order_id,
                        door_frame
                ),
                summed_ny_order AS (
                    SELECT
                        ny_order_id,
                        big_category,
                        SUM(amount) AS total_amount
                    FROM
                        ny_order_item
                    WHERE
                        progress != '取消訂單'
                    GROUP BY
                        ny_order_id,
                        big_category
                ),
                combined_results AS (
                    SELECT
                        DATE(trad_order_item_history.date) AS date,
                        '一般' AS category,
                        project.project_id,
                        ny_order.ny_order_id,
                        project.site,
                        ny_order.order_number,
                        trad_order_item.door_frame AS type,
                        SUM(trad_order_item.amount_l + trad_order_item.amount_r) AS amount_by_type,
                        summed_trad_order.total_amount AS total_amount_by_type
                    FROM
                        trad_order_item_history
                    JOIN trad_order_item ON trad_order_item_history.trad_order_item_id = trad_order_item.trad_order_item_id
                    JOIN ny_order ON trad_order_item.ny_order_id = ny_order.ny_order_id
                    JOIN project ON ny_order.project_id = project.project_id
                    JOIN summed_trad_order ON trad_order_item.ny_order_id = summed_trad_order.ny_order_id AND trad_order_item.door_frame = summed_trad_order.door_frame
                    WHERE
                        trad_order_item_history.new_value = '已出貨'
                        AND DATE(trad_order_item_history.date) BETWEEN NOW() - INTERVAL 120 DAY AND NOW()
                    GROUP BY
                        DATE(trad_order_item_history.date),
                        project.site,
                        ny_order.order_number,
                        trad_order_item.door_frame,
                        summed_trad_order.total_amount

                    UNION ALL

                    SELECT
                        DATE(ny_order_item_history.date) AS date,
                        '南亞' AS category,
                        project.project_id,
                        ny_order.ny_order_id,
                        project.site,
                        ny_order.order_number,
                        CASE ny_order_item.big_category WHEN 'BN' THEN '框' WHEN 'BS' THEN '扇' END AS type,
                        SUM(ny_order_item.amount) AS amount_by_type,
                        summed_ny_order.total_amount AS total_amount_by_type
                    FROM
                        ny_order_item_history
                    JOIN ny_order_item ON ny_order_item_history.ny_order_item_id = ny_order_item.ny_order_item_id
                    JOIN ny_order ON ny_order_item.ny_order_id = ny_order.ny_order_id
                    JOIN project ON ny_order.project_id = project.project_id
                    JOIN summed_ny_order ON ny_order_item.ny_order_id = summed_ny_order.ny_order_id AND ny_order_item.big_category = summed_ny_order.big_category
                    WHERE
                        ny_order_item_history.new_value = '已出貨'
                        AND DATE(ny_order_item_history.date) BETWEEN NOW() - INTERVAL 120 DAY AND NOW()
                    GROUP BY
                        DATE(ny_order_item_history.date),
                        project.site,
                        ny_order.order_number,
                        ny_order_item.big_category,
                        summed_ny_order.total_amount
                )
                SELECT
                    date,
                    site,
                    category,
                    order_number,
                    project_id,
                    ny_order_id,
                    CASE WHEN SUM(CASE WHEN type = '框' THEN amount_by_type ELSE 0 END) = 0 THEN '' ELSE SUM(CASE WHEN type = '框' THEN amount_by_type ELSE 0 END) END AS kuang_amount,
                    CASE WHEN SUM(CASE WHEN type = '框' THEN total_amount_by_type ELSE 0 END) = 0 THEN '' ELSE SUM(CASE WHEN type = '框' THEN total_amount_by_type ELSE 0 END) END AS kuang_total_amount,
                    CASE WHEN SUM(CASE WHEN type = '扇' THEN amount_by_type ELSE 0 END) = 0 THEN '' ELSE SUM(CASE WHEN type = '扇' THEN amount_by_type ELSE 0 END) END AS shan_amount,
                    CASE WHEN SUM(CASE WHEN type = '扇' THEN total_amount_by_type ELSE 0 END) = 0 THEN '' ELSE SUM(CASE WHEN type = '扇' THEN total_amount_by_type ELSE 0 END) END AS shan_total_amount
                FROM
                    combined_results
                GROUP BY
                    date,
                    site,
                    order_number,
                    project_id,
                    ny_order_id,
                    category
                ORDER BY
                    date desc,
                    site,
                    order_number;";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            $url = site_url('order') . "?get=1&p=" . $row['project_id'] . "&o=" . $row['ny_order_id'] . "&n=" . $row['order_number'];

            $v = array();
            $v['dates'] = $row['date'];
            $v['order_type'] = $row['category'];
            $v['name'] = '<a href="' . $url . '">' . $row['site'] . '('. $row['order_number'] . ')</a>';
            $v['frame'] = $this->_getColourText($row['kuang_amount'], $row['kuang_total_amount']);
            $v['door'] = $this->_getColourText($row['shan_amount'], $row['shan_total_amount']);

            $data[] = $v;
        }
        $q->free_result();

        return $data;
    }

    function _getColourText($amount, $total) {
        if ($total == "") {
            return 0;
        }

        $colour = ($amount != $total) ? "danger" : "success";

        return "<span class='text-$colour'>$amount/$total</span>";
    }
}