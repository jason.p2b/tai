<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model
{
    function checkUserExists($username, $password)
    {
        $sql = "SELECT 1 FROM user WHERE username='$username' AND password=SHA1('$password') LIMIT 1";
        $q = $this->db->query($sql);

        if ($q->num_rows() == 1)
        {
            $q->free_result();
            return true;
        }

        $q->free_result();
        return false;
    }

    function checkUserSession($username, $uid)
    {
        $sql = "SELECT 1 FROM user WHERE username='$username' AND uid=$uid LIMIT 1";
        $q = $this->db->query($sql);

        if ($q->num_rows() == 1)
        {
            $q->free_result();
            return true;
        }

        $q->free_result();
        return false;
    }

    function getUserDetail($username, $password)
    {
        date_default_timezone_set('Asia/Taipei');
        $date = date("Y-m-d H:i:s");

        $sql = "SELECT u.uid, u.name, c.company, u.position, u.first_time
                FROM user u INNER JOIN customer c ON u.customer_id = c.customer_id
                WHERE username = '$username' 
                AND password = SHA1('$password') LIMIT 1";
        $q = $this->db->query($sql);
        $row = $q->result();
        $q->free_result();

        $data = array('last_login' => $date);
        $this->db->where('uid', $row[0]->uid);
        $this->db->update('user', $data); 

        return $row;
    }

    function insertLastLogin($data)
    {
        $this->db->insert('last_login',$data);
        return true;
    }

    function changePassword($uid, $password)
    {
        $hash = $this->encrypt->sha1($password);
        $data = array('password'=>$hash, 'first_time'=>'0');
        $this->db->where('uid', $uid);
        $this->db->update('user', $data);
        return true;
    }
}