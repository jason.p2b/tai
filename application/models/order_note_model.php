<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class order_note_model extends CI_Model
{
	public function show($ny_order_id)
	{
		$data = array();
		$sql = "SELECT * 
				FROM ny_order_note n INNER JOIN user u ON n.user_id = u.uid
				WHERE n.ny_order_id = $ny_order_id
				ORDER BY created_by DESC";
		$q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
        	$oNote = array();
        	$oNote['date'] = $row->created_by;
        	$oNote['user'] = $row->name;
        	$oNote['note'] = $row->note;
        	$data[] = $oNote;
        }
        $q->free_result();
        return $data;
	}

	public function create($ny_order_id, $user_id, $note)
	{
		$data = array('ny_order_id'=>$ny_order_id, 'user_id'=>$user_id, 'note'=>$note);
		$this->db->set('created_by', 'NOW()', FALSE);
		$this->db->insert('ny_order_note', $data); 
        return $this->db->insert_id();
	}
}