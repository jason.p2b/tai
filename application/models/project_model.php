<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class project_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->model('status_model');
    }

    function getProject($uid)
    {
        $where = $this->status_model->getWhere($uid, 'c.customer_id', 'customer_id');

        $data = array();
        $sql = "SELECT p.*, c.company
                FROM project p INNER JOIN customer c ON p.customer_id = c.customer_id WHERE $where ORDER BY p.date DESC";
        $q   = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[] = $row;
        }
        $q->free_result();

        return $data;
    }

    // Get allowed customer in modal company select options
    function getCustomerOption() {
        $status_id = $this->session->userdata('status_id');
        $criteria  = "";

        switch ($status_id) 
        {
            case '4':
                $criteria = "WHERE status_id = 4";
                break;
            
            case '5':
                $criteria = "WHERE status_id = 5";
                break;
        }

        $sql = "SELECT customer_id, company
                FROM customer $criteria";

        $q = $this->db->query($sql);

        foreach($q->result() as $row)
        {
            $data[] = $row;
        }
        $q->free_result();

        return $data;         
    }

    function update($data, $id)
    {
        $this->db->where('project_id', $id);
        $this->db->update('project', $data);
        return true;
    }
}