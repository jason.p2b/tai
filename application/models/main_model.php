<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the common functions used across entire website
 *
 */

class main_model extends CI_Model
{
    function getOrder($user_status, $order_status = array(), $time = "")
    {
        date_default_timezone_set('Asia/Taipei');
        $today = new DateTime();
        $today->setTime(0,0,0);

        $id_array = array();
        $count = 1;

        // Get order id by status
        $this->db->select('order_id');
        $this->db->from('order_frame');
        $this->db->where($order_status);
        $q = $this->db->get();
        foreach ($q->result_array() as $row)
        {
            array_push($id_array, $row['order_id']);
        }
        $q->free_result();

        $this->db->select('order_id');
        $this->db->from('order_door');
        $this->db->where($order_status);
        $q = $this->db->get();
        foreach ($q->result_array() as $row)
        {
            array_push($id_array, $row['order_id']);
        }
        $q->free_result();

        $sql = "SELECT oh.*, o.order_name, o.model, concat(p.customer, p.site) as 'project_name'
                FROM order_history oh INNER JOIN `order` o ON oh.order_id = o.order_id
                    INNER JOIN project p ON o.project_id = p.project_id
                WHERE o.order_id IN (" . implode(',', $id_array) . ")";
        if ($status_id > 3)
        {
            $sql .= " AND p.status_id = $status_id";
        }
        $sql .= " ORDER BY oh.date DESC";
        $q = $this->db->query($sql);

        foreach ($q->result_array() as $row)
        {
            $arr = $row;
            $arr['new'] = ("" != $time && $arr['date'] >= $time) ? true : false;
            $arr['category'] = ($arr['new']) ? "<sup><span class='glyphicon glyphicon-star bright-yellow'></span></sup><span class='change'>修改</span>" : "<span class='change'>修改</span>";
            $arr['date'] = $this->_changeDate($today, $arr['date']);
            if (!isset($data[$row['date']]))
            {
                $data[$row['date']] = $arr;
            }
            else
            {
                $data[$row['date'] . $count] = $arr;
                $count++;
            }
        }
        $q->free_result();

        $sql = "SELECT 
                    IF (f.order_id IS NULL, d.create_date, f.create_date) as 'date',
                    o.model, 
                    IF (f.order_id IS NULL, d.model, f.model) as 'sub_model',
                    o.order_name,
                    concat(p.customer, p.site) as 'project_name',
                    '新訂單' as 'history',
                    IF (f.order_id IS NULL, '門扇', '門框') as 'type',
                    u.name
                FROM `order` o INNER JOIN project p ON o.project_id = p.project_id
                    INNER JOIN user u ON u.uid = o.uid
                    LEFT JOIN order_frame f ON o.order_id = f.order_id
                    LEFT JOIN order_door d ON o.order_id = d.order_id
                WHERE o.order_id IN (" . implode(',', $id_array) . ")";

        if ($status_id > 3)
        {
            $sql .= " AND p.status_id = $status_id";
        }

        $sql .= " ORDER BY date DESC";

        $q = $this->db->query($sql);
        foreach ($q->result_array() as $row)
        {
            $arr = $row;
            $arr['new'] = ("" != $time && $arr['date'] >= $time) ? true : false;
            $arr['category'] = ($arr['new']) ? "<sup><span class='glyphicon glyphicon-star bright-yellow'></span></sup><span class='new'>新增</span>" : "<span class='new'>新增</span>";
            $arr['date'] = $this->_changeDate($today, $arr['date']);
            $arr['history'] = "<span class='new'>" . $arr['history'] . "</span>";
            $arr['type'] = "<span class='new'>" . $arr['type'] . "</span>";
            $arr['model'] = ($arr['sub_model'] != $arr['model']) ? $arr['model'] . "(" . $arr['sub_model'] . ")" : $arr['model'];
            if (!isset($data[$row['date']]))
            {
                $data[$row['date']] = $arr;
            }
            else
            {
                $data[$row['date'] . $count] = $arr;
                $count++;
            }
        }
        $q->free_result();

        rsort($data);
        return $data;
    }

    function getOrderChange($status_id=1, $interval=0)
    {
        date_default_timezone_set('Asia/Taipei');
        $today = new DateTime();
        $today->setTime(0,0,0);

        $data = array();

        $sql = "SELECT oh.*, o.order_name, o.model, concat(p.customer, p.site) as 'project_name'
                FROM order_history oh INNER JOIN `order` o ON oh.order_id = o.order_id
                    INNER JOIN project p ON o.project_id = p.project_id";

        if ($status_id > 3)
        {
            $sql .= " WHERE p.status_id = $status_id";
        }

        if ($interval != 0)
        {
            $sql .= ($status_id > 3) ? ' AND ' : ' WHERE ';
            $sql .= " oh.date > NOW() - INTERVAL $interval SECOND";
        }

        $sql .= " ORDER BY oh.date DESC";

        $q = $this->db->query($sql);
        foreach ($q->result_array() as $row)
        {
            $arr = $row;
            $arr['category'] = "<span class='change'>修改</span>";
            $arr['date'] = $this->_changeDate($today, $arr['date']);
            $data[$row['date']] = $arr;
        }
        $q->free_result();

        $sql = "SELECT 
                    IF (f.order_id IS NULL, d.create_date, f.create_date) as 'date',
                    o.model, 
                    IF (f.order_id IS NULL, d.model, f.model) as 'sub_model',
                    o.order_name,
                    concat(p.customer, p.site) as 'project_name',
                    '新訂單' as 'history',
                    IF (f.order_id IS NULL, '門扇', '門框') as 'type',
                    u.name
                FROM `order` o INNER JOIN project p ON o.project_id = p.project_id
                    INNER JOIN user u ON u.uid = o.uid
                    LEFT JOIN order_frame f ON o.order_id = f.order_id
                    LEFT JOIN order_door d ON o.order_id = d.order_id";

        if ($status_id > 3)
        {
            $sql .= " WHERE p.status_id = $status_id";
        }

        if ($interval != 0)
        {
            $sql .= ($status_id > 3) ? ' AND ' : ' WHERE ';
            $sql .= " f.create_date > NOW() - INTERVAL $interval SECOND OR d.create_date > NOW() - INTERVAL $interval SECOND ";
        }

        $sql .= " ORDER BY date DESC";

        $q = $this->db->query($sql);
        foreach ($q->result_array() as $row)
        {
            $arr = $row;
            $arr['category'] = "<span class='new'>新增</span>";
            $arr['date'] = $this->_changeDate($today, $arr['date']);
            $arr['history'] = "<span class='new'>" . $arr['history'] . "</span>";
            $arr['type'] = "<span class='new'>" . $arr['type'] . "</span>";
            $arr['model'] = ($arr['sub_model'] != $arr['model']) ? $arr['model'] . "(" . $arr['sub_model'] . ")" : $arr['model'];
            $data[$row['date']] = $arr;
        }
        $q->free_result();

        krsort($data);
        return $data;
    }

    function _changeDate($today, $dateTime)
    {
        date_default_timezone_set('Asia/Taipei');
        $date = new DateTime($dateTime);
        $date->setTime(0,0,0);

        $diff = $today->diff( $date );
        $diffDays = (integer) $diff->format( "%R%a" );

        if ($diffDays == 0)
        {
            return "今天 " . date("g:ia", strtotime($dateTime));
        }
        else if ($diffDays == -1)
        {
            return "昨天 " . date("g:ia", strtotime($dateTime));
        }
        else if ($diffDays == -2)
        {
            return "前天 " . date("g:ia", strtotime($dateTime));
        }
        else if ($diffDays > -7)
        {
            setlocale(LC_ALL, 'zh-Hant-TW');
            $mydate = $this->_convert_week(strftime('%A', strtotime($dateTime)));
            return  $mydate . ' ' . date("g:ia", strtotime($dateTime));
        }
        else
        {
            return date("Y-m-d g:ia", strtotime($dateTime));
        }
    }

    function _convert_week($dow)
    {
        switch ($dow) {
            case 'Monday':
                return "星期一";
            case 'Tuesday':
                return "星期二";
            case 'Wednesday':
                return "星期三";
            case 'Thursday':
                return "星期四";
            case 'Friday':
                return "星期五";
            case 'Saturday':
                return "星期六";
            case 'Sunday':
                return "星期天";
            default:
                return "";
        }
    }
}