<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the common functions used across entire website
 *
 */

class common_model extends CI_Model
{
    function insert($table, $data)
    {
        $this->db->insert($table, $data); 
        return $this->db->insert_id();
    }

    function update($table, $data, $condition)
    {
        $old_data = array();

        // Get old update value
        $q = $this->db->get_where($table, $condition);
        $old_data = $q->first_row('array');
        $q->free_result();

        // Update data
        $this->db->where($condition);
        $this->db->update($table, $data);

        return $old_data;
    }

    function getData($table, $columns, $condition)
    {
        $criteria = array();
        foreach ($condition as $key => $value)
        {
            $criteria[] = $key . " = '" . $value;
        }

        $sql =  "SELECT " . implode(',', $columns) .
                " FROM `" . $table . "`" . 
                " WHERE " . implode("' AND ", $criteria) . "'";
        $q = $this->db->query($sql);
        $old_data = $q->first_row('array');
        $q->free_result();

        return $old_data;
    }

    function delete($table, $condition)
    {
        $this->db->delete($table, $condition);
        return true;
    }
}