<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class order_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->model('status_model');
    }

    public function getCustomerId($project_id)
    {
        $sql = "SELECT customer_id FROM project WHERE project_id = $project_id";
        $q = $this->db->query($sql);
        $row = $q->first_row();
        $q->free_result();
        return $row->customer_id;
    }

    public function getSite($customer_id)
    {
        $data = array();
        $sql = "SELECT project_id, site, `date` FROM project WHERE customer_id = '$customer_id' ORDER BY `date` DESC";
        $q = $this->db->query($sql);
        foreach($q->result() as $row)
        {
            $date = date("Y", strtotime($row->date)) . "年" . date("m", strtotime($row->date)) . "月";
            $data[$date][$row->project_id] = $row->site;
        }
        $q->free_result();
        return $data;
    }

    public function getProjectOrderType($ny_order_id) 
    {
        $sql = "SELECT p.order_type 
                FROM project p INNER JOIN ny_order n ON p.project_id = n.project_id
                WHERE n.ny_order_id = $ny_order_id";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $q->free_result();

        return $row->order_type;
    }
    
    public function getNYOrder($project_id, $uid)
    {
        $show = $this->status_model->getHideNYOrder($uid);
        $data = array();

        $sql = "SELECT * FROM ny_order WHERE project_id = $project_id $show";
        $q   = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            $row['checker'] .= '<div class="btn-group btn-group-xs margin-right-2" role="group"><button type="button" class="btn btn-default footable-edit"><span class="fooicon fooicon-pencil" aria-hidden="true"></span></button></div>';
            $row['order_number'] = "<a href='#original' id='" . $row['ny_order_id'] . $row['order_number'] . "'' onclick=\"getOrderItem('" . $row['ny_order_id'] . "','" . $row['order_number'] . "');\">" . $row['order_number'] . "</a>";
            $amount = $this->_getOrderAmount($row['ny_order_id']);
            $row['door'] = $amount['door'];
            $row['frame'] = $amount['frame'];
            $data[] = $row;
        }
        $q->free_result();

        return $data;
    }

    function _getOrderAmount($ny_order_id)
    {
        $amount = array('frame'=>0, 'door'=>0);
        $sql = "SELECT `big_category`, `amount` FROM ny_order_item WHERE ny_order_id = $ny_order_id AND progress != '取消訂單'";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            if ("BS" == $row['big_category'])
            {
                $amount['door'] += $row['amount'];
            }
            else
            {
                $amount['frame'] += $row['amount'];
            }
        }
        $q->free_result();

        $sql = "SELECT door_frame, amount_l, amount_r FROM trad_order_item WHERE ny_order_id = $ny_order_id";
        $q = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            if ("扇" == $row['door_frame'])
            {
                $amount['door'] += $row['amount_l']+$row['amount_r'];
            }
            else
            {
                $amount['frame'] += $row['amount_l']+$row['amount_r'];
            }
        }
        $q->free_result();

        return $amount;
    }


    public function updateOrder($data, $id)
    {
        $this->db->where('ny_order_id', $id);
        $this->db->update('ny_order', $data);
        return true;
    }

    public function updateOrderItem($data, $id)
    {
        $this->db->where('ny_order_item_id', $id);
        $this->db->update('ny_order_item', $data);
        return true;
    }

    public function getTradOrderItem($ny_order_id)
    {
        $data = array();
        $sql = "SELECT * FROM trad_order_item WHERE ny_order_id = $ny_order_id";
        $q   = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            
            $row['checker'] = '<input type="checkbox" class="order_items margin-right-2" value="' . $row['trad_order_item_id'] . '" name="items[]">';
            $row['checker'] .= '<div class="btn-group btn-group-xs margin-right-2" role="group"><button type="button" class="btn btn-default footable-edit"><span class="fooicon fooicon-pencil" aria-hidden="true"></span></button></div>';
            if ('框' == $row['door_frame'])
            {
                $row['checker'] .= '<div class="btn-group btn-group-xs" role="group"><button onclick="openFrameSVG(\'' . $row['trad_order_item_id'] . '\', \'trad\')" type="button" class="btn btn-default"><a href="#svg-viewer"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></a></button></div>';
            }
            $row['amount_t'] = $row['amount_r'] + $row['amount_l'];
            $row['frame_shape'] = $this->_convertFrameShape($row['frame_shape']);
            $row['fail_safe_lock'] = (!("0" == $row['fail_safe_lock'])) ? "yes" : "";
            $row['fail_safe_lock_son'] = (!("0" == $row['fail_safe_lock_son'])) ? "yes" : "";
            $row['magnetic_hole'] = (!("0" == $row['magnetic_hole'])) ? "yes" : "";
            $row['magnetic_hole_son'] = (!("0" == $row['magnetic_hole_son'])) ? "yes" : "";
            $row['ball'] = (!("0" == $row['ball'])) ? "yes" : "";
            $data[] = $row;
        }
        $q->free_result();

        return $data;
    }

    public function _convertFrameShape($frame)
    {
        switch ($frame) {
            case 'l_frame':
                return "L型框";
            case 'la_frame':
                return "L型氣密框";
            case 't_frame':
                return "凸型框";
            case 'ta_frame':
                return "凸型氣密框";
            case '120_frame':
                return "120A框";
            case 'entrance_frame':
                return "玄關框";
            case '180度':
                return "180度框";
            case '270度':
                return "270度框";
            default:
                return "找不到";
        }
    }

    public function getNYOrderItem($ny_order_id)
    {
        $data = array();
        $sql = "SELECT * FROM ny_order_item WHERE ny_order_id = $ny_order_id";
        $q   = $this->db->query($sql);
        foreach($q->result_array() as $row)
        {
            $row['checker'] = '<input type="checkbox" class="order_items margin-right-2" value="' . $row['ny_order_item_id'] . '" name="items[]">';
            $row['checker'] .= '<div class="btn-group btn-group-xs margin-right-2" role="group"><button type="button" class="btn btn-default footable-edit"><span class="fooicon fooicon-pencil" aria-hidden="true"></span></button></div>';
            $row['checker'] .= '<div class="btn-group btn-group-xs" role="group"><button onclick="openFrameSVG(\'' . $row['ny_order_item_id'] . '\', \'ny\')" type="button" class="btn btn-default"><a href="#svg-viewer"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></a></button></div>';
            $row['total_price'] = $row['amount'] * $row['price'];
            $data[] = $row;
        }
        $q->free_result();

        return $data;
    }

    function changeAllProgress($id, $progress, $type, $uid)
    {
        if (count($id) < 1)
        {
            return "error";
        }

        $table = ("ny" == $type) ? "ny_order_item" : "trad_order_item";
        $data = array('progress'=>$progress, 'user_id'=>$uid);

        $this->db->where_in($table . "_id", $id);
        $this->db->update($table, $data);
        $p = $this->changeMainOrderStatus($id[0], $table);
        
        return $p;
    }

    function changeMainOrderStatus($order_item_id, $table)
    {
        $progress = "";
        $id_field = $table . "_id";

        $sql = "SELECT ny_order_id FROM $table WHERE $id_field = $order_item_id";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $id  = $row->ny_order_id;
        $q->free_result();

        $sql = "SELECT DISTINCT progress FROM $table WHERE ny_order_id = $id AND progress != '取消訂單' GROUP BY progress";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 1)
        {
            $progress = "混合進度"; 
        }
        else if ($q->num_rows() == 0)
        {
            $progress = "取消訂單";
        }
        else
        {
            $row = $q->row();
            $progress = $row->progress; 
        }
        $q->free_result();
        $this->updateOrderStatus($id, $progress);

        return $progress;
    }

    function updateOrderStatus($ny_order_id, $progress)
    {
        $cond = array('ny_order_id'=>$ny_order_id);
        $data = array('progress'=>$progress, 'progress_date' => date('Y-m-d',time()));

        $this->db->where($cond);
        $this->db->update("ny_order", $data);
    }

    function getLock()
    {
        $data = array();
        $sql = "SELECT * FROM `lock` WHERE display = 1 ORDER BY model";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->model] = $row->model . " " . $row->restriction;
        }
        $q->free_result();

        return $data;
    }

    function getHinge()
    {
        $data = array();
        $sql = "SELECT * FROM `hinge` ORDER BY model";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->model] = $row->model . " " . $row->restriction;
        }
        $q->free_result();

        return $data;
    }

    function getHandle()
    {
        $data = array();
        $sql = "SELECT * FROM `handle` ORDER BY model";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->model] = $row->model . " " . $row->restriction;
        }
        $q->free_result();

        return $data;
    }

    function getCloser()
    {
        $data = array();
        $sql = "SELECT * FROM `closer` ORDER BY model";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->model] = $row->model . " " . $row->restriction;
        }
        $q->free_result();

        return $data;
    }

    function getBolt()
    {
        $data = array();
        $sql = "SELECT * FROM `bolt` ORDER BY model";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->model] = $row->model . " " . $row->restriction;
        }
        $q->free_result();

        return $data;
    }

    function getOrderMatch($id)
    {
        $data = array();
        $sql = "SELECT CONCAT(t.name, ' (',t.coupled,')') AS 'myname' ,t.door_frame, t.is_mother, SUM(t.amount_l) AS 'L', SUM(t.amount_r) AS 'R'
                FROM `trad_order_item` t INNER JOIN ny_order n ON n.ny_order_id = t.ny_order_id 
                WHERE n.project_id = $id AND t.progress != '取消訂單'
                GROUP BY myname, t.door_frame, t.is_mother
                ORDER BY myname, t.door_frame DESC";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $name_breaks = explode(' ', $row->myname);
            $new_name = $name_breaks[0] . " " . end($name_breaks);

            if (!isset($data[$new_name]))
            {
                $data[$new_name]['f'] = array('l'=>0,'r'=>0);
                $data[$new_name]['d'] = array('l'=>0,'r'=>0);
            }

            $l = ("子" == $row->is_mother) ? $row->R : $row->L;
            $r = ("子" == $row->is_mother) ? $row->L : $row->R;
            $type = ("框" != $row->door_frame) ? "d" : "f";
            $data[$new_name][$type]['l'] += $l;
            $data[$new_name][$type]['r'] += $r;
        }
        $q->free_result();
        return $data;
    }

    function getFSL($id)
    {
        $sql = "SELECT * FROM fsl_info WHERE project_id = $id LIMIT 1";
        $q   = $this->db->query($sql);
        $row = $q->first_row('array');
        $q->free_result();
        unset($row['fsl_id']);
        unset($row['project_id']);
        return $row;
    }

    function checkFSL($id)
    {
        $sql = "SELECT 1 FROM fsl_info WHERE project_id = $id LIMIT 1";
        $q   = $this->db->query($sql);
        $r   = $q->num_rows();
        $q->free_result();

        return $r;
    }

    function getFS($id)
    {
        $sql = "SELECT * FROM frame_setting WHERE project_id = $id LIMIT 1";
        $q   = $this->db->query($sql);
        $row = $q->first_row('array');
        $q->free_result();
        unset($row['fsl_id']);
        unset($row['project_id']);
        return $row;
    }

    public function updateFSL($data, $id)
    {
        $this->db->where('project_id', $id);
        $this->db->update('fsl_info', $data);
        return true;
    }

    function checkFS($id)
    {
        $sql = "SELECT 1 FROM frame_setting WHERE project_id = $id LIMIT 1";
        $q   = $this->db->query($sql);
        $r   = $q->num_rows();
        $q->free_result();

        return $r;
    }

    public function updateFS($data, $id)
    {
        $this->db->where('project_id', $id);
        $this->db->update('frame_setting', $data);
        return true;
    }
}