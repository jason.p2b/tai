<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report_model extends CI_Model
{
    private function _query($sql)
    {
        $data = array();
        $q = $this->db->query($sql);

        foreach($q->result() as $row)
        {
            $data[] = $row;
        }
        $q->free_result();

        return $data;
    }

    function getFrameShape()
    {
        $sql = "SELECT frame_shape_id, html_id, file FROM frame_shape";
        return $this->_query($sql);
    }

    function getMainReport()
    {
        $sql = "SELECT fireproof_report_id as \"id\", name 
                FROM fireproof_report 
                WHERE main_report_id IS NULL
                ORDER BY name ASC";
        return $this->_query($sql);
    }

    function getFrameMaterial()
    {
        $sql = "SELECT * FROM frame_material ORDER BY name";
        return $this->_query($sql);
    }

    function getDoorMaterial()
    {
        $sql = "SELECT * FROM door_material ORDER BY name";
        return $this->_query($sql);
    }

    function getLock()
    {
        $sql = "SELECT * FROM `lock` ORDER BY model";
        return $this->_query($sql);
    }

    function getHinge()
    {
        $sql = "SELECT * FROM hinge ORDER BY model";
        return $this->_query($sql); 
    }

    function getHardware()
    {
        $sql = "SELECT * FROM hardware ORDER BY model";
        return $this->_query($sql); 
    }

    function getHandle()
    {
        $sql = "SELECT * FROM handle ORDER BY model";
        return $this->_query($sql); 
    }

    function getCloser()
    {
        $sql = "SELECT * FROM closer ORDER BY model";
        return $this->_query($sql); 
    }

    function getBolt()
    {
        $sql = "SELECT * FROM bolt ORDER BY model";
        return $this->_query($sql); 
    }

    function viewData()
    {
        $data = array();

        $sql = "SELECT fr . * , nfr.name AS  'main_name'
                FROM fireproof_report fr
                LEFT JOIN fireproof_report nfr ON fr.main_report_id = nfr.fireproof_report_id
                ORDER BY fr.name ASC";
        $q = $this->db->query($sql);

        foreach($q->result_array() as $row)
        {
            $data['fr' . $row['fireproof_report_id']] = $row;
            $data['fr' . $row['fireproof_report_id']]['frame_material'] = array();
            $data['fr' . $row['fireproof_report_id']]['frame_shape'] = array();
            $data['fr' . $row['fireproof_report_id']]['door_material'] = array();
            $data['fr' . $row['fireproof_report_id']]['lock'] = array();
            $data['fr' . $row['fireproof_report_id']]['hinge'] = array();
            $data['fr' . $row['fireproof_report_id']]['hardware'] = array();
            $data['fr' . $row['fireproof_report_id']]['closer'] = array();
            $data['fr' . $row['fireproof_report_id']]['handle'] = array();
            $data['fr' . $row['fireproof_report_id']]['bolt'] = array();
        }
        $q->free_result();

        // Get frame_material
        $data = $this->_viewRelatedData($data, 'frame_material', 'name');

        // Get frame_shape
        $data = $this->_viewRelatedData($data, 'frame_shape', 'html_id');

        // Get door_material
        $data = $this->_viewRelatedData($data, 'door_material', 'name');

        // Get lock
        $data = $this->_viewRelatedData($data, 'lock', 'model');

        // Get hinge
        $data = $this->_viewRelatedData($data, 'hinge', 'model');

        // Get hardware
        $data = $this->_viewRelatedData($data, 'hardware', 'model');

        // Get closer
        $data = $this->_viewRelatedData($data, 'closer', 'model');

        // Get handle
        $data = $this->_viewRelatedData($data, 'handle', 'model');

        // Get bolt
        $data = $this->_viewRelatedData($data, 'bolt', 'model');

        return $data;
    }

    function _viewRelatedData($data, $table, $column)
    {
        $sql = "SELECT DISTINCT ft.fireproof_report_id, t.$column
                FROM `fireproof_report_$table` ft INNER JOIN `$table` t ON ft.{$table}_id = t.{$table}_id
                ORDER BY t.$column";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0)
        {
            foreach($q->result_array() as $row)
            {
                array_push($data['fr' . $row['fireproof_report_id']][$table], $row[$column]);
            }
        }
        
        $q->free_result();

        // $data['fr' . $row['fireproof_report_id']][$table] = implode(', ', $data['fr' . $row['fireproof_report_id']][$table]);
        return $data;
    }
}