<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the status functions used across entire website
 *
 */

class stock_model extends CI_Model
{
    function getMaterial()
    {
        $data = array();

        $sql = "SELECT * FROM material";
        $q = $this->db->query($sql);
        foreach ($q->result_array() as $row)
        {
            $data[] = $row;
        }
        $q->free_result();

        return $data;
    }

    function updateMaterial($data, $id)
    {
        $this->db->where('mid', $id);
        $this->db->update('material', $data);
        return true;
    }

    function getMaterialSelect()
    {
        $data = array();
        $sql = "SELECT mid, type, name FROM material ORDER BY type, name";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->type][$row->mid] = $row->name;
        }
        $q->free_result();

        return $data;
    }

    function checkMaterial($name)
    {
        $sql = "SELECT 1 FROM material WHERE name = '$name' LIMIT 1";
        $q = $this->db->query($sql);
        $check = $q->num_rows();
        $q->free_result();
        return $check;
    }

    function getUnit($mid)
    {
        $sql = "SELECT unit FROM material WHERE mid = $mid";
        $q   = $this->db->query($sql);
        $row = $q->first_row();
        $q->free_result();

        return $row->unit;
    }

    function getStock($from="0000-00-00")
    {
        setlocale(LC_MONETARY, 'zh-TW');

        $date = array();
        $sql = "SELECT s.stock_id, s.mid, s.isIn, s.date, m.name, m.unit, s.amount, s.price, s.supplier, s.note, s.ny_order_id
                FROM stock s INNER JOIN material m ON s.mid = m.mid
                WHERE s.date > '$from'
                ORDER BY s.stock_id DESC";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $arr = array();
            $arr['stock_id'] = $row->stock_id;
            $arr['mid'] = $row->mid;
            $arr['isIn'] = $row->isIn;
            $arr['date'] = $row->date;
            $arr['name'] = $row->name;
            $arr['unit'] = $row->unit;
            $arr['amount'] = ($row->isIn) ? "<span class='stock-in'>$row->amount</span>" : "<span class='stock-out'>-$row->amount</span>";
            $arr['price'] = number_format($row->price);
            $arr['total'] = number_format($row->amount * $row->price);
            $arr['supplier'] = $row->supplier;
            $arr['ny_order_id'] = $row->ny_order_id;
            $arr['note'] = $row->note;
            $data[] = $arr;
        }
        $q->free_result();

        return $data;
    }

    function updateStock($data, $id)
    {
        $this->db->where('stock_id', $id);
        $this->db->update('stock', $data);
        return true;
    }

    function getStocktake()
    {
        $data = array();
        $sql = "SELECT s.isIn,  m.name, m.type, m.unit, sum(s.amount) AS 'amount'
                FROM material m LEFT JOIN stock s ON s.mid=m.mid
                GROUP BY m.type, m.mid, s.isIn";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->type][$row->name][0] += ($row->isIn) ? $row->amount : $row->amount * -1;
            $data[$row->type][$row->name][1] = $row->unit;
        }
        $q->free_result();

        return $data;
    }

    function getUnderStock()
    {
        $data = $this->getStocktake();
        $result = array();

        $sql = "SELECT name, type, undersupply FROM material";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            if ($row->undersupply > $data[$row->type][$row->name][0])
            {
                $result[] = array('type'=>$row->type, 'name'=>$row->name, 'amount'=>$data[$row->type][$row->name][0] . " (" . $data[$row->type][$row->name][1] . ")", 'undersupply'=>$row->undersupply);
            }
        }
        $q->free_result();

        return $result;
    }

    function getFile()
    {
        $data = array();
        $sql = "SELECT DISTINCT name, file_location FROM material";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[$row->name] = $row->file_location;
        }
        $q->free_result();

        return $data;
    }

    function getAmount($mid)
    {
        $sql = "SELECT CONCAT(SUM(IF(isIN = 0, CONCAT('-',amount), amount)),unit) AS 'total', MAX(price) AS 'price', file_location
                FROM stock s INNER JOIN material m ON s.mid = m.mid
                WHERE s.mid = $mid";
        $q = $this->db->query($sql);
        $r = $q->first_row();
        $q->free_result();
        return array($r->total, $r->price, $r->file_location);
    }

    function getFilter()
    {
        $data = array();
        $sql = "SELECT DISTINCT type FROM material ORDER BY type";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row)
        {
            $data[] = $row->type;
        }
        $q->free_result();

        return $data;
    }

    function getPhoto($name)
    {
        $this->db->select('file_location');
        $this->db->where('name', $name);
        $q = $this->db->get('material');
        $row = $q->first_row();
        $q->free_result();

        return $row->file_location;
    }
}