<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This store the status functions used across entire website
 *
 */

class status_model extends CI_Model
{
   
    function isEditable($uid)
    {
        $editable = false;
        $sql = "SELECT c.customer_id 
                FROM customer c INNER JOIN user u ON c.customer_id = u.customer_id
                WHERE u.uid = $uid LIMIT 1";
        $q = $this->db->query($sql);
        $r = $q->first_row();
        $customer_id = $r->customer_id;
        $q->free_result();

        if ("1" == $customer_id)
        {
            $editable = true;
        }

        return $editable;
    }

    function getHideNYOrder($uid)
    {
        $show = "";
        $sql = "SELECT c.customer_id 
                FROM customer c INNER JOIN user u ON c.customer_id = u.customer_id
                WHERE u.uid = $uid LIMIT 1";
        $q = $this->db->query($sql);
        $r = $q->first_row();
        $customer_id = $r->customer_id;
        $q->free_result();

        if ("3" == $customer_id)
        {
            $show = "AND ny_show = '1'";
        }

        return $show;
    }

    function getWhere($uid, $field_text, $field)
    {
        $use_f = "";
        $use_id = "";
        $id_array = array();

        $sql = "SELECT c.customer_id, c.status_id
                FROM customer c INNER JOIN user u ON c.customer_id = u.customer_id
                WHERE u.uid = $uid LIMIT 1";
        $q = $this->db->query($sql);
        $r = $q->first_row();
        $customer_id = $r->customer_id;
        $status_id = $r->status_id;
        $q->free_result();

        if ("1" == $customer_id)
        {
            return " 1 = 1 " ;
        }
        else if ("2" == $customer_id)
        {
            switch ($field) {
                case 'customer_id':
                    $id_array = $this->_getCustomer('status_id', '2');
                    break;
            }
        }
        else if ("3" == $customer_id)
        {
            switch ($field) {
                case 'customer_id':
                    $id_array = $this->_getCustomer('status_id', '3');
                    break;
            }

        }
        else
        {
            switch ($field) {
                case 'customer_id':
                    $id_array = array($customer_id);
                    break;
            }
        }

        $statement = $field_text . ' IN (' . implode(',', $id_array)  . ')';
        return $statement;
    }

    // field = status or customer
    function _getCustomer($field, $id)
    {
        $data = array();
        $sql = "SELECT customer_id
                FROM customer
                WHERE $field = $id";
        $q = $this->db->query($sql);
        foreach ($q->result_array() as $row)
        {
            $data[] = $row['customer_id'];
        }
        $q->free_result();

        return $data;
    }
}