/* Setting Up variables */
active = "newOrderTab";
$newTable = $('#newOrderTable');
$oldTable = $('#oldOrderTable');
$myTable  = $('#myIssueTable');
$companyTable = $('#companyIssueTable');

/* Init */
getNewOrder();
updateTime('new_order', 'first');
getOldOrder();
getMyTracker();
getCompanyTracker();

$('#newOrderTab').click(function () {getNewOrder(); updateTime('new_order', 'newOrderTab');});
$('#changeOrderTab').click(function () {getOldOrder(); updateTime('change_order', 'changeOrderTab');});
$('#myIssueTab').click(function () {getMyTracker(); updateTime('my_issue', 'myIssueTab')});
$('#companyIssueTab').click(function () {getCompanyTracker(); updateTime('company_issue', 'companyIssueTab')});

/* Get New Order */
function getNewOrder() {
    $.ajax({
        url: '/pmsbs/index.php/main/get_new_order',
        type: 'POST',
        dataType: 'json',
        success: function (msg) {
            var count = 0;
            var row = [];
            $.each(msg, function(key,value) {
                if (value['new'] == true) {count++;}
                row.push({
                    'category' : value['category'],
                    'date' : value['date'],
                    'model' : value['model'],
                    'name' : value['name'],
                    'order_name' : value['order_name'],
                    'project_name' : value['project_name'],
                    'type' : value['type'],
                    'history' : value['history'].replace("\n","<br>")
                });
            });
            $('a[href="#newOrder"] span').html(count);
            $newTable.bootstrapTable('destroy');
            $newTable.bootstrapTable({data: row});
            $newTable.children('thead').children('tr').children('th:nth-child(2)').children('div').trigger('click');
        }
    });
}

/* Get Old Order */
function getOldOrder() {
    $.ajax({
        url: '/pmsbs/index.php/main/get_old_order',
        type: 'POST',
        dataType: 'json',
        success: function (msg) {
            var count = 0;
            var row = [];
            $.each(msg, function(key,value) {
                if (value['new'] == true) {count++;}
                row.push({
                    'category' : value['category'],
                    'date' : value['date'],
                    'model' : value['model'],
                    'name' : value['name'],
                    'order_name' : value['order_name'],
                    'project_name' : value['project_name'],
                    'type' : value['type'],
                    'history' : value['history'].replace("\n","<br>")
                });
            });
            $('a[href="#changeOrder"] span').html(count);
            $oldTable.bootstrapTable('destroy');
            $oldTable.bootstrapTable({data: row});
            $oldTable.children('thead').children('tr').children('th:nth-child(2)').children('div').trigger('click');
        }
    });
}

function updateTime(col, id) {
    $('#' + active + ' a span').html('0');
    if (id != "first") {active = id;}
    $.ajax({
        url: '/pmsbs/index.php/main/update_time',
        type: 'POST',
        data: {col: col},
        dataType: 'json',
        success: function (msg) {

        }
    });
}

/* Get My Tracker */
function getMyTracker() {
    $.ajax({
        url: '/pmsbs/index.php/main/get_my_tracker',
        type: 'POST',
        dataType: 'json',
        success: function (msg) {
            var count = 0;
            var row = [];
            $.each(msg, function(key,value) {
                if (value['new'] == true) {count++;}
                row.push({
                    'tracker' : value['tracker'],
                    'date' : value['date'],
                    'subject' : value['subject'],
                    'issue_type' : value['issue_type'],
                    'conversation' : value['conversation']
                });
            });
            $('a[href="#myIssue"] span').html(count);
            $myTable.bootstrapTable('destroy');
            $myTable.bootstrapTable({data: row});
            //$myTable.children('thead').children('tr').children('th:nth-child(2)').children('div').trigger('click');
        }
    });
}


/* Get Company Tracker */
function getCompanyTracker() {
    $.ajax({
        url: '/pmsbs/index.php/issue/get_company_tracker',
        type: 'POST',
        dataType: 'json',
        success: function (msg) {
            var count = 0;
            var row = [];
            $.each(msg, function(key,value) {
                if (value['new'] == true) {count++;}
                row.push({
                    'tracker' : value['tracker'],
                    'date' : value['date'],
                    'subject' : value['subject'],
                    'to' : value['to'],
                    'issue_type' : value['issue_type'],
                    'conversation' : value['conversation']
                });
            });
            $('a[href="#companyIssue"] span').html(count);
            $companyTable.bootstrapTable('destroy');
            $companyTable.bootstrapTable({data: row});
            //$companyTable.children('thead').children('tr').children('th:nth-child(2)').children('div').trigger('click');
        }
    });
}
