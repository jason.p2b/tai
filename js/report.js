// Initial Setting


$('svg').css('width', '120px').css('height','60px');
$('svg g').css('transform', 'scale(0.4, 0.4)');


$('.check_show').click(function () {
    isChecked = $(this).is(":checked");

    if ($(this).val() == "frame") {
        for (i=3; i<=9; i++) {
            toggleColumn(i, isChecked);
            if (isChecked) {
                $('#f' + i).prop('checked', 'true');
            } else {
                $('#f' + i).removeAttr('checked');
            }
        }
    } else if ($(this).val() == "f_width") {
        for (i=3; i<=4; i++) {
            toggleColumn(i, isChecked);
        }
        checkCheckbox('frame_show');

    } else if ($(this).val() == "f_length") {
        for (i=5; i<=6; i++) {
            toggleColumn(i, isChecked);
        }
        checkCheckbox('frame_show');

    } else if ($(this).val() == "door") {
        for (i=10; i<=18; i++) {
            toggleColumn(i, isChecked);
            if (isChecked) {
                $('#f' + i).prop('checked', 'true');
            } else {
                $('#f' + i).removeAttr('checked');
            }
        }
    } else if ($(this).val() == "d_width") {
        for (i=10; i<=11; i++) {
            toggleColumn(i, isChecked);
        }
        checkCheckbox('door_show');
    } else if ($(this).val() == "d_length") {
        for (i=12; i<=13; i++) {
            toggleColumn(i, isChecked);
        }
        checkCheckbox('door_show');
    } else if ($(this).val() == "glass") {
        for (i=17; i<=18; i++) {
            toggleColumn(i, isChecked);
        }
        checkCheckbox('door_show');
    } else if ($(this).val() == "hardware") {
        for (i=19; i<=24; i++) {
            toggleColumn(i, isChecked);
            if (isChecked) {
                $('#f' + i).prop('checked', 'true');
            } else {
                $('#f' + i).removeAttr('checked');
            }
        }
    } else {
        toggleColumn($(this).val(), isChecked);
        ind = $(this).val().replace('/f/', '');

        if (parseInt(ind) <= 9) {
            checkCheckbox('frame_show');
        } else if(parseInt(ind) >= 19) {
            checkCheckbox('hardware');
        } else {
            checkCheckbox('door_show');
        }
    }
});

function toggleColumn(col, isChecked) {
    $head = $('table thead tr th:nth-child(' + col + ')');
    $col  = $('table tbody tr td:nth-child(' + col + ')');
    if (isChecked) {
        $head.show();
        $col.show();
    } else {
        $head.hide();
        $col.hide();
    }
}

function checkCheckbox(id) {
    state = true;

    if (id == "frame_show") {
        for (i=5; i<=9; i++) {
            if (!$('#f' + i).is(":checked")) {
                $('#' + id).removeAttr('checked');
                state = false;
                break;
            }
        }
    } else if (id == "door_show") {
        for (i=12; i<=17; i++) {
            if (!$('#f' + i).is(":checked")) {
                $('#' + id).removeAttr('checked');
                state = false;
                break;
            }
        }
    } else if (id == "hardware") {
        for (i=19; i<=24; i++) {
            if (!$('#f' + i).is(":checked")) {
                $('#' + id).removeAttr('checked');
                state = false;
                break;
            }
        }
    }
    
    if (state) {
        $('#' + id).prop('checked', 'true');
    }
}

// Search functions
$('#search').change(function () {
    if ($(this).val() == "shape") {
        $('#shape_search').val('').trigger('change').show();
        $('#t_search').val('').hide();
    } else if ($(this).val() != "") {
        $('#shape_search').val('').trigger('change').hide();
        $('#t_search').val('').show();
    } else {
        $('#shape_search').val('').trigger('change').hide();
        $('#t_search').val('').trigger('keyup').hide();
    }
});

$('#shape_search').change(function () {
    if ($(this).val() != "") {
        $('table tbody tr').hide();
        $('.' + $(this).val()).closest('tr').show();
    } else {
        $('table tbody tr').show();
    }
});

$('#t_search').keyup(function () {
    type   = $('#search').val();
    search = $(this).val();

    if (search == "") {
        $('table tbody tr').show();
    } else {
        $('table tbody tr').each(function () {
            if (type == 'f_width') {
                compareNumber(this, search, 3, 4);
            } else if (type == 'f_length') {
                compareNumber(this, search, 5, 6);
            } else if (type == 'd_width') {
                compareNumber(this, search, 10, 11);
            } else if (type == 'd_length') {
                compareNumber(this, search, 12, 13);
            } else {
                html = $(this).find('td:nth-child(' + type + ')').html();
                if (html.toLowerCase().indexOf(search) != -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            }
        });
    }
});

function compareNumber(el, search, s_index, l_index) {
    if (!isNaN(parseFloat(search))) {
        small = $(el).find('td:nth-child(' + s_index + ')').html();
        large = $(el).find('td:nth-child(' + l_index + ')').html();

        if (search >= parseFloat(small) && search <= parseFloat(large)) {
            $(el).show();
        } else {
            $(el).hide();
        }
    } else {
        $(el).show();
    }
}

$('#report_table').footable();