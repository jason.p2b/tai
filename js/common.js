/*
 * Change Placeholder value of search input
 */

function changePlaceholder(id, input_id, value) {
    key = $('#' + id + ' option:selected').text();
    $('#' + input_id).attr('placeholder', value + ' - ' + key);
}

/*
 * Get Search data
 */

function getSearchResult(mydata, myUrl) {
    var data = "";

    $.ajax({
        url: '/taiching/index.php/' + myUrl,
        type: 'POST',
        dataType: 'json',
        async: false,
        data: mydata,
        success: function(msg) {
            data = msg;
        }
    });

    return data;
}

/*
 * Table header sort
 */
 /*
$(document).ready(function () {
    $('th').click(function () {
        table_id = $(this).closest('table').attr('id');
        c = $(this).children('span').attr('class');

        if (c.match('up')) {
            $('th').each(function () {
                if ($(this).closest('table').attr('id') == table_id) {
                    $(this).children('span').addClass('hide');
                }
            });
            $(this).children('span').removeClass('hide').removeClass('arrow-up');
            $(this).children('span').addClass('arrow-down');
        } else {
            $('th').each(function () {
                if ($(this).closest('table').attr('id') == table_id) {
                    $(this).children('span').addClass('hide');
                }
            });
            $(this).children('span').removeClass('hide').removeClass('arrow-down');
            $(this).children('span').addClass('arrow-up');
        }
    });
});

/*
 * Print button
 */
$(document).ready(function () {
    $('.print').hover(function () {
        $(this).toggleClass('printHover');
        $('.print span').show({delay: 1000});
    }); 

    $('body').on('mouseenter', '.print', function(e) {
        $(this).addClass('printHover');
    });
    $('body').on('mouseleave', '.print', function(e) {
        $(this).removeClass('printHover');
        $('.print span').hide();
    });
});

function printScreen(id, cssFile) {
    $("#" + id).printThis({
       debug: false,
       importCSS: true,
       printContainer: true,
       loadCSS: cssFile,
       pageTitle: "",
       removeInline: false
   });

    // $('#' + id).printThis({
    //     debug: false,
    //     importCSS: true,
    //     printContainer: true,
    //     loadCSS: cssFile,
    //     removeInline: false 
    // });
}