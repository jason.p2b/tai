// $.ready( function() {
//      CKEDITOR.replace( 'conversation' );
// } );

$('#project').change(function () {
    var project = $(this).val();
    $('#order option:not(:first-child)').remove();

    if (project != "") {
        $.post('/pms/index.php/issue/get_order', {project : project}, function (msg){
            $.each(msg, function (key,value) {
                $('#order').append($("<option></option>").attr("value",key).text(value));
            });
        }, 'json');
    }
});

function add_file(num) {
    num++;
    html = '<input class="attachments" id="file' + num + '" type="file" name="file[]" multiple="multiple">';
    $('.add_file').before(html);
    $('.add_file').attr('onclick', 'add_file(' + num + ')');
    $('#file' + num).trigger('click');
}

$('#order_form').validate({
    rules: {
        project: {required: true},
        subject: {required: true},
        to: {required: true},
        type: {required: true},
        conversation: {required: true}
    }
});