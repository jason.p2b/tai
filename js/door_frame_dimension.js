svgHover('svg .ground');
svgHover('svg .door_width');
svgHover('svg .door_length');
svgHover('svg .below_gap');
svgHover('svg .beneath_length');
svgHover('svg .inner_length');
svgHover('svg .frame_length');
svgHover('svg .frame_width');
svgHover('svg .frame_length_son');
svgHover('svg .frame_width_son');

function svgHover(origin) {
    $(origin).hover(
        function () {
            $(origin).each(function () {
                classNames = $(this).attr('class');
                if (classNames.match('svg_text')) {
                    svgAddClass(this, 'hoverText');
                } else if (classNames.match(/[shapes|arrows]/)) {
                    svgAddClass(this, 'hoverShape');
                }
            });
        },
        function () {
            svgRemoveClass(origin, 'hoverText');
            svgRemoveClass(origin, 'hoverShape');
        }
    );
}

function svgAddClass(origin, newClass) {
    $(origin).attr('class', function (i, classNames) {
        return classNames + ' ' + newClass;
    });
}
function svgRemoveClass(origin, newClass) {
    $(origin).attr('class', function (i, classNames) {
        return classNames.replace(' ' + newClass, '');
    });
}

// On change value, change dimension value
$('input[name=frame_width]').keyup(function () {
    $('text.frame_width').html($(this).val());
});
$('input[name=frame_width]').change(function () {$(this).trigger('keyup');})

$('input[name=frame_length]').keyup(function () {
    fl = $(this).val();
    dl = $('input[name=door_length').val();
    bl = $('input[name=beneath_length]').val();
    wl = $('input[name=v_west_length]').val();
    cl = fl - bl - wl;
    gl = fl - wl - 5 - dl - bl;

    $('text.frame_length').html(fl);
    $('text.inner_length').html(cl);
    $('text.below_gap').html(gl);
});
$('input[name=frame_length]').change(function () {$(this).trigger('keyup');})

$('input[name=door_width]').keyup(function () {
    $('text.door_width').html($(this).val());
});
$('input[name=door_width]').change(function () {$(this).trigger('keyup');})

$('input[name=son_width]').keyup(function () {
    $('text.door_width_son').html($(this).val());
});
$('input[name=son_width]').change(function () {$(this).trigger('keyup');})

$('input[name=door_length]').keyup(function () {
    $('input[name=son_length]').val($(this).val()).trigger('keyup');
    fl = $('input[name=frame_length]').val();
    dl = $(this).val();
    bl = $('input[name=beneath_length]').val();
    wl = $('input[name=v_west_length]').val();
    gl = fl - dl - bl - wl - 5;

    $('text.below_gap').html(gl);
    $('text.door_length').html(dl);
});
$('input[name=door_length]').change(function () {$(this).trigger('keyup');})

$('input[name=son_length]').keyup(function () {
    $('text.door_length_son').html($(this).val());
});


$('input[name=beneath_length]').keyup(function () {
    fl = $('input[name=frame_length]').val();
    dl = $('input[name=door_length').val();
    bl = $(this).val();
    wl = $('input[name=v_west_length]').val();
    cl = fl - bl - wl;
    gl = fl - wl - 5 - dl - bl;

    $('text.beneath_length').html(bl);
    $('text.inner_length').html(cl);
    $('text.below_gap').html(gl);
});
$('input[name=beneath_length]').change(function () {$(this).trigger('keyup');})

function changeDimension() {
    // Used for v_west_length
    fl = $('input[name=frame_length]').val();
    dl = $('input[name=door_length').val();
    bl = $('input[name=beneath_length').val();
    wl = $('input[name=v_west_length]').val();
    cl = fl - bl - wl;
    gl = fl - wl - 5 - dl - bl;

    $('text.inner_length').html(cl);
    $('text.below_gap').html(gl);
}