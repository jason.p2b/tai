/* Initialize Page */
var $companyTable = $('#companyIssueTable');

$('#project').change(function () {
    getTrackerByProject($(this).val());
});

getTrackerByProject(0);

/* Clear filestack when change tab (coz this filestack is a hack!)*/
$('#viewTab').click(function () {
    if ($el.data('fileinput')) {
        $el.fileinput('destroy');
    }
    $el = $('#attachment');
    initPlugin();
});     
$('#addTab').click(function () {
    if ($el.data('fileinput')) {
        $el.fileinput('destroy');
    }
    $el = $('#new_attachment');
    initPlugin();
});


/* Get Company Tracker */
function getTrackerByProject(id) {
    $.ajax({
        url: '/pmsbs/index.php/issue/get_tracker_by_project',
        type: 'POST',
        data: {myId: id},
        dataType: 'json',
        success: function (msg) {
            var count = 0;
            var row = [];
            $.each(msg, function(key,value) {
                if (value['new'] == true) {count++;}
                row.push({
                    'tracker' : value['tracker'],
                    'date' : value['date'],
                    'subject' : value['subject'],
                    'to' : value['to'],
                    'issue_type' : value['issue_type'],
                    'conversation' : value['conversation']
                });
            });
            $('a[href="#companyIssue"] span').html(count);
            $companyTable.bootstrapTable('destroy');
            $companyTable.bootstrapTable({data: row});
            //$companyTable.children('thead').children('tr').children('th:nth-child(2)').children('div').trigger('click');
        }
    });
}

/* Submit */
$('input[type=submit]').click(function () {
    $('#tempForm').validator('validate');

    if (!$(this).hasClass('disabled')) {
        formdata = new FormData();

        for (i=0; i<myfilestack.length; i++) {
            file = myfilestack[i];

            if (window.FileReader) {
                reader = new FileReader();
                reader.readAsDataURL(file);
            }

            if (formdata) {
                formdata.append("file" + i, file);
            }
        }

    
        formdata.append('project', $('#new_project').val());
        formdata.append('subject', $('#new_subject').val());
        formdata.append('type', $('#type').val());
        formdata.append('to', $('#new_to').val());
        formdata.append('conversation', $('#new_conversation').summernote('code'));

        if (formdata) {
            $.ajax({
                url: '/pmsbs/index.php/issue/insert',
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function(res) {
                    $('input[type=reset]').trigger('click');
                    $('#project').trigger('change');
                    $('#viewTab').trigger('click');
                    $('#viewTab a').trigger('click');
                }   
            });
        }
    } 
});

/* Reset */
$('input[type=reset]').click(function () {
    $('#tempForm').validator('destroy');
    $('#tempForm input[type=text], #tempForm select').val("");
    $('.fileinput-remove-button').trigger('click');
    $('#new_conversation').summernote('code', "");
});