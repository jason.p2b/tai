$('#m_report').click(function () {
    if ($(this).is(":checked")) {
        $('#main_report').show();
    } else {
        $('#main_report').hide();
    }
});

$('#glass_yes, #glass_both').click(function () {
    $('#glass_dimension').show();
});
$('#glass_no').click(function () {
    $('#glass_dimension').hide();
});

$('.svg_class').click(function () {
    if ($(this).find('input').is(':checked')) {
        $(this).addClass('svg_checked');
    } else {
        $(this).removeClass('svg_checked');
    }
});


//////////////////////////////////////////////////////////////////
//
//
//              Add & Delete Buttons functions
//
//
//////////////////////////////////////////////////////////////////

function add_item(el, name, assoc_class, placeholder, error) {
    html = "<span class='check_new_items'>" +
            "<img src='../../img/delete_item.png' onclick='delete_item(this);'>" + 
            "<input type='text' name='" + name + "[]' value='' placeholder='" + placeholder + "' onblur='check_item(this, \"" + assoc_class + "\");'>" +
            "<span class='error'>" + error + "</span>"
            "</span>";
    $(el).prev('span').append(html);
}

function check_item(el, myclass) {
    $('.' + myclass).each(function () {
        if ($(this).html().toUpperCase() == $(el).val().toUpperCase()) {
            $(el).focus();
            $(el).next('.error').show();
            return false;
        }
        $(el).next('.error').hide();
    });
}

function delete_item(el) {
    $(el).parent().remove();
}
