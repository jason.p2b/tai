

// ////////////////////////////////////////////////////
// //
// // Frame Shape selection functions
// //
// ////////////////////////////////////////////////////

$('#frames').click(function () {
    alert('ul');
});

$('li.my-frame').click(function () {
    alert('hi');
    var id = $(this).attr('id');
    $(this).removeClass('frames_active');
    $('#'+id).addClass('frames_active');

});

function setFrameShape(el) {

    var val = el.id;
    $('.frames_active').removeClass('frames_active');
    $('#' + val).addClass('frames_active');
    $('#' + val).scrollintoview();

    $('#frame_shape').val(val);

    if (!('la_frame' == val || 'l_frame' == val)) {
        $('input[name=v_south_east_length]').parent().show();
    } else {
        $('input[name=v_south_east_length]').parent().hide();
    }

    $('input[name=v_total_length]').val(0).focus();
    $('input[name=v_north_length]').val(0);
    $('input[name=v_west_length]').val(0).trigger('change');
    $('input[name=v_east_length]').val(0);
    $('input[name=v_south_east_length]').val(0);

    $('#addOrder svg #v_total_length').html('A');
    $('#addOrder svg #v_north_length').html('B');
    $('#addOrder svg #v_west_length').html('C');
    $('#addOrder svg #v_east_length').html('D');
    $('#addOrder svg #v_south_east_length').html('E');
}


function changeSVGvalue(ele) {
    var frame = $("#frame_shape").val();
    $('.my-frame svg #' + ele.name).text(ele.value);
}

function selectFrame(id) {
    $('.my-frame').removeClass('frames_active');
    $('#' + id).addClass('frames_active');
    $('#frame_shape').val(id);
}