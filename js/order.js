/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Functions & jquery for order
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Add 建案 based on 客戶 selected
$('#customer').change(function () {
    var val = $(this).val();
    $.post(customer_url, {id: val}, function (data) {
        var $site = $('#site-order');
        $site.html("");
        $site.append("<option value=''> --- </option>");

        $.each(data, function (key, arr) {
            $site.append("<optgroup label='" + key + "'>");
            $.each(arr, function (k,v) {
                $site.append("<option value='" + k + "'>" + k + " - " + v + "</option>")
            });
            $site.append("</optgroup>");
        });

    }, 'json');
    $('#site-order').val("").trigger('change');
    $('#site-order').select2('destroy');
    $('#site-order').select2();
});

// View 訂單 based on 建案 selected
$('#site-order').change(function () {
    var id = $(this).val();

    // Reset all values
    if (check) {
        $editor[0].reset();
        $editor1[0].reset();
        $trad_e[0].reset();
        showOrderNote("");
        $('#orderNote-note').val('');
    }
    
    $('#current_ny_order_id').val("");
    $('.pvtTable').html("");
    $('#item-history').html("");

    if ( id == "") {
        $('#order-table').html("");
        $('#order-item-table').html("");
        return true;
    }

    $('#order-title').html($('#site-order option:selected').text());

    $.post(site_url, {id: id}, function (result) {
        $('#order-title').html($(this).text());
        $('#order-table').html("");
        if (result['success']) {
            orderModal(result['col'], result['row']);
            setProgressColour('#order-table tbody td:nth-of-type(2)');
        }
    },'json');

});

function setProgressColour(selector) {
    $(selector).each(function () {
        var $t = $(this).html();

        if ("收到訂單" == $t ||
            "等排程" == $t) {
            $(this).addClass('bg-warning');
        } else if ( "已出貨" == $t || 
                    "等出貨" == $t) {
            $(this).addClass('bg-success');
        } else if ( "等SKIN板" == $t ||
                    "等客戶回復" == $t ||
                    "取消訂單" == $t) {
            $(this).addClass('bg-danger');
        } else if ( "發包中" == $t ||
                    "生產中" == $t ||
                    "烤漆包裝" == $t) {
            $(this).addClass('bg-primary');
        } else {
            $(this).addClass('bg-light');
        }
    });
}

function setDoorColour(selector) {
    $(selector).each(function () {
        var $t = $(this).html();

        if ("扇" == $t) {
            $(this).addClass('text-danger');
        }
    });
}

function setDoubleDoorColour(selector) {
    $(selector).each(function () {
        var $t = $(this).html();

        if ("雙開" == $t) {
            $(this).addClass('text-danger');
        }
    });
}

/*
 * Ajax call is used in #site-order select to get order information
 */
$('#export_me').click(function () {
    const $body = $('#order-item-table-translate tbody:contains("No results")').html();
    let $table = "";

    if (!$body) {
        $table = $('#order-item-table-translate');
    } else {
        $table = $('#order-item-table-traditional');
    }

    $table.tableExport({type:'csv'});
});

function get_order_history(id) {
    $('#item-history').html("");
    $.post(h_url, {id:id}, function (diary) {
        var html = "";
        $.each(diary, function (d, arr) {
            html = html + "<div class='row'><div class='col-sm-12 item-history-date'><h4>" + d + "</h4></div>";
            $.each(arr, function (t, lists) {
                html = html + '<div class="col-md-1 col-sm-3 item-history-time">' + t + '</div>';
                html = html + '<div class="col-md-11 col-sm-9 item-history-content"><ul>';
                $.each(lists, function (k, item) {
                    html = html + "<li>" + item + "</li>";
                });
                html = html + '</ul></div>';
            });
            html = html + "</div>";
        });

        $('#item-history').html(html);
    }, 'json');
}

// Function for getting order item details
function getOrderItem(id, title) {
    var data = {id: id};
    var heading = "訂單: " + title;
    var $original = $('#order-item-table-original');
    var $translate = $('#order-item-table-translate');
    var $traditional = $('#order-item-table-traditional');
    $('#current_ny_order_id').val(id);

    $.post(o_url, data, function (result) {
        $('.order-item-title').html(heading);
        $original.html("");
        orderItemModal(result['col'], result['row']);
        getProgressList('#all-ny-progress', result['stage']);

        $original.find('tr th:first-of-type').attr('class', 'sticky-cell');
        $original.find('tr td:first-of-type').attr('class', 'sticky-cell');

    },'json');

    $.post(t_url, data, function (result) {
        $('.order-item-title').html(heading);
        $translate.html("");
        orderItemModalTranslate(result['col'], result['row']);

        $translate.find('tr th:first-of-type').attr('class', 'sticky-cell');
        $translate.find('tr td:first-of-type').attr('class', 'sticky-cell');

    },'json');

    $.post(tr_url, data, function (result) {
        $('.order-item-title').html(heading);
        $traditional.html("");
        orderItemModalTraditional(result['col'], result['row']);
        getProgressList('#all-trad-progress', result['stage']);

        $traditional.find('tr th:first-of-type').attr('class', 'sticky-cell');
        $traditional.find('tr td:first-of-type').attr('class', 'sticky-cell');

        if ('一般' == result['type']) {
            $('.trad').show();
            $('.trad a').trigger('click');
            $('.ny').removeClass('active').hide();
            $('#translate').removeClass('show');
            $('#original').removeClass('show');
        } else {
            if ($('.trad').hasClass('active')) {
                $('.ny:first-of-type a').trigger('click');
            }
            $('.ny').show();
            $('.trad').removeClass('active').hide();
            $('#traditional').removeClass('show');
        }

    },'json');

    // Get Order Note
    showOrderNote(id);

    // Get History
    get_order_history(id);

    // Highlight clicked order 
    $('#order-table tbody tr').removeClass('selectedRow');

    // $('#order-table tbody tr').each(function () {
    //     var check = $(this).find('td:first-of-type').html();
    //     var head  = $(this).find('td:nth-of-type(3)').html();

    //     if (check == id && head.match(title)) {
    //         $(this).addClass("selectedRow");
    //     }
    // });
    $('#order-table tbody tr td:first-child:contains('+id+')').parent().addClass('selectedRow');
    // $('td:contains("' + title + '")').parent('tr').addClass("selectedRow");

    // Start quality control sub panel
    $('#qc_site').html($("#site-order option:selected").text());
    $('#qc_order_number').html(title);
    getQCTable(data);
}

function getQCTable(data) {
    $.post(qc_url, data, function (result) {
        jQuery(function($){
            $('#qc_table').footable({
                "columns": result.col,
                "rows": result.row
            });
        });
    },'json');
}

// Used to change all progress of an order & its order items
$('#progress-all').change(function () {
    if ($(this).val() == "") {
        return;
    }
    $('#progress-all-text').html($(this).val());
    $("#mi-modal").modal('show');
});

$('#modal-btn-yes').on("click", function () {
    $("#mi-modal").modal('hide');
    var progress = $('#progress-all').val();
    $.post(progress_url, {id: $('#current_ny_order_id').val(), progress: progress}, function(result) {
        $('.selectedRow').find('td:nth-of-type(2)').html(progress);
        $('.pvtTable tbody tr').find('td:nth-of-type(3)').html(progress);
    },'json');
});

$('#modal-btn-no').on("click", function () {
    $("#mi-modal").modal('hide');
});

// function for getting today's date
var curday = function(sp){
    today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //As January is 0.
    var yyyy = today.getFullYear();

    if(dd<10) dd='0'+dd;
    if(mm<10) mm='0'+mm;
    return (yyyy+sp+mm+sp+dd);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Functions & jquery for traditional order modal
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// Allow frame changes based on door type selected
$('#type').change(function ()　{
    var val = $(this).val();

    if ("180度" == val || "270度" == val) {
        $('#frame_info').slideUp("slow");
        $('#frame_shape').val(val);
    } else {
        $('#frame_info').slideDown("slow");
        $('#frame_shape').val($('.frames_active').attr('id'));
    }
});


// Adding amount Total
$('#amount_l, #amount_r').change(function () {
    var l = parseInt($('#amount_l').val());
    var r = parseInt($('#amount_r').val());
    var t = l + r;
    $('#amount_t').val(t);
});




    // ////////////////////////////////////
    // //
    // // Frame Shape selection functions
    // //
    // ////////////////////////////////////

$('li.my-frame').click(function () {
    var id = $(this).attr('id');
    $(this).removeClass('frames_active');
    $('#'+id).addClass('frames_active');

});

function setFrameShape(el) {

    var val = el.id;
    $('.frames_active').removeClass('frames_active');
    $('#' + val).addClass('frames_active');
    $('#' + val).scrollintoview();

    $('#frame_shape').val(val);

    if (!('la_frame' == val || 'l_frame' == val)) {
        $('input[name=v_south_east_length]').parent().show();
    } else {
        $('input[name=v_south_east_length]').parent().hide();
    }

    $('input[name=v_total_length]').val(0).focus();
    $('input[name=v_north_length]').val(0);
    $('input[name=v_west_length]').val(0).trigger('change');
    $('input[name=v_east_length]').val(0);
    $('input[name=v_south_east_length]').val(0);

    $('#addOrder svg #v_total_length').html('A');
    $('#addOrder svg #v_north_length').html('B');
    $('#addOrder svg #v_west_length').html('C');
    $('#addOrder svg #v_east_length').html('D');
    $('#addOrder svg #v_south_east_length').html('E');
}


function changeSVGvalue(ele) {
    var frame = $("#frame_shape").val();
    $('.my-frame svg #' + ele.name).text(ele.value);
}

function selectFrame(id) {
    $('.my-frame').removeClass('frames_active');
    $('#' + id).addClass('frames_active');
    $('#frame_shape').val(id);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Functions & jquery for traditional SVG
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var svgFile = "";
function openFrameSVG($id, $type) {
    $('#svg-view').html('<img src="svg/rings.svg" style="display:block;margin:auto;width:10%;">');
    $.post(sf_url, {id: $id, type: $type}, function (data) {
        $('#svg-view').html(data.h);
        svgFile = data.f;
        $('.save-svg').removeClass('hide');
    }, 'json');
}

function saveSvg() {
    var w = $('#svg-view').width();
    $('#svg-view svg').attr('width', w+'px');
    html2canvas(document.querySelector("#svg-view")).then(canvas => {
        document.querySelector("#svg-view2").appendChild(canvas);
        var a = document.createElement('a');
            a.href = canvas.toDataURL("image/jpeg");
            a.download = svgFile;
            a.click();
        $('#svg-view svg').removeAttr('width');
    });
    $('#svg-view2').addClass('hide');
}

function printSvg() {
    $('#svg-view').print();
}

function printQC() {
    $('#view_qc').print();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Functions & jquery for checking tables
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function checkAll(id) {
    
    if( $(id).find('th input.order_items').is(':checked') ) {
        $('td input.order_items').attr("checked", false);
        $('td input.order_items').trigger('click');
    } else {
        $('td input.order_items').attr("checked", false);
        console.log('no');
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Functions for order Notes
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function showOrderNote(id) {
    var $orderNote = $('#item-orderNote');
    $orderNote.html('');

    if (!(id == 0 || id == "")) {
        $.post(showNote_url, {id: id}, function(result) {
            $.each(result, function (key, arr) {
                var html = orderNoteCard(arr.date, arr.user, arr.note);
                $orderNote.append(html);
            });
        },'json');
    }
}

function saveOrderNote() {
    var note = $('#orderNote-note').val();
    var id   = $('#current_ny_order_id').val();
    if (note.length > 5) {
        $.post(createNote_url, {id: id, note: note}, function(result) {
            $('#orderNote-note').val('');
            showOrderNote(id);
        },'json');
    }
}

function orderNoteCard(date, user, note) {
    var block = '<div class="row">' +
                    '<div class="col">' +
                        '<div class="card border border-dark mb-3">' +
                            '<div class="card-header border-bottom border-dark">' + date + '  ' + user + ':</div>' +
                                '<div class="card-body">' +
                                    '<p class="card-text">' + note + '</p>' +
                '</div></div></div></div>';
    return block;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Functions populating progress dropdown
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getProgressList(id, list) {
    // Clear current list
    if (list.length !== 0) {
        $(id).empty();
        let lastOPT = "";

        // Append new options
        for (var key in list) {
            const values = list[key].split("-");

            if (values.length > 1 && !(lastOPT == values[0])) {
                $(id).append('</optgroup><optgroup label="' + values[0] + '">');
                lastOPT = values[0];
            } else if (lastOPT != "" && values.length == 1) {
                $(id).append('</optgroup><optgroup label="其他">');
                lastOPT = "";
            }

            $(id).append($('<option>', {
                value: list[key],
                text: key
            }));
        }
    } 
}