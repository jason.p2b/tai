/****
 *
 * Print orders by 訂單編號
 * Param: className = class of the 訂單編號
          area      = id of the print area
 *////

$(document).ready(function () {
    //$('g').attr('transform', 'scale(1,1)');
    //$('#hidden_svg li:not(:last-child) svg').css('height', '160px').css('width', '300px');
});

function printByClassName(className, area) {
    // Clear print area
    $('#' + area).html("");

    // Set up Page header
    html = "<h2>" + $('#project option:selected').text() + "  /  " + $('label[for=' + className + ']').html() + "</h2>";
    $('#' + area).append(html);

    // Get each Info
    $.ajax({
        url: '/pms/index.php/order/print_by_class',
        type: 'POST',
        async: false,
        dataType: 'json',
        data: {id: $('#project').val(), name: $('label[for=' + className + ']').html()},
        success: function(msg) {
            $.each(msg, function (key, value) {
                var df;
                var model;
                var length; var width; var inner_length = 0;
                var sum =  parseInt(value['left_amount']) + parseInt(value['right_amount']);
                if (value['frame'] != undefined) {
                    df = "門框";
                    model = value['frame']['model'] + ' (' + value['model'] + ')';
                    length = value['frame']['length'];
                    width = value['frame']['width'];
                    inner_length = value['frame']['inner_length'];
                } else {
                    df = "門扇";
                    model = value['door']['model'] + ' (' + value['model'] + ')';
                    length = value['door']['length'];
                    width = value['door']['width'];
                }
                
                // Hardware section
                var hardware = "";
                for (i=0; i<value['hardware'].length; i++) {
                    if (value['hardware'][i] != "" && value['hardware'][i] != null && hardware == "") {
                        hardware = value['hardware'][i];
                    } else if (value['hardware'][i] != "" && value['hardware'][i] != null) {
                        hardware += "<br>" + value['hardware'][i];
                    }
                }

                html = "<table class='print_table' id='print_table_" + key + "'>";
                html += "<tr><td rowspan='3'>" + df + "</td><th rowspan='2'>型號</th><th colspan='2'>規格</th><th rowspan='2'>內距</th><th rowspan='2'>總數量</th><th colspan='2'>腳鍊方向</th><th rowspan='2'>門鎖</th><th rowspan='2'>鉸鍊</th><th rowspan='2'>其他五金</th></tr>";
                html += "<tr><th>W(寬)</th><th>H(高)</th><th>左(L)</th><th>右(R)</th></tr>";
                html += "<tr><td>" + model + "<br>" + value['report_name'] + "</td><td>" + width + "</td><td>" + length + "</td><td>" + inner_length + "</td><td>" + sum + "</td><td>" + value['left_amount'] + "</td><td>" + value['right_amount'] + "</td><td style='min-width:150px;'>" + value['lock_name'] + "</td><td style='min-width:150px;'>" + value['hinge_name'] + "</td><td style='min-width:200px;'>" + hardware + "</td></tr>";
                html += "<tr><td colspan='4' class='svg1' style='vertical-align: top;'>" + $('#' + value['frame_shape']).html() + "</td><td colspan='4' class='svg2' style='vertical-align: top;'>" + $('#hidden_svg #df_dimension').html() + "</td><td colspan='3' style='vertical-align: top;'>" + value['note'].replace(/\n/g,"<br>") + "</td></tr>";
                html += "</table>";
                html += "<div style='page-break-after:always'></div>";

                $('#' + area).append(html);
                $('table#print_table_' + key + ' .svg1 svg').css('height', '160px').css('width', '300px');
                $('table#print_table_' + key + ' .svg1 svg g').attr('transform', 'scale(1,1)');
                $('table#print_table_' + key + ' .svg1 svg #v_total_length').html(value['v_total_length']);
                $('table#print_table_' + key + ' .svg1 svg #v_north_length').html(value['v_north_length']);
                $('table#print_table_' + key + ' .svg1 svg #v_west_length').html(value['v_west_length']);
                $('table#print_table_' + key + ' .svg1 svg #v_south_length').html(value['v_south_length']);
                $('table#print_table_' + key + ' .svg1 svg #v_east_length').html(value['v_east_length']);
                $('table#print_table_' + key + ' .svg1 svg #v_south_east_length').html(value['v_south_east_length']);

                $('table#print_table_' + key + ' .svg2 svg g').attr('transform', 'scale(1,1)');
                $('table#print_table_' + key + ' .svg2 svg').css('height', "400px").css('width', "350px");
                $('table#print_table_' + key + ' .svg2 svg text.frame_width').html(width);
                $('table#print_table_' + key + ' .svg2 svg text.frame_length').html(length);
                $('table#print_table_' + key + ' .svg2 svg text.inner_length').html(inner_length);
                $('table#print_table_' + key + ' .svg2 svg text.beneath_length').html(value['frame']['beneath_length']);
                

                if (value['frame'] != undefined) {
                    $('table#print_table_' + key + ' .svg2 svg .double_door_frame_svg').hide();
                    $('table#print_table_' + key + ' .svg2 svg .door_frame_svg .doorComponent').hide();
                } else if (value['door_amount'] != 'single') {
                    $('table#print_table_' + key + ' .svg2 svg .door_frame_svg').hide();
                } else {
                    $('table#print_table_' + key + ' .svg2 svg .double_door_frame_svg').hide();
                }
                
            });
        },
        error: function (msg) {
            alert('fail');
            console.log(msg);
        }
    });

    // Add to print area
    //$('#' + area).show();

    $("#" + area).printThis({
       debug: false,
       importCSS: true,
       printContainer: true,
       loadCSS: './css/print.css',
       pageTitle: "",
       removeInline: false
   });

}

