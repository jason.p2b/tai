/* Get Project's related reports */
    $('#new_project').change(function () {
        $('#new_report option:not(:first-child)').remove();
        $('.non-report').hide();
        val = $(this).val();

        if ("" != val) {
            $.post('/pmsbs/index.php/order/get_reports', {id : val}, function (msg) {
                $.each(msg, function (key, value) {
                    $('#new_report').append($("<option></option>").attr("value",key).text(value));
                });
            }, 'json');
        }
    });

/* Get Limits of the selected report */
    $('#new_report').change(function () {
        $('.non-report').hide();
        val = $(this).val();

        if ("" != val) {
            //$.post('/pmsbs/index.php/order/', {id : val}, function (msg) {
                
                $('.non-report').show();
            //}, 'json');
        }
    });

/* Form and Label functions */
    
    /* Datepicker */
    $('#due_date').datepicker(
        { changeMonth: true, changeYear: true, minDate: 1 },
        $.datepicker.regional[ "zh-TW" ]
    );

    /* Sum of left & right amount */
    $('#new_left_amount').keyup(function () {
        alert('hi');
        var left  = $('#new_left_amount').val();
        var right = $('#new_right_amount').val();
        var total = parseInt(left) + parseInt(right);
        $('#new_total_amount').val(total);
    });

// // Init Variables
// var f_l_min = 0;
// var f_l_max = 0;

// ////////////////////////////////////////////////////
// //
// // Projects & Reports selection functions
// //
// ////////////////////////////////////////////////////

// $('#project').change(function () {
//     $('#report option:not(:first-child)').remove();
//     if ($(this).val() == "") {
//         $('#report').closest('p').hide();
//         $('.hiding').hide();
//         $('#frame').hide();
//         $('#door').hide();
//         $('#special').val("").trigger('change');
//     } else {
//         // Get Reports
//         $.ajax({
//             url: '/pms/index.php/order/get_reports',
//             type: 'POST',
//             async: false,
//             dataType: 'json',
//             data: {id : $(this).val()},
//             success: function(msg) {
//                 $.each(msg, function (key, value) {
//                     $('#report').append($("<option></option>").attr("value",key).text(value));
//                 });
//                 $('.hiding').hide();
//                 $('#special').trigger('change'); 
//             }
//         });
//     }
// });

// $('#special').change(function () {
//     if ($(this).val() == "0") {
//         $('.hiding').hide();
//         $('#frame').hide();
//         $('#door').hide();
//         $('#report').closest('p').show();

//     } else if ($(this).val() == "1") {
//         $('.hiding').hide();
//         $('#report').val('');
//         $('#special_info').show();
//         $('#submit_div').show();
//     } else {
//         $('.hiding').hide();
//     }
// });

// $('#report').change(function () {
    
//     $('#same_model option:not(:first-child)').remove();
//     $('#lock_type option:not(:first-child)').remove();
//     $('#handle_type option:not(:first-child)').remove();
//     $('#hinge_type option:not(:first-child)').remove();
//     $('#closer_type option:not(:first-child)').remove();
//     $('#bolt_type option:not(:first-child)').remove();
//     $('#frame_material option:not(:first-child)').remove();
//     $('#door_material option:not(:first-child)').remove();
//     $('#frames li, .hide').hide();

//     if ($(this).val() == "") {
//         $('.hiding').hide();
//         $('#frame').hide();
//         $('#door').hide();
//         $('#report').closest('p').show();
//     } else {
//         // Get Same Report Orders
//         $.post('/pms/index.php/order/get_same_model', {id : $(this).val(), project: $('#project').val()}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#same_model').append($("<option></option>").attr("value",value).text(key));
//             });
//         }, 'json');

//         // Get Locks
//         $.post('/pms/index.php/order/get_locks', {id : $(this).val()}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#lock_type').append($("<option></option>").attr("value",value).text(key));
//             });
//         }, 'json');
        
//         // Get Handles
//         $.post('/pms/index.php/order/get_handles', {id : $(this).val()}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#handle_type').append($("<option></option>").attr("value",value).text(key));
//             });
//         }, 'json');

//         // Get Hinges
//         $.post('/pms/index.php/order/get_hinges', {id : $(this).val()}, function (msg){
//              $.each(msg, function (key, value) {
//                     $('#hinge_type').append($("<option></option>").attr("value",value).text(key));
//                 });    
//         }, 'json');

//         // Get Closers
//         $.post('/pms/index.php/order/get_closers', {id : $(this).val()}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#closer_type').append($("<option></option>").attr("value",value).text(key));
//             });
//         }, 'json');

//         // Get Bolts
//         $.post('/pms/index.php/order/get_bolts', {id : $(this).val()}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#bolt_type').append($("<option></option>").attr("value",value).text(key));
//             });
//         }, 'json');

//         // Get Frame Materials
//         $.post('/pms/index.php/order/get_materials', {id : $(this).val(), type : "frame"}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#frame_material').append($("<option></option>").attr("value",value).text(value));
//             });
//         }, 'json');

//         // Get Door Materials
//         $.post('/pms/index.php/order/get_materials', {id : $(this).val(), type : "door"}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#door_material').append($("<option></option>").attr("value",value).text(value));
//             });
//         }, 'json');

//         // Get Frame Shapes
//         $.post('/pms/index.php/order/get_frame_shapes', {id : $(this).val()}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#' + value).show();
//             });   
//         }, 'json');

//         // Get Hardware
//         $.post('/pms/index.php/order/get_hardware', {id : $(this).val()}, function (msg) {
//             $.each(msg, function (key, value) {
//                 $('#hw' + key).show();
//             });   
//         }, 'json');

//         // Get Basics
//         $.post('/pms/index.php/order/get_basics', {id : $(this).val()}, function (msg) {
//             // Frame Dimension
//             insertMMBoundry('frame_width', msg);
//             insertMMBoundry('frame_length', msg);

//             // Frame Type
//             $('.frame_type').hide();
//             $('#' + msg['frame_type'] + '_frame').show();
//             $('input[name=frame_type]').val(msg['frame_type']);

//             // Door Amount
//             $('.door_amount').hide();
//             $('#' + msg['door_amount'] + '_door').show();
//             $('input[name=door_amount]').val(msg['door_amount']);

//             if (msg['door_amount'] == "double") {
//                 $('.son').show();
//                 $('#door .door_frame_svg').hide();
//                 $('#door .double_door_frame_svg').show();
//                 $('#door .door_frame_dimension').css('width','380px');

//                 insertMMBoundry('son_width', msg);
//                 insertMMBoundry('son_length', msg);
//             } else {
//                 $('.son').hide();
//                 $('#door .door_frame_svg').show();
//                 $('#door .double_door_frame_svg').hide();
//                 $('#door .door_frame_dimension').css('width','300px');
//             }

//             // Door Dimension
//             insertMMBoundry('door_width', msg);
//             insertMMBoundry('door_length', msg);
//         }, 'json');

//         $('.hiding:not(#special_info)').show();
//         $('#order_frame').trigger('click');
//     }
    
// });

// $('#same_model').change(function () {
//     if ($(this).val() != "") {
//         $.post('/pms/index.php/order/get_same_model_values ', {id : $(this).val()}, function (msg) {
//             $('.same_hide').hide();
//             $('.hd').next('label').hide();
//             $('.same_show').show();

//             // Lock
//             $('#lock_type').val(msg['lock']);
//             $('#lock_type_same').html($('#lock_type option:selected').text());

//             // Lock Height
//             $('#lock_height').val(msg['lock_height']);
//             $('#lock_height_same').html($('#lock_height').val());

//             // Hinge
//             $('#hinge_type').val(msg['hinge']);
//             $('#hinge_type_same').html($('#hinge_type option:selected').text());

//             // Frame Shape
//             $('#' + msg['frame_shape']['frame_shape']).trigger('click');
//             $('input[name=v_total_length]').val(msg['frame_shape']['v_total_length']).trigger('change');
//             $('input[name=v_north_length]').val(msg['frame_shape']['v_north_length']).trigger('change');
//             $('input[name=v_east_length]').val(msg['frame_shape']['v_east_length']).trigger('change');
//             $('input[name=v_west_length]').val(msg['frame_shape']['v_west_length']).trigger('change');

//             // Frame Dimension
//             $('input[name=frame_length]').val(msg['length']).trigger('keyup');
//             $('input[name=frame_width]').val(msg['width']).trigger('keyup');
//             $('input[name=beneath_length]').val(msg['beneath_length']).trigger('keyup');

//             if ($('input[name=v_south_east_length]').length > 0) {
//                 $('input[name=v_south_east_length]').val(msg['frame_shape'] ['v_south_east_length']).trigger('change');
//             }
//             $('#frame_shape_same').html($('#' + msg['frame_shape']['frame_shape']).html());

//             if (msg['hardware'] != undefined) {
//                 $.each(msg['hardware'], function (key, value) {
//                     if (!$('#hardware_' + key).is(":checked")) {
//                         $('#hardware_' + key).trigger('click');
//                     }
//                     for (i=0; i<value.length; i++) {
//                         num = i + 1;
//                         $('input[name=hardware_' + key + '_' + num).val(value[i]).trigger('keyup').hide();
//                     }
//                     $('#hardware_' + key).next('label').show();
//                 });
//             }

//             // Door Suggestions
//             if (!$('input[name=son_length]').is(":hidden")) {
//                 w = (msg['width'] - msg['frame_shape']['v_west_length']*2 - 18)/2;
//                 $('#suggest_son_width').html('建議: ' + w + ' mm');
//             } else {
//                 w = msg['width'] - msg['frame_shape']['v_west_length']*2 - 10;
//             }
//             $('#suggest_width').html('建議: ' + w + ' mm');

//             l = msg['length'] - msg['frame_shape']['v_west_length'] - msg['beneath_length'] - 15
//             $('#suggest_length').html('建議: ' + l + ' mm');
//             $('#suggest_son_length').html('建議: ' + l + ' mm');

//         }, 'json');

//     } else {
//         $('.same_hide').show();
//         $('.hd').next('label').show();
//         $('.same_show').hide();
//     }
// });

// $('input[name=door_width]').keyup(function () {
//     fw = $('input[name=frame_width]').val();
//     dw = $(this).val();
//     wl = $('input[name=v_west_length]').val();
//     sw = fw - dw - wl*2 - 18;
//     $('#suggest_son_width').html('建議: ' + sw + ' mm');

//     if (dw == "" || dw == "0") {
//         $('#suggest_son_width').html($('#suggest_width').html());
//     }
// });

// function insertMMBoundry(type, array) {
//     $('input[name=' + type + ']')
//         .attr('min',array['min_' + type + ''])
//         .attr('max',array['max_' + type + ''])

//     if (type.match(/frame/)) {
//         $('input[name=' + type + ']')
//             .next('.instruction')
//             .html(array['min_' + type] + 'mm ~ ' + array['max_' + type] + 'mm');
//     } else {
//         $('input[name=' + type + ']')
//             .next()
//             .next('.instruction')
//             .html(array['min_' + type] + 'mm ~ ' + array['max_' + type] + 'mm');
//     }
        

//     if (type == "frame_length") {
//         f_l_min = array['min_' + type];
//         f_l_max = array['max_' + type];
//     }
// }

// $('#order_frame').click(function () {
//     $('#frame').show();
//     $('#door').hide();
//     $('#same_model_p').hide();
// });
// $('#order_door').click(function () {
//     $('#door').show();
//     $('#frame').hide();
//     $('#same_model_p').show();
// });

// $('.hd').click(function () {

//     if ($(this).is(':checked')) {
//         $(this).next('label').next('span').show();
//     } else {
//         $(this).next('label').next('span').hide();
//     }
// });

// function text2SVG(el) {
//     $('#' + el.name).html(el.value);
// }

// ////////////////////////////////////////////////////
// //
// // Beneath Length calculation for total length
// //
// ////////////////////////////////////////////////////

// $('#beneath_length').keyup(function () {
//     $inst = $('input[name=frame_length]');
//     if ($inst.next('.instruction').html() != "" && $.isNumeric($(this).val()) ) {
//         min = parseFloat(f_l_min) + parseFloat($(this).val());
//         max = parseFloat(f_l_max) + parseFloat($(this).val());

//         $inst.attr('min', min).attr('max', max);
//         $inst.next('.instruction').html(min + 'mm ~ ' + max + 'mm');    
//     } else {
//         min = parseFloat(f_l_min);
//         max = parseFloat(f_l_max);

//         $inst.attr('min', min).attr('max', max);
//         $inst.next('.instruction').html(min + 'mm ~ ' + max + 'mm');   
//     }
// });


// ////////////////////////////////////////////////////
// //
// // Frame Shape selection functions
// //
// ////////////////////////////////////////////////////

// $('#frames li').click(function () {

//     $('#frame_shapes .error').html('');

//     // clearPrevSVG();
//     $('#frames li').each(function () {
//         $(this).removeClass('frames_active');
//     });
//     $(this).addClass('frames_active');
//     $(this).scrollintoview();

//     val = $(this).children("svg").attr("class");
//     $('#frame_shape').val(val);

//     getFrameDimension(val);
// });

// function getFrameDimension(type) {
//      input = '<span class="list">A.</span> <input type="number" step="any" min="0" value="0" name="v_total_length"  onkeyup="changeSVGvalue(this);" onchange="changeSVGvalue(this);"> mm' +
//              '<span class="list">B.</span> <input type="number" step="any" min="0" value="0" name="v_north_length" onkeyup="changeSVGvalue(this);" onchange="changeSVGvalue(this);"> mm' +
//              '<span class="list">C.</span> <input type="number" step="any" min="0" value="0" name="v_west_length" onkeyup="changeSVGvalue(this);changeDimension();changeInnerLength();" onchange="changeSVGvalue(this);changeDimension();changeInnerLength();"> mm' +
//              '<span class="list">D.</span> <input type="number" step="any" min="0" value="0" name="v_east_length" onkeyup="changeSVGvalue(this);" onchange="changeSVGvalue(this);"> mm';
//     if (type == "ta_frame" || type == "t_frame") {
//         input = input + '<span class="list">E.</span> <input type="number" step="any" min="0" value="0" name="v_south_east_length" onkeyup="changeSVGvalue(this);" onchange="changeSVGvalue(this);"> mm';
//     } 
//     $('#frame_dimension').html(input);
//     $('input[name=v_west_length').trigger('change');
// }

// function changeSVGvalue(ele) {
//     frame = $("#frame_shape").val();
//     $('#' + frame + ' svg #' + ele.name).text(ele.value);
// }

// // Date picker jquery ui
// $('#due_date, #s_due_date').datepicker(
//     { changeMonth: true, changeYear: true, minDate: 1 },
//     $.datepicker.regional[ "zh-TW" ]
// );

// ////////////////////////////////////////////////////
// //
// // Validation functions
// //
// ////////////////////////////////////////////////////

// function checkSize(el) {
//     if (el.files[0].size > 10485760) {
//         alert("檔案不能大於10MB");
//         $('#' + el.id).trigger('click');
//     }
// }

// $('input[type=submit]').click(function () {
//     if ($('#frame_dimension').html() != "" || $('#frame_dimension').is(':hidden')) {
//         return true;
//     }
//     $('#frame_shapes .error').html('必須選取一種框形').scrollintoview();
//     return false;
// });

// $.validator.addMethod('checked_at_least', function(value, element, params) {
//     return $(params[0] + ":checked").length >= params[1];
// }, "必須勾選至少{1}樣");

// $.validator.addMethod('greater', function(value, element, params) {
//     var field_1 = $('input[name="' + params[1] + '"]').val(),
//         field_2 = $('input[name="' + params[2] + '"]').val();
//     return params[0] < parseInt(field_1) + parseInt(field_2);
// }, "數量必須大於 {0}");

// $.validator.addMethod('minHandleHeight', function (value, el, param) {
//         if ($(param[0]).val()) {
//             return value > param[1];
//         }
//         return true;
// }, "必須大於{1}");

// $('#order_form').validate({
//     rules: {
//         order_type: {
//             checked_at_least: ['.order_type', 1]
//         },
//         right: {
//             required: true,
//             greater: [0,'left', 'right'],
//             number: true
//         },
//         lock_height: {
//             minHandleHeight: ['#lock_type', 0]
//         }
//     },
//     ignore: ":hidden"
// });

// $('input[name=frame_length]').keyup(function () {
//         fl = $('input[name=frame_length]').val();
//         bl = $('input[name=beneath_length]').val();
//         wl = $('input[name=v_west_length]').val();

//         l = fl - bl - wl;
//         $('input[name=inner_length]').val(l);
// });
// $('input[name=beneath_length]').keyup(function () {
//         fl = $('input[name=frame_length]').val();
//         bl = $('input[name=beneath_length]').val();
//         wl = $('input[name=v_west_length]').val();

//         l = fl - bl - wl;
//         $('input[name=inner_length]').val(l);
// });
// function changeInnerLength() {
//         fl = $('input[name=frame_length]').val();
//         bl = $('input[name=beneath_length]').val();
//         wl = $('input[name=v_west_length]').val();

//         l = fl - bl - wl;
//         $('input[name=inner_length]').val(l);
// }

// function add_file(num) {
//     num++;
//     html = '<input class="attachments" id="file' + num + '" type="file" name="file[]" onchange="checkSize(this);" multiple="multiple">';
//     $('.add_file').before(html);
//     $('.add_file').attr('onclick', 'add_file(' + num + ')');
//     $('#file' + num).trigger('click');
// }
