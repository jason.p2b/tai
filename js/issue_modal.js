// File Upload
var $el = $('#attachment');
initPlugin = function() {
                $el.fileinput({
                    uploadUrl: "/tmp/",
                    maxFileCount: 10,
                    validateInitialCount: true,
                    overwriteInitial: false,
                    showUpload: false,
                    language: 'zh-TW',
                    previewFileIcon: '<i class="fa fa-file"></i>',
                    allowedPreviewTypes: null, // set to empty, null or false to disable preview for all types
                    previewFileIconSettings: {
                        'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                        'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                        'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                        'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                        'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                        'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                        'htm': '<i class="fa fa-file-code-o text-info"></i>',
                        'txt': '<i class="fa fa-file-text-o text-info"></i>',
                        'mov': '<i class="fa fa-file-movie-o text-warning"></i>',
                        'mp3': '<i class="fa fa-file-audio-o text-warning"></i>',
                    },
                    previewFileExtSettings: {
                        'doc': function(ext) {
                            return ext.match(/(doc|docx)$/i);
                        },
                        'xls': function(ext) {
                            return ext.match(/(xls|xlsx)$/i);
                        },
                        'ppt': function(ext) {
                            return ext.match(/(ppt|pptx)$/i);
                        },
                        'zip': function(ext) {
                            return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                        },
                        'htm': function(ext) {
                            return ext.match(/(php|js|css|htm|html)$/i);
                        },
                        'txt': function(ext) {
                            return ext.match(/(txt|ini|md)$/i);
                        },
                        'mov': function(ext) {
                            return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                        },
                        'mp3': function(ext) {
                            return ext.match(/(mp3|wav)$/i);
                        },
                    }
                });
            };
initPlugin();

// WYSWIG Editor
$('.summernote').summernote({
    placeholder: '請輸入 ...',
    lang: 'zh-TW',
    height: 300,
    toolbar: [
            ['style', ['fontname','fontsize','color']],
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            ['style', ['paragraph','ul','ol','table']]
          ]
});




// Clicking issue link to get conversation

function getConversation(id) {
    $('#to').val("");
    $('.collapse').collapse('hide');
    $('#attachment').fileinput('clear');
    $('.modal-title').html("");
    $('#issueType').html("");
    $('#conversation').summernote('code', "");
    $('#cl').html("");
    $('#issue_id').val(id);

    $.ajax({
        url: '/pmsbs/index.php/issue/get_conversation',
        type: 'POST',
        async: false,
        dataType: 'json',
        data: {id: id}, 
        success: function (msg) {
            $('.modal-title').html(msg[0]['subject']);
            $('#issueType').html(msg[0]['issue_type']);

            for (i=0; i<msg.length; i++) {
                html = "<div class='conv'>";
                html += "<span>" + msg[i]['create_date'] + "&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;" + msg[i]['o_user'] + " 說&nbsp;&nbsp;<span class='glyphicon glyphicon-chevron-right'></span>&nbsp;&nbsp;" + msg[i]['t_user'] + "</span>";
                html += msg[i]['conversation'];
                html += "</span>";
                html += "<ul class='conv_files'>";
                for (j=0; j<msg[i]['files'].length; j++) {
                    html += '<li><a href="' + '/pmsbs/index.php/issue/download?c=' + msg[i]['conversation_id'] + '&i=' + msg[i]['files'][j]['file_id'] + '">';
                    html += "<img src='" + msg[i]['files'][j]['img'] + "' alt='" + msg[i]['files'][j]['filename'] + "' title='" + msg[i]['files'][j]['filename'] + "'>";
                    html += "<br><span class='file_text'>" + msg[i]['files'][j]['filename'] + "</span></a></li>";        
                }
                html += "</ul></div>";

                $('#cl').append(html);

                if (i+1 == msg.length) {
                    $('#issue_id').val(id);
                    if ($("#to option[value='" + msg[i]['uid'] + "']").length > 0) {
                        $('#to').val(msg[i]['uid']);
                    } else {
                        $('#to').val("");
                    }
                }
            }
        }
    });
}


function submitConversation() {
    $('#change').addClass('disabled');
    if (window.FormData) {
        formdata = new FormData();
    }

    for (i=0; i<myfilestack.length; i++) {
        file = myfilestack[i];

        if (window.FileReader) {
            reader = new FileReader();
            reader.readAsDataURL(file);
        }
        if (formdata) {
            formdata.append("file" + i, file);
        }
    }

    formdata.append('issue_id', $('#issue_id').val());
    formdata.append('to', $('#to').val());
    formdata.append('conversation', $('#conversation').summernote('code'));

    if (formdata) {
        $.ajax({
            url: '/pmsbs/index.php/issue/insert_conv',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function(res) {
                if (typeof getMyTracker == 'function') {
                    // Main View
                    getMyTracker();
                    getCompanyTracker();
                } else {
                    // Issue View
                    $('#project').trigger('change');
                }

                $('#change').removeClass('disabled');
                $('#issue_modal').modal('hide');
            }   
        });
    }
}

$(document).ready(function(){
    $(".panel-heading").click(function() {
        if ($('#collapse1').is(':visible')) {
            $(".panel-title span").attr('class','glyphicon glyphicon-collapse-down');
        } else {
            $(".panel-title span").attr('class','glyphicon glyphicon-collapse-up');
        }
    })
});