export default {
    name: 'Mytable',
    template: `
        <div class="row">
          <div class="col-md-7"></div>
          <div class="col-md-1 p-0">
            <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">匯出</button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#" @click="exportToCSV">CSV</a>
              <a class="dropdown-item" href="#" @click="exportToExcel">Excel</a>
              <a class="dropdown-item" href="#"></a>
            </div>
          </div>
          <div class="col-md-4 pl-0" >
            <div class='input-group table-search-box'>
              <div class="input-group-prepend bg-secondary">
                <div class="input-group-text" style="padding: 0 10px;"><span class="mdi mdi-magnify"></span></div>
                <div class="input-group-text" style="padding: 0 5px;"><input type="checkbox" :id="'exclude-' + id" aria-label="exclude" v-model="exclude"><label :for="'exclude-' + id" class="text-warning small" style="margin: 0 1px;">不含</label></div>
              </div>
              <input class="form-control" type="text" name="table-search-input" v-model="searchKey" placeholder="搜尋">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class='table-responsive mt-1'>
              <table :id="id" class="myTable table table-bordered table-striped table-hover">
                <thead>
                  <tr class="table-primary">
                    <th v-for="column in columns" :key="column" @click="sortBy(column.name)" :class="column.align">
                      {{ column.title }}
                      <i v-if="column.sortable == 1" :class="'mdi mdi-' + getSortIcon(column.name)" style="cursor:pointer;"></i>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in pagedData" :key="item.id">
                    <td v-for="column in columns" :key="column" :class="column.align">
                      <a v-if="column.url" :href="item['url']">
                      <span :class="userFormatter(item, column)" 
                            data-toggle="tooltip"
                            :title="item['titles'][column.name]"
                      >
                        {{ item[column.name] }}
                      </span>
                      </a>
                      <span v-else :class="userFormatter(item, column)" 
                            data-toggle="tooltip"
                            :title="item['titles'][column.name]"
                      >
                        {{ item[column.name] }}
                      </span>
                    </td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr class="table-secondary">
                    <th v-for="column in columns" :key="column">
                      <span v-if="column.sumColumn" class="text-primary font-weight-bold">{{sumColumn(column.name)}}</span>
                    </th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>

        <div v-if="pagination" class="row">
          <div v-if="totalPages > 1" class="col ">
            <nav>
              <ul class="pagination">
                <li @click="previousPage()" :class="{disabled: currentPage <= 1}" class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li v-for="n in totalPages" @click="goToPage(n)" v-show="showPageButton(n)" :class="{active: currentPage == n}" class="page-item">
                  <a class="page-link" href="#" aria-label="Go To Page {{ n }}">
                    <span aria-hidden="true">{{ n }}</span>
                    <span class="sr-only">(current)</span>
                  </a>
                </li>
                <li @click="nextPage()" :class="{disabled: currentPage >= totalPages}" class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="col-1">
            <input class="form-control text-center" type="number" v-model="rpp">
          </div>
        </div>
    `,

    props: {
        id: {
          type: String,
          required: false,
          default: "",
        },
        data: {
          type: Array,
          required: true
        },
        columns: {
          type: Array,
          required: true
        },
        formatter: {
          type: Object,
          required: false
        },
        pagination: {
          type: Boolean,
          required: false,
          default: false,
        },
        rowsPerPage: {
          type: String,
          required: false,
          default: "10",
        }
    },

    data() {
        return {
            sortKey: 'due_date',
            sortOrder: 'asc',
            searchKey: '',
            exclude: '',
            currentPage: 1,
            rpp: this.rowsPerPage,
        }
    },

    computed: {
        processData() {
          // Start with the original data
          let results = this.data;

          // Filter data (search data)
          if ("" != this.searchKey) {
            let r = [];
            for (const item of this.data) {
              for (const key in item) {
                if (String(item[key]).toLowerCase().search(this.searchKey.toLowerCase()) > -1) {
                  r.push(item);
                  break;
                }
              }
            }

            if (this.exclude) {
              const diff = this.data.filter(element => !r.includes(element));
              r = diff;
            }

            results = r;
          }

          if (Math.ceil(results.length / this.rpp) < this.currentPage) {
            this.currentPage = 1;
          }

          // Sorting
          return results.sort((a, b) => {
              if (a[this.sortKey] < b[this.sortKey]) {
                  return this.sortOrder === 'asc' ? -1 : 1;
              }
              if (a[this.sortKey] > b[this.sortKey]) {
                  return this.sortOrder === 'asc' ? 1 : -1;
              }
              return 0;
          });
        },

        prepareExportData() {
          let results = [];
          const headers = this.columns.map(function (item) {
                            return item.title;
                          });
          results.push(headers);

          this.processData.forEach(row => {
            let r = this.columns.map(function (item) {
                      return row[item.name];
                    });
            results.push(r);
          });
                        
          return results;
        },

        totalPages() {
          return Math.ceil(this.processData.length / this.rpp);
        },

        pagedData() {
          if (this.pagination) {
            const init = (this.currentPage - 1) * this.rpp;           
            const end  = this.currentPage * this.rpp;
            let result = this.processData;

            return result.slice(init, end);
          }
          return this.processData;
        },
    },

    methods: {
        goToPage(n) {
          this.currentPage = n;
        },

        nextPage() {
          if (this.currentPage < this.totalPages)
            this.currentPage++;
        },

        previousPage() {
          if (this.currentPage > 1)
            this.currentPage--;
        },
        
        showPageButton(n) {
          if (this.currentPage < 3 && n < 6) {
            return true;
          } else if (this.currentPage > (this.totalPages-3) && n > (this.totalPages - 5)) {
            return true;
          } else if (n > (this.currentPage - 3) && n < (this.currentPage + 3)) {
            return true;
          }
          return false;
        },

        sortBy(column) {
          const sortable = this.columns.find((item) => item.name === column)?.sortable;

          if (sortable != 0) {
            this.sortKey = column;
            this.sortOrder = (this.sortOrder === 'asc') ? 'desc' : 'asc';
            this.processData;
          }
        },

        getSortIcon(key) {
          if (key != this.sortKey) {
            return "unfold-more-horizontal";
          }
          if ("asc" != this.sortOrder) {
            return "chevron-down text-danger";
          }
          return "chevron-up text-danger";
        },

        userFormatter(item, column) {
          if (!(column.formatter === undefined || column.formatter == false)) {
            let func = this.formatter[column.name];
            if (typeof func === 'function') {
              return func(item, column.name);
            } 
          }
          return "";
        },

        exportToCSV() {
          let csvContent = "data:text/csv;charset=utf-8,";
          
          // Adding Data
          this.prepareExportData.forEach(row => {
            csvContent += row.map(item => item).join(",") + "\r\n";
          });
          
          // Encoding URI
          const encodedUri = encodeURI(csvContent);
          const link = document.createElement("a");
          link.setAttribute("href", encodedUri);
          link.setAttribute("download", "table_data.csv");
          document.body.appendChild(link); // Required for FF
          link.click(); // This will download the data file
          document.body.removeChild(link);
        },

        exportToExcel() {        
          const wb = XLSX.utils.book_new();
          const ws = XLSX.utils.json_to_sheet(this.prepareExportData);

          XLSX.utils.book_append_sheet(wb, ws, "Sheet1");
          XLSX.writeFile(wb, "table_data.xlsx");
        },

        sumColumn(column) {
          let total = 0;
          this.pagedData.forEach(row => {
            total += row[column];
          });
          return total;
        },
    }
}