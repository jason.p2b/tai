import Filterselect from './Filterselect.js';
import Mymodal from './MyModal.js';

export default {
    components: {
      Filterselect: Filterselect,
      Mymodal: Mymodal,
    },


    data() {
      return {
        customers: customersA,
        customerInfo: [],
        projectsDisabled: "true",
        projects: [],
        projectInfo: [],
        orders: [],
        projectId: '',
        projectButton: {icon: 'plus', text: '新增', bg: 'primary'},
        projectModalForms: [
          {type:"input", name:"site", label:"建案名稱", disableOnEdit:true},
          {type:"input", name:"address", label:"出貨地址"},
          {type:"input", name:"company_contact", label:"聯繫人"},
          {type:"input", name:"company_phone", label:"電話"},
          {type:"hidden", name:"project_id", label:""},
          {type:"hidden", name:"customer_id", label:""},
        ],
      }
    },

    watch: {
      projectId(val) {
        if (val == "") {
          this.projectButton = {icon: 'plus', text: '新增', bg: 'primary'};
        } else {
          this.projectButton = {icon: 'edit', text: '修改', bg: 'info'};
        }
      }
    },

    methods: {
      customerData(data) {
        this.projects = data.projects;
        this.customerInfo = data.info;
        this.projectsDisabled = "false";
        this.projectId = "";
      },

      async getProjects(id) {
        if (id != "") {
          try {
            const response = await axios.post('/tai/order/get_site.html', {id: id});
            return response.data;
          } catch (error) {
            console.error('Axios called Failed!', error);
          }
        }
        return "";
      },

      projectData(data) {
        this.orders = data.orders;
        this.projectInfo = data.info;
      },

      async getOrders(id) {
        if (id != "") {
          try {
            const response = await axios.post('/tai/order/get_order_list.html', {id: id});
            this.projectId = id;
            return response.data;
          } catch (error) {
            console.error('Axios called Failed!', error);
          }
        }
        this.projectId = id;
        return {orders:[], info:[]};
      }
    }
}