export default {
    name: 'Mymodal',
    template: `
    <div class="modal fade" :id="id" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title">{{ title }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body bg-white">
                <template v-for="(item, index) in forms" :key="index">
                <div v-if="item.type != 'hidden'" class="form-group row">
                    <label :for="id + '-' + item.name" class="col-sm-2 col-form-label">{{ item.label }}</label>
                    <div class="col-sm-10">
                        <input :type="item.type" :id="id + '-' + item.name" class="form-control" :name="item.name" :value="data[item.name]" :placeholder="item.label" 
                        :disabled="data[item.name] !== undefined && item.disableOnEdit ? '' : disabled">
                    </div>
                </div>
                <input v-else type='hidden' :name="index.name" :value="data[index.name]" >
                </template>
            </div>
                <div class="modal-footer bg-white">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    <button type="button" class="btn btn-primary">確認</button>
                </div>
            </div>
        </div>
    </div>
    `,

    props: {
        forms: {
            type: Array,
            required: false,
        },
        data: {
            type: Array,
            default: [],
            required: false,
        },
        id: {
            type: String,
            required: true,
        },
        title: {
            type: String,
            required: true,
        },
        func: {
            type: Function,
            required: false,
        }
    },

    data() {
        return {

        }
    },

    watch: {
    
      },

    computed: {
        
    },

    methods: {
    
    }
}

