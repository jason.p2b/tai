import Tab from './Tab.js';
import Mytable from './MyTable.js'

export default {
    components: {
        Tab: Tab,
        Mytable: Mytable,
    },

    created() {
        this.loadData();
    },

    data() {
        return {
            mainTabs: [
                { id:0, title:'進度總覽', url:'status', slot: "status-tab-slot"},
                { id:1, title:'未出貨進度總表', url:'not-shipped-old', slot: "not-shipped-old-tab-slot"},
                { id:2, title:'未出貨進度總表(新)', url:'not-shipped', slot: "not-shipped-tab-slot"},
            ],
            tabs: [
                { id:0, title:'看板說明', url:'manual-tab-pane', slot: "manual-tab-slot", name: "manual"},
                { id:1, title:'門框', url:'frame-tab-pane', slot: "frame-tab-slot", name: "frame"},
                { id:2, title:'SMC門扇', url:'smc-tab-pane', slot: "smc-tab-slot", name: "smc"},
                { id:3, title:'鐵門', url:'door-tab-pane', slot: "door-tab-slot", name: "door"},
                { id:4, title:'玄關門', url:'main-door-tab-pane', slot: "main-door-tab-slot", name: "main-door"},
            ],
            frameHeader: [],
            frameData: [],
            frameFunction: {site: this.checkProgress, 
                            due_date: this.checkDate, 
                            current_progress: this.checkProgress, 
                            '成型': this.checkComplete, 
                            '組裝': this.checkComplete, 
                            '烤漆': this.checkComplete, 
                            '包裝': this.checkComplete, 
                            'other_progress': this.checkComplete},
            smcHeader: [],
            smcData: [],
            smcFunction: {site: this.checkProgress, 
                          due_date: this.checkDate, 
                          current_progress: this.checkProgress, 
                          '封邊': this.checkComplete, 
                          SKIN: this.checkComplete, 
                          '芯材': this.checkComplete, 
                          '骨架': this.checkComplete,
                          'other_progress': this.checkComplete},
            doorHeader: [],
            doorData: [],
            doorFunction: {site: this.checkProgress, 
                           due_date: this.checkDate, 
                           current_progress: this.checkProgress, 
                           '封邊': this.checkComplete, 
                           '門板': this.checkComplete, 
                           '芯材': this.checkComplete, 
                           '骨架': this.checkComplete,
                           'other_progress': this.checkComplete},
            mainDoorHeader: [],
            mainDoorData: [],
            mainDoorFunction: {site: this.checkProgress, 
                               due_date: this.checkDate, 
                               current_progress: this.checkProgress,
                               '封邊': this.checkComplete, 
                               '門板': this.checkComplete, 
                               '芯材': this.checkComplete, 
                               '骨架': this.checkComplete, 
                               '木板': this.checkComplete, 
                               '鑄鋁板': this.checkComplete, 
                               '組裝鐵門': this.checkComplete,
                               'other_progress': this.checkComplete},
        }
    },

    computed: {
    },

    methods: {
        loadData() {
            Promise.all([
                this.getTableData('smc'),
                this.getTableData('frame'),
                this.getTableData('door'),
                this.getTableData('mainDoor'),
            ]);
        },

        getTableData(infoType) {
            axios.post("/tai/dashboard/getTableData.html", { type: infoType })
            .then((response) => {
                this[infoType + 'Header'] = response.data.header;
                this[infoType + 'Data'] = response.data.data;
            })
            .catch((error) => { console.log(error); });
        },

        checkDate(row, current) {
            const today = new Date();
            const date  = new Date(row['due_date']);
            const diff = today - date;

            const days = diff / (1000 * 60 * 60 * 24);
            if (days <= -7) {
                return "text-success";
            } else if (days > -7 && days < 1) {
                return "text-warning";
            } else {
                return "text-danger";
            }
        },

        checkProgress(row, current) {
            const progress = row['current_progress'];

            if (progress == "等出貨") {
                return "text-success";
            } else if (progress == "收到訂單") {
                return "text-warning";
            } else if (progress == "等客戶回復" || progress == "客戶暫停") {
                return "text-danger";
            }
            return "";
        },


        checkComplete(row, current) {
            let style = "";
            const keyword = (current != "其他進度") ? current : 'other_progress';
            if (row.titles[keyword] !== undefined) {
                const lists = row.titles[keyword].split('\r\n');
                const dates = lists[lists.length - 2].split(':');
                const today = new Date();
                const date = new Date(dates[0]);
                const days = (today - date) / (1000 * 60 * 60 * 24);

                if (days <= 1) {
                    style = "bg-success p-1 pl-2 pr-2 rounded";
                } else if (days <= 3) {
                    style = "bg-warning p-1 rounded";
                } else if (days <= 7) {
                    style =  "bg-secondary pl-1 pr-1 rounded";
                }
            }
            
            if (row[current] == "完成") {
                return  style + " text-info";
            }
            return style;
        }

    },

    // mounted() {
    //     this.getTableData('frame');
    //     this.getTableData('smc');
    // },
}