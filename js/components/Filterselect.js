export default {
    template: `
    <div class="filter-select" :class="{'filter-select-disabled': disabled == 'true'}" @mouseleave="showDropdown = false">
        <div class="filter-select-toggle" @click="enableClick()">
            <span class="filter-select-text">{{ selectedOption ? selectedOption.text : '請選擇' }}</span>
            <span :class="{'filter-select-icon': showDropdown}">▾</span>
        </div>
        <ul v-if="showDropdown" class="filter-select-dropdown">
            <li><input class="form-control filter-select-search" v-model="filterKeyword" placeholder="搜尋" ref="search"></li>
            <div class="filter-list" v-if="filterOptions.length != 0">
            <template v-for="(item, index) in filterOptions" :key="index">
            <li v-if="item.group" class="filter-select-option-group">
                <span class="filter-select-option-group-label">{{ item.opt }}</span>
                <ul>
                <template v-for="(groupItem, groupIndex) in item.group" :key="groupIndex">
                    <li @click="handleSelection(groupItem)">
                    <span class="filter-select-option" :class="{ 'selected': groupItem === selectedOption }">{{ groupItem.text }}</span>
                    </li>
                </template>
                </ul>
            </li>
            <li v-else @click="handleSelection(item)">
                <span class="filter-select-option" :class="{ 'selected': item === selectedOption }">{{ item.text }}</span>
            </li>
            </template>
            </div>
        </ul>
    </div>
    `,

    props: {
        options: {
            type: Array,
            required: true,
        },
        enable: {
            type: String,
            default: "false",
            required: false,
        },
        func: {
            type: Function,
            required: false,
        },
        disabled: {
            type: String,
            default: "false",
            required: false,
        }
    },

    data() {
        return {
            showDropdown: false,
            selectedOption: null,
            filterKeyword: '',
        }
    },

    watch: {
        showDropdown(val) {
          if (val) {
            this.$nextTick(() => {
              this.$refs.search.focus();
            });
          }
        },

        options: {
            handler(newOptions) {
                // Reset the user selection when the options change
                this.selectedOption = null;
            },
            deep: true, // Watch for deep changes in the options array
        },
      },

    computed: {
        filterOptions() {
            const filtered = [];

            if (this.filterKeyword != "") {
                const keyword = this.filterKeyword.toLowerCase();

                return this.options.reduce((filtered, item) => {
                if (item.group) {
                    // Filter and include only matching child options
                    const matchedOptions = item.group.filter(groupItem => groupItem.text.toLowerCase().includes(keyword));
                    if (matchedOptions.length) {
                        filtered.push({ ...item, group: matchedOptions }); // Include optgroup with matched options
                    }
                } else {
                    // Include single options matching the keyword
                    if (item.text.toLowerCase().includes(keyword)) {
                        filtered.push(item);
                    }
                }
                return filtered;
                }, []);
            } else {
                return this.options; // No keyword, include all options
            }
        },
        
    },

    methods: {
        enableClick() {
            if (this.disabled == "false") {
                this.showDropdown = !this.showDropdown; 
                this.filterKeyword = '';
            }
        },

        async handleSelection(event) {
            this.selectedOption = event;
            this.showDropdown = false;

            if (this.enable != "true") {
                return "";
            }

            if (typeof this.func === 'function') {
                const received = await this.func(event.value);
                this.$emit('data-received', received);
            }
        }
    }
}

