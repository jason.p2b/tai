export default {
    template:`
        <ul class="nav" :class="tabStyle">
            <li v-for="tab in tabs" :key="tab.id" class="nav-item">
                <a  class="nav-link" 
                    :class="{ active: activeTab == tab.id }" 
                    :href="'#'+tab.url" 
                    @click="setActiveTab(tab.id)"
                >
                        {{ tab.title }}
                </a>
            </li>
        </ul>

        <div class="tab-content mt-4">
            <div v-for="tab in tabs" 
                class="tab-pane fade" 
                :class="{ show : activeTab == tab.id }" 
                :id="tab.url" 
                role="tabpanel" 
                :aria-labelledby="tab.text" 
                :tabindex="tab.id">
                    <slot :name="tab.slot">
                    </slot>
            </div>
        </div>
    `,

    props: {
        tabs: {
            type: Array,
            default: []
        },
        tabStyle: {
            type: String,
            default: 'nav-tabs'
        },
        startTab: {
            type: String,
            default: "0"
        }
    },

    data() {
        return {
            activeTab: this.startTab,
        }
    },

    methods: {
        setActiveTab(id) {
            // this.$emit('update:activeTab', id);
            this.activeTab = id;
        }
    }
}