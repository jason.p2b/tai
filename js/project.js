
/**
 *  Open and Close Detail Div
 */
var myId = 0;

// Open & load detail
function getDetail(id) {
    myId = id;
    $tr = $('#' + id);

    customer = $tr.children(':nth-last-of-type(8)').html();
    site     = $tr.children(':nth-last-of-type(7)').html();
    address  = $tr.children(':nth-last-of-type(6)').html();
    contact  = $tr.children(':nth-last-of-type(5)').html();
    phone    = $tr.children(':nth-last-of-type(4)').html();
    user     = $tr.children(':nth-last-of-type(3)').html();
    note     = $tr.children(':last-child').html();

    $('.modal-title').html(customer + site);
    $('#old_user').html(user);
    $('#old_contact_name').val(contact);
    $('#old_phone').val(phone);
    $('#old_address').val(address);
    $('#old_note').val(note);
    $('.report_checkbox').removeAttr('disabled').removeAttr('checked');

    $.post('/pms/index.php/project/get_project_report', {id: id}, function (msg) {
        for (i=0; i<msg.length; i++) {
            $('#old_report_' + msg[i]).prop('checked','true').prop('disabled','true');
        }
    }, 'json');
}

/**
 *  Submit Detail Div & change row detail
 */

$('#change').click(function () {
    submitChange();
    changeRow(myId);
    $('#detail').modal('hide');
});

function submitChange() {
    // Update Project
    data = {pid: myId, address: $('#old_address').val(), contact: $('#old_contact_name').val(), phone: $('#old_phone').val(),note: $('#old_note').val()};
    $.post('/pms/index.php/project/update_project', data, function (msg) {
            
    }, 'json');


    // Connect project to report
    $('.report_checkbox:checked:not(:disabled)').each(function () {
        $.post('/pms/index.php/project/connect_report', {pid: myId, rid: $(this).val()}, function (msg) {
            
        }, 'json');
    });
}

function changeRow(id) {
    $row = $('#' + id);
    address = $('#old_address').val();
    contact = $('#old_contact_name').val();
    phone   = $('#old_phone').val();
    note    = $('#old_note').val();

    $row.children(':nth-last-of-type(6)').html(address);
    $row.children(':nth-last-of-type(5)').html(contact);
    $row.children(':nth-last-of-type(4)').html(phone);
    $row.children(':last-child').html(note);

    $('.report_checkbox:checked:not(:disabled)').each(function () {
        myId = $(this).attr('id');
        $label = $('label[for=' + myId + ']');

        if ($label.children().html() == undefined) {
            $row.children(':nth-last-of-type(2)').append($label.html());
        } else {
            $row.children(':nth-last-of-type(2)').append($label.children().html());
        }
    });
}
